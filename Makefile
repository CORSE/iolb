#Flags
CC = gcc
CXX = g++
FLAGS = -DVERBOSE=2 -g -Wall #-DEBUG -O3
CFLAGS += $(FLAGS) -std=c99 #-fsanitize-recover=address,undefined
CXXFLAGS += $(FLAGS) -std=c++11
LDFLAGS += $(FLAGS) -lm


C_SRC=iolb.c reduction.c dfg.c complexity.c path.c wavefront.c hourglass.c expr_utils.c utility.c
CXX_SRC=expr.cpp
INC=$(wildcard *.h)
OBJ=$(C_SRC:%.c=%.o)
OBJ+=$(CXX_SRC:%.cpp=%.o)

.phony: all clean
all: iolb-affine

iolb-affine: $(OBJ) 
	$(CXX) $^ $(LDFLAGS)   -Wl,-rpath,/usr/include -lgmp -lpet -lpiplibMP -lbarvinok -lntl -lpolylibgmp -lisl -lcln  -lginac -o $@

%.o: %.cpp
	$(CXX) -c $< $(CXXFLAGS)  -o $@

%.o: %.c $(INC)
	$(CC) -c $< $(CFLAGS)  -o $@

clean:
	rm -r $(OBJ) iolb-affine results/*

