# Build
IOLB depends on the following library/tools:
[ISL-0.14](https://repo.or.cz/w/isl.git), [barvinok](http://freshmeat.sourceforge.net/projects/barvinok), [PET-0.06](https://repo.or.cz/pet.git), [clang-3.4.2](https://github.com/llvm/llvm-project/tree/main/clang), [piplib-1.4.0](https://repo.or.cz/w/piplib.git), [ginac](https://www.ginac.de/ginac.git/), and [GMP](https://gmplib.org/)

Command `make` builds the executable `./iolb-affine`.

# docker
Alternatively you can use a docker container as follow.
## Setting up the docker container:

```bash
	docker login registry.gitlab.inria.fr   # Should not be required anymore
	docker pull registry.gitlab.inria.fr/corse/iolb
```

## Using the container interactively:
```bash
   docker run -ti --rm  -v$(pwd):/hst -w/hst registry.gitlab.inria.fr/corse/iolb bash
   make
   ./iolb-affine examples/polybench/gemm.c 
```

## Using the container externally (/!\ the container only sees files below $(pwd) /!\\)
```bash
   docker run --rm  -v$(pwd):/hst -w/hst registry.gitlab.inria.fr/corse/iolb make
   docker run --rm -v$(pwd):/hst -w/hst registry.gitlab.inria.fr/corse/iolb ./iolb-affine examples/polybench/gemm.c 
```
Or alternatively with the corresponding sh scripts `./make-indocker.sh` and `./iolb-affine-indocker.sh` 

# Usage
## Analyzed C kernel
See the examples in directory `examples`. 
The scope to be analyzed should be in a C file that clang can parse.
The scope itself should be surounded by `#pragma scop`, `#pragma endscop`.
There can be only one scope per C file.
The code within the scope should be a *fully affine static control part* (SCoP).

## Default usage
By default, IOLB considers all parameters to be *big* and outputs a lower bound of the corresponding IO complexity.
e.g.
```bash 
	./iolb-affine examples/polybench/gemm.c
```
The output is quite verbose. 
The last three lines of output respectively correspond  to *input size*, *simplified asymptotic lower bound* and *full lower bound*.
We suggest to filter it using the python script `./pretty.py`:
```bash
    ./iolb-affine examples/polybench/gemm.c | ./pretty.py
```

## Small sizes
It it possible to tell iolb that some of the dimensions are *small*. 
While the output expression is *always correct* (valid lower bound whatever the options given to iolb), doing so can improve the output bound (the maximum of all parametric expressions output by the tool is a valid IO complexity lower bound).
In particular, doing so is critically important to get a tight bound for convolutions as those used in CNN.

Small dimensions can be provided to the tool through a text file (one line per small parameter as `PARAM size`) using the `-s` option.
```bash 
./iolb-affine examples/convolution/convolution.c | ./pretty.py
./iolb-affine examples/convolution/convolution.c -s examples/convolution/convolution_sd_RS.txt | ./pretty.py
```

## Artifact for our PLDI 2020 paper [Automated derivation of parametric data movement lower bounds for affine programs](https://hal.inria.fr/hal-02910961) paper 
Inside the container you can run:
```
	make
	./gen_all.sh
```
This will compile iolb, and generate:
* results/table1.pdf is an automatically generated version of the Table on page
  12 in the submitted paper
* results/all.csv contains raw results from iolb, in semicolon-separated csv
  format

