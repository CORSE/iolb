/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "complexity.h"

#define TIMEOUT 0.5
#define EPSILON 0.01


FILE *fmemopen(void *buf, size_t size, const char *mode);

/* ==================== */
// Builder fucntions for the upper bound on |E|, and the lower bound on the IO

// Compute Theta = \sum_{i not LB} s_i  (= \sigma in the paper)
expr* compute_Theta(const PipList * const lst) {
	PipVector* vec = lst->vector;
	assert(vec->nb_elements == 1);

	long numer = mpz_get_si(vec->the_vector[0]);
	long denom = mpz_get_si(vec->the_deno[0]);
	assert(denom!=0);

	// Special case: Theta = (numer/denom) = 0
	if (numer==0) {
		return expr_new("1");
	}

	// Theta = \sum_{i not LB} s_i  (= \sigma in the paper)
	expr *Theta = expr_new_frac(numer, denom);

	/* DEBUG
	printf("Theta =\n");
	expr_print(Theta);
	printf("\n");
	//*/

	return Theta;
}

// Compute \alpha, such that we consider K-partitioning (where K = (\alpha+1) . S and S = mem size)
// \alpha = 1/(Theta - 1)
expr* compute_alpha(const PipList * const lst) {
	expr* Theta = compute_Theta(lst);

	// Constant (for construction)
	expr* one = expr_new("1");
	
	// alpha = 1/(Theta - 1)
	// Note: K = (\alpha+1) . S (for K-partitioning, as in the paper)
	expr* alpha;
	if (expr_is_zero(expr_sub(Theta, one))) {
		// alpha does not work with that value => we need to do it differently
		// We leave "alpha = 2" here (will be used at the very end of this function)
		alpha = expr_new("2");
	} else {
		// alpha = 1 / (Theta -1)
		alpha = expr_div(one, expr_sub(Theta, one));
	}

	return alpha;
}


// Variation of the previous function, where Theta is substracted by the first "si"
//		(second element of the lst, which is from an extra subspace)
// Used in the context of hourglass - inner part
expr* compute_alpha_hourglass_inner(const PipList * const lst) {
	expr* Theta = compute_Theta(lst);

	// Modifs here
	PipVector* vec_s_extra = lst->next->vector;
	assert(vec_s_extra->nb_elements == 1);

	long numer_s_extra = mpz_get_si(vec_s_extra->the_vector[0]);
	long denom_s_extra = mpz_get_si(vec_s_extra->the_deno[0]);
	assert(denom_s_extra!=0);
	expr* s_extra = expr_new_frac(numer_s_extra, denom_s_extra);

	Theta = expr_sub(Theta, s_extra);
	if (expr_is_zero(Theta)) {		// Catch-up if issue
		Theta = expr_new("1");
	}


	// END modifs

	// Constant (for construction)
	expr* one = expr_new("1");
	
	// alpha = 1/(Theta - 1)
	// Note: K = (\alpha+1) . S (for K-partitioning, as in the paper)
	expr* alpha;
	if (expr_is_zero(expr_sub(Theta, one))) {
		// alpha does not work with that value => we need to do it differently
		// We leave "alpha = 2" here (will be used at the very end of this function)
		alpha = expr_new("2");
	} else {
		// alpha = 1 / (Theta -1)
		alpha = expr_div(one, expr_sub(Theta, one));
	}

	return alpha;


}


typedef struct {
	expr *U;
	expr* alpha;
} res_comp_upper_bound_Kset;

// Aux-function to "compute_bound"
// - Compute the upper bound of a K-set (once all the components are provided)
// - Support the "beta" trick and the small dims
res_comp_upper_bound_Kset* compute_upper_bound_Kset(const PipList * const lst, int* betas_denom,
		bool is_k_small, expr* small_dims_volume) {
	PipVector* vec = lst->vector;
	assert(vec->nb_elements == 1);

	long numer = mpz_get_si(vec->the_vector[0]);
	long denom = mpz_get_si(vec->the_deno[0]);
	assert(denom!=0);

	// Special case: Theta = (numer/denom) = 0
	if (numer==0) {
		res_comp_upper_bound_Kset* res_ubound = malloc(sizeof(res_comp_upper_bound_Kset));
		res_ubound->U = expr_new("1");
		res_ubound->alpha = expr_new("1");
		return res_ubound;
	}

	// Theta = \sum_{i not LB} s_i  (= \sigma in the paper)
	expr *Theta = compute_Theta(lst);
	/* DEBUG
	printf("Theta =\n");
	expr_print(Theta);
	printf("\n");
	//*/

	// Constant (for construction)
	expr* one = expr_new("1");
	
	// alpha = 1/(Theta - 1)
	// Note: K = (\alpha+1) . S (for K-partitioning, as in the paper)
	expr* alpha = compute_alpha(lst);


	// (Final) U = \prod_j (s_j * \beta_j)^{s_j} * ( S * (\alpha+1) / Theta)^{Theta}
	// where: K = S * (\alpha+1)
	
	// Constant part of U = ( (S * (alpha+1) ) / Theta ) ^ Theta
	expr* U = expr_pow(
			expr_div(
				expr_mul( expr_S(), expr_add(alpha, one) ),
				Theta
			),
		Theta);

	/* DEBUG
	printf("U =\n");
	expr_print(U);
	printf("\n");
	//*/

	PipList* ptr_sol = lst->next;

	// Add the contribution for small dimension (s_{small_dim} must be second place if it exists)
	if (is_k_small) {
		numer = mpz_get_si(ptr_sol->vector->the_vector[0]);
		denom = mpz_get_si(ptr_sol->vector->the_deno[0]);
		expr* s = expr_new_frac(numer, denom);		// s_{small_dim}

		// Contribution to U: "small_dims_vols ^ s_{small_dim}"
		U = expr_mul(U,
				expr_pow(
					small_dims_volume,		// Volume of the small_dims
					s
				)
			);

		/* DEBUG
		printf("small_dims_volume = ");
		expr_print(small_dims_volume);
		printf("\nU = ");
		expr_print(U);
		printf("\ns = ");
		expr_print(s);
		printf("\n\n");
		//*/

		// Shift solution set by 1
		ptr_sol = ptr_sol->next;
	}

	// Add the contribution for all the projections (rest of lst)
	int i = 0;
	for(PipList* curr = ptr_sol; curr != NULL; curr = curr->next) {
		numer = mpz_get_si(curr->vector->the_vector[0]);
		denom = mpz_get_si(curr->vector->the_deno[0]);
		expr* s = expr_new_frac(numer, denom);		// s_i

		if(expr_is_zero(s))
			continue;

		// U = U * (s_i * betas_denom[i] )^{s_i}
		U = expr_mul(U,
				expr_pow(
					expr_mul( s, expr_new_frac(betas_denom[i], 1) ),
					// Trick to convert betas_denom[i] into an expression
					s
				)
			);
		i++;
		/* DEBUG
		printf("U = ");
		expr_print(U);
		printf("\ns = ");
		expr_print(s);
		printf("\n\n");
		//*/
	}


	// Building and returning the result
	res_comp_upper_bound_Kset* res_ubound = malloc(sizeof(res_comp_upper_bound_Kset));
	res_ubound->U = U;
	res_ubound->alpha = alpha;
	return res_ubound;
}


// Alternate aux-function to "compute_upper_bound_Kset"
//		- No "beta" trick and no small dim trick
//		- BUT possible to provide a list of upper bound for each of the |\phi_k(E)| to be used
expr* compute_upper_bound_nobeta(const PipList * const lst, list_item *expr_bounds) {
	PipVector* vec = lst->vector;
	assert(vec->nb_elements == 1);

	long numer = mpz_get_si(vec->the_vector[0]);
	long denom = mpz_get_si(vec->the_deno[0]);
	assert(denom!=0);

	// Special case: Theta = (numer/denom) = 0
	if (numer==0) {
		return expr_new("1");
	}


	// Goal: U = \prod_j ( expr_using_S_and_not_K )^{s_j}
	// where: K = S * (\alpha+1)
	expr* U = expr_new("1");

	// Add the contribution for all the projections (rest of lst)
	PipList* ptr_sol = lst->next;
	list_item *ptr_expr_bounds = expr_bounds;
	int i = 0;
	for(PipList* curr = ptr_sol; curr != NULL; curr = curr->next) {
		assert(ptr_expr_bounds!=NULL);
		expr* expr_bnd = ptr_expr_bounds->data;

		numer = mpz_get_si(curr->vector->the_vector[0]);
		denom = mpz_get_si(curr->vector->the_deno[0]);
		expr* s = expr_new_frac(numer, denom);		// s_i

		if(expr_is_zero(s)) {
			i++;
			ptr_expr_bounds = ptr_expr_bounds->next;
			continue;
		}

		// U = U * (expr_bnd )^{s_i}
		U = expr_mul(U,
				expr_pow(expr_bnd, s)
			);

		/* DEBUG
		printf("i = %d\n", i);
		printf("U = ");
		expr_print(U);
		printf("\ns = ");
		expr_print(s);
		printf("\n\n");
		//*/

		i++;
		ptr_expr_bounds = ptr_expr_bounds->next;
	}
	assert(ptr_expr_bounds==NULL);


	// Building and returning the result
	return U;
}



// Aux-function to "compute_bound"
lb_data* compute_lower_bound_IO(isl_set* domain_p, isl_union_set* dom_inter,
		expr* U, expr* alpha, expr* front_size)
{
	// Get the size of the current statement domain (dom_size) and dom_inter
	isl_set *dom_params;
	expr *dom_size = get_set_cardinality(isl_set_copy(domain_p), 0, &dom_params);
	dom_inter = isl_union_set_intersect(isl_union_set_from_set(dom_params), dom_inter);


	// Filter: check if the bound is bounded or not
	isl_set* param_set = get_param_constraints(dom_inter);
	param_set = isl_set_remove_divs(param_set);
	int npar = isl_set_dim(param_set, isl_dim_param);
	for(int i=0; i<npar; i++) {
		bool small_param = param_is_upper_bounded(param_set, i);
		if(small_param) {
#if (VERBOSE > 1)
			printf("Dropping bound because a parameter is bounded:\n");
			PRINT_ISL(p_out, isl_printer_print_set, param_set);
#endif
			isl_set_free(param_set);
			return NULL;
		}
	}

#if (VERBOSE > 1)
	if(dom_size == NULL) {
		fprintf(stderr, "\nWarning: cannot find cardinality of domain:\n");
		PRINT_ISL(p_err, isl_printer_print_set, domain_p);
	} else {
		printf("\nDomain size: ");
		expr_print(dom_size);
		printf("\n");
	}
#endif

	// Finish: convert U to the full bound
	expr *h = expr_div(dom_size, expr_div(U, alpha));
	expr *io = expr_sub(expr_mul(h, expr_S()), expr_S());
	expr *io_minus_front = expr_expand(expr_sub(io, front_size));

	/* DEBUG
	printf("h = ");
	expr_print(h);
	printf("\nio = ");
	expr_print(io);
	printf("\nfront_size = ");
	expr_print(front_size);
	printf("\nio_minus_front = ");
	expr_print(io_minus_front);
	printf("\n");
	//*/


	expr_free(h);
	expr_free(io);
	expr_free(dom_size);
	expr_free(U);

	lb_data *lb = malloc(sizeof(lb_data));
	lb->bnd = io_minus_front;
	lb->dom = isl_union_set_copy(dom_inter);
	
	return lb;
}


// From the different components obtains through solvers, we assemble the final bound
lb_data* compute_bound(isl_set* domain_p, int gamma, expr* front_size, const PipList * const lst,
	isl_union_set* dom_inter, int* betas_denom, bool is_k_small, expr* small_dims_volume)
{

	// No solution from the PIP solving (s_i) => Early exit
	if(lst == NULL)
		return NULL;


	// Theta = sum of the s_i (first component of the solution from Pip)
	PipVector* vec = lst->vector;
	assert(vec->nb_elements == 1);
	long denom = mpz_get_si(vec->the_deno[0]);
	if (denom == 0) //unbounded tile, no lower bound
		return NULL;


	// Get the upper bound of a K-set (and "alpha")
	res_comp_upper_bound_Kset* res_ubound =
		compute_upper_bound_Kset(lst, betas_denom, is_k_small, small_dims_volume);
	expr* U = res_ubound->U;
	expr* alpha = res_ubound->alpha;
	free(res_ubound);



#if (VERBOSE > 1)
	printf("Solution domain:\n");
	PRINT_ISL(p_out, isl_printer_print_set, domain_p);
	printf("Size of largest vertex set: ");
	if(denom != 0)
		expr_print(U);
	else 
		printf("infty");
	printf("\n");
#endif

	// Convert this upper bound into a lower bound
	lb_data *lb = compute_lower_bound_IO(domain_p, dom_inter, U, alpha, front_size);
	return lb;
}

/* ==================== */
// Linear algebra auxilliary function

//Note: mat is transposed
bool is_independent(isl_ctx *ctx, const double ** const mat, const size_t n_bases, const double * const v, const int dim)
{
	//assert(n_bases < dim);
	if(n_bases >= dim)
		return false;
	isl_mat *M = isl_mat_alloc(ctx, n_bases+1, dim);
	//Populate M
	for(size_t i=0; i<n_bases; ++i)
	{
		for(size_t j=0; j<dim; ++j)
		{
			assert(mat[i][j] == floor(mat[i][j])); //make sure the entry is an integer
			M = isl_mat_set_element_si(M, i, j, mat[i][j]);
		}
	}
	for(size_t j=0; j<dim; ++j)
	{
		assert(v[j] == floor(v[j]));
		M = isl_mat_set_element_si(M, n_bases, j, v[j]);
	}

	//compute inverse
	M = isl_mat_right_inverse(M);
	if(M==NULL)
		return false;

	isl_mat_free(M);
	return true;
}


static isl_set *projection(__isl_take isl_set *s, const double * const dir, const int dim)
{
	isl_space *space = isl_space_map_from_set(isl_set_get_space(s));
	//Create multi-affine relation from direction vector
	isl_multi_aff *m_aff = isl_multi_aff_identity(space);
	for(int i=0; i<dim; ++i)
	{
		isl_aff *aff = isl_aff_set_constant_si(isl_multi_aff_get_aff(m_aff, i), dir[i]);
		m_aff = isl_multi_aff_set_aff(m_aff, i, aff);
	}
	isl_map *m = isl_map_from_multi_aff(m_aff);
	if(m==NULL)
	{
		fprintf(stderr, "\nError creating projection relation\n");
		return NULL;
	}
	//printf("\nProjection relation:\n");
	//PRINT_ISL(p_out, isl_printer_print_map, m);
	isl_set *img = isl_set_apply(isl_set_copy(s), m);
	//printf("\nImage:\n");
	//PRINT_ISL(p_out, isl_printer_print_set, img);
	isl_set *diff = isl_set_subtract(s, img);
	//printf("\nDiff:\n");
	//PRINT_ISL(p_out, isl_printer_print_set, diff);
	return diff;
}


// If v is in the base, return 0
// If v is linearly independent from the base, return 1
// if v is not independent from the base, return -1
int can_expand_base(isl_ctx *ctx, const double ** const base, const size_t n_bases, const double * const v, const int dim)
{
	//If v \in base, return 0
	//If v is linearly-independent to base, return 1
	//otherwise, return -1

	//Check if basis can be expanded without zero-dims
	if(n_bases > 0)
	{
		for(size_t i=0; i<n_bases; ++i)
		{
			if(is_vector_equal(base[i], v, dim))
				return 0;
		}
		if(!is_independent(ctx, base, n_bases, v, dim))
			return -1;
	}
	return 1;
}


/* ==================== */
// Auxilliary function for the solve "s_i coefficient" part

typedef struct {
	int** vect_group;		// List of vector composing a group
	int num_vect_gr;		// Num of vector in vect_group
	int dim_gr;				// Dimension of the vector in vect_group

	int dim_group;			// Rank of the group
} kervec_group;


void free_kervec_group(kervec_group* kvg) {
	free(kvg->vect_group);		// We do not free the vectors inside
								// Because they are coming from the "path.k.M"
	return;
}


void print_kervec_group(kervec_group* kvg) {
	printf("[[[ (num_vect_gr = %i | dim_gr = %i || dim_group = %i)\n",
		kvg->num_vect_gr, kvg->dim_gr, kvg->dim_group);
	for (int i=0; i<kvg->num_vect_gr; i++) {
		printf("\t[");
		for (int j=0; j<kvg->dim_gr; j++)
			printf(" %i", kvg->vect_group[i][j]);
		printf("]\n");
	}
	printf("]]]\n");
	return;
}


// Copy from ISL implementation (because the old version of isl used did not have this function)
static int hermite_first_zero_col(isl_mat *H, int first_col, int n_row) {
	int row, col;

	int H_n_col = isl_mat_cols(H);
	for (col = first_col, row = 0; col < H_n_col; ++col) {
		for (; row < n_row; ++row) {
			isl_val* val = isl_mat_get_element_val(H, row, col);
			if (!isl_val_is_zero(val)) {
				isl_val_free(val);
				break;
			}
			isl_val_free(val);
		}
		if (row == n_row)
			return col;
	}
	return H_n_col;
}


// Copy from ISL implementation (because the old version of isl used did not have this function)
int isl_mat_rank(isl_mat *mat) {
	isl_mat *H = isl_mat_left_hermite(isl_mat_copy(mat), 0, NULL, NULL);
	if (H==NULL)
		assert(false);
	int rank = hermite_first_zero_col(H, 0, isl_mat_rows(H));
	isl_mat_free(H);
	return rank;
}


// Auxiliary function that gets back the integrality of vec_kernels
int** get_integral_vec_kernels(double** vec_kernels, int num_kervec, int dim) {
	// Note: vec_kernels are guarantied to be integral, by construction (cf "kernel" method)
	int** mat_ker_int = (int**) malloc(num_kervec * sizeof(int*));
	for (int i=0; i<num_kervec; i++) {
		int* vker_int = (int*) malloc(dim * sizeof(int));
		for (int j=0; j<dim; j++) {
			// Check that the value is integral
			double temp = vec_kernels[i][j];
			int temp_int = (int) temp;
			assert( fabs(temp - ((double) temp_int) < EPSILON) );

			vker_int[j] = temp_int;
		}
		mat_ker_int[i] = vker_int;
	}

	return mat_ker_int;
}


// Auxiliary function that groups vectors from vec_kernels depending on their independence
list_item* group_vec_kernels(double** vec_kernels, int num_kervec, int dim, isl_ctx *ctx) {

	// Note: vec_kernels are guaranteed to be integral, by construction (cf "kernel" method)
	int** mat_ker_int = get_integral_vec_kernels(vec_kernels, num_kervec, dim);

	// Transform mat_ker_int into a isl matrix + transpose
	isl_mat* mat_ker_isl = isl_mat_alloc(ctx, dim, num_kervec);
	for (int i=0; i<dim; i++) {
		for (int j=0; j<num_kervec; j++) {
			mat_ker_isl = isl_mat_set_element_si(mat_ker_isl, i, j, mat_ker_int[j][i]);
		}
	}

	/* DEBUG
	printf("mat_ker_isl =\n");
	isl_mat_print_internal(mat_ker_isl, stdout, 1);
	//*/

	// Apply Hermite normalization to mat_ker_isl =>  M.U = H
	isl_mat* U = NULL;	// Will be allocated inside isl_mat_left_hermite
	isl_mat* H = isl_mat_left_hermite(mat_ker_isl, 0, &U, NULL);	// mat_ker_isl taken here

	// Note: U is square (num_kervec * num_kervec)

	// Check the last columns of H: if there are 0 columns, then
	// int n_nonzero_col = hermite_first_zero_col(H, 0, dim);
	int n_nonzero_col = -1;
	for (int j = 0, row = 0; j<num_kervec; j++) {
		for (; row < dim; ++row) {
			isl_val* v = isl_mat_get_element_val(H, row, j);
			if (!isl_val_is_zero(v)) {
				isl_val_free(v);
				break;
			}
			isl_val_free(v);
		}
		if (row == dim) {			// Reached the last row
			n_nonzero_col = j;
			break;
		}
	}
	if (n_nonzero_col==(-1))		// Reached the last column
		n_nonzero_col = num_kervec;

	/* DEBUG
	printf("H =\n");
	isl_mat_print_internal(H, stdout, 1);
	printf("U =\n");
	isl_mat_print_internal(U, stdout, 1);
	printf("n_nonzero_col = %i\n", n_nonzero_col);
	//*/

	// If there are no zero columns => all columns are independents
	if (n_nonzero_col==num_kervec) {
		list_item* res_groups = NULL;
		for (int i=0; i<num_kervec; i++) {
			kervec_group* kvg = (kervec_group*) malloc(sizeof(kervec_group));

			int** m_kvg = (int**) malloc(1 * sizeof(int*));
			m_kvg[0] = (int*) malloc(dim * sizeof(int*));
			for (int j=0; j<dim; j++)
				m_kvg[0][j] = mat_ker_int[i][j];

			kvg->num_vect_gr = 1;
			kvg->dim_gr = dim;
			kvg->dim_group = 1;
			kvg->vect_group = m_kvg;

			list_item* n_res_groups = (list_item*) malloc(sizeof(list_item));
			n_res_groups->data = kvg;
			n_res_groups->next = res_groups;
			res_groups = n_res_groups;
		}

		// Free data structures
		isl_mat_free(H);
		isl_mat_free(U);
		for (int i=0; i<num_kervec; i++)
			free(mat_ker_int[i]);
		free(mat_ker_int);

		return res_groups;
	}

	// There are zero columns in H => get the part of U corresponding to them
	int n_zero_col = num_kervec-n_nonzero_col;
	assert(n_zero_col>0);
	isl_mat* U_zcol = isl_mat_alloc(ctx, num_kervec, n_zero_col);
	for (int i=0; i<num_kervec; i++) {
		for (int j=0; j<n_zero_col; j++)
			U_zcol = isl_mat_set_element_val(U_zcol, i, j,
					isl_mat_get_element_val(U, i, n_nonzero_col+j)
				);
	}

	/* DEBUG
	printf("U_zcol =\n");
	isl_mat_print_internal(U_zcol, stdout, 1);
	//*/

	// The columns of U_zcol corresponds to linear combinations of the vec_kernels
	//		that are equal to \vec{0}. We pass them inside a isl_mat_left_hermite
	//		to be sure to "separate" as much the dims between them
	isl_mat* HU_zcol = isl_mat_left_hermite(U_zcol, 0, NULL, NULL);	// U_zcol taken here

	/* DEBUG
	printf("HU_zcol =\n");
	isl_mat_print_internal(HU_zcol, stdout, 1);
	//*/


	// For each column of HU_zcol, the non-zero elements are placed on the indexes of
	//		kernel vectors (rows from mat_ker_int) that are inside the same group (lin. dependent)
	// We form these groups, then place the remaining kernel vectors in singleton groups

	// Note: by construction HU_zcol does not have 0-columns.
	list_item* res_groups = NULL;

	// Mask to know which kernel vect were already selected
	bool* b_is_in_non_singl_group = (bool*) malloc(num_kervec * sizeof(bool));
	for (int i=0; i<num_kervec; i++)
		b_is_in_non_singl_group[i] = false;

	// For the non-singleton groups
	for (int k=0; k<num_kervec; k++) {
		if (b_is_in_non_singl_group[k])	// Kernel vector already selected previously
			continue;					// => Skip

		/* DEBUG
		printf("Iteration #%d (max=%d)\n", k, n_zero_col);
		printf("\tb_is_in_non_singl_group = [");
		for (int i=0; i<num_kervec; i++)
			printf(" %d", b_is_in_non_singl_group[i]);
		printf(" ]\n");
		//*/


		bool* b_curr_group = (bool*) malloc(num_kervec * sizeof(bool));
		for (int i=0; i<num_kervec; i++)
			b_curr_group[i] = false;
		b_curr_group[k] = true;

		// Recursive function to find the kernel vector
		//	(those in the same combi than those in the same combi than
		//			... than k)
		//	Algo similar to finding the Connected Components of a graph
		// num_comb = number of rows of HUz_col involved in the group
		bool changed = true;	// Is there a change in b_curr_group ?
		int num_comb = 0;
		while (changed) {
			changed = false;

			num_comb = 0;
			for (int j=0; j<n_zero_col; j++) {
				// Does this equality between elem involve an element of group ?
				bool equality_in_group = false;
				for (int i=0; i<num_kervec; i++) {
					isl_val* vHU_zcol = isl_mat_get_element_val(HU_zcol, i, j);
					if (!isl_val_is_zero(vHU_zcol) && b_curr_group[i]) {
						equality_in_group = true;
					}
					isl_val_free(vHU_zcol);
				}
				if (equality_in_group) {
					num_comb++;

					for (int i=0; i<num_kervec; i++) {
						isl_val* vHU_zcol = isl_mat_get_element_val(HU_zcol, i, j);
						if (!isl_val_is_zero(vHU_zcol) && !b_curr_group[i]) {
							b_curr_group[i] = true;
							changed = true;
						}
						isl_val_free(vHU_zcol);
					}
				}
			}
		}

		int num_elem_group = 0;
		for (int i=0; i<num_kervec; i++)
			if (b_curr_group[i])
				num_elem_group++;

		/* DEBUG
		printf("\tb_curr_group = [");
		for (int i=0; i<num_kervec; i++)
			printf(" %d", b_curr_group[i]);
		printf(" ]\n");
		//*/


		// We obtain after this loop:
		// - b_curr_group  (and num_elem_group, the number of its elements at "true")
		// - num_comb

		// Build kvg from "b_curr_group"
		kervec_group* kvg = (kervec_group*) malloc(sizeof(kervec_group));

		int** m_kvg = (int**) malloc(num_elem_group * sizeof(int*));
		int i_pos_m_kvg = 0;		// Current row in m_kvg
		for (int i=0; i<num_kervec; i++) {
			if (b_curr_group[i]) {
				m_kvg[i_pos_m_kvg] = (int*) malloc(dim * sizeof(int*));
				for (int j=0; j<dim; j++)
					m_kvg[i_pos_m_kvg][j] = mat_ker_int[i][j];
				i_pos_m_kvg++;
			}
		}

		kvg->num_vect_gr = num_elem_group;
		kvg->dim_gr = dim;
		kvg->dim_group = num_elem_group - num_comb;
		kvg->vect_group = m_kvg;

		// Update res_groups
		list_item* n_res_groups = (list_item*) malloc(sizeof(list_item));
		n_res_groups->data = kvg;
		n_res_groups->next = res_groups;
		res_groups = n_res_groups;

		// Update "b_is_in_non_singl_group" from b_curr_group
		for (int i=0; i<num_kervec; i++) {
			if (b_curr_group[i]) {
				assert(!b_is_in_non_singl_group[i]);
				b_is_in_non_singl_group[i] = true;
			}
		}

		free(b_curr_group);
	}

	/* DEBUG
	printf("res_groups =\n");
	for (list_item* ptr_res_group=res_groups; ptr_res_group!=NULL; ptr_res_group = ptr_res_group->next) {
		kervec_group* kvg = (kervec_group*) ptr_res_group->data;
		print_kervec_group(kvg);
		printf("\n");
	}
	//*/


	// Complete with the singleton groups
	for (int i=0; i<num_kervec; i++) {
		if (! b_is_in_non_singl_group[i]) {
			// Place that kernel vector in the new singleton group
			kervec_group* kvg = (kervec_group*) malloc(sizeof(kervec_group));

			int** m_kvg = (int**) malloc(1 * sizeof(int*));
			m_kvg[0] = (int*) malloc(dim * sizeof(int*));
			for (int j=0; j<dim; j++)
				m_kvg[0][j] = mat_ker_int[i][j];

			kvg->num_vect_gr = 1;
			kvg->dim_gr = dim;
			kvg->dim_group = 1;
			kvg->vect_group = m_kvg;

			list_item* n_res_groups = (list_item*) malloc(sizeof(list_item));
			n_res_groups->data = kvg;
			n_res_groups->next = res_groups;
			res_groups = n_res_groups;
		}
	}

	// Free data structures
	isl_mat_free(H);
	isl_mat_free(U);
	for (int i=0; i<num_kervec; i++)
		free(mat_ker_int[i]);
	free(mat_ker_int);
	isl_mat_free(HU_zcol);
	free(b_is_in_non_singl_group);

	return res_groups;
}


// Auxiliary function that, for each group of vector, generate the combinations
//	(one constraint on the s_i is generated by combination)
list_item* generate_combinations(list_item* g_veckernels, int dim) {
	// Algo: For each kvg, for each k \in [|1; d|] avec d = kvg->dim_group ,
	//		generate all combination of k row vectors of kvg->vect_group
	//		and create the new group.
	// Note: we don't care about redundancy of the generated constraints
	list_item* g_vec_ker_all_group = NULL;

	for (list_item* ptr_gvk = g_veckernels; ptr_gvk!=NULL; ptr_gvk=ptr_gvk->next) {
		kervec_group* kvg = (kervec_group*) ptr_gvk->data;

		int** vgroup = kvg->vect_group;
		int num_vect_gr = kvg->num_vect_gr;
		int dim_gr = kvg->dim_gr;

		int d = kvg->dim_group;

		// Selections are all integral arrays of size "d".
		//	"-1" = empty cell
		//	First positions in the arrays are filled first
		//		=> Easy to know if a selection is of size less than a number

		// k=1: only singleton
		list_item* g_combis = NULL;
		for (int i=0; i<num_vect_gr; i++) {
			int* combi_singl = (int*) malloc(d * sizeof(int));
			for (int i=0; i<d; i++)
				combi_singl[i] = -1;	// Empty cell
			combi_singl[0] = i;

			list_item* n_g_combis = (list_item*) malloc(sizeof(list_item));
			n_g_combis->data = combi_singl;
			n_g_combis->next = g_combis;

			g_combis = n_g_combis;
		}

		/* DEBUG
		printf("g_combis (after singleton) =\n");
		for (list_item* ptr_g_combis=g_combis; ptr_g_combis!=NULL; ptr_g_combis=ptr_g_combis->next) {
			int* combi = (int*) ptr_g_combis->data;
			printf("\t* [");
			for (int k=0; k<d; k++)
				printf(" %d", combi[k]);
			printf(" ]\n");
		}
		//*/

		// k>2 : g_combis[k] = g_combi[k-1] \crossproducts (last_elem_of_combi < ... < d)
		for (int k=2; k<=d; k++) {
			list_item* g_combis_prev = g_combis;	// g_combi[k-1]

			for (list_item* ptr_combi_prev = g_combis_prev; ptr_combi_prev!=NULL;
					ptr_combi_prev=ptr_combi_prev->next) {

				int* combi_km1 = (int*) ptr_combi_prev->data;

				int last_elem = combi_km1[k-2];
				if (last_elem==(-1)) {	// Combination is not of size "k-1"
					// Do nothing
					continue;
				}

				for (int e = last_elem+1; e<=d; e++) {
					// New combination: [combi_km1] ++ [e]
					int* ncombi_k = (int*) malloc(d * sizeof(int));
					for (int i=0; i<d; i++)
						ncombi_k[i] = combi_km1[i];		// Also copy the "-1"
					ncombi_k[k-1] = e;

					// Add it to g_combis
					list_item* n_g_combis = (list_item*) malloc(sizeof(list_item));
					n_g_combis->data = ncombi_k;
					n_g_combis->next = g_combis;

					g_combis = n_g_combis;
				}
			}
		}

		/* DEBUG
		printf("g_combis =\n");
		for (list_item* ptr_g_combis=g_combis; ptr_g_combis!=NULL; ptr_g_combis=ptr_g_combis->next) {
			int* combi = (int*) ptr_g_combis->data;
			printf("\t* [");
			for (int k=0; k<d; k++)
				printf(" %d", combi[k]);
			printf(" ]\n");
		}
		printf("\n");
		//*/


		// At that point g_combis contains all the wanted permutation for this group.
		//	=> We iterate over its elements & replace the "int*" by array of pointers
		//		to the lines of vgroups
		//	+ build the corresponding  kervec_group
		for (list_item* ptr_combi = g_combis; ptr_combi!=NULL;
				ptr_combi=ptr_combi->next) {
			int* combi = (int*) ptr_combi->data;

			int dim_combi = d;
			for (int i=0; i<d; i++)
				if (combi[i]==(-1)) {
					dim_combi = i;
					break;
				}
			assert(dim_combi != (-1));

			int** arr_vgroup = (int**) malloc(dim_combi * sizeof(int*));
			for (int i=0; i<dim_combi; i++)
				arr_vgroup[i] = vgroup[combi[i]];

			kervec_group* kvg_combi = (kervec_group*) malloc(sizeof(kervec_group));
			kvg_combi->vect_group = arr_vgroup;
			kvg_combi->num_vect_gr = dim_combi;
			kvg_combi->dim_gr = dim_gr;
			kvg_combi->dim_group = (-1);	// Will not be used later

			
			// Add kvg_combi to g_vec_ker_all_group
			list_item* n_g_vec_ker_all_group = (list_item*) malloc(sizeof(list_item));
			n_g_vec_ker_all_group->data = kvg_combi;
			n_g_vec_ker_all_group->next = g_vec_ker_all_group;
			g_vec_ker_all_group = n_g_vec_ker_all_group;
		}


		// Free temporary data struct (gcombi)
		while (g_combis != NULL) {
			list_item* next = g_combis->next;
			free(g_combis->data);
			free(g_combis);
			g_combis = next;
		}


		// ... And that's all for that group (at last!)

	}	// End iteration of all groups

	return g_vec_ker_all_group;
}


// Auxiliary function that build the complementary of [vect_kvg] + [kerPhi] (row major matrix) from vectors of base
isl_mat* complete_vector_base(isl_ctx* ctx, double** base, int n_b, int** vect_kvg, int n_vect_kvg, int dim,
		double** kerPhi, int rank_kerPhi) {
	assert(n_b==dim);		// Check that base is full dimensional

	// Matrix of the accumulated vector kM + kerPhi + currently selected vector of the complement space
	// Note: overapprox of the allocated size (rows that are actually used are "n_row_curr_KM_comp")
	double** curr_kM_selected_compl_vector = (double**) malloc( (n_b+rank_kerPhi) * sizeof(double*));
	for (int i=0; i<(n_b+rank_kerPhi); i++)
		curr_kM_selected_compl_vector[i] = (double*) calloc(dim, sizeof(double));

	// First columns are kerPhi, then vect_kvg
	for (int i=0; i<rank_kerPhi; i++)
		for (int j=0; j<dim; j++)
			curr_kM_selected_compl_vector[i][j] = kerPhi[i][j];
	int n_row_curr_KM_comp = rank_kerPhi;

	// Add row of vect_kvg only if they are independent !!!
	for (int i=0; i<n_vect_kvg; i++) {

		double* dbl_vect_kvg = (double*) malloc(dim * sizeof(double));
		for (int j=0; j<dim; j++)
			dbl_vect_kvg[j] = (double) vect_kvg[i][j];

		bool is_indep = is_independent(ctx,
			(const double**) curr_kM_selected_compl_vector, n_row_curr_KM_comp,
			(const double*) dbl_vect_kvg, dim);

		if (is_indep) {
			for (int j=0; j<dim; j++)
				curr_kM_selected_compl_vector[n_row_curr_KM_comp][j] = (double) vect_kvg[i][j];
			n_row_curr_KM_comp++;
		}

		free(dbl_vect_kvg);
	}


	/* DEBUG
	printf("curr_kM_selected_compl_vector (before loop)=\n");
	for (size_t i=0; i<(n_b+rank_kerPhi); ++i) {
		printf("\t(");
		for (size_t j=0; j<dim; ++j) {
			printf("%lf, ", curr_kM_selected_compl_vector[i][j]);
		}
		printf(")\n");
	}
	printf("n_row_curr_KM_comp = %d\n", n_row_curr_KM_comp);
	//*/



	// Returned matrix - we might over-allocate it (in term of number of rows)
	int num_vect_compl_approx = n_b - rank_kerPhi;
	isl_mat* mat_compl_kernel = isl_mat_alloc(ctx, num_vect_compl_approx, dim);
	for (int i=0; i<num_vect_compl_approx; i++)
		for (int j=0; j<dim; j++)
			mat_compl_kernel = isl_mat_set_element_si(mat_compl_kernel, i, j, 0);

	int count_compl = 0;	// Count of the actual number of vectors in the complementary
	for (int i=0; i<n_b; i++) {
		/* DEBUG
		printf("curr_kM_selected_compl_vector (during loop)=\n");
		for (size_t i=0; i<(n_b+rank_kerPhi); ++i) {
			printf("\t(");
			for (size_t j=0; j<dim; ++j) {
				printf("%lf, ", curr_kM_selected_compl_vector[i][j]);
			}
			printf(")\n");
		}
		printf("n_row_curr_KM_comp = %d\n", n_row_curr_KM_comp);
		printf("New candidate is : [");
		for (size_t j=0; j<dim; j++) {
			printf(" %lf", base[i][j]);
		}
		printf(" ]\n");
		//*/
		
		bool is_indep = is_independent(ctx,
			(const double**) curr_kM_selected_compl_vector, n_row_curr_KM_comp,
			(const double*) base[i], dim);

		// DEBUG
		//printf("\t=> is_indep = %d\n", (int) is_indep);

		// Add to complement if independent from [previously selected vectors + k.M]
		if (is_indep) {
			// base[i] is a new independent vector => add it to complement
			// Still the assumption that base[i][j] is an integer (already checked multiple times before)
			for (int j=0; j<dim; j++)
				mat_compl_kernel = isl_mat_set_element_si(mat_compl_kernel,
					count_compl, j, (int) base[i][j]);
			count_compl++;

			// Add it to curr_kM_selected_compl_vector
			for (int j=0; j<dim; j++)
				curr_kM_selected_compl_vector[n_row_curr_KM_comp][j] = base[i][j];
			n_row_curr_KM_comp++;
		}
	}

	/* DEBUG
	printf("curr_kM_selected_compl_vector (after loop)=\n");
	for (size_t i=0; i<(n_b+rank_kerPhi); ++i) {
		printf("\t(");
		for (size_t j=0; j<dim; ++j) {
			printf("%lf, ", curr_kM_selected_compl_vector[i][j]);
		}
		printf(")\n");
	}
	printf("n_row_curr_KM_comp = %d\n", n_row_curr_KM_comp);
	//*/



	// Free temporary data struct
	for (int i=0; i<n_b; i++)
		free(curr_kM_selected_compl_vector[i]);
	free(curr_kM_selected_compl_vector);

	// If there were an over-allocation, we drop the (unused) last rows of mat_compl_kernel
	if (count_compl < num_vect_compl_approx) {
		mat_compl_kernel = isl_mat_drop_rows(mat_compl_kernel, count_compl, num_vect_compl_approx - count_compl);
	}

	/* DEBUG
	printf("vect_kvg=\n");
	for (size_t i=0; i<n_vect_kvg; ++i) {
		printf("\t(");
		for (size_t j=0; j<dim; ++j) {
			printf("%d, ", vect_kvg[i][j]);
		}
		printf(")\n");
	}
	printf("kerPhi=\n");
	for (size_t i=0; i<rank_kerPhi; ++i) {
		printf("\t(");
		for (size_t j=0; j<dim; ++j) {
			printf("%lf, ", kerPhi[i][j]);
		}
		printf(")\n");
	}
	printf("count_compl = %d | num_vect_compl_approx = %d\n", count_compl, num_vect_compl_approx);
	printf("mat_compl_kernel=\n");
	isl_mat_print_internal(mat_compl_kernel, stdout, 0);
	//*/

	return mat_compl_kernel;

}


// Auxiliary function to "solve_si_coefficients".
// Build the domain matrix (constraints on the "s_j" + 1 objective var) before calling Piplib
//	subspace_K is a list_item of subspaces
//  num_k_extra are the number of extra subspace we have append at the BEGINNING of the previous list
//		and that should not be counted in the sum of the s_j (to compute \sigma)
PipMatrix* build_domain_matrix_solve(size_t n_b, int dim, int n_K, list_item *subspace_K, double **base,
	size_t num_k_extra, isl_map *rel_for_context) {
	assert(n_b == dim); // Make sure that the base spans whole dim
	assert(subspace_K!=NULL);	// At least a path


	/* DEBUG
	printf("=== build_domain_matrix_solve - BEGIN ===\n");
	size_t npaths = 0;
	for(list_item *curr = K; curr != NULL; curr = curr->next) {
		path* p = curr->data;
		printf("\n\nPath #%zu\n--------\nType %d:\n Relation:\n", npaths, p->type);
		PRINT_ISL(p_out, isl_printer_print_map, p->rel);
		printf("\nDomain:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, p->dom);
		printf("\n");
		printf("Kernel:\n");
		for(size_t i=0; i<p->k.n; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", p->k.M[i][j]);
			}
			printf(")\n");
		}
		npaths++;
	}
	for(list_item *curr = subspace_K; curr != NULL; curr = curr->next) {
		subspace* k = curr->data;
		printf("Kernel:\n");
		for(size_t i=0; i<k->n; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", k->M[i][j]);
			}
			printf(")\n");
		}
	}
	printf("base =\n");
	for(size_t i=0; i<n_b; ++i){
		printf("(");
		for(size_t j=0; j<dim; ++j){
			printf("%lf, ", base[i][j]);
		}
		printf(")\n");
	}
	//*/


	// 1) Group the kernels dimensions according to their independence
	
	// We recover the kernel vectors
	int num_kervec = 0;
	for (list_item* ptr_K = subspace_K; ptr_K!=NULL; ptr_K=ptr_K->next) {
		subspace* k = ptr_K->data;
		num_kervec += k->n;
	}

	double** vec_kernels = (double**) malloc(num_kervec * sizeof(int*));
	int temp_i_kervec = 0;		// Counter to know which kervec we are looking at
	for (list_item* ptr_K = subspace_K; ptr_K!=NULL; ptr_K=ptr_K->next) {
		subspace* k = ptr_K->data;

		for (int i=0; i<k->n; i++) {
			vec_kernels[temp_i_kervec] = k->M[i];
			temp_i_kervec++;
		}
	}
	assert(temp_i_kervec==num_kervec);

	isl_ctx *ctx = isl_map_get_ctx(rel_for_context);	// Get the first context available

	list_item* g_veckernels = group_vec_kernels(vec_kernels, num_kervec, dim, ctx);
	free(vec_kernels);		// Note: do not free the inside
	// "g_veckernels->data" is of type kervec_group*

	/* DEBUG
	printf("g_veckernels =\n");
	for(list_item* ptr_kvg=g_veckernels; ptr_kvg!=NULL; ptr_kvg=ptr_kvg->next) {
		kervec_group* kvg = ptr_kvg->data;
		print_kervec_group(kvg);
	}
	printf("\n");
	//*/

	list_item* comb_veckernels = generate_combinations(g_veckernels, dim);
	// "g_veckernels->data" is of type kervec_group*

	/* DEBUG
	printf("comb_veckernels =\n");
	for(list_item* ptr_kvg=comb_veckernels; ptr_kvg!=NULL; ptr_kvg=ptr_kvg->next) {
		kervec_group* kvg = ptr_kvg->data;
		print_kervec_group(kvg);
	}
	//*/


	// Free g_veckernels
	while (g_veckernels!=NULL) {
		list_item* ln = g_veckernels->next;
		free_kervec_group(g_veckernels->data);
		free(g_veckernels);
		g_veckernels = ln;
	}

	
	// 1.4) Prepare the matrix
	
	// Count n_proj_const from g_veckernels  (=n_K if only singletons in g_veckernels)
	int n_proj_const = 0;
	for (list_item* g_ptr = comb_veckernels; g_ptr!=NULL; g_ptr=g_ptr->next)
		n_proj_const ++;

	const size_t n_cols = 1 + 1 + n_K + 0 + 1; //eq/ineq + obj_var + n_vars + no param + const
	const size_t n_rows = 1 + n_proj_const + n_K; //obj + n_proj_const projection constraints  n_K positiveness conditions

	// Populate domain matrix (build the constraint matrix), which is:
	// Ineq? | Obj |    s_i    | const
	// [  1  |  1  | -1 ... -1 |   0 ]		// Goal: minimize "Obj" variable
	// [  1  |  0  |  1s or 0s |  -1 ]		// Projection constraints (example here with 1-D groups)
	// [  1  |  0  |    Id     |   0 ]		// Positivity constraints (of the s_i)
	size_t file_size = ((n_rows+1)*n_cols*3) + 1;
	char *buffer = (char*)malloc(file_size * sizeof(char));
	FILE *mat_file = fmemopen(buffer, file_size, "w");
	if(mat_file == NULL)
	{
		fprintf(stderr, "Error opening file stream\n");
		pip_close();
		free(buffer);
		return NULL;
	}

#if (VERBOSE > 0)
	printf("\nSolving...\n");
#endif
	fprintf(mat_file, "%zu %zu\n", n_rows, n_cols);		// Matrix dimensions
	
	// Objective constraint
	fprintf(mat_file, "1 ");
	fprintf(mat_file, "1 ");
	if (num_k_extra>0) {		// If small dim kernel (first "K"), we add it after the sum of the others
		for (size_t i=0; i<num_k_extra; i++)
			fprintf(mat_file, "0 ");
		for (size_t i=num_k_extra; i<n_K; i++)
			fprintf(mat_file, "-1 ");
	} else {
		for(size_t i=0; i<n_K; ++i)
			fprintf(mat_file, "-1 ");
	}
	fprintf(mat_file, "0\n");


	// 2) For each group, generate the corresponding constraint
	for(const list_item *curr = comb_veckernels; curr != NULL; curr=curr->next) {
		kervec_group *kvg = (kervec_group*) curr->data;

		int** vect_group = kvg->vect_group;
		int num_vect_gr = kvg->num_vect_gr;
		int dim_gr = kvg->dim_gr;
		assert(kvg->dim_group==(-1));	// After "generate_combinations"
		assert(dim_gr == dim);



		// We need the following data to form the constraint:
		//	- Constant coeff is rank of the vect_group (to be computed)
		//	- linear coeff of s_i is rank of the image of vect_group through \phi_i (k->rel)
		isl_mat* isl_vect_group = isl_mat_alloc(ctx, num_vect_gr, dim_gr);	//	vect_group
		for (int i=0; i<num_vect_gr; i++)
			for (int j=0; j<dim_gr; j++)
				isl_vect_group = isl_mat_set_element_si(isl_vect_group, i, j, vect_group[i][j]);

		int rank_vect_group = isl_mat_rank(isl_vect_group);

		
		int* rank_im_phi_i = (int*) malloc(n_K * sizeof(int));
		for (int i=0; i<n_K; i++)
			rank_im_phi_i[i] = 0;

		int i_K = 0;
		for (list_item* ptr_K =subspace_K; ptr_K!=NULL; ptr_K=ptr_K->next) {
			subspace* phi_i = (subspace*) ptr_K->data;
			int rank_Ker = phi_i->n;

			// We complete the set of vector [kvg + phi_i->k.M] using the base "base"
			isl_mat* compl_K = complete_vector_base(ctx, base, n_b, vect_group, num_vect_gr, dim_gr,
				phi_i->M, rank_Ker);
			// Note: comp_K should be organized in rows

			// DEBUG
			//printf("compl_K (%d, %d)=\n", isl_mat_rows(compl_K), isl_mat_cols(compl_K));
			//isl_mat_print_internal(compl_K,stdout,0);


			// Values: "rank_im_phi_i = dim( \phi_i(isl_vect_group) )"
			//	We note isl_vect_group as "vg" for conciseness
			//	We decompose \phi_i = \tilde\phi_i o \pi_{Ker(\phi_i)} , where:
			//		- \tilde\phi_i is bijective
			//		- \pi_{Ker(\phi_j)} is the projection along the dimensions of Ker(\phi_i)
			// dim( \phi_i(vg) ) = dim( \pi_{Input(\phi_i) / Ker(\phi_i)}(vg) )
			//				= dim( vg \cap (Input(\phi_i) / Ker(\phi_i) ))
			//	Let us call " Compl = Input(\phi_i) / Ker(\phi_i) " (complementary of the basis of ker(\phi_i) = k.M) 
			//
			// Now, to compute "dim( vg \cap Compl )"" , we consider the matrix "A = [ Compl | vg ]"
			//  rank(A) = dim( Im(Compl) + Im(vg) ) = rank(Compl) + rank(vg) - dim(vg \cap Compl )
			// Conclusion:
			// 		dim( \phi_i(vg) ) = (n_dim_in - rank(k.M)) + rank(vg) - rank(A)
			//			(tadam!)
			//
			// In practice:
			//	-> We compute "compl_K" the complementary base of k.M  (note: this is independent from ivg => cf 1.5) )
			//	-> We build A = [ compl_K | isl_vect_group ] and get its rank ("isl_mat_rank")
			//	-> We compute everything (all according to the pl... formula)
			int n_row_compl_K = isl_mat_rows(compl_K);

			isl_mat* A = isl_mat_copy(compl_K);
			A = isl_mat_add_rows(A, num_vect_gr);
			for (int i=0; i<num_vect_gr; i++) {
				for (int j=0; j<dim_gr; j++) {
					A = isl_mat_set_element_val(A, i+n_row_compl_K, j,
							isl_mat_get_element_val(isl_vect_group, i, j)
						);
				}
			}
			A = isl_mat_transpose(A);

			// DEBUG
			//printf("\tA = \n");
			//isl_mat_print_internal(A, stdout, 2);
			
			int rank_A = isl_mat_rank(A);

			// DEBUG
			//printf("\t=>rank_A = %i\n", rank_A);


			// Get the rank of the group vector here (which might NOT be num_vect_gr)
			isl_mat* isl_vect_group_transp = isl_mat_transpose(isl_mat_copy(isl_vect_group));
			int rank_vg = isl_mat_rank(isl_vect_group_transp);
			isl_mat_free(isl_vect_group_transp);

			int dim_rank_phi_i = (dim - rank_Ker) + rank_vg - rank_A;
			assert(dim_rank_phi_i>=0);

			rank_im_phi_i[i_K] = dim_rank_phi_i;

			isl_mat_free(A);

			i_K++;
		}
		assert(i_K == n_K);

		/* DEBUG
		printf("Current kvg = ");
		print_kervec_group(kvg);
		printf("\t=> rank_im_phi_i = [");
		for (int i=0; i<n_K; i++)
			printf(" %d", rank_im_phi_i[i]);
		printf("]\n\n");
		//*/


		// Print-out the constraint
		fprintf(mat_file, "1 ");
		fprintf(mat_file, "0 ");
		for(size_t j=0; j<n_K; ++j) {
			fprintf(mat_file, "%d ", rank_im_phi_i[j]);
		}
		fprintf(mat_file, "-%d ", rank_vect_group);
		fprintf(mat_file, "\n");
		
		/* OLD Simple Code for 1D-groups only
		fprintf(mat_file, "1 ");
		fprintf(mat_file, "0 ");
		for(size_t j=0; j<n_b; ++j) {
			if(is_vector_in(((const double**)(k.M)), ((const double*)(base[j])), k.n, dim) == false)
				fprintf(mat_file, "1 ");
			else
				fprintf(mat_file, "0 ");
		}
		fprintf(mat_file, "-1 ");
		fprintf(mat_file, "\n");
		*/

		// Free temp data struct
		isl_mat_free(isl_vect_group);
		free(rank_im_phi_i);
	}

	// 3) Positiveness constraints
	for(size_t i = 0; i < n_K; i++) {
		fprintf(mat_file, "1 0 ");
		for(size_t j=0; j<n_K; j++)
			fprintf(mat_file, "%d ", (i == j));
		fprintf(mat_file, "0\n");
	}
	fclose(mat_file);

#if (VERBOSE > 2)
	printf("Basis:\n");
	for(size_t i=0; i<n_b; ++i)
	{
		printf("(");
		for(size_t j=0; j<dim; ++j)
		{
			printf("%lf, ", base[i][j]);
		}
		printf(")\n");
	}
#endif
#if (VERBOSE > 1)
	printf("dim: %d, n_proj_const: %d, n_K: %d, n_b: %zu\n", dim, n_proj_const, n_K, n_b);
	printf("Constraint matrix:\n");
	printf("%s", buffer);
#endif

	mat_file = fmemopen(buffer, file_size, "r");
	PipMatrix* domain = pip_matrix_read(mat_file);
	fclose(mat_file);
	
	free(buffer);

	// Free comb_veckernels
	while (comb_veckernels!=NULL) {
		list_item* ln = comb_veckernels->next;
		free_kervec_group(comb_veckernels->data);
		free(comb_veckernels);
		comb_veckernels = ln;
	}

	return domain;
}


// Auxiliary function to "solve_si_coefficients".
// Build the context matrix (constraints on the parameters) before calling Piplib
PipMatrix* build_context_matrix_solve() {
	// Populate context matrix : no constraints on the parameters (because no parameters)
	size_t file_size_context_mat = 10;
	char *buffer = (char*)malloc(file_size_context_mat * sizeof(char));
	FILE *mat_file = fmemopen(buffer, file_size_context_mat, "w");
	if(mat_file == NULL) {
		fprintf(stderr, "Error opening file stream\n");
		pip_close();
		free(buffer);
		return NULL;
	}
	fprintf(mat_file, "0 2\n"); //cols: eq/ineq + n_params + const
	fclose(mat_file);
	
	mat_file = fmemopen(buffer, file_size_context_mat, "r");
	PipMatrix* context = pip_matrix_read(mat_file);
	fclose(mat_file);
	
	free(buffer);

	return context;
}


// Auxiliary function to "solve" (part 1) - Manage the resolution of the s_i coefficient
//	through a call to PIP
// Note: list_item* subspace_K is a list of subspaces (with potentially extra projections at the start)
PipQuast* solve_si_coefficients(size_t n_b, int dim, double** base, list_item *subspace_K,
	int n_K, bool is_k_small, isl_map* rel_for_context) {

	// Setting-up the call to PIP
	PipOptions* options = pip_options_init();
	options->Nq = 0; //Need rational solution
	options->Verbose = -1;
	options->Simplify = 1;
	options->Maximize = 0;
	// OLD CODE: options->Maximize = 1;
	options->Urs_parms = 0; //All parameters are non-negative
	options->Urs_unknowns = 0; //All unknows are non-negative


	// DEBUG
	//printf("Building matrices...\n");

	// Build the domain (constraints) and context (constraints on params) matrix
	int num_k_small = (is_k_small)?1:0;
	PipMatrix *domain = build_domain_matrix_solve(n_b, dim, n_K, subspace_K, base, num_k_small, rel_for_context);
	PipMatrix *context = build_context_matrix_solve();


	// Solving the problem using Piplib ! (to obtain the s_i)
	PipQuast* solution = pip_solve(domain, context, -1, options);

	pip_options_free(options);
	pip_matrix_free(domain);
	pip_matrix_free(context);

	// Solution is the lexmin of the polyhedron defined by "domain"
	//	=> First element is "obj = \sum_j s_j". Rest are the s_j (rational elements)
	if (solution == NULL) {
		fprintf(stderr, "\nError solving the LP\n");
		pip_close();
		return NULL;
	}

	return solution;
}

/* ==================== */

typedef struct {
	expr *front_size;
	isl_union_set* dom_inter;
} res_aux_comp_front_size;


// Auxiliary function to "solve" (part 2) - Compute the front_size + dom_inter
res_aux_comp_front_size* solve_compute_front_size(const space_info * const tuple,
		 isl_set* D, list_item *K) {
	
	int dim = tuple->dim;

	isl_union_map* inv_map_inter = isl_union_map_reverse(isl_union_map_copy(tuple->dom_inter));
	isl_union_set *dom_inter = isl_union_set_apply(
			isl_union_set_from_set(isl_set_copy(D)),
			inv_map_inter);
	dom_inter = isl_union_set_apply(dom_inter, isl_union_map_copy(tuple->dom_inter));
#if (VERBOSE > 2)
	printf("\nInterference domain for path combination:\n");
	PRINT_ISL(p_out, isl_printer_print_union_set, dom_inter);
	printf("\n");
#endif

	//find frontier cardinality
	isl_set *frontier = isl_set_empty(isl_set_get_space(D));
	//bool first_K = true;
	for(const list_item *curr = K; curr != NULL; curr=curr->next) {
		subspace *k = &((path*)(curr->data))->k;
		//project along all subspace directions
		isl_set *proj = isl_set_copy(D);
		//isl_set *proj = isl_set_from_basic_set(isl_basic_set_copy(D));
		for(int i=0; i<k->n; ++i) {
			proj = projection(proj, k->M[i], dim);
		}
		frontier = isl_set_union(frontier, proj);
	}
	isl_set* dom_valid_front;
	
	isl_ctx* ctx = isl_set_get_ctx(frontier);
	isl_ctx_set_max_operations(ctx, 10000000);
	isl_ctx_reset_operations(ctx);

	expr *front_size = get_set_cardinality(isl_set_copy(frontier), 1, &dom_valid_front);
	dom_valid_front = isl_set_remove_divs(dom_valid_front);
	if(front_size != NULL)
	{
		//front_size = expr_expand(expr_mul(front_size, expr_new_frac(gamma, 1)));
		dom_inter = isl_union_set_intersect(dom_inter, isl_union_set_from_set(dom_valid_front));
	}

	isl_ctx_set_max_operations(ctx, INT_MAX);

#if (VERBOSE > 1)
	if(front_size == NULL)
	{
		fprintf(stdout, "\nWarning: cannot find cardinality for the frontier:\n");
		PRINT_ISL(p_out, isl_printer_print_set, frontier);
	}
	else
	{
		printf("\nFrontier:\n");
		PRINT_ISL(p_out, isl_printer_print_set, frontier);
		printf("\nFrontier size: ");
		expr_print(front_size);
		printf("\n");
		//PRINT_ISL(p_out, isl_printer_print_pw_qpolynomial,
		//	isl_pw_qpolynomial_to_polynomial(isl_set_card(isl_set_copy(frontier)), 1));
		//printf("\n");
	}
#endif

	isl_set_free(frontier);


	// Buiding the output, before returning it
	res_aux_comp_front_size* res = malloc(sizeof(res_aux_comp_front_size));
	res->front_size = front_size;
	res->dom_inter = dom_inter;

	return res;
}


// Auxiliary function to "solve" (part 3) - Compute the \beta_i coefficients
int* solve_compute_betas_denom(int n_K, list_item* K, isl_set* D) {
	int* betas_denom = malloc(n_K * sizeof(int));
	int ninter = 0;
	int i = 0;
	//printf("DEBUG Dp\n");
	for (const list_item *curr = K; curr != NULL; curr=curr->next) {
		path *p = (path*)(curr->data);
		isl_union_set* Dp = isl_union_set_from_set(
				isl_set_apply(isl_set_copy(D),
						isl_map_reverse(isl_map_copy(p->rel))
				)
			);

		// DEBUG
		//PRINT_ISL(p_out, isl_printer_print_union_set, Dp);

		int inter = 0;
		for (const list_item *currp = K; currp != NULL; currp=currp->next) {
			path *pp = (path*)(currp->data);
			if (pp != p) {
				isl_union_set* Dpp = isl_union_set_from_set(
						isl_set_apply(isl_set_copy(D),
								isl_map_reverse(isl_map_copy(pp->rel))
						)
					);
				if(!isl_union_set_is_empty(isl_union_set_intersect(isl_union_set_copy(Dp), Dpp))) {
					inter = 1;
					ninter++;
					break;
				}
			}
		}
		betas_denom[i++] = inter;
		isl_union_set_free(Dp);
	}
	for(int i = 0; i < n_K; i++)
		betas_denom[i] = betas_denom[i]? ninter: 1;


	// TODO - Note: computation of betas_denom[i] might be sub-optimal
	//			in certain situations where there is interferences
	// Ex: 1 + 2 <= K
	//	   2 + 3 <= K
	//	   3 + 4 <= K
	// => Wanter \beta_i (in the paper notation): 1/3 2/3 2/3 1/3]
	// => Wanted/optimal: betas_denom[i] (= 1/\beta_i in the paper) = [3 3/2 3/2 3]
	// => Computed betas_denom[i] = [6 6 6 6]  (which is valid, but we lose at best
	//										a factor of 2^\sigma in the bound)
	//		where \sigma = \sum_i s_i


	// DEBUG
	//printf("ninter = %i\n", ninter);

	return betas_denom;
}


/* ==================== */

bool _debug_hourglass_solve = false;

// Aux-function to "solve_hourglass": Bound on E' (the internal part of the K-set E)
// alpha_inner is an output
expr* get_ub_inner_hourglass(const compl_hourglass_p * const hg_info, int dim, isl_set* D,
		double** base, size_t n_b, list_item *K, isl_ctx* ctx_isl, expr** ptr_alpha_inner) {
	// Modifications:
	// - Extra projection on the red_bcst dimensions
	//		=> Associated upper bound: expr_card
	// - For each projection involving a red_bcst dim (but not only them), remove these dims
	//		=> Associated upper bound: K / (card(set_inters projected on these red_bcst) )
	// - Other proj: leave them as they are
	//		=> Associated upper bound: K

	// 1.a) Building the extra projection (subspace) on the red_bcst dims
	//	=> Its kernel is the complementary of these dimension
	int num_red_bcst_dim = 0;
	for (int i=0; i<dim; i++)
		if (hg_info->is_red_bcst_dim[i])
			num_red_bcst_dim++;

	double** kM_red_bcst = (double**) malloc((dim-num_red_bcst_dim) * sizeof(double*));
	for (int i=0; i<(dim-num_red_bcst_dim); i++) {
		kM_red_bcst[i] = (double*) malloc(dim * sizeof(double));
		for (int j=0; j<dim; j++)
			kM_red_bcst[i][j] = 0.0;
	}
	int num_line = 0;
	for (int i=0; i<dim; i++) {
		if (!hg_info->is_red_bcst_dim[i]) {		// Invert to get the value of the kernel
			kM_red_bcst[num_line][i] = 1.0;
			num_line++;
		}
	}
	assert(num_line==dim-num_red_bcst_dim);

	subspace* k_red_bcst = malloc(sizeof(subspace));
	k_red_bcst->M = kM_red_bcst;
	k_red_bcst->n = dim-num_red_bcst_dim;

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("[Hourglass solve - 1a] num_red_bcst_dim = %i\n", num_red_bcst_dim);
		printf("kM_red_bcst =\n");
		for(size_t i=0; i<dim-num_red_bcst_dim; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", kM_red_bcst[i][j]);
			}
			printf(")\n");
		}
		fflush(stdout);
	}
	//*/

	// 1.b) We convert "K" into its list of subspace "subspace_K"
	//		(corresponding to the kernel of the projections of the paths)
	//	+ We do the adaptation of the projection here (depending on red_bcst_dim presence)

	// Matrix of the row vector for the base of the red_bcst_dim
	double** vect_red_bcst = (double**) malloc(num_red_bcst_dim * sizeof(double*));
	for (int i=0; i<num_red_bcst_dim; i++) {
		vect_red_bcst[i] = (double*) malloc(dim * sizeof(double));
		for (int j=0; j<dim; j++)
			vect_red_bcst[i][j] = 0.0;
	}
	num_line = 0;
	for (int i=0; i<dim; i++) {
		if (hg_info->is_red_bcst_dim[i]) {
			vect_red_bcst[num_line][i] = 1.0;
			num_line++;
		}
	}
	assert(num_line==num_red_bcst_dim);

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("\n[Hourglass solve - 1b] vect_red_bcst =\n");
		for(size_t i=0; i<num_red_bcst_dim; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", vect_red_bcst[i][j]);
			}
			printf(")\n");
		}
		fflush(stdout);
	}
	//*/


	// Get the number of planes
	int n_K = 0;
	for (list_item* ptr_K = K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		n_K++;
	}

	// Array to store the absence of red_bcst dims for all the subspace
	//	Size: (n_K * num_red_bcst_dim)
	// 	Should be in the same order than subspace_K (built in the next segment)
	bool** l_missing_red_bcst_dim = malloc(n_K * sizeof(bool*));

	// Tracking of the non-red/bcst dims that are covered by homomorphism
	//	that also involve red_bcst  (=> the assoc. upper bounds will be like "K/M")
	// => This will be used later to (potentially) remove homomorphisms that do not involve
	//	 red_bcst (ie, their kernel has all the red_bcst dims) and that only use these dims
	bool* l_dim_red_bcst_covered = malloc(dim * sizeof(bool));
	for (int i=0; i<dim; i++)
		l_dim_red_bcst_covered[i] = false;

	list_item* subspace_K = NULL;
	int count_subspace_K = n_K-1; // Reverse order
	for (list_item* ptr_K = K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		path* p = (path*) ptr_K->data;

		subspace* sub_p = &(p->k);
		// vec_kernel: sub_p->M;
		// vect_red_bcst : contains the vector for reduction_bcst

		// 2 cases:
		// 1) The kernel ("sub_p->M") already contains all red_bcst dimensions: no change
		// 2) The kernel has red_bcst dimensions missing
		//		=> Add them to the kernel + register them (changes the "M" in the bound)

		// We check that by iterating on the rows of "vect_red_bcst"
		// 	and checking with "path.c :: can_expand_base" if it is lin indep from "sub_p->M"
		// => If at least one of them is, then we are in "case 2".
		// => If none of them are, then we are in "case 1".

		bool are_all_red_bcst_here = true;
		bool* missing_red_bcst_dim = malloc(num_red_bcst_dim * sizeof(bool));
		for (int i=0; i<num_red_bcst_dim; i++)
			missing_red_bcst_dim[i] = false;

		for (int i=0; i<num_red_bcst_dim; i++) {
			// If v is linearly independent from the base, return 1
			int res_expand = can_expand_base(ctx_isl, (const double**) sub_p->M, sub_p->n, vect_red_bcst[i], dim);
			if (res_expand==1) {
				// Independent from the base: case 2
				are_all_red_bcst_here = false;
				missing_red_bcst_dim[i] = true;
			}
		}

		// Saving these infos in "l_missing_red_bcst_dim" (needed for later)
		l_missing_red_bcst_dim[count_subspace_K] = missing_red_bcst_dim;
		count_subspace_K--;

		subspace* nsub_p;
		if (are_all_red_bcst_here) {
			// Case 1: no change
			nsub_p = sub_p;

			// Add it to subspace_K
			list_item* nlssK = (list_item*) malloc(sizeof(list_item));
			nlssK->data = nsub_p;
			nlssK->next = subspace_K;
			subspace_K = nlssK;
		} else {
			// Case 2: we need to add the missing vectors to the base
			int n_extra_dim = 0;
			for (int i=0; i<num_red_bcst_dim; i++)
				if (missing_red_bcst_dim[i])
					n_extra_dim++;

			int n_numkernel_vec = sub_p->n + n_extra_dim;

			double** nM = (double**) malloc(n_numkernel_vec * sizeof(double*));
			for (int i=0; i<n_numkernel_vec; i++) {
				nM[i] = (double*) malloc(dim * sizeof(double));
			}

			// First rows/vectors: the ones from sub_p->M
			for (int i=0; i<sub_p->n; i++)
				for (int j=0; j<dim; j++)
					nM[i][j] = sub_p->M[i][j];
			
			// Last rows/vectors: the ones from vect_red_bcst
			int curr_row = sub_p->n;
			for (int i=0; i<num_red_bcst_dim; i++) {
				if (missing_red_bcst_dim[i]) {
					for (int j=0; j<dim; j++)
						nM[curr_row][j] = vect_red_bcst[i][j];
					curr_row++;
				}
			}
			assert(curr_row == n_numkernel_vec);

			// Building the new subspace (with the extra vectors)
			nsub_p = malloc(sizeof(subspace));
			nsub_p->M = nM;
			nsub_p->n = n_numkernel_vec;	// We have added n_extra_dim vects

			// While we are at it, update "l_dim_red_bcst_covered"
			//	=> Put the corresponding dim at "true" if their "nM" column is all "0s"
			for (int j=0; j<dim; j++) {
				bool dim_all_0s = true;
				for (int i=0; i<n_numkernel_vec; i++) {
					if (nM[i][j]!=0.0) {
						dim_all_0s = false;
						break;
					}
				}
				if (dim_all_0s) {
					l_dim_red_bcst_covered[j] = true;
				}
			}

			// Add it to subspace_K
			list_item* nlssK = (list_item*) malloc(sizeof(list_item));
			nlssK->data = nsub_p;
			nlssK->next = subspace_K;
			subspace_K = nlssK;
		}
		
	}

	assert(count_subspace_K==-1);

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("n_K = %d\n", n_K);
		printf("subspace_K (mid preprocessing) = [[[\n");
		int count_debug_1b = 0;
		for (list_item* ptr_K = subspace_K; ptr_K != NULL; ptr_K = ptr_K->next ) {
			subspace* sub_p = ptr_K->data;
			printf("  * Proj %d - num_kervec = %lu\n", count_debug_1b, sub_p->n);

			for(size_t i=0; i<sub_p->n; ++i){
				printf("      (");
				for(size_t j=0; j<dim; ++j){
					printf("%lf, ", sub_p->M[i][j]);
				}
				printf(")\n");
			}

			count_debug_1b++;
		}
		printf("]]]\n");
		fflush(stdout);
	}
	//*/

	// 1.b.bis) Post-processing: we remove the subspaces corresponding to projections
	//		that project into no red_bcst dims, and whose dimensions are all covered
	//		by other projections (involving red_bst dims).
	// This leads to an asymptotically better bound (ex: 2S -> 4 S^2 / M^2 for Gramschmidt)
	bool* b_mark_to_be_removed = malloc(n_K * sizeof(bool));  // Index of the subspace to be removed
	for (int i=0; i<n_K; i++)
		b_mark_to_be_removed[i] = false;

	count_subspace_K = 0;
	for (list_item* ptr_subspace_K = subspace_K; ptr_subspace_K != NULL; ptr_subspace_K = ptr_subspace_K->next) {
		subspace* Kcurr = ptr_subspace_K->data;

		bool* missing_red_bcst_dim = l_missing_red_bcst_dim[count_subspace_K];

		// Check if this missing_red_bcst_dim is all "false"
		bool are_all_red_bcst_here = true;
		for (int i=0; i<num_red_bcst_dim; i++) {
			if (missing_red_bcst_dim[i]) {
				are_all_red_bcst_here = false;
				break;
			}
		}

		// If this is not a proj on which we added stuffs, skip
		if (!are_all_red_bcst_here) {
			count_subspace_K++;
			continue;
		}

		// Let's check if all its dims are covered by other proj
		// (cf "l_dim_red_bcst_covered")
		bool all_dim_covered = true;
		for (int j=0; j<dim; j++) {
			if (hg_info->is_red_bcst_dim[j]) {	// Skip the red/bcst dimensions
				continue;
			}

			bool dim_non_kernel = true;
			for (int i=0; i<Kcurr->n; i++) {
				if (Kcurr->M[i][j] != 0.0) {
					dim_non_kernel = false;
					break;
				}
			}

			if (!dim_non_kernel) {
				// j is a dim of the projection
				// We check if it is covered by l_dim_red_bcst_covered
				if (!l_dim_red_bcst_covered[j]) {
					all_dim_covered = false;
					break;
				}
			}
		}
		if (all_dim_covered) {
			// We need to remove this subspace!
			b_mark_to_be_removed[count_subspace_K] = true;
		}

		count_subspace_K++;
	}


	// Actual remove the subspace marked in "b_mark_to_be_removed"
	// => Update subspace_K, n_K and l_missing_red_bcst_dim
	int n_nK = n_K;
	for (int i=0; i<n_K; i++) {
		if (b_mark_to_be_removed[i])
			n_nK--;
	}
	n_K = n_nK;

	count_subspace_K = 0;
	list_item* dummy_cell = malloc(sizeof(list_item));  // Dummy cell (iteration number "-1")
	dummy_cell->next = subspace_K;
	list_item* prev_cell = dummy_cell;
	for (list_item* ptr_subspace_K = subspace_K; ptr_subspace_K != NULL; ptr_subspace_K = ptr_subspace_K->next) {
		if (b_mark_to_be_removed[count_subspace_K]) {
			// We remove that cell
			prev_cell->next = ptr_subspace_K->next;
		} else {
			// We keep it, and advance prev_cell
			prev_cell = prev_cell->next;
		}
		count_subspace_K++;
	}
	subspace_K = dummy_cell->next;
	free(dummy_cell);

	// l_missing_red_bcst_dim: we reduce its size: no need for a new alloc
	int icurr = 0;
	for (int i=0; i<dim; i++) {
		if (b_mark_to_be_removed[i]) {
			// Nothing to be done here
		} else {
			l_missing_red_bcst_dim[icurr] = l_missing_red_bcst_dim[i];
			icurr++;
		}
	}


	// We add the new projection (on red_bcst dims) at the start
	list_item* nlssK = (list_item*) malloc(sizeof(list_item));
	nlssK->data = k_red_bcst;
	nlssK->next = subspace_K;
	subspace_K = nlssK;


	// Clean-up (of vect_red_bcst and l_dim_red_bcst_covered)
	for (int i=0; i<num_red_bcst_dim; i++)
		free(vect_red_bcst[i]);
	free(vect_red_bcst);
	free(l_dim_red_bcst_covered);
	free(b_mark_to_be_removed);



	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("n_K+1 = %d\n", (n_K+1));
		printf("subspace_K (after preprocessing) = [[[\n");
		int count_debug_1bb = 0;
		for (list_item* ptr_K = subspace_K; ptr_K != NULL; ptr_K = ptr_K->next ) {
			subspace* sub_p = ptr_K->data;
			printf("  * Proj %d - num_kervec = %lu\n", count_debug_1bb, sub_p->n);

			for(size_t i=0; i<sub_p->n; ++i){
				printf("      (");
				for(size_t j=0; j<dim; ++j){
					printf("%lf, ", sub_p->M[i][j]);
				}
				printf(")\n");
			}

			count_debug_1bb++;
		}
		printf("]]]\n");
		fflush(stdout);
	}
	//*/


	// 1.c) The call to compute the s_i
	isl_map* rel_for_context = ((path*) K->data)->rel;
	bool is_k_small = false;		// s_i of the extra projection included in \sigma
	PipQuast* solution = solve_si_coefficients(n_b, dim, base, subspace_K, (n_K+1), is_k_small, rel_for_context);
	assert(solution->condition == NULL); //no QUAST

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("\n[Hourglass solve - 1c] Solution:\n");
		pip_quast_print(stdout,solution,0);
		fflush(stdout);
	}
	//*/

	
	// 1.d) Prepare the expr_bounds

	// Correction on the value of sigma (first value of solution),
	//	 due to the inclusion of the first
	// Indeed, the sigma used to compute the next "alpha" is supposed to be the power of the "K" term
	//   and the extra subspace we have added has an upperbound of "card_expr" instead of "K"
	// => We substract it by the first s_1
	expr* alpha_inner = compute_alpha_hourglass_inner(solution->list);

	// We use alpha to compute the value of Kexpr
	expr* Kexpr = expr_mul( expr_add(alpha_inner, expr_new("1")), expr_S()); // K = (\alpha+1).S

	// Because it is way easier, we build the expr_bounds in the inverted order first
	list_item* expr_bounds_inverted = NULL;
	count_subspace_K = 0;
	for (list_item* ptr_K = subspace_K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		// Proj on all red_bcst dims always the first element
		if (count_subspace_K==0) {
			// Corresponding ub for this projection: expr_card
			expr* ub_proj_red_bcst = hg_info->expr_card;

			// Add it to the list
			list_item* nebinv = malloc(sizeof(list_item));
			nebinv->data = ub_proj_red_bcst;
			nebinv->next = expr_bounds_inverted;
			expr_bounds_inverted = nebinv;

			count_subspace_K++;
			continue;
		}

		// Other 2 cases: cf "l_missing_red_bcst_dim" for the infos
		//	Note: the extra proj was added in the meantime, thus the "-1"
		bool* missing_red_bcst_dim = l_missing_red_bcst_dim[count_subspace_K-1];

		// Check if all these dim are in the kernel
		bool b_all_red_bcst_in_kernel = true;
		bool b_no_red_bcst_in_kernel = true;
		for (int i=0; i<num_red_bcst_dim; i++) {
			if (missing_red_bcst_dim[i]) {
				b_all_red_bcst_in_kernel = false;
			} else {
				b_no_red_bcst_in_kernel = false;
			}
		}

		if (b_all_red_bcst_in_kernel) {
			// No modification required
			// Corresponding ub for this projection: K
			expr* ub_proj_no_red_bcst = Kexpr;

			// Add it to the list
			list_item* nebinv = malloc(sizeof(list_item));
			nebinv->data = ub_proj_no_red_bcst;
			nebinv->next = expr_bounds_inverted;
			expr_bounds_inverted = nebinv;
		} else {
			// Complicated case (case where we had to change the projection)
			// We need to compute the cardinality of set_inters, projected on the
			//		present red/bcst dimensions.
			// (neutral dim should not have an impact on the cardinality of set_inters)


			// Special-quick case: all the red/bcst dimensions are present
			//		=> just use expr_card
			expr* card_partial_proj;
			if (b_no_red_bcst_in_kernel) {
				card_partial_proj = hg_info->expr_card;
			} else {
				// We definitively need to do the projection
				//	on the dimensions which are not present in the kernel
				isl_set* proj_set_inters = isl_set_copy(hg_info->res_set_inters);

				// Index corresponds to the real dimension indexes
				bool* is_projected_dim = malloc(dim * sizeof(bool));
				int ind_missing_red_bcst_dim = 0;
				for (int i=0; i<dim; i++) {
					if (hg_info->is_red_bcst_dim[i]) {
						is_projected_dim[i] = missing_red_bcst_dim[ind_missing_red_bcst_dim];
						ind_missing_red_bcst_dim++;
					} else
						is_projected_dim[i] = false;
				}
				assert(ind_missing_red_bcst_dim == num_red_bcst_dim);

				// Project out the dimensions
				for (unsigned int i=dim-1; i>=0; i--) {
					if (is_projected_dim[i]) {
						proj_set_inters = isl_set_project_out(proj_set_inters, isl_dim_set, i, 1);
					}
				}

				// Getting the card of the projection
				isl_set *dom_params = isl_set_params(isl_set_copy(hg_info->res_set_inters));
				int rounding = 0; // Lower bound on cardinality
				card_partial_proj = get_set_cardinality(proj_set_inters, rounding, &dom_params);

				// Clean-up
				free(is_projected_dim);
			}
			expr* ub_proj_some_red_bcst = expr_div( Kexpr, card_partial_proj);


			// Add it to the list
			list_item* nebinv = malloc(sizeof(list_item));
			nebinv->data = ub_proj_some_red_bcst;
			nebinv->next = expr_bounds_inverted;
			expr_bounds_inverted = nebinv;

		} // End complicated case

		// Don't forget to iterate
		count_subspace_K++;
	}
	assert(count_subspace_K = n_K+1);

	// Invert the order of expr_bounds_inverted to get expr_bounds
	list_item* expr_bounds = list_item_reverse(expr_bounds_inverted);

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("[Hourglass solve - 1d] expr_bounds (after preprocessing) = [[[\n");
		int count_debug_1d = 0;
		for (list_item* ptr_eb = expr_bounds; ptr_eb != NULL; ptr_eb = ptr_eb->next ) {
			expr* eb_proj = ptr_eb->data;
			printf("  * Proj %d - expr_bound = ", count_debug_1d);
			expr_print(eb_proj);
			printf("\n");

			count_debug_1d++;
		}
		printf("]]]\n");
		fflush(stdout);
	}
	//*/


	// Clean-up
	for (int i=0; i<n_K; i++)
		free(l_missing_red_bcst_dim[i]);
	free(l_missing_red_bcst_dim);
	// We free "subspace K" (but not its content!)
	while (subspace_K!=NULL) {
		list_item* ln = subspace_K->next;
		free(subspace_K);
		subspace_K = ln;
	}
	// We also free expr_bounds_inverted (not its content which is shared with expr_bounds)
	while (expr_bounds_inverted!=NULL) {
		list_item* ln = expr_bounds_inverted->next;
		free(expr_bounds_inverted);
		expr_bounds_inverted = ln;
	}


	// 1.e) Build the upper bound for the inner part
	expr* ub_inner = compute_upper_bound_nobeta(solution->list, expr_bounds);

	// We free expr_bounds (not sure if we should free its content (subexpr of ub_inner) ?)
	while (expr_bounds!=NULL) {
		list_item* ln = expr_bounds->next;
		free(expr_bounds);
		expr_bounds = ln;
	}

	*ptr_alpha_inner = alpha_inner;
	return ub_inner;
}


// Aux-function to "solve_hourglass": Bound on F (the border part of the K-set E)
expr* get_ub_border_hourglass(const compl_hourglass_p * const hg_info, int dim, isl_set* D,
	double** base, size_t n_b, list_item *K, isl_ctx* ctx_isl, expr* alpha_inner) {

	// Modifications:
	// - Extra projection on the temporal dimensions
	//		=> Associated upper bound: 2
	// - Do not modify the proj involving neutral dim
	//		=> Pick 1 of them, assoc. UB: 1  (one dim is sacrificed to do the sum on the split)
	//		=> For the rest, assoc. UB: K
	// - Other proj not involving neutral dim: leave them as they are
	//		=> Associated upper bound: K
	//
	// BUT! The total is multipled by K (because the BL theorem is applied on a slice of a single neutral dim)
	//
	// ===> Bound is always K, except for the extra projection on the temporal dim

	// 2.a) Building the extra projection (subspace) on the temporal dims
	//	=> Its kernel is the complementary of these dimension
	int num_temporal_dim = 0;
	for (int i=0; i<dim; i++)
		if (hg_info->is_temporal_dim[i])
			num_temporal_dim++;

	double** kM_temporal = (double**) malloc((dim-num_temporal_dim) * sizeof(double*));
	for (int i=0; i<(dim-num_temporal_dim); i++) {
		kM_temporal[i] = (double*) malloc(dim * sizeof(double));
		for (int j=0; j<dim; j++)
			kM_temporal[i][j] = 0.0;
	}
	int num_line = 0;
	for (int i=0; i<dim; i++) {
		if (!hg_info->is_temporal_dim[i]) {		// Invert to get the value of the kernel
			kM_temporal[num_line][i] = 1.0;
			num_line++;
		}
	}
	assert(num_line==dim-num_temporal_dim);

	subspace* k_temporal = malloc(sizeof(subspace));
	k_temporal->M = kM_temporal;
	k_temporal->n = dim-num_temporal_dim;
	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("[Hourglass solve - 2a] num_temporal_dim = %i\n", num_temporal_dim);
		printf("kM_temporal =\n");
		for(size_t i=0; i<dim-num_temporal_dim; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", kM_temporal[i][j]);
			}
			printf(")\n");
		}
	}
	//*/


	// 2.b) We convert "K" into its list of subspace "subspace_K"
	//		(corresponding to the kernel of the projections of the paths)
	//	+ No need of adapatation here

	// Get the number of planes
	int n_K = 0;
	for (list_item* ptr_K = K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		n_K++;
	}

	list_item* subspace_K = NULL;
	for (list_item* ptr_K = K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		path* p = (path*) ptr_K->data;
		subspace* sub_p = &(p->k);

		// No modif here, just add it to the list
		list_item* nlssK = (list_item*) malloc(sizeof(list_item));
		nlssK->data = sub_p;
		nlssK->next = subspace_K;
		subspace_K = nlssK;
	}

	// We add the new projection (on temporal dims) at the start
	list_item* nlssK = (list_item*) malloc(sizeof(list_item));
	nlssK->data = k_temporal;
	nlssK->next = subspace_K;
	subspace_K = nlssK;

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("[Hourglass solve - 2b] n_K+1 = %d\n", (n_K+1));
		printf("subspace_K (after preprocessing) = [[[\n");
		int count_debug_1bb = 0;
		for (list_item* ptr_K = subspace_K; ptr_K != NULL; ptr_K = ptr_K->next ) {
			subspace* sub_p = ptr_K->data;
			printf("  * Proj %d - num_kervec = %lu\n", count_debug_1bb, sub_p->n);

			for(size_t i=0; i<sub_p->n; ++i){
				printf("      (");
				for(size_t j=0; j<dim; ++j){
					printf("%lf, ", sub_p->M[i][j]);
				}
				printf(")\n");
			}

			count_debug_1bb++;
		}
		printf("]]]\n");
		fflush(stdout);
	}
	//*/


	// 2.c) The call to compute the s_i
	isl_map* rel_for_context = ((path*) K->data)->rel;
	bool is_k_small = true;		// the first proj has an UB of 2 => do not include in \sigma to minimize
	PipQuast* solution = solve_si_coefficients(n_b, dim, base, subspace_K, (n_K+1), is_k_small, rel_for_context);
	assert(solution->condition == NULL); //no QUAST


	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("\n[Hourglass solve - 2c] Solution:\n");
		pip_quast_print(stdout,solution,0);
		fflush(stdout);
	}
	//*/


	// 2.d) Prepare the expr_bounds
	expr* Kexpr = expr_mul( expr_add(alpha_inner, expr_new("1")), expr_S()); // K = (\alpha+1).S

	// Matrix of the row vector for the base of the neutral_dim
	int num_neutral_dim = 0;
	for (int i=0; i<dim; i++)
		if (hg_info->is_neutral_dim[i])
			num_neutral_dim++;

	double** vect_neutral = (double**) malloc(num_neutral_dim * sizeof(double*));
	for (int i=0; i<num_neutral_dim; i++) {
		vect_neutral[i] = (double*) malloc(dim * sizeof(double));
		for (int j=0; j<dim; j++)
			vect_neutral[i][j] = 0.0;
	}
	num_line = 0;
	for (int i=0; i<dim; i++) {
		if (hg_info->is_neutral_dim[i]) {
			vect_neutral[num_line][i] = 1.0;
			num_line++;
		}
	}
	assert(num_line==num_neutral_dim);


	// Because it is way easier, we build the expr_bounds in the inverted order first
	list_item* expr_bounds_inverted = NULL;
	int count_subspace_K = 0;
	//bool is_first_neutral_dim_proj = true;  // Not needed anymore
	for (list_item* ptr_K = subspace_K; ptr_K != NULL; ptr_K = ptr_K->next ) {

		// Proj on all red_bcst dims always the first element
		if (count_subspace_K==0) {
			// Corresponding ub for this projection: 2
			expr* ub_proj_temporal = expr_new("2");

			// Add it to the list
			list_item* nebinv = malloc(sizeof(list_item));
			nebinv->data = ub_proj_temporal;
			nebinv->next = expr_bounds_inverted;
			expr_bounds_inverted = nebinv;

			count_subspace_K++;
			continue;
		}

		// If first projection with component on neutral dim: corr. ub is 1
		// Else: corresponding ub is Kexpr
		//if (!is_first_neutral_dim_proj) {
		expr* ub_proj_default = Kexpr;

		// Add it to the list
		list_item* nebinv = malloc(sizeof(list_item));
		nebinv->data = ub_proj_default;
		nebinv->next = expr_bounds_inverted;
		expr_bounds_inverted = nebinv;

		count_subspace_K++;
		//	continue;
		//}


		// Here, we really need to check the kernel to see if all neutral dims are here
		// vec_kernel: sub_p->M;
		// vect_neutral : contains the vectors of neutral
		/*subspace* sub_p = ptr_K->data;

		bool are_all_neutral_here = true;
		for (int i=0; i<num_neutral_dim; i++) {
			// If v is linearly independent from the base, return 1
			int res_expand = can_expand_base(ctx_isl, (const double**) sub_p->M, sub_p->n, vect_neutral[i], dim);
			if (res_expand==1) {
				// Independent from the base: case 2
				are_all_neutral_here = false;
			}
		}

		// are_all_neutral_here (in the kernel) = no neutral dim in the projected space
		//		=> K as a bound
		expr* ub_proj;
		if (are_all_neutral_here) {
			ub_proj = Kexpr;				// Nothing to see here
		} else {
			// Is it the first time we have a projection on part of the neutral dim?
			assert(is_first_neutral_dim_proj); // "Else" already covered previously
			ub_proj = expr_new("1");	// Sacrifice it to do the slicing (in the proof)
			is_first_neutral_dim_proj = false;
		}

		// Add it to the list
		list_item* nebinv = malloc(sizeof(list_item));
		nebinv->data = ub_proj;
		nebinv->next = expr_bounds_inverted;
		expr_bounds_inverted = nebinv;

		// Don't forget to iterate
		count_subspace_K++;
		*/
	}
	assert(count_subspace_K = n_K+1);


	// Invert the order of expr_bounds_inverted to get expr_bounds
	list_item* expr_bounds = list_item_reverse(expr_bounds_inverted);

	//* DEBUG TODO
	if (_debug_hourglass_solve) {
		printf("[Hourglass solve - 2d] expr_bounds (after preprocessing) = [[[\n");
		int count_debug_2 = 0;
		for (list_item* ptr_eb = expr_bounds; ptr_eb != NULL; ptr_eb = ptr_eb->next ) {
			expr* eb_proj = ptr_eb->data;
			printf("  * Proj %d - expr_bound = ", count_debug_2);
			expr_print(eb_proj);
			printf("\n");

			count_debug_2++;
		}
		printf("]]]\n");
		fflush(stdout);
	}
	//*/

	// Clean-up
	// We free "subspace K" (but not its content!)
	while (subspace_K!=NULL) {
		list_item* ln = subspace_K->next;
		free(subspace_K);
		subspace_K = ln;
	}
	// We also free expr_bounds_inverted (not its content which is shared with expr_bounds)
	while (expr_bounds_inverted!=NULL) {
		list_item* ln = expr_bounds_inverted->next;
		free(expr_bounds_inverted);
		expr_bounds_inverted = ln;
	}

	// 2.e) Build the upper bound for the border part
	expr* ub_border = compute_upper_bound_nobeta(solution->list, expr_bounds);

	// We free expr_bounds (not sure if we should free its content (subexpr of ub_inner) ?)
	while (expr_bounds!=NULL) {
		list_item* ln = expr_bounds->next;
		free(expr_bounds);
		expr_bounds = ln;
	}

	return ub_border;
}



// Alternate implementation for "solve"
// No small dimensions, or beta trick compatibility
lb_data* solve_hourglass(const space_info * const tuple, compl_hourglass_p * hg_info) {
	isl_set *D = tuple->D;
	int dim = tuple->dim;
	double** base = tuple->base;
	size_t n_b = tuple->n_bases;
	list_item *K = tuple->K;		// List of paths (the chosen combination)

	// Needed for "can_expand_base"
	isl_ctx* ctx_isl = isl_set_get_ctx(D);

	assert(dim == hg_info->ndim_S2);

#if (VERBOSE > 1)
	printf("[Hourglass solve] hg_info = ");
	print_compl_hourglass_p(hg_info);
	printf("\n");
	fflush(stdout);
#endif

	// === Part 1 - Bound on E' (the internal part of the K-set E)
	expr* dummy_expr = expr_new("1");
	expr** ptr_alpha_inner = &dummy_expr;

	expr* ub_inner = get_ub_inner_hourglass(hg_info, dim, D, base, n_b, K, ctx_isl, ptr_alpha_inner);
	
	expr* alpha_inner = *ptr_alpha_inner;

#if (VERBOSE > 1)
	printf("[Hourglass solve - END Inner] ub_inner = ");
	expr_print(ub_inner);
	printf("\n alpha_inner = ");
	expr_print(alpha_inner);
	printf("\n\n");
	fflush(stdout);
#endif


	// === Part 2 - Bound on F (the border part of the K-set E)
	expr* ub_border = get_ub_border_hourglass(hg_info, dim, D, base, n_b, K, ctx_isl, alpha_inner);


#if (VERBOSE > 1)
	printf("[Hourglass solve - END Border] ub_border = ");
	expr_print(ub_border);
	printf("\n\n");
	fflush(stdout);
#endif


	// === Part 3 - Sum upper bound bounds on E' and F, then get the lower bound on the IO
	
	// 3.a) Actual sum
	expr* sum_upp_bounds = expr_add(ub_inner, ub_border);

	// 3.b) Compute interference domain
	// Outputs of this step are "front_size" and "dom_inter"
	res_aux_comp_front_size* res_fr_size = solve_compute_front_size(tuple, D, K);
	expr *front_size = res_fr_size->front_size;
	isl_union_set* dom_inter = res_fr_size->dom_inter;
	free(res_fr_size);

	if(front_size == NULL)
		return NULL;

	// 3.c) Tranform sum_upp_bounds into lb
	lb_data* lb = compute_lower_bound_IO(D, dom_inter, sum_upp_bounds, alpha_inner, front_size);
	
	/* DEBUG
	printf("Compute_bound done!\n");
	expr_print(lb->bnd);
	printf("\n");
	fflush(stdout);
	//*/

	if(lb != NULL && lb->bnd != NULL)
		lb->bnd = expr_mul(lb->bnd, tuple->mul);

	return lb;
}


/* ==================== */


// We have identified a collection of paths that form a base of the space
//	Let's combine them to get a bound, from the Brascamp-Lieb theorem
lb_data* solve(const dfg * const g, const space_info * const tuple) {
	isl_set *D = tuple->D;
	int dim = tuple->dim;
	double** base = tuple->base;
	size_t n_b = tuple->n_bases;
	list_item *K = tuple->K;		// List of paths (the chosen combination)
	bool* small_dims = tuple->small_dims;
	expr* small_dims_volume = tuple->small_dims_volume;

	assert(n_b == dim); //make sure that the base spans whole dim

	int num_non_small_dims = 0;		// Counting the NOT small dimensions
	for (int i=0; i<dim; i++)
		if (! small_dims[i])
			num_non_small_dims++;

	// Hourglass detection - activated only if option enabled (by default it is)
	list_item* hg_info;
	if (_hourglass_detection) {
		// Hourglass detection insert point (special reasonning)
		hg_info = hourglass_path_detection_list_path(g, K);
	} else {
		hg_info = NULL;
	}

	if (hg_info!=NULL) {
		// Trigger the hourglass special reasonning
		//	Note: this is incompatible with the betas trick + the small dimensions
		//		=> betas needs to reimplement/rethink about the betas_denom computation
		//		(which is improvable anyway)
		//		=> Small dims just need to be implemented (an assert has been placed)
		assert(num_non_small_dims==dim);

		// Assert that only one hourglass was found. If not the case, implement that possibility
		printf("WARNING - selecting only the first hourglass\n");
		//assert(hg_info->next==NULL);

		// Switching now to the special hourglass reasonning...
		lb_data* lb_hourglass = solve_hourglass(tuple, hg_info->data);
		return lb_hourglass;
	}


	
	/* DEBUG (checking that small_dims is correct)
	printf("complexity.c :: solve - small_dims = [");
	for (int i=0; i<dim; i++) {
		if (small_dims[i])
			printf(" True");
		else
			printf(" False");
	}
	printf("]\n");
	//*/

	// Preprocessing - Small dimensions management: Adding an extra projection here
	subspace* k_small = NULL;
	bool is_k_small = (num_non_small_dims<dim);		// Presence of small dims
	if (num_non_small_dims<dim) {
		// Building the subspace k_small (corresponding to the small dimensions)
		double** kM_small = (double**) malloc(num_non_small_dims * sizeof(double*));
		for (int i=0; i<num_non_small_dims; i++) {
			kM_small[i] = (double*) malloc(dim * sizeof(double));
			for (int j=0; j<dim; j++)
				kM_small[i][j] = 0.0;
		}

		int num_line_kM_small = 0;
		for (int i=0; i<dim; i++) {
			if (!small_dims[i]) {		// Invert to get the value of the kernel
				kM_small[num_line_kM_small][i] = 1.0;
				num_line_kM_small++;
			}
		}
		assert(num_line_kM_small==num_non_small_dims);
		/* DEBUG
		printf("num_line_kM_small = %i\n", num_line_kM_small);
		printf("kM_small =\n");
		for(size_t i=0; i<num_line_kM_small; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", kM_small[i][j]);
			}
			printf(")\n");
		}
		//*/

		k_small = malloc(sizeof(subspace));
		k_small->M = kM_small;
		k_small->n = num_non_small_dims;
	}


	// === STEP 1 - solving the ILP to get the "s_i" (power coefficients)

	// We convert "K" into its list of subspace "subspace_K"
	list_item* subspace_K = NULL;
	int n_K = 0;	// Number of planes
	for (list_item* ptr_K = K; ptr_K != NULL; ptr_K = ptr_K->next ) {
		path* p = (path*) ptr_K->data;

		list_item* nlssK = (list_item*) malloc(sizeof(list_item));
		nlssK->data = &(p->k);
		nlssK->next = subspace_K;
		subspace_K = nlssK;

		n_K++;
	}

	// Additional path to count (for the si solving part)
	if (is_k_small) {		// Add k_small in first position if activated
		list_item* nlssK = (list_item*) malloc(sizeof(list_item));
		nlssK->data = k_small;
		nlssK->next = subspace_K;
		subspace_K = nlssK;

		n_K++;
	}

	// Let's go!
	isl_map* rel_for_context = ((path*) K->data)->rel;
	PipQuast* solution = solve_si_coefficients(n_b, dim, base, subspace_K, n_K, is_k_small, rel_for_context);
	assert(solution->condition == NULL); //no QUAST

	// We free "subspace K" (but not its content!)
	while (subspace_K!=NULL) {
		list_item* ln = subspace_K->next;
		free(subspace_K);
		subspace_K = ln;
	}

	// Free k_small here (extra projection not needed anymore)
	if (is_k_small) {
		free(k_small->M);
		free(k_small);

		// Correct back the number of paths
		n_K--;
	}

#if (VERBOSE > 1)
	printf("\nSolution:\n");
	pip_quast_print(stdout,solution,0);
#endif

	// === STEP 2 - Compute interference domain
	// Outputs of this step are "front_size" and "dom_inter"
	res_aux_comp_front_size* res_fr_size = solve_compute_front_size(tuple, D, K);

	expr *front_size = res_fr_size->front_size;
	isl_union_set* dom_inter = res_fr_size->dom_inter;
	free(res_fr_size);

	if(front_size == NULL)
		return NULL;


	// === STEP 3 - Compute the betas (actually, the 1/beta in the paper)
	int* betas_denom = solve_compute_betas_denom(n_K, K, D);


#if (VERBOSE > 1)
	printf("betas_denom: ");
	for(int i = 0; i<n_K; i++)
		printf("1/%d ", betas_denom[i]);
	printf("\n");
#endif



	// From all these infos, build the final bound
	lb_data* lb = compute_bound(D, 1, front_size, solution->list, dom_inter,
			betas_denom, is_k_small, small_dims_volume);
	if(lb != NULL && lb->bnd != NULL)
		lb->bnd = expr_mul(lb->bnd, tuple->mul);

#if (VERBOSE > 1)
	printf("=> Computed lower bound :\n");
	expr_print(lb->bnd);
	printf("\n");
#endif

	return lb;
}

