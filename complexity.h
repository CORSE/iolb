#ifndef COMPLEXITY_H
#define COMPLEXITY_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */


#include <math.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include <gmp.h>

#include <isl/val.h>
#include <isl/mat.h>
#include <isl/set.h>
#include <isl/map.h>
#include <isl/aff.h>
#include <isl/multi.h>
#include <isl/polynomial.h>
#include <isl/local_space.h>
#include <isl/constraint.h>

#include <barvinok/isl.h>

#include <piplib/piplibMP.h>

#include "utility.h"
#include "dfg.h"
#include "expr_utils.h"
#include "kpart_type.h"
#include "hourglass.h"


// Auxilliary linear algebra function
bool is_independent(isl_ctx *ctx, const double ** const mat, const size_t n_bases, const double * const v, const int dim);


// If v is in the base, return 0
// If v is linearly independent from the base, return 1
// if v is not independent from the base, return -1
int can_expand_base(isl_ctx *ctx, const double ** const base, const size_t n_bases, const double * const v, const int dim);



// Main function (on a space_info, that corresponds to a combination of paths that cover the whole space)
lb_data* solve(const dfg * const g, const space_info * const tuple);

#endif
