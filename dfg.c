/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "dfg.h"
#include <math.h>
#include <string.h>

int bmap_rev_is_single_valued(__isl_take isl_basic_map *bmap, void* v)
{
	if(isl_basic_map_is_single_valued(isl_basic_map_reverse(bmap)))
		return 0;
	return -1;
}

int simplify_union_rel(__isl_take isl_map *map, void* v_umap)
{
	isl_union_map** umap = v_umap;
	isl_map *map_coalesced = isl_map_coalesce(isl_map_copy(map));
	if(isl_map_foreach_basic_map(map_coalesced, bmap_rev_is_single_valued, NULL) < 0) {
		*umap = isl_union_map_union(isl_union_map_from_map(map), *umap);
		isl_map_free(map_coalesced);
	}
	else {
		*umap = isl_union_map_union(isl_union_map_from_map(map_coalesced), *umap);
		isl_map_free(map);
	}
	return 0;
}

static int create_edge(__isl_take isl_basic_map *map, void *v_g)
{
	dfg *g = ((dfg*)(v_g));
	//Don't create edge if the reverse relation is not single-valued
	isl_basic_map *reverse_map = isl_basic_map_reverse(isl_basic_map_copy(map));
	if(!isl_basic_map_is_single_valued(reverse_map))
	{
#if (VERBOSE > 1)
		printf("\nNo edge built for map:\n");
		PRINT_ISL(p_out, isl_printer_print_basic_map, map);
		PRINT_ISL(p_out, isl_printer_print_basic_map, reverse_map);
#endif
		isl_basic_map_free(reverse_map);
		isl_basic_map_free(map);
		return 0;
	}
	if(isl_basic_map_dim(map, isl_dim_in) == 0) {
		return 0;
		isl_basic_map_free(reverse_map);
		isl_basic_map_free(map);
	}
	isl_basic_map_free(reverse_map);
	//printf("\nDEBUG\n");
	//PRINT_ISL(p_out, isl_printer_print_map, isl_map_from_basic_map(isl_basic_map_copy(map)));

	//Create new edge
	edge *e = (edge*)malloc(sizeof(edge));
	e->rel = map;
	e->visited = false;
	e->is_reduction=false;
	e->rel_red = NULL;
	//assert(isl_map_has_tuple_name(map, isl_dim_in) == 1);
	//assert(isl_map_has_tuple_name(map, isl_dim_out) == 1);
	int src_id = -1, sink_id = -1;
	//sscanf(isl_basic_map_get_tuple_name(map, isl_dim_in), "S_%d", &src_id);
	//sscanf(isl_basic_map_get_tuple_name(map, isl_dim_out), "S_%d", &sink_id);
	const char* src_name = isl_basic_map_get_tuple_name(map, isl_dim_in);
	const char* sink_name = isl_basic_map_get_tuple_name(map, isl_dim_out);
	for(int i=0; i < g->n_nodes; i++) {
		if(src_name == isl_set_get_tuple_name(g->nodes[i].dom)) {
			src_id = i;
		}
		if(sink_name == isl_set_get_tuple_name(g->nodes[i].dom)) {
			sink_id = i;
		}
	}
	assert(src_id != -1 && sink_id != -1);
	e->source_idx = src_id;
	e->sink_idx = sink_id;
	//Add the edge to injective/bcast/small-bcast list_item
	list_item *t = (list_item*)calloc(1, sizeof(list_item));
	t->data = e;
	isl_map *temp_map;
	//Injection
	if(isl_map_is_injective(temp_map=isl_map_from_basic_map(isl_basic_map_copy(map))) && isl_basic_map_is_single_valued(map))
	{
		e->type = INJECTION;
		t->next = g->inj_list[src_id];
		g->inj_list[src_id] = t;
#if (VERBOSE > 0)
		printf("\nInjective edge %zu -> %zu:\n", e->source_idx, e->sink_idx);
		PRINT_ISL(p_out, isl_printer_print_map, isl_map_from_basic_map(isl_basic_map_copy(map)));
#endif
	}


	else //BCast
	{
		int dom_dim = find_basic_set_dim(isl_basic_map_domain(isl_basic_map_copy(map))); 		
		int range_dim = find_basic_set_dim(isl_basic_map_range(isl_basic_map_copy(map))); 		
		if(dom_dim == range_dim-1) //Bcast with kernel dim 1
		{
			e->type = BCAST;
			t->next = g->bcast_list[src_id];
			g->bcast_list[src_id] = t;
		}
		else
		{
			e->type = BCAST;
			t->next = g->small_bcast_list[src_id];
			g->bcast_list[src_id] = t;
		}
#if (VERBOSE > 0)
		printf("\nBroadcast edge %zu -> %zu\n", e->source_idx, e->sink_idx);
		PRINT_ISL(p_out, isl_printer_print_map, isl_map_from_basic_map(isl_basic_map_copy(map)));
#endif
	}

	//if this is a loop broacast, make sure the domain and range of the relation are disjoint
	//if(e->type != INJECTION && e->sink_idx == e->source_idx) {
	//	e->rel = isl_basic_map_subtract_range(isl_basic_map_copy(e->rel), isl_basic_map_domain(e->rel));
	//}

	isl_map_free(temp_map);

	return 0;
}

static int create_edges(__isl_take isl_map *map, void *g)
{
	int ret = isl_map_foreach_basic_map(map, create_edge, g);
	isl_map_free(map);
	return ret;
}

//static int count_map(isl_map *map, size_t *cnt)
//{
//	(*cnt) += isl_map_n_basic_map(map);
//	return 0;
//}

//Get the d-dimensional statement location in source code
int create_sched(isl_map *m, void *g_v)
{
	dfg *g = ((dfg*)(g_v));
	int stmt_id;
	assert(isl_map_has_tuple_name(m, isl_dim_in));
	sscanf(isl_map_get_tuple_name(m, isl_dim_in), "S_%d", &stmt_id);
	stmt_id += g->n_arrays;
	assert(stmt_id < g->n_nodes);
	int out_dim = isl_map_dim(m, isl_dim_out);
	int in_dim = isl_map_dim(m, isl_dim_in);

	g->nodes[stmt_id].sched = (int*)malloc(out_dim * sizeof(int));
	int depth = 0;
	isl_space *sp = isl_map_get_space(m);
	for(int i=0; i<out_dim; ++i)
	{
		isl_val *val = isl_map_plain_get_val_if_fixed(m, isl_dim_out, i);
		int elem;
		if(isl_val_is_nan(val) == 1)
		{
			elem = -1;
			assert(isl_dim_get_name(sp, isl_dim_in, depth) != NULL);
			++depth;
		}
		else
			elem = isl_val_get_d(val) + 1;
		g->nodes[stmt_id].sched[i] = elem;
		isl_val_free(val);
	}
	g->nodes[stmt_id].sched_size = out_dim;
	g->nodes[stmt_id].loop_depth = depth;
	assert(depth == in_dim);
	int dom_dim = g->nodes[stmt_id].dim;
	assert(depth == dom_dim || !(printf("Failed due to depth: %d; out_dim: %d\n", depth, dom_dim)));

#if (VERBOSE > 0)
	printf("\nSchedule for stmt %d: (", stmt_id);
	for(int i=0; i<out_dim; ++i)
	{
		printf("%d, ", g->nodes[stmt_id].sched[i]);
	}
	printf(")\n");
#endif
	isl_map_free(m);
	return 0;
}


// Auxilliary data structure to gather info from "get_small_dims_bset"
typedef struct {
	bool* small_dims;
	expr* small_dims_volume;
} small_dim_inference_info;

int get_small_dims_bset(isl_basic_set* bset, void* v_sdii) {
	// True output, will be modified
	small_dim_inference_info* sdii = (small_dim_inference_info*) (v_sdii);
	bool* small_dims = sdii->small_dims;
	expr* small_dims_volume = sdii->small_dims_volume;	// Currently at "1"

	// Getting the matrix of inequality constraints of bset
	isl_mat* mat_ineq = isl_basic_set_inequalities_matrix(bset,
		isl_dim_set, isl_dim_param, isl_dim_div, isl_dim_cst);

	// HEURISTIC:
	//	 => If they is a ""i <= Param"" with "Param" a small dimension,
	//		and ""i >= Cst" with Cst something non-negative
	//			then we register "i" as a small_dim (with associated value "param")

	int ndim = isl_basic_set_dim(bset, isl_dim_set);
	int nparam = isl_basic_set_dim(bset, isl_dim_param);
	int index_cst = ndim + nparam;

	assert(isl_basic_set_dim(bset, isl_dim_div)==0);

	// Preparation - get the list of parameter names of bset
	//	+ recognize them if they are registered as small param
	bool* small_params = (bool*) malloc(nparam * sizeof(bool)); 
	for (int i=0; i<nparam; i++) {
		const char* name_param = isl_basic_set_get_dim_name(bset, isl_dim_param, i);
		small_params[i] = is_small_dim(name_param);

		// Add it to the available symbols for expr construction
		expr_symbol(name_param);
	}


	/* DEBUG
	print_l_small_dims();
	printf("small_params = [");
	for (int i=0; i<nparam; i++) {
		if (small_params[i])
			printf(" True");
		else
			printf(" False");
	}
	printf(" ]\n");
	//*/

	// Checking if dimension "d" is a small dimensions
	for (int d=0; d<isl_basic_set_dim(bset, isl_dim_set); d++) {
		bool ub_small_dim_constraint = false;
		isl_val** ub_parcst = (isl_val**) malloc( (nparam+1) * sizeof(isl_val*));
		bool lb_small_dim_constraint = false;
		isl_val** lb_parcst = (isl_val**) malloc( (nparam+1) * sizeof(isl_val*));

		// Check all constraints of the matrix
		for (int i=0; i<isl_mat_rows(mat_ineq); i++) {
			
			bool is_small_constraint_on_dim_d = true;
			int is_coeff_on_d_negative = -1; 
			for (int j=0; j<ndim; j++) {
				isl_val* vij = isl_mat_get_element_val(mat_ineq, i, j);

				if (j==d) {
					if (isl_val_is_one(vij)) {
						is_coeff_on_d_negative = 0;
					} else if (isl_val_is_negone(vij)) {
						is_coeff_on_d_negative = 1;
					} else {
						is_small_constraint_on_dim_d = false;
					}
				} else {
					if (!isl_val_is_zero(vij)) {
						is_small_constraint_on_dim_d = false;
					}
				}

				isl_val_free(vij);
			}

			// If constraint does not match
			if (is_coeff_on_d_negative==(-1))	// Happens when all coeff are "0"
				is_small_constraint_on_dim_d = false;

			if (!is_small_constraint_on_dim_d) { // Index part did not match
				continue;
			}
			
			// Check the parameters
			//	=> Can have severals if lin coeff was negative
			//		but all their "small_params[dim_param]" should be true
			//  => If lin coeff was positive, should not have any parameter here !
			for (int j=0; j<nparam; j++) {
				isl_val* vparam = isl_mat_get_element_val(mat_ineq, i, j+ndim);

				if (is_coeff_on_d_negative==0) {
					// Linear coefficient was positive
					if (! isl_val_is_zero(vparam))
						is_small_constraint_on_dim_d = false;
				} else {
					assert(is_coeff_on_d_negative==1);	// Linear coefficient was negative
					
					if (!isl_val_is_zero(vparam)) {
						// If the param is there, is it at least a small param?
						if (! small_params[j]) {
							is_small_constraint_on_dim_d = false;
						}
					}
				}

				isl_val_free(vparam);
			}

			if (!is_small_constraint_on_dim_d) { // Param part did not match
				continue;
			}

			// Check the constant (if index coeff positif, check its negativity: "i - C >= 0")
			if (is_coeff_on_d_negative==0) {
				isl_val* vconst = isl_mat_get_element_val(mat_ineq, i, index_cst);
				if (isl_val_is_pos(vconst)) {
					is_small_constraint_on_dim_d = false;
				}
				isl_val_free(vconst);
			}

			// Constraint match
			if (is_small_constraint_on_dim_d) {
				if (is_coeff_on_d_negative==0) {
					lb_small_dim_constraint = true;

					// Filling lb_parcst
					for (int j=0; j<nparam; j++)
						lb_parcst[j] = isl_mat_get_element_val(mat_ineq, i, ndim + j);
					lb_parcst[nparam] = isl_mat_get_element_val(mat_ineq, i, index_cst);

				} else if (is_coeff_on_d_negative==1) {
					ub_small_dim_constraint = true;

					// Filling ub_parcst
					for (int j=0; j<nparam; j++)
						ub_parcst[j] = isl_mat_get_element_val(mat_ineq, i, ndim + j);
					ub_parcst[nparam] = isl_mat_get_element_val(mat_ineq, i, index_cst);
				} else {
					// Issue on the value of "is_coeff_on_d_negative"
					assert(false);
				}
			}
		} // Finished checking all rows of the matrix

		// Small dim only if lb and ub which matches the pattern
		small_dims[d] = lb_small_dim_constraint && ub_small_dim_constraint;

		// NOTE: if several ub constraints with small params, only consider the last one
		//		=> TODO: improve the selection (by evaluating the param values and by taking
		//			the smallest one?)


		// Small dims volume management
		if (small_dims[d]) {
			// Still same assumption on the "rectangularity" of the small dims

			// Upper bound
			expr* ub_val = expr_new("0");
			for (int i=0; i<nparam; i++) {
				long n = isl_val_get_num_si(ub_parcst[i]);
				long d = isl_val_get_den_si(ub_parcst[i]);

				const char* name_param = isl_basic_set_get_dim_name(bset, isl_dim_param, i);
				expr* coeff = expr_new_frac(n, d);
				expr* param = expr_new(name_param);
				expr* contrib = expr_mul(coeff, param);

				expr* o_ub_val = ub_val;
				ub_val = expr_add(ub_val, contrib);

				expr_free(coeff);
				expr_free(param);
				expr_free(contrib);
				expr_free(o_ub_val);
			}
			long n_cst_ub = isl_val_get_num_si(ub_parcst[nparam]);
			long d_cst_ub = isl_val_get_den_si(ub_parcst[nparam]);

			expr* coeff_cst_ub = expr_new_frac(n_cst_ub, d_cst_ub);
			expr* temp_ub = expr_add(ub_val, coeff_cst_ub);
			expr_free(coeff_cst_ub);

			expr* exp_one = expr_new("1");
			expr* o_ub_val_cst_ub = ub_val;
			ub_val = expr_add(temp_ub, exp_one);	// Upper bound is a <=, not a <
			expr_free(temp_ub);
			expr_free(exp_one);
			expr_free(o_ub_val_cst_ub);

			
			/* DEBUG
			printf("ub_val =");
			expr_print(ub_val);
			printf("\n");
			//*/

			// Lower bound
			expr* lb_val = expr_new("0");
			for (int i=0; i<nparam; i++) {
				long n = isl_val_get_num_si(lb_parcst[i]);
				long d = isl_val_get_den_si(lb_parcst[i]);

				const char* name_param = isl_basic_set_get_dim_name(bset, isl_dim_param, i);
				expr* coeff = expr_new_frac(n, d);
				expr* param = expr_new(name_param);
				expr* contrib = expr_mul(coeff, param);

				expr* o_lb_val = lb_val;
				lb_val = expr_add(lb_val, contrib);

				expr_free(coeff);
				expr_free(param);
				expr_free(contrib);
				expr_free(o_lb_val);
			}
			long n_cst_lb = isl_val_get_num_si(lb_parcst[nparam]);
			long d_cst_lb = isl_val_get_den_si(lb_parcst[nparam]);

			expr* coeff_cst_lb = expr_new_frac(n_cst_lb, d_cst_lb);
			expr* o_lb_val_cst_lb = lb_val;

			lb_val = expr_add(lb_val, coeff_cst_lb);
			expr_free(coeff_cst_lb);
			expr_free(o_lb_val_cst_lb);

			/* DEBUG
			printf("lb_val =");
			expr_print(ub_val);
			printf("\n");
			//*/

			// Conclude on that dimension
			expr* new_contrib = expr_sub(ub_val, lb_val);
			expr* o_small_dims_volume = small_dims_volume;
			small_dims_volume = expr_mul(small_dims_volume, new_contrib);

			// Free temp expression variables
			expr_free(ub_val);
			expr_free(lb_val);
			expr_free(new_contrib);
			expr_free(o_small_dims_volume);
		}


		/* DEBUG
		printf("small_dims_volume (get_small_dims_bset) = ");
		expr_print(small_dims_volume);
		printf("\n");
		//*/


		// Free temporary data struct
		if (lb_small_dim_constraint) {
			for (int i=0; i<nparam+1; i++) {
				isl_val_free(lb_parcst[i]);
			}
		}
		if (ub_small_dim_constraint) {
			for (int i=0; i<nparam+1; i++) {
				isl_val_free(ub_parcst[i]);
			}
		}
		free(ub_parcst);
		free(lb_parcst);
	}

	// Note: can potentially be improved by checking if some dimensions have
	//	a constant size (or is bound by an equality, i.e. domain is not full dimensional)
	sdii->small_dims_volume = small_dims_volume;

	// Note: need to be a "isl_take" on bset
	isl_basic_set_free(bset);
	free(small_params);

	return 0;
}


// Setup the small dimensions
small_dim_inference_info* get_small_dims(isl_set* domain, int dim) {
	bool* small_dims = (bool*) malloc(dim * sizeof(bool));
	for (int i=0; i<dim; i++)
		small_dims[i] = false;

	small_dim_inference_info* sdii = malloc(sizeof(small_dim_inference_info));
	sdii->small_dims = small_dims;
	sdii->small_dims_volume = expr_new("1");

	// Case where there is no small dimensions defined
	if (! _small_dim) {
		return sdii;
	}

	// If we have small dimensions, we match the bounds of each index

	assert(isl_set_n_basic_set(domain)==1);		// Assume only one basic set
	// Should be called only once - modify small_dims
	isl_set_foreach_basic_set(isl_set_copy(domain), get_small_dims_bset, sdii);

	/* DEBUG
	printf("domain = ");
	PRINT_ISL(p_out, isl_printer_print_set, domain);
	printf("\n");
	printf("small_dims = [");
	for (int i=0; i<dim; i++) {
		if (sdii->small_dims[i])
			printf(" True");
		else
			printf(" False");
	}
	printf(" ]\n");
	//*/

	/* DEBUG
	printf("small_dims_volume (get_small_dims) = ");
	expr_print(sdii->small_dims_volume);
	printf("\n");
	//*/

	return sdii;
}


//returns input cardinality
expr* create_dfg(struct pet_scop * const scop, dfg * const g, int verbose_lvl)
{

#if (VERBOSE > 0)
	if (verbose_lvl) {
		printf("======== START of the DFG construction ========\n\n");
	}
#endif

	size_t n_stmts = scop->n_stmt;
	size_t n_arrays = scop->n_array;
	size_t n_nodes = n_stmts + n_arrays;

	node *nodes = g->nodes = (node*)malloc(n_nodes * sizeof(node));
	g->inj_list = (list_item**)calloc(n_nodes, sizeof(list_item*)); //malloc and initialize to NULL
	g->bcast_list = (list_item**)calloc(n_nodes, sizeof(list_item*));
	g->small_bcast_list = (list_item**)calloc(n_nodes, sizeof(list_item*));

	int j=0;
	for(int i=0; i<n_arrays; i++)
	{
		int d = isl_set_dim(scop->arrays[i]->extent, isl_dim_set); 
		if(d == 0)
			continue;
		isl_set* domain = isl_set_copy(scop->arrays[i]->extent);

		small_dim_inference_info* sdii = get_small_dims(domain, d);

		bool* small_dims = sdii->small_dims;
		expr* small_dims_volume = sdii->small_dims_volume;
		free(sdii);

		nodes[j].dom = domain;
		nodes[j].dim = d;
		nodes[j].small_dims = small_dims;
		nodes[j].small_dims_volume = small_dims_volume;
		nodes[j].red_chain_bcst_rel = NULL;


#if (VERBOSE > 0)
		if (verbose_lvl) {
			printf("Array %d (dim %d):\n", j, nodes[j].dim);
			PRINT_ISL(p_out, isl_printer_print_set, nodes[j].dom);
			printf("\n");
		}
#endif
		j++;
	}
	
	n_arrays = j;
	n_nodes = n_stmts + n_arrays;

	//Collect the domain and its dimension for each stmt
	for(int i=0; i<n_stmts; ++i)
	{
		isl_set* domain = isl_set_copy(scop->stmts[i]->domain);
		int dim = isl_set_dim(domain, isl_dim_set);

		// Collect the small dims of a statement (from dom & dim)

		small_dim_inference_info* sdii = get_small_dims(domain, dim);
		bool* small_dims = sdii->small_dims;
		expr* small_dims_volume = sdii->small_dims_volume;
		free(sdii);

		nodes[i+n_arrays].dom = domain;
		//nodes[i].dim = find_set_dim(isl_set_copy(nodes[i].dom));
		nodes[i+n_arrays].dim = dim;
		nodes[i+n_arrays].small_dims = small_dims;
		nodes[i+n_arrays].small_dims_volume = small_dims_volume;
		nodes[i+n_arrays].red_chain_bcst_rel = NULL;

#ifndef NDEBUG
		assert(isl_set_has_tuple_name(nodes[i+n_arrays].dom) == 1);
		int stmt_id = -1;
		sscanf(isl_set_get_tuple_name(nodes[i+n_arrays].dom), "S_%d", &stmt_id);
		assert(stmt_id == i); //This is the standard format from pet.
#endif
#if (VERBOSE > 0)
		if (verbose_lvl) {
			printf("Stmt %d (dim %d):\n", i, nodes[i+n_arrays].dim);
			PRINT_ISL(p_out, isl_printer_print_set, nodes[i+n_arrays].dom);
			printf("\n");
		}
#endif
	}

	g->n_nodes = n_nodes;
	g->n_arrays = n_arrays;

	//Collect read and write access functions
	isl_union_map *reads = pet_scop_collect_may_reads(scop);
	isl_union_map *writes = pet_scop_collect_must_writes(scop);
	isl_union_map *sched = pet_scop_collect_schedule(scop);
	isl_union_map *may_sources = isl_union_map_empty(isl_union_map_get_space(reads));
	isl_union_map *deps_union;
	isl_union_map *must_no_source; //input variables
	//Compute flow dependences
	isl_union_map_compute_flow(reads, writes, may_sources, isl_union_map_copy(sched), &deps_union, NULL, &must_no_source, NULL);
	must_no_source = isl_union_map_reverse(must_no_source);
	//must_no_source = isl_union_map_coalesce(must_no_source);
	
	isl_union_map *deps_union_simplif = isl_union_map_empty(isl_union_map_get_space(deps_union));
	isl_union_map_foreach_map(deps_union, simplify_union_rel, &deps_union_simplif);
	isl_union_map_free(deps_union);
	deps_union = deps_union_simplif;

#if (VERBOSE > 1)
	if (verbose_lvl) {
		printf("\nFlow dependences:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, deps_union);
		printf("\nInput arrays dependencies:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, must_no_source);
	}
#endif
	//Create injective and bcast edges
	isl_union_map_foreach_map(deps_union, create_edges, g);
	isl_union_map_foreach_map(must_no_source, create_edges, g);

	//concatenate bcast and smallbcast
	for(int i=0; i<n_nodes; i++) {
		list_item *tl_bcast = g->bcast_list[i];
		if(tl_bcast == NULL) {
			g->bcast_list[i] = g->small_bcast_list[i];
			continue;
		}
		for(; tl_bcast->next != NULL; tl_bcast = tl_bcast->next);
		tl_bcast->next = g->small_bcast_list[i]; 
	}
	g->all_edges = isl_union_map_union(deps_union, isl_union_map_copy(must_no_source));

	//Collect schedule
	isl_union_map_foreach_map(sched, create_sched, g);
	isl_union_map_free(sched);

	//array initializations are scheduled before everything
	const size_t sched_size = g->nodes[n_arrays].sched_size;
	for(int i=0; i<n_arrays; i++) {
		g->nodes[i].sched_size = sched_size;
		g->nodes[i].sched = calloc(sched_size,  sizeof(int));
		g->nodes[i].loop_depth = 0;
	}

	for(int i=0; i<n_nodes; i++)
		g->nodes[i].n_iterations = expr_one();

	//compute input size
	isl_union_set *inputs = isl_union_map_domain(must_no_source);
	g->inputs = inputs;

	expr* inputs_card = get_union_set_cardinality(isl_union_set_copy(inputs), 0, NULL);

#if (VERBOSE > 1)
	if (verbose_lvl) {
		//if(inputs_card != NULL) {
		printf("\nInput set:\n");
		PRINT_ISL(p_out, isl_printer_print_union_set, inputs);
		printf("\nInput cardinality: ");
		expr_print(inputs_card);
		printf("\n");
		printf("@@@ ");
		expr_print(expr_simplify_expr(expr_params_equal(inputs_card)));
		printf("\n\n");
		//}
	}
#endif

#if (VERBOSE > 0)
	if (verbose_lvl) {
		printf("======== END of the DFG construction ========\n\n");
	}
#endif

	return inputs_card;
}

//Compute set of nodes reachable from each node through injective paths
// At the end: bitmap[i][i'] == 1 <=> node i' is reachable from node i
//	Careful: i' is stored in bits (=> use add/shift to access it)
void compute_reachable_nodes(const dfg * const g, const size_t count, bits (* const bitmap)[count])
{
	const size_t n_nodes = g->n_nodes;

	//For each injective edge (u,v), set the bit bit[u][v]=1
	// bitmap = transition table of the graph (and output of the algo)
	for(size_t i=0; i<n_nodes; ++i)
	{
		const list_item * curr = g->inj_list[i];
		while(curr != NULL)
		{
			size_t target = ((edge*)(curr->data))->sink_idx;
			bitmap[i][target/BITS_SIZE] |= (1 << (target%BITS_SIZE));
			curr = curr->next;
		}
	}

	//Iterate for (at most) n_nodes+1 times
	for(size_t t=0; t<n_nodes+1; ++t)
	{
		for(size_t i=0; i<n_nodes; ++i)
		{
			const list_item * curr = g->inj_list[i];
			while(curr != NULL)
			{
				size_t target = ((edge*)(curr->data))->sink_idx;
				//For injective-edge (u,v), do bitwise-or of the bit-vector of node u and
				//node v.
				//After 't' iterations, for all the injective paths u->w with length at
				//least 't', bit[u][w] will be set to 1
				for(size_t j=0; j<count; ++j)
				{
					bitmap[i][j] |= bitmap[target][j];
				}
				curr = curr->next;
			}
		}
	}
}


// Auxilliary function of "remove_outer_dim"
list_item* new_edges(size_t idx, int param_pos, const list_item* const edges, isl_union_map **all_edges, const dfg* const g)
{
	list_item *new_edges = NULL;
	for(const list_item *l = edges; l != NULL; l = l->next) {
		edge* e = l->data;
		int sink = e->sink_idx;
		
		//Is this too brutal??
		if(g->nodes[idx].sched[0] != g->nodes[sink].sched[0])
			continue;

		isl_basic_map *new_rel = isl_basic_map_copy(e->rel);
		if(param_pos != -1) {
			isl_constraint *cst = isl_equality_alloc(isl_basic_map_get_local_space(new_rel));
			cst = isl_constraint_set_coefficient_si(cst, isl_dim_in, 0, -1);
			cst = isl_constraint_set_coefficient_si(cst, isl_dim_out, 0, 1);
			new_rel = isl_basic_map_add_constraint(new_rel, cst);
			assert(new_rel != NULL);
			if(isl_basic_map_is_empty(new_rel)) {
				isl_basic_map_free(new_rel);
				new_rel = isl_basic_map_copy(e->rel);
				char new_name[64];
				sprintf(new_name, "%s_X", isl_basic_map_get_tuple_name(new_rel, isl_dim_in));
				new_rel = isl_basic_map_set_tuple_name(new_rel, isl_dim_in, new_name);
			}
			new_rel = isl_basic_map_project_out(new_rel, isl_dim_in, 0, 1);
			new_rel = isl_basic_map_project_out(new_rel, isl_dim_out, 0, 1);
		}

		edge* new_e = malloc(sizeof(edge));
		*new_e = *e;
		new_e->rel = new_rel;
		list_item* new_item = malloc(sizeof(list_item));
		new_item->next = new_edges;
		new_item->data = new_e;
		new_edges = new_item;
		*all_edges = isl_union_map_union(*all_edges, isl_union_map_from_basic_map(isl_basic_map_copy(e->rel)));
	}
	return new_edges;
}

dfg* remove_outer_dim(const dfg* const g)
{
	size_t n_arrays = g->n_arrays;
	size_t n_nodes = g->n_nodes;

	node *nodes = (node*)malloc(n_nodes * sizeof(node));
	list_item** inj_list = calloc(n_nodes, sizeof(list_item*)); 
	list_item** bcast_list = calloc(n_nodes, sizeof(list_item*));

	isl_union_map *all_edges = isl_union_map_empty(isl_union_map_get_space(g->all_edges));

	for(size_t i = 0; i < n_nodes; i++) {
		node *v = &nodes[i];
		
		v->dom = isl_set_copy(g->nodes[i].dom);
		v->dim = g->nodes[i].dim;
		v->sched = g->nodes[i].sched + 1;
		v->sched_size = g->nodes[i].sched_size - 1;
		v->loop_depth = g->nodes[i].loop_depth;
		v->n_iterations = g->nodes[i].n_iterations;

		int curr_sched = g->nodes[i].sched[0];
		if(curr_sched != -1) { //not removing a loop parameter
			inj_list[i] = new_edges(i, -1, g->inj_list[i], &all_edges, g);
			bcast_list[i] = new_edges(i, -1, g->bcast_list[i], &all_edges, g);
		}
		else {
			const int depth = v->loop_depth;
			const int full_depth = isl_set_n_dim(v->dom);
			assert(depth > 0);
			v->loop_depth--;

			v->dom = isl_set_project_out(v->dom, isl_dim_set, 0, 1);

			inj_list[i] = new_edges(i, full_depth-depth, g->inj_list[i], &all_edges, g);
			bcast_list[i] = new_edges(i, full_depth-depth, g->bcast_list[i], &all_edges, g);

			bounds* dim_bounds = malloc(v->dim*sizeof(bounds));
		   	get_dim_bounds(isl_set_copy(g->nodes[i].dom), v->dim, dim_bounds);

			v->dim--;

			// Update "small_dims" (by removing the last cell)
			bool* n_small_dims = (bool*) malloc(v->dim * sizeof(bool));
			for (int k=0; k<v->dim; k++)
				n_small_dims[k] = v->small_dims[k];
			free(v->small_dims);
			v->small_dims = n_small_dims;

			v->n_iterations = expr_mul(v->n_iterations, expr_sub(dim_bounds[0].ub, dim_bounds[0].lb));
		}
	}

	dfg *new_g = malloc(sizeof(dfg));
	new_g->n_nodes = n_nodes;
	new_g->n_arrays = n_arrays;
	new_g->nodes = nodes;
	new_g->inj_list = inj_list;
	new_g->bcast_list = bcast_list;
	new_g->all_edges = all_edges;
	new_g->small_bcast_list = NULL; //not used
	new_g->inputs = NULL;
	return new_g;
}

void print_dfg(const dfg* const g) 
{
	for(int i=0; i<g->n_nodes; i++) {
		printf("Node #%d:\n------\n\n", i);
		printf("Domain (dim = %d):\n", g->nodes[i].dim);
		PRINT_ISL(p_out, isl_printer_print_set, g->nodes[i].dom);
		printf("Small dims: [");
		for (int k=0; k<g->nodes[i].dim; k++) {
			if (g->nodes[i].small_dims[k]) {
				printf(" True");
			} else {
				printf(" False");
			}
		}
		printf(" ]\n");
		printf("Small dims volume = ");
		expr_print(g->nodes[i].small_dims_volume);
		printf("\n");

		printf("\n\nInjective edges:\n");
		for(list_item *l = g->inj_list[i]; l != NULL; l = l->next) {
			edge* e = l->data;
			if (e->is_reduction) {
				printf("( %zu -> %zu ) [Reduction]\n", e->source_idx, e->sink_idx);
			} else {
				printf("( %zu -> %zu )\n", e->source_idx, e->sink_idx);
			}
			isl_map* m = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
			PRINT_ISL(p_out, isl_printer_print_map, m);
			isl_map_free(m);
		}
		printf("\n\nBroadcast edges:\n");
		for(list_item *l = g->bcast_list[i]; l != NULL; l = l->next) {
			edge* e = l->data;
			if (e->is_reduction) {
				printf("( %zu -> %zu ) [Reduction]\n", e->source_idx, e->sink_idx);
			} else {
				printf("( %zu -> %zu )\n", e->source_idx, e->sink_idx);
			}
			isl_map* m = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
			PRINT_ISL(p_out, isl_printer_print_map, m);
			isl_map_free(m);
		}
		printf("\n\nSchedule:\n(");
		for(int j=0; j<g->nodes[i].sched_size; j++) {
			printf("%d, ", g->nodes[i].sched[j]);
		}
		printf(")\n");
		printf("Loop depth: %d\n\n", g->nodes[i].loop_depth);
		printf("#Iterations: ");
		expr_print(g->nodes[i].n_iterations);
		printf("\n\n");
	}
}

void dfg_to_dot(const dfg* const g, FILE* f)
{
	fprintf(f, "digraph {\n");

	for(int i=0; i<g->n_arrays; i++) {
		fprintf(f, "%s [color=gray];\n", isl_set_get_tuple_name(g->nodes[i].dom));
	}
	for(int i=g->n_arrays; i<g->n_nodes; i++) {
		fprintf(f, "%s;\n", isl_set_get_tuple_name(g->nodes[i].dom));
	}
	for(int i=0; i<g->n_nodes; i++) {
		for(list_item *l = g->inj_list[i]; l != NULL; l = l->next) {
			edge* e = l->data;
			isl_printer* ptr = isl_printer_to_str(isl_basic_map_get_ctx(e->rel));
			isl_map* m = isl_map_from_basic_map(e->rel);
			isl_printer_print_map(ptr, m);
			char* str = isl_printer_get_str(ptr);
			str = strchr(str, '{') + 1;
			char* str1 = strchr(str, '[') + 1;
			char* end = strchr(str1, ']');
			*end = '\0';
			char* str2 = strchr(end+1, '[') + 1;
			end = strchr(str2, ']');
			*end = '\0';
			isl_map_free(m);
			fprintf(f, "%s -> %s [label=\"%s -> %s\"];\n", isl_basic_map_get_tuple_name(e->rel, isl_dim_in),
					isl_basic_map_get_tuple_name(e->rel, isl_dim_out),
					str1, str2);
		}
		for(list_item *l = g->bcast_list[i]; l != NULL; l = l->next) {
			edge* e = l->data;
			isl_printer* ptr = isl_printer_to_str(isl_basic_map_get_ctx(e->rel));
			isl_map* m = isl_map_from_basic_map(e->rel);
			isl_printer_print_map(ptr, m);
			char* str = isl_printer_get_str(ptr);
			str = strchr(str, '{') + 1;
			char* str1 = strchr(str, '[') + 1;
			char* end = strchr(str1, ']');
			*end = '\0';
			char* str2 = strchr(end+1, '[') + 1;
			end = strchr(str2, ']');
			*end = '\0';
			isl_map_free(m);
			char* str_red = "";
			if (e->is_reduction) {
				str_red = ", RED";
			}
			fprintf(f, "%s -> %s [label=\"%s -> %s\"%s];\n", isl_basic_map_get_tuple_name(e->rel, isl_dim_in),
					isl_basic_map_get_tuple_name(e->rel, isl_dim_out),
					str1, str2, str_red);
		}
	}
	fprintf(f, "}\n");
}


// =================================

// Alternate version of compute_reachable node (reachability through inj edges)
//	which is much easier to be debugged (and did correct one of my bug)
bool** compute_reachable_nodes_bool(const dfg * const g) {

	// Initialisation
	const size_t n_nodes = g->n_nodes;
	bool** matrix = (bool**) malloc(n_nodes * sizeof(bool*));
	for (size_t i=0; i<n_nodes; i++) {
		matrix[i] = (bool*) malloc(n_nodes * sizeof(bool));
		for (size_t j=0; j<n_nodes; j++)
			if (i==j) {
				matrix[i][j] = true;
			} else {
				matrix[i][j] = false;
			}
	}

	// For each injective edge (u,v), set matrix[u][v]=1
	for(size_t i=0; i<n_nodes; ++i) {
		const list_item * curr = g->inj_list[i];
		while(curr != NULL) {
			size_t target = ((edge*)(curr->data))->sink_idx;
			matrix[i][target] = true;
			curr = curr->next;
		}
	}

	// Iterate for (at most) n_nodes+1 times
	for(size_t t=0; t<n_nodes+1; ++t) {
		for(size_t i=0; i<n_nodes; ++i) {
			const list_item * curr = g->inj_list[i];
			while(curr != NULL) {
				size_t target = ((edge*)(curr->data))->sink_idx;
				
				// We have an injective edge (i ---> target)
				//	=> Update things accessible by i
				for (size_t j=0; j<n_nodes; j++) {
					// If no path from i to j was discovered, now one exists going through target
					matrix[i][j] = matrix[i][j] || matrix[target][j];
				}
				curr = curr->next;
			}
		}
	}
	return matrix;
}


// Find the first injective relation between two nodes
//	(note: heuristic to avoid enumerating all the possibilities,
//			might not be the best in some convoluted cases)
isl_basic_map* get_injective_relation(const dfg * const g, const size_t node_src, const size_t node_dst) {

	// Special case where node_src = node_dst / return identity:
	if (node_src == node_dst) {
		isl_space* space = isl_set_get_space(g->nodes[node_src].dom);
		//printf("num in_dim = %d\n", isl_space_dim(space, isl_dim_in));
		//printf("num out_dim = %d\n", isl_space_dim(space, isl_dim_out));

		// This space only have out dim => we need to replicate them to the in dims
		space = isl_space_map_from_set(space);
		isl_basic_map* id_relation = isl_basic_map_identity(space);

		// DEBUG
		//printf("space id_relation = ");
		//PRINT_ISL(p_out, isl_printer_print_space, space);
		//printf("result (id_relation) = ");
		//PRINT_ISL(p_out, isl_printer_print_basic_map, id_relation);
		//fflush(stdout);

		return id_relation;
	}


	// Allocation of the array keeping the smallest relation from node_src to "i"
	//		Smallest in term of number of edges + id of the node it comes from
	isl_basic_map** arr_rel = (isl_basic_map**) malloc(g->n_nodes * sizeof(isl_basic_map**));
	for (int i=0; i<g->n_nodes; i++) {
		arr_rel[i] = NULL;
	}

	// DEBUG
	//printf("DEBUG - PING BEFORE FIRST LOOP\n");
	//fflush(stdout);

	// Initialisation with the edges from node_src
	list_item* ptr_inj_edge = g->inj_list[node_src];
	while (ptr_inj_edge!=NULL) {
		edge* e_inj = (edge*) ptr_inj_edge->data;
		// Approx: if several injective edges between node_src and i, the last one win

		assert(e_inj->source_idx==node_src);
		// Note: always work with copies
		arr_rel[e_inj->sink_idx] = isl_basic_map_copy(e_inj->rel);

		ptr_inj_edge = ptr_inj_edge->next;
	}

	// DEBUG
	//printf("DEBUG - PING MID INJECTION\n");
	//fflush(stdout);


	// If after g->n_nodes loops node_dst is still not reached, there is no destination
	int num_iter = 0;
	while ((num_iter < g->n_nodes) && arr_rel[node_dst]==NULL) {
		
		// DEBUG
		//printf("DEBUG - dfg::get_injective_relation - start of loop\n");
		//fflush(stdout);

		for (int i=0; i<g->n_nodes; i++) {
			if (arr_rel[i]==NULL) {
				continue;
			}

			// DEBUG
			//printf("\tCheck node %d...\n", i);
			//fflush(stdout);

			ptr_inj_edge = g->inj_list[i];
			while (ptr_inj_edge!=NULL) {
				edge* e_inj = (edge*) ptr_inj_edge->data;

				// If a connection to a new node is discovered
				if ( arr_rel[e_inj->sink_idx]==NULL ) {
					// Note: always work with copies
					isl_basic_map* rel_edge = isl_basic_map_copy(e_inj->rel);
					// Note: isl_basic_map_apply_range eats its arguments
					isl_basic_map* rel_dst = isl_basic_map_apply_range(
							isl_basic_map_copy(arr_rel[e_inj->source_idx]), rel_edge
						);
					arr_rel[e_inj->sink_idx] = rel_dst;
				}

				// Iterate
				ptr_inj_edge = ptr_inj_edge->next;
			}
		}
		num_iter++;
	}


	/* DEBUG
	printf("DEBUG - PING END INJECTION (node_src = %lu | node_dst = %lu)\n", node_src, node_dst);
	for (int i=0; i<g->n_nodes; i++) {
		printf("arr_rel[%d] = ", i);
		if (arr_rel[i] == NULL) {
			printf("NULL\n");
		} else {
			PRINT_ISL(p_out, isl_printer_print_basic_map, arr_rel[i]);
		}
	}
	fflush(stdout);
	//*/


	// Return the (potentially NULL) result
	isl_basic_map* result = arr_rel[node_dst];

	// Clean-up
	for (int i=0; i<g->n_nodes; i++) {
		if ((arr_rel[i]!=NULL) && (i!=node_dst)) {
			isl_basic_map_free(arr_rel[i]);
		}
	}
	free(arr_rel);
	
	return result;
}


// Find the first reduction relation looping on a node and return its "rel_red"
// WARNING: do not return a fresh copy !
// Remark: there should be only 1 reduction relation max per node (no abiguity here)
isl_basic_map* get_reduction_relation(const dfg * const g, const size_t node_red) {
	return g->nodes[node_red].red_chain_bcst_rel;

	// OLD VERSION (for reduction only) - Check the broadcast edges to census the reduction edges
	/*list_item* ptr_bcast_edge = g->bcast_list[node_red];
	while (ptr_bcast_edge!=NULL) {
		edge* e_bcast = (edge*) ptr_bcast_edge->data;
		if (e_bcast->is_reduction) {
			assert(e_bcast->source_idx == e_bcast->sink_idx);
			assert(e_bcast->source_idx==node_red);

			// Is it the right reduction edge we are looking for?
			return e_bcast->rel_red;
		}
		ptr_bcast_edge = ptr_bcast_edge->next;
	}

	return NULL; */
}

