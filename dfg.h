#ifndef DFG_H
#define DFG_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <pet.h>
//#include <isl/dim.h>
#include <isl/set.h>
#include <isl/union_map.h>
#include <isl/map.h>
#include <isl/flow.h>
#include <isl/space.h>
#include <isl/vec.h>
#include <isl/mat.h>
#include <isl/aff.h>
#include "expr_utils.h"

typedef struct
{
	isl_set *dom; //Domain
	int dim; //Dimension of the domain
	int *sched; //static schedule vector
	int sched_size; //size of array sched
	int loop_depth; //Loop depth of the statement
	expr* n_iterations; //Multiplication factor when outer dimensions have been parametrized
	bool* small_dims;   // Is a dimension a small dims (array of size "dim")
	expr* small_dims_volume;

    isl_basic_map* red_chain_bcst_rel;  // Filled by "reduction::reduction_detection" ,
                                        //   detection of "reduction-like" dep chains
} node;

typedef enum {INJECTION, BCAST} edge_type;

typedef struct
{
	size_t source_idx;
	size_t sink_idx;
	isl_basic_map *rel;
	edge_type type;
    bool is_reduction;
    isl_basic_map *rel_red; // NULL by default, value only if this is a reduction
                            // Contains map (i_red -> last elem of the accumulation)
	bool visited;
} edge;

typedef struct
{
	node *nodes;
	size_t n_nodes;
	list_item **inj_list; //list_item of injective edges for each node
	list_item **bcast_list; //list_item of bcast edges
	list_item **small_bcast_list; //list_item of bcast edges
	isl_union_map *all_edges; //Set of all flow edges
	size_t n_arrays;
	isl_union_set *inputs;
} dfg;

expr* create_dfg(struct pet_scop * const, dfg * const g, int verbose_lvl);

// Note: reachable nodes only through injective paths
void compute_reachable_nodes(const dfg * const g, const size_t count, bits (* const bitmap)[count]);

void print_dfg(const dfg* const);
dfg* remove_outer_dim(const dfg* const g);
void dfg_to_dot(const dfg* const, FILE* f);


// Alternate version of compute_reachable node (reachability through inj edges)
//  which is much easier to be debugged
bool** compute_reachable_nodes_bool(const dfg * const g);


// Find the first injective relation between two nodes
isl_basic_map* get_injective_relation(const dfg * const g, const size_t node_src, const size_t node_dst);

// Find the first reduction relation looping on a node
isl_basic_map* get_reduction_relation(const dfg * const g, const size_t node_red);


#endif
