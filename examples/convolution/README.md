Convolution kernels with the same notations as the one used in [Communication-Optimal Convolutional Neurol Nets](https://arxiv.org/abs/1802.06905) from J. Demmel and G. Dinh.

*S* is renamed as *Sp* as *S* corresponds to the cache size in our framework.

The C files correspond to the two cases either with a stride 1 or a stride 2 along h.

The txt files correspond to different cases where some of the dimensions are considered to be *small* (to be used wit option `s`).
