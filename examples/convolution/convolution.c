
/* Convolution kernel, as specified in Demmel paper "Communication-Optimal Convolutional Neural Nets" */
void kernel_convolution(int B, int C, int K, int W, int H, int R, int Sp,
//   int sig_w, int sig_h,
   double Out[K][H][W][B],
   double Image[R+W][Sp+H][C][B],
   //double Image[R+sig_w*W][S+sig_h*H][C][B],
   double Filter[K][R][Sp][C])
{
  int b, c, k, w, h, r, s;
#pragma scop
  for (k = 0; k < K; k++) {
  for (h = 0; h < H; h++) {
  for (w = 0; w < W; w++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (r = 0; r < R; r++) {
  for (s = 0; s < Sp; s++) {
    Out[k][h][w][b] += Image[r + w][s + h][c][b] * Filter[k][r][s][c];
    //Out[k][h][w][b] += Image[r + sig_w*w][s + sig_h*h][c][b] * Filter[k][r][s][c];
  }
  }
  }
  }
  }
  }
  }
#pragma endscop
}

// Note: Sp, because tool is naming "S" the size of the cache.


/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2+B*C*R-B*C*H*R+4*K-K*C*R*Sp+B*C*W+B*K*C*H*S^(-1)*R*W*Sp-B*C*H*W+C+B*W-B*C*W*Sp+2*B*H*W-S+B*C*Sp+B*C*H-B*C-3*B*K*H*W-B*C*R*Sp-B*K*H*S^(-1)*W+B*H
-2-8*n^4+n^2-S+S^(-1)*n^7+6*n^3-S^(-1)*n^4+5*n




Best lower bound:
-2+B*C*R-B*C*H*R+4*K-K*C*R*Sp+B*C*W+B*K*C*H*S^(-1)*R*W*Sp-B*C*H*W+C+B*W-B*C*W*Sp+2*B*H*W-S+B*C*Sp+B*C*H-B*C-3*B*K*H*W-B*C*R*Sp-B*K*H*S^(-1)*W+B*H

-2-8*n^4+n^2-S+S^(-1)*n^7+6*n^3-S^(-1)*n^4+5*n

-8*n^4-S+S^(-1)*n^7


Complete lower bound:
-B*C*R+B*C*H*R+K*C*R*Sp-B*C*W+B*C*H*W+B*C*W*Sp-B*C*Sp-B*C*H+B*C+B*K*H*W+B*C*R*Sp + max(0, -2+B*C*R-B*C*H*R+4*K-K*C*R*Sp+B*C*W+B*K*C*H*S^(-1)*R*W*Sp-B*C*H*W+C+B*W-B*C*W*Sp+2*B*H*W-S+B*C*Sp+B*C*H-B*C-3*B*K*H*W-B*C*R*Sp-B*K*H*S^(-1)*W+B*H)

%B*C*H*R+K*C*R*Sp+B*C*H*W+B*C*W*Sp+B*K*H*W+B*C*R*Sp

$6*n^4 + \max(0, -8*n^4-S+S^(-1)*n^7)$

When all params >> S:
B*K*C*H*S^(-1)*R*W*Sp+C
S^(-1)*n^7+n



B*C*H*R+K*C*R*Sp+B*C*H*W+B*C*W*Sp+B*K*H*W+B*C*R*Sp
B*K*C*H*S^(-1)*R*W*Sp
-B*C*R+B*C*H*R+K*C*R*Sp-B*C*W+B*C*H*W+B*C*W*Sp-B*C*Sp-B*C*H+B*C+B*K*H*W+B*C*R*Sp+max(0,-2+B*C*R-B*C*H*R+4*K-K*C*R*Sp+B*C*W+B*K*C*H*S^(-1)*R*W*Sp-B*C*H*W+C+B*W-B*C*W*Sp+2*B*H*W-S+B*C*Sp+B*C*H-B*C-3*B*K*H*W-B*C*R*Sp-B*K*H*S^(-1)*W+B*H)
*/

/* === Result - small dims R + Sp ===
// ./iolb-affine -s examples/convolution_smalldim.txt examples/convolution.c
// R => 3 | Sp => 7


Summed lower bounds:
-----------------------
-2+W*B+H*C*B+2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B-Sp*W*C*B-Sp*R*C*B-2*Sp^(-1/2)*W*S^(-1/2)*R^(-1/2)*H*K*B-S+H*B+Sp*C*B+2*W*H*B-W*H*C*B-3*W*H*K*B-Sp*R*K*C-C*B-R*H*C*B+W*C*B+4*K+R*C*B+C
-2-2*Sp*R*n^2+Sp*n^2+2*sqrt(Sp)*S^(-1/2)*sqrt(R)*n^5-Sp*n^3-4*n^4-S-2*Sp^(-1/2)*S^(-1/2)*R^(-1/2)*n^4+5*n+4*n^3+R*n^2+n^2-R*n^3




Best lower bound:
-2+W*B+H*C*B+2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B-Sp*W*C*B-Sp*R*C*B-2*Sp^(-1/2)*W*S^(-1/2)*R^(-1/2)*H*K*B-S+H*B+Sp*C*B+2*W*H*B-W*H*C*B-3*W*H*K*B-Sp*R*K*C-C*B-R*H*C*B+W*C*B+4*K+R*C*B+C

-2-2*Sp*R*n^2+Sp*n^2+2*sqrt(Sp)*S^(-1/2)*sqrt(R)*n^5-Sp*n^3-4*n^4-S-2*Sp^(-1/2)*S^(-1/2)*R^(-1/2)*n^4+5*n+4*n^3+R*n^2+n^2-R*n^3

-2*Sp*R*n^2+2*sqrt(Sp)*S^(-1/2)*sqrt(R)*n^5-Sp*n^3-4*n^4-S-R*n^3


Complete lower bound:
-H*C*B+Sp*W*C*B+Sp*R*C*B-Sp*C*B+W*H*C*B+W*H*K*B+Sp*R*K*C+C*B+R*H*C*B-W*C*B-R*C*B + max(0, -2+W*B+H*C*B+2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B-Sp*W*C*B-Sp*R*C*B-2*Sp^(-1/2)*W*S^(-1/2)*R^(-1/2)*H*K*B-S+H*B+Sp*C*B+2*W*H*B-W*H*C*B-3*W*H*K*B-Sp*R*K*C-C*B-R*H*C*B+W*C*B+4*K+R*C*B+C)

%Sp*W*C*B+Sp*R*C*B+W*H*C*B+W*H*K*B+Sp*R*K*C+R*H*C*B

$2*Sp*R*n^2+Sp*n^3+2*n^4+R*n^3 + \max(0, -2*Sp*R*n^2+2*sqrt(Sp)*S^(-1/2)*sqrt(R)*n^5-Sp*n^3-4*n^4-S-R*n^3)$

When all params >> S and all small parameters << others parameters:
2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B+C
2*sqrt(Sp)*S^(-1/2)*sqrt(R)*n^5+n



Sp*W*C*B+Sp*R*C*B+W*H*C*B+W*H*K*B+Sp*R*K*C+R*H*C*B
2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B
-H*C*B+Sp*W*C*B+Sp*R*C*B-Sp*C*B+W*H*C*B+W*H*K*B+Sp*R*K*C+C*B+R*H*C*B-W*C*B-R*C*B+max(0,-2+W*B+H*C*B+2*sqrt(Sp)*W*S^(-1/2)*sqrt(R)*H*K*C*B-Sp*W*C*B-Sp*R*C*B-2*Sp^(-1/2)*W*S^(-1/2)*R^(-1/2)*H*K*B-S+H*B+Sp*C*B+2*W*H*B-W*H*C*B-3*W*H*K*B-Sp*R*K*C-C*B-R*H*C*B+W*C*B+4*K+R*C*B+C)
*/
