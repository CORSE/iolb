void
gehd2_householder (int N,
	    double A[N][N],
	    double tmp[N])
{
  int i, j, k;
  double norma, norma2, tau;

#pragma scop
for ( j = 0; j < N-2; j++ ) {
  norma2 = 0.0 ;
  for ( i = j+2; i < N; i++ ) {
     norma2 += A[i][j] * A[i][j] ;
  }
  norma = sqrt( A[j+1][j] * A[j+1][j] + norma2 ) ;
  A[j+1][j] = ( A[j+1][j] > 0 ) ? ( A[j+1][j] + norma ) : ( A[j+1][j] - norma ) ;
  tau = 2.0 / ( 1.0 + norma2 / ( A[j+1][j] * A[j+1][j] ) ) ;
  for ( i = j+2; i < N; i++ ) {
     A[i][j] /= A[j+1][j] ;
  }
  A[j+1][j] = ( A[j+1][j] > 0 ) ? ( - norma ) : ( norma ) ;
  for (i = j+1; i < N; i++) {
     tmp[i] = A[j+1][i] ;
     for (k = j+2; k < N; k++) {
        tmp[i] += A[k][j] * A[k][i];
     }
  }
  for (i = j+1; i < N; i++) {
     tmp[i] *= tau ;
  }
  for (i = j+1 ; i < N ; i++) {
     A[j+1][i] -= tmp[i] ;
  }
  for (i = j+2; i < N; i++) {
     for (k = j+1; k < N; k++) {
        A[i][k] -= A[i][j] * tmp[k];
     }
  }
  for (i = 0; i < N; i++) {
     tmp[i] = A[i][j+1];
     for (k = j+2; k < N; k++) {
        tmp[i] += A[i][k] * A[k][j];
     }
  }
  for (i = 0; i < N; i++) {
     tmp[i] *= tau ;
  }
  for (i = 0; i < N; i++) {
     A[i][j+1] -= tmp[i] ;
  }
  for (i = 0; i < N; i++) {
     for (k = j+2; k < N; k++) {
        A[i][k] -= tmp[i] * A[k][j] ;
     }
  }
}
#pragma endscop

}

