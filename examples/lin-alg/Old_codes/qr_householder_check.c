void
qr_householder (int m, int n, int p,
	    double A[m][p],
	    double Q[m][n],
	    double tmp[n])
{
  int i, j, k;
  double norma2, tau;

#pragma scop

for(k = p-1; k > -1; k--){

	norma2 = 0.e+00;
	for(i = k+1; i < m; i++){
		norma2 += A[i][k] * A[i][k];
	}
	tau = 2.0e+00 / ( 1.0e+00 + norma2 );

	for(j = k+1; j < n; j++){
		tmp[j] = 0.e+00;
		for(i = k+1; i < m; i++){
			tmp[j] += A[i][k] * Q[i][j];
		}
	}

	tmp[k] = 1.0e+00;
	for(j = k; j < n; j++){ 
		tmp[j] *= tau;
	}

	Q[k][k] = 1.0e+00 - tmp[k];
	for(j = k+1; j < n; j++){
		Q[k][j] = -tmp[j];
	}

	for(j = k; j < n; j++){
		for(i = k+1; i < m; i++){
			Q[i][j] -= A[i][k] * tmp[j];
		}
	}
}
#pragma endscop

}

// Borne IOLB asymptotique: 2/3*p^3*S^(-1/2) + 2*p*n*S^(-1/2)*m
