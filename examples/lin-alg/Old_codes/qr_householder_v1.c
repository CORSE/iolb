void
qr_householder (int M, int N, int P,
//	    double A[M][P],
//	    double tmp[N]
	    double **A,
		double *tmp
		)
{
  int i, j, k;
  double norma2, norma, tau;

#pragma scop

for(k = 0; k < P; k++){
   norma2 = 0.e+00;
   for(i = k+1; i < M; i++){
      norma2 += A[i][k] * A[i][k];
   }
   norma = sqrt( A[k][k] * A[k][k] + norma2 );
   
   A[k][k] = ( A[k][k] > 0 ) ? ( A[k][k] + norma ) : ( A[k][k] - norma ) ;
   
   tau = 2.0 / ( 1.0 + norma2 / ( A[k][k] * A[k][k] ) ) ;
   
   for(i = k+1; i < M; i++){
      A[i][k] /= A[k][k];
   }
   A[k][k]= ( A[k][k] > 0 ) ? ( - norma ) : ( norma ) ;
   
   for(j = k+1; j < N; j++){
      tmp[j] = A[k][j];
      for(i = k+1; i < M; i++){
            tmp[j] += A[i][k] * A[i][j];
      }
      tmp[j] = tau * tmp[j];
      A[k][j] = A[k][j] - tmp[j];
      for(i = k+1; i < M; i++){
         A[i][j] = A[i][j] - A[i][k] * tmp[j];
      }
   }
}
#pragma endscop

}
// Note: parameter starting with "_" makes Ginac bug

// Signatures:
// * "int M, int N, int P, double A[M][P], double tmp[N]"
//		=> N*S^(-1/2)*M^2 + P*M + 1/2*N^2
//
// * "int M, int N, int P, double **A, double *tmp"
//		=> 2/3*S^(-1/2)*P^3 + 2*N*M*S^(-1/2)*P + 1/2*N^2

