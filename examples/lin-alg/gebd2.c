#include <math.h>

void gebd2(int N, int M, double B[M][N], double tauq[N-1], double taup[N-1])
{
   int i, j, k;

   double norma2, norma, ttmp;


#pragma scop
if (M>N) {
for(k = 0; k < N; k++){
   // on the left
   norma2 = 0.e+00;
   for(i = k+1; i < M; i++){
      norma2 += B[i][k] * B[i][k];
   }
   norma = sqrt( B[k][k] * B[k][k] + norma2 );
   B[k][k] = ( B[k][k] > 0 ) ? ( B[k][k] + norma ) : ( B[k][k] - norma ) ;
   tauq[k] = 2.0 / ( 1.0 + norma2 / ( B[k][k] * B[k][k] ) ) ;
   for(i = k+1; i < M; i++){
      B[i][k] /= B[k][k];
   }
   B[k][k]= ( B[k][k] > 0 ) ? ( - norma ) : ( norma ) ;
   for(j = k+1; j < N; j++){
      ttmp = B[k][j];
      for(i = k+1; i < M; i++){
         ttmp += B[i][k] * B[i][j];
      }
      ttmp = tauq[k] * ttmp;
      B[k][j] = B[k][j] - ttmp;
      for(i = k+1; i < M; i++){
         B[i][j] = B[i][j] - B[i][k] * ttmp;
      }
   }

   // on the right
   norma2 = 0.e+00;
   for(j = k+2; j < N; j++){
      norma2 += B[k][j] * B[k][j];
   }
   norma = sqrt( B[k][k+1] * B[k][k+1] + norma2 );
   B[k][k+1] = ( B[k][k+1] > 0 ) ? ( B[k][k+1] + norma ) : ( B[k][k+1] - norma ) ;
   taup[k] = 2.0 / ( 1.0 + norma2 / ( B[k][k+1] * B[k][k+1] ) ) ;
   for(j = k+2; j < N; j++){
      B[k][j] /= B[k][k+1];
   }
   B[k][k+1]= ( B[k][k+1] > 0 ) ? ( - norma ) : ( norma ) ;
   for(i = k+1; i < M; i++){
      ttmp = B[i][k+1];
      for(j = k+2; j < N; j++){
         ttmp += B[i][j] * B[k][j];
      }
      ttmp = ttmp * taup[k];
      B[i][k+1] = B[i][k+1] - ttmp;
      for(j = k+2; j < N; j++){
         B[i][j] = B[i][j] - ttmp * B[k][j] ;
      }
   } 
 }
}
#pragma endscop
}

