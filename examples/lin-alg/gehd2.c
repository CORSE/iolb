#include <math.h>

void gehd2(int n, double A[n][n] )
{
   int i, j, k;

   double norma2, norma, tau, tmp[n];

#pragma scop
   for ( j = 0; j < n-2; j++ ) {
      norma2 = 0.0;                                                                       // S_0
      for ( i = j+2; i < n; i++ ) {
         norma2 += A[i][j] * A[i][j] ;                                                    // S_1
      }
      norma = sqrt ( A[j+1][j] * A[j+1][j] + norma2 ) ;                                   // S_2
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( A[j+1][j] + norma ) : ( A[j+1][j] - norma ) ;     // S_3
      tau = 2.0 / ( 1.0 + norma2 / ( A[j+1][j] * A[j+1][j] ) ) ;                          // S_4
      for ( i = j+2; i < n; i++ ) {
         A[i][j] /= A[j+1][j] ;                                                           // S_5
      }
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( - norma ) : ( norma ) ;                           // S_6
      for (i = j+1; i < n; i++) {
         tmp[i] = A[j+1][i] ;                                                             // S_7
         for (k = j+2; k < n; k++) {
            tmp[i] += A[k][j] * A[k][i];                                                  // S_8
         }
      }
      for (i = j+1; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_9
      }
      for (i = j+1 ; i < n ; i++) {
         A[j+1][i] -= tmp[i] ;                                                            // S_10
      }
      for (i = j+2; i < n; i++) {
         for (k = j+1; k < n; k++) {
            A[i][k] -= A[i][j] * tmp[k];                                                  // S_11
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] = A[i][j+1];                                                              // S_12
         for (k = j+2; k < n; k++) {
            tmp[i] += A[i][k] * A[k][j];                                                  // S_13
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_14
      }
      for (i = 0; i < n; i++) {
         A[i][j+1] -= tmp[i] ;                                                            // S_15
      }
      for (i = 0; i < n; i++) {
         for (k = j+2; k < n; k++) {
            A[i][k] -= tmp[i] * A[k][j] ;                                                 // S_16
         }
      }
   }
#pragma endscop
}

// n^2
// 5/3*n^3*S^(-1/2)
// -1+n^2+max(0,-55+69/2*n-3*S+55/3*n*S^(-1/2)-11/2*n^2-10*n^2*S^(-1/2)-10*S^(-1/2)+5/3*n^3*S^(-1/2))