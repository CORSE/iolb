#include <math.h>

void gehd2(int n, int m, double A[n][n] )
{
   int i, j, k;

   double norma2, norma, tau, tmp[n];

#pragma scop
   for ( j = 0; j < min(m,n-2); j++ ) {
      norma2 = 0.0;                                                                       // S_0
      for ( i = j+2; i < n; i++ ) {
         norma2 += A[i][j] * A[i][j] ;                                                    // S_1
      }
      norma = sqrt ( A[j+1][j] * A[j+1][j] + norma2 ) ;                                   // S_2
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( A[j+1][j] + norma ) : ( A[j+1][j] - norma ) ;     // S_3
      tau = 2.0 / ( 1.0 + norma2 / ( A[j+1][j] * A[j+1][j] ) ) ;                          // S_4
      for ( i = j+2; i < n; i++ ) {
         A[i][j] /= A[j+1][j] ;                                                           // S_5
      }
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( - norma ) : ( norma ) ;                           // S_6
      for (i = j+1; i < n; i++) {
         tmp[i] = A[j+1][i] ;                                                             // S_7
         for (k = j+2; k < n; k++) {
            tmp[i] += A[k][j] * A[k][i];                                                  // S_8
         }
      }
      for (i = j+1; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_9
      }
      for (i = j+1 ; i < n ; i++) {
         A[j+1][i] -= tmp[i] ;                                                            // S_10
      }
      for (i = j+2; i < n; i++) {
         for (k = j+1; k < n; k++) {
            A[i][k] -= A[i][j] * tmp[k];                                                  // S_11
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] = A[i][j+1];                                                              // S_12
         for (k = j+2; k < n; k++) {
            tmp[i] += A[i][k] * A[k][j];                                                  // S_13
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_14
      }
      for (i = 0; i < n; i++) {
         A[i][j+1] -= tmp[i] ;                                                            // S_15
      }
      for (i = 0; i < n; i++) {
         for (k = j+2; k < n; k++) {
            A[i][k] -= tmp[i] * A[k][j] ;                                                 // S_16
         }
      }
   }
   for ( j = m; j < n-2; j++ ) {
      norma2 = 0.0;                                                                       // S_0
      for ( i = j+2; i < n; i++ ) {
         norma2 += A[i][j] * A[i][j] ;                                                    // S_1
      }
      norma = sqrt ( A[j+1][j] * A[j+1][j] + norma2 ) ;                                   // S_2
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( A[j+1][j] + norma ) : ( A[j+1][j] - norma ) ;     // S_3
      tau = 2.0 / ( 1.0 + norma2 / ( A[j+1][j] * A[j+1][j] ) ) ;                          // S_4
      for ( i = j+2; i < n; i++ ) {
         A[i][j] /= A[j+1][j] ;                                                           // S_5
      }
      A[j+1][j] = ( A[j+1][j] > 0 ) ? ( - norma ) : ( norma ) ;                           // S_6
      for (i = j+1; i < n; i++) {
         tmp[i] = A[j+1][i] ;                                                             // S_7
         for (k = j+2; k < n; k++) {
            tmp[i] += A[k][j] * A[k][i];                                                  // S_8
         }
      }
      for (i = j+1; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_9
      }
      for (i = j+1 ; i < n ; i++) {
         A[j+1][i] -= tmp[i] ;                                                            // S_10
      }
      for (i = j+2; i < n; i++) {
         for (k = j+1; k < n; k++) {
            A[i][k] -= A[i][j] * tmp[k];                                                  // S_11
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] = A[i][j+1];                                                              // S_12
         for (k = j+2; k < n; k++) {
            tmp[i] += A[i][k] * A[k][j];                                                  // S_13
         }
      }
      for (i = 0; i < n; i++) {
         tmp[i] *= tau ;                                                                  // S_14
      }
      for (i = 0; i < n; i++) {
         A[i][j+1] -= tmp[i] ;                                                            // S_15
      }
      for (i = 0; i < n; i++) {
         for (k = j+2; k < n; k++) {
            A[i][k] -= tmp[i] * A[k][j] ;                                                 // S_16
         }
      }
   }

#pragma endscop
}

// n^2
// 5/24*S*n^3*(S+S^2*(-2+n-m)^(-1))^(-1)+7*S^(-1/2)*n*m^2+1/12*S*n^3*(S^2*(-1+n-m)^(-1)+S)^(-1)+10/3*S^(-1/2)*n^3
// -1+n^2+max(0,-107-10*S-2*S^(-1/2)*m^3-8*S^(-1/2)*n^2*m+145/2*n-5/4*S*(S+S^2*(-2+n-m)^(-1))^(-1)-2*m^2-13*S^(-1/2)*m^2-5/4*S*n^2*(S+S^2*(-2+n-m)^(-1))^(-1)-27*S^(-1/2)*m+11/12*S*n*(S^2*(-1+n-m)^(-1)+S)^(-1)+5/24*S*n^3*(S+S^2*(-2+n-m)^(-1))^(-1)-23*S^(-1/2)+235/6*S^(-1/2)*n+7*S^(-1/2)*n*m^2-1/2*S*n^2*(S^2*(-1+n-m)^(-1)+S)^(-1)-1/2*S*(S^2*(-1+n-m)^(-1)+S)^(-1)+31*S^(-1/2)*n*m-25/2*n^2-9*m+55/24*S*n*(S+S^2*(-2+n-m)^(-1))^(-1)-41/2*S^(-1/2)*n^2+1/12*S*n^3*(S^2*(-1+n-m)^(-1)+S)^(-1)+5*n*m+10/3*S^(-1/2)*n^3)


// Asymptote reformulated in human-readible syntax:
//  where "0 <= m < n-3"
// 7*m^2*n*S^(-1/2)+10/3*n^3*S^(-1/2)+1/12*(S-(1+m-n)^(-1)*S^2)^(-1)*n^3*S+5/24*n^3*((-2+n-m)^(-1)*S^2+S)^(-1)*S
// ===>  5*n^3 / (24 * (1 + S/(n-m-2)) ) + n^3 / (12 * (1 + S/(n-m-1)) )  +  (7*n*m^2 + (10/3)*n^3) / \sqrt(S)

// (calcul dans un coin de table: ====>  5*n^3*(n-m-2) / (24 * (S+n-m-2) )   )



// a) Assuming "m = n/2"
//   5*n^3 / (24 * (1 + S/(n/2-2)) ) + n^3 / (12 * (1 + S/(n/2-1)) )  +  (7*n*n^2/4 + (10/3)*n^3) / \sqrt(S)
// ===>   5*n^4 / (24 * (n+2S-4) ) + n^3 / (12 * (1 + 2S/(n-2)) )  +  (7*n^3/4 + (10/3)*n^3) / \sqrt(S)



// b) Assuming n >> S, we can take "m = n-S"
// ===> 5*n^3 / (24 * (1 + S/(S-2)) ) + n^3 / (12 * (1 + S/(S-1)) ) + (7*n*(n-S)^2 + (10/3)*n^3) / \sqrt(S)
// 	~=  7*n^3 /48 + 31*n^3 / ( 3 *\sqrt(S) )

//n^2
//10/3*S^(-1/2)*n^3+5/24*S*n^3*(S-S^2*(2+m-n)^(-1))^(-1)+1/12*S*n^3*(S-S^2*(1+m-n)^(-1))^(-1)+7*m^2*S^(-1/2)*n
//-1+n^2+max(0,-107+31*m*S^(-1/2)*n-41/2*S^(-1/2)*n^2-13*m^2*S^(-1/2)-9*m+11/12*S*n*(S-S^2*(1+m-n)^(-1))^(-1)-1/2*S*(S-S^2*(1+m-n)^(-1))^(-1)+10/3*S^(-1/2)*n^3-25/2*n^2+5/24*S*n^3*(S-S^2*(2+m-n)^(-1))^(-1)-10*S-2*m^3*S^(-1/2)+145/2*n-5/4*S*n^2*(S-S^2*(2+m-n)^(-1))^(-1)-23*S^(-1/2)-8*m*S^(-1/2)*n^2+235/6*S^(-1/2)*n-5/4*S*(S-S^2*(2+m-n)^(-1))^(-1)+55/24*S*n*(S-S^2*(2+m-n)^(-1))^(-1)+1/12*S*n^3*(S-S^2*(1+m-n)^(-1))^(-1)-2*m^2+7*m^2*S^(-1/2)*n+5*m*n-1/2*S*n^2*(S-S^2*(1+m-n)^(-1))^(-1)-27*m*S^(-1/2))




