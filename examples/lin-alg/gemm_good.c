void gemm(int m, int n, int p,
   double alpha,
   double **A,
   double **B,
   double beta,
   double **C)
{
  int i, j, k;
  double tmp;

#pragma scop
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      tmp = 0.0;
      for (k = 0; k < p; k++) {
        tmp += A[i][k] * B[k][j];
      }
      C[i][j] *= beta;
      C[i][j] += alpha * tmp;
    }
  }
#pragma endscop

}
