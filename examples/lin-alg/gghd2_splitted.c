#include <math.h>
#define _PB_N n
#define SCALAR_VAL(x) x
#define SQRT_FUN sqrt

//void gghd2(int n, double A[n][n], double B[n][n], double Q[n][n], double Z[n][n] )
void gghd2(int n, int m, double A[n][n], double B[n][n] )
{
   int i, j, k;

   double nrm, tau, tmp, c, s;

#pragma scop
  // First half of the split
  for ( j = 0; j < min(m,n-2); j++ ) {
//  for (j = 0; j < _PB_N-2; j++) {
    for (i = _PB_N-2; i > j; i--) {
      nrm = SQRT_FUN ( A[i][j] * A[i][j] + A[i+1][j] * A[i+1][j] );  
      c = A[i][j] / nrm;  
      s = A[i+1][j] / nrm; 
      A[i][j] = nrm;
      A[i+1][j] = SCALAR_VAL(0.0);
      for (k = j+1; k < _PB_N; k++) {
        tmp = c * A[i][k] + s * A[i+1][k];
        A[i+1][k] = - s * A[i][k] + c * A[i+1][k];
        A[i][k] = tmp;
      }
      for (k = i; k < _PB_N; k++) {
        tmp = c * B[i][k] + s * B[i+1][k];
        B[i+1][k] = - s * B[i][k] + c * B[i+1][k];
        B[i][k] = tmp;
      }
//    for (k = 0; k < _PB_N; k++) {
//      tmp = c * Q[i][k] + s * Q[i+1][k];
//      Q[i+1][k] = - s * Q[i][k] + c * Q[i+1][k];
//      Q[i][k] = tmp;
//    }
      nrm = SQRT_FUN ( B[i+1][i+1] * B[i+1][i+1] + B[i+1][i] * B[i+1][i] );  
      c = B[i+1][i+1] / nrm;  
      s = B[i+1][i] / nrm; 
      B[i+1][i+1] = nrm;
      B[i+1][i] = SCALAR_VAL(0.0);
      for (k = 0; k <= i; k++) {
        tmp = c * B[k][i] - s * B[k][i+1];
        B[k][i+1] = s * B[k][i] + c * B[k][i+1];
        B[k][i] = tmp;
      }
      for (k = 0; k < _PB_N; k++) {
        tmp = c * A[k][i] - s * A[k][i+1];
        A[k][i+1] = s * A[k][i] + c * A[k][i+1];
        A[k][i] = tmp;
      }
//    for (k = i-j; k < _PB_N; k++) {
//      tmp = c * Z[k][i] - s * Z[k][i+1];
//      Z[k][i+1] = s * Z[k][i] + c * Z[k][i+1];
//      Z[k][i] = tmp;
//    }
    }
  }


  // Second half of the split
  for ( j = m; j < n-2; j++ ) {
//  for (j = 0; j < _PB_N-2; j++) {
    for (i = _PB_N-2; i > j; i--) {
      nrm = SQRT_FUN ( A[i][j] * A[i][j] + A[i+1][j] * A[i+1][j] );  
      c = A[i][j] / nrm;  
      s = A[i+1][j] / nrm; 
      A[i][j] = nrm;
      A[i+1][j] = SCALAR_VAL(0.0);
      for (k = j+1; k < _PB_N; k++) {
        tmp = c * A[i][k] + s * A[i+1][k];
        A[i+1][k] = - s * A[i][k] + c * A[i+1][k];
        A[i][k] = tmp;
      }
      for (k = i; k < _PB_N; k++) {
        tmp = c * B[i][k] + s * B[i+1][k];
        B[i+1][k] = - s * B[i][k] + c * B[i+1][k];
        B[i][k] = tmp;
      }
//    for (k = 0; k < _PB_N; k++) {
//      tmp = c * Q[i][k] + s * Q[i+1][k];
//      Q[i+1][k] = - s * Q[i][k] + c * Q[i+1][k];
//      Q[i][k] = tmp;
//    }
      nrm = SQRT_FUN ( B[i+1][i+1] * B[i+1][i+1] + B[i+1][i] * B[i+1][i] );  
      c = B[i+1][i+1] / nrm;  
      s = B[i+1][i] / nrm; 
      B[i+1][i+1] = nrm;
      B[i+1][i] = SCALAR_VAL(0.0);
      for (k = 0; k <= i; k++) {
        tmp = c * B[k][i] - s * B[k][i+1];
        B[k][i+1] = s * B[k][i] + c * B[k][i+1];
        B[k][i] = tmp;
      }
      for (k = 0; k < _PB_N; k++) {
        tmp = c * A[k][i] - s * A[k][i+1];
        A[k][i+1] = s * A[k][i] + c * A[k][i+1];
        A[k][i] = tmp;
      }
//    for (k = i-j; k < _PB_N; k++) {
//      tmp = c * Z[k][i] - s * Z[k][i+1];
//      Z[k][i+1] = s * Z[k][i] + c * Z[k][i+1];
//      Z[k][i] = tmp;
//    }
    }
  }


#pragma endscop
}
