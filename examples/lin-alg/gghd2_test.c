#include <math.h>
#define _PB_N n
#define SCALAR_VAL(x) x
#define SQRT_FUN sqrt

//void gghd2(int n, double A[n][n], double B[n][n], double Q[n][n], double Z[n][n] )
void gghd2(int n, double A[n][n], double B[n][n] )
{
   int i, j, k;

   double nrm, tau, tmp, c, s;

#pragma scop
  for (j = 0; j < _PB_N-2; j++) {
    for (i = _PB_N-2; i > j; i--) {
      nrm = SQRT_FUN ( A[i][j] * A[i][j] + A[i+1][j] * A[i+1][j] );  // S2
      c = A[i][j] / nrm;                                             // S3
      s = A[i+1][j] / nrm;                                           // S4
      A[i][j] = nrm;                                                 // S5
      A[i+1][j] = SCALAR_VAL(0.0);                                   // S6
      for (k = j+1; k < _PB_N; k++) {
        //tmp = c * A[i][k] + s * A[i+1][k];                           // S7
        //A[i+1][k] = - s * A[i][k] + c * A[i+1][k];                   // S8
        //A[i][k] = tmp;                                               // S9
        tmp = - s * A[i][k] + c * A[i+1][k];                         // S7
        A[i][k] = c * A[i][k] + s * A[i+1][k];                       // S8
        A[i+1][k] = tmp;                                             // S9
      }
      for (k = i; k < _PB_N; k++) {
        //tmp = c * B[i][k] + s * B[i+1][k];                           // S10
        //B[i+1][k] = - s * B[i][k] + c * B[i+1][k];                   // S11
        //B[i][k] = tmp;                                               // S12
        tmp = - s * B[i][k] + c * B[i+1][k];                         // S10
        B[i][k] = c * B[i][k] + s * B[i+1][k];                       // S11
        B[i+1][k] = tmp;                                             // S12
      }
//    for (k = 0; k < _PB_N; k++) {
//      tmp = c * Q[i][k] + s * Q[i+1][k];
//      Q[i+1][k] = - s * Q[i][k] + c * Q[i+1][k];
//      Q[i][k] = tmp;
//    }
      nrm = SQRT_FUN ( B[i+1][i+1] * B[i+1][i+1] + B[i+1][i] * B[i+1][i] );    // S13
      c = B[i+1][i+1] / nrm;                                         // S14 
      s = B[i+1][i] / nrm;                                           // S15
      B[i+1][i+1] = nrm;                                             // S16
      B[i+1][i] = SCALAR_VAL(0.0);                                   // S17
      for (k = 0; k <= i; k++) {
        //tmp = c * B[k][i] - s * B[k][i+1];                           // S18
        //B[k][i+1] = s * B[k][i] + c * B[k][i+1];                     // S19
        //B[k][i] = tmp;                                               // S20
        tmp = s * B[k][i] + c * B[k][i+1];                          // S18
        B[k][i] = c * B[k][i] - s * B[k][i+1];                       // S19
        B[k][i+1] = tmp;                                            // S20
      }
      for (k = 0; k < _PB_N; k++) {
        //tmp = c * A[k][i] - s * A[k][i+1];                           // S21
        //A[k][i+1] = s * A[k][i] + c * A[k][i+1];                     // S22
        //A[k][i] = tmp;                                               // S23
        tmp = s * A[k][i] + c * A[k][i+1];                     // S21
        A[k][i] = c * A[k][i] - s * A[k][i+1];                 // S22
        A[k][i+1] = tmp;                                       // S23
      }
//    for (k = i-j; k < _PB_N; k++) {
//      tmp = c * Z[k][i] - s * Z[k][i+1];
//      Z[k][i+1] = s * Z[k][i] + c * Z[k][i+1];
//      Z[k][i] = tmp;
//    }
    }
  }
#pragma endscop
}
