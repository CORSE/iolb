#include <math.h>

void qr_householder_a2v ( int M, int N, double A[M][N], double tau[N] )
{
  int i, j, k;
  double norma2, norma;

// M>=N: comment le préciser ici?
#pragma scop
if (M>N) {
for(k = 0; k < N; k++){
   norma2 = 0.e+00;                                                           // S_0
   for(i = k+1; i < M; i++){
      norma2 += A[i][k] * A[i][k];                                            // S_1
   }
   norma = sqrt( A[k][k] * A[k][k] + norma2 );                                // S_2
   
   A[k][k] = ( A[k][k] > 0 ) ? ( A[k][k] + norma ) : ( A[k][k] - norma ) ;    // S_3
   
   tau[k] = 2.0 / ( 1.0 + norma2 / ( A[k][k] * A[k][k] ) ) ;                  // S_4
   
   for(i = k+1; i < M; i++){
      A[i][k] /= A[k][k];                                                     // S_5
   }
   A[k][k]= ( A[k][k] > 0 ) ? ( - norma ) : ( norma ) ;                       // S_6
   
   for(j = k+1; j < N; j++){
      tau[j] = A[k][j];                                                       // S_7
      for(i = k+1; i < M; i++){
            tau[j] += A[i][k] * A[i][j];                                      // S_8
      }
      tau[j] = tau[k] * tau[j];                                               // S_9
      A[k][j] = A[k][j] - tau[j];                                             // S_10
      for(i = k+1; i < M; i++){
         A[i][j] = A[i][j] - A[i][k] * tau[j];                                // S_11
      }
   }
}
}
#pragma endscop
}
