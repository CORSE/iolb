void rot_sequence(int m,
                  int n,
                  int k,
                  double A[m][n],
                  double C[n][k],
                  double S[n][k]
                  )
{
    int i, j, p;
    double c, s, temp;

#pragma scop
for (p = 0; p < k; p++) {
    for (j = k - 1 - p; j < n - 1 - p; j++) {
    //for (j = 0; j < n - 1; j++) {
        c = C[j][p];
        s = S[j][p];
        for (i = 0; i < m; i++) {
            temp = c * A[i][j] + s * A[i][j + 1];
            A[i][j + 1] = -s * A[i][j] + c * A[i][j + 1];
            A[i][j] = temp;
        }
    }
}
#pragma endscop

}