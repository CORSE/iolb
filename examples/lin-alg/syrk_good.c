void kernel_syrk(int n, int m,
   double alpha,
   double beta,
   double C[ n][n],
   double A[ n][m])
{
  int i, j, k;
  double tmp;

#pragma scop
  for (i = 0; i < n; i++) {
    for (j = 0; j <= i; j++)
      C[i][j] *= beta;

    for (j = 0; j <= i; j++) {
      tmp = 0.0;
      for (k = 0; k < m; k++)
        tmp += A[i][k] * A[j][k];
      C[i][j] += alpha * tmp;
    }
  }
#pragma endscop
}
