#include <math.h>

void gebd2(int n, double A[n][n], double tau[n])
{
   int i, j, k;
   double taui, alpha, norma, norma2;

#pragma scop
 for(i = 0; i < n-2; i++){
	// Generate elementary reflector H(i) = I - tau * v * v**T
	//  to annihilate A(i+2:n,i)
    norma2 = 0.0e+00 ;
    for ( k = i+2; k < n ; k++ ) {
       norma2 += A[k][i] * A[k][i];
    }
    norma = sqrt( A[i+1][i] * A[i+1][i] + norma2 ) ;
    A[i+1][i] = ( A[i+1][i] > 0 ) ? (A[i+1][i] + norma) : (A[i+1][i] - norma) ;
    taui = 2.0e+00 / ( 1.0e+00 + norma2 / ( A[i+1][i] * A[i+1][i] ) ) ;

    for (k = i+2; k < n ; k++) {
       A[k][i] /= A[i+1][i] ;
    }
    A[i+1][i] = ( A[i+1][i] > 0.0e+00 ) ? ( - norma ) : ( norma ) ;

	// Apply H(i) from both sides to A(i+1:n,i+1:n)
	// Compute  x := tau * A * v  storing x in TAU(i:n-1)
    for(k = i+1; k < n; k++){
       tau[k-1] = A[k][i+1];
       for(j = i+2; j <= k; j++){
          tau[k-1] += A[k][j] * A[j][i];
       }
       for(j = k+1; j < n; j++){
          tau[k-1] += A[j][k] * A[j][i];
       }
       tau[k-1] *= taui;
    }
   // alpha(ligne 43) = - 1/2 * tau * (x**T * v)      [scalaire]
	// Compute  w := x - 1/2 * tau * (x**T * v) * v storing w in TAU(i:n-1)
   // ??? Note that shift of 1 on w (does not store first element) ???
    alpha = tau[i];
    for(k = i+2; k < n; k++){
       alpha += tau[k-1] * A[k][i];
    }
    alpha = -0.5e+00 * taui * alpha;

    tau[i] += alpha ;
    for(k = i+2; k < n; k++){
       tau[k-1] += alpha * A[k][i];
    }
	// Apply the transformation as a rank-2 update: 
	//    A := A - v * w**T - w * v**T
   // v(k) : (A[k,i])_{i+2<=k<n} parce que v[i+1] = 1
   //   (  A(i+1)[i] : valeur tridiag)
    A[i+1][i+1] -= 2.0e+00 * tau[i] ;
    for(j = i+2; j < n; j++){
       A[j][i+1] -= tau[j-1] ;
       A[j][i+1] -= tau[i] * A[j][i];
       for(k = i+2; k <= j; k++){
          A[j][k] -= tau[j-1] * A[k][i];
          A[j][k] -= tau[k-1] * A[j][i];
       }
    }
   /* REFORMULATION of previous loops (in a more understandable way)
      A[i+1][i+1] -= 2.0e+00 * tau[i] ; // v(i+1) on both size is 1 => both v*wT and w*vT are the same
      for(j = i+2; j < n; j++){
         A[j][i+1] -= tau[j-1] ;          // w(j) * 1  (where v(i+1) = 1)
         for(k = i+2; k <= j; k++){
            A[j][k] -= tau[j-1] * A[k][i]; // w(j) * v(k)
         }
      }

      for(j = i+2; j < n; j++){
         A[j][i+1] -= tau[i] * A[j][i];   // k=i+1  (that's all - no simplification)
         for(k = i+2; k <= j; k++){
            A[j][k] -= tau[k-1] * A[j][i]; // v(j) * w(k)
         }
      }
   */

    tau[i] = taui;
  }

#pragma endscop
}

