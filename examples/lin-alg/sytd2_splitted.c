#include <math.h>

void gebd2(int n, int ksplit, double A[n][n], double tau[n])
{
   int i, j, k;
   double taui, alpha, norma, norma2;

#pragma scop
if (ksplit<n) {
 for(i = 0; i < n-2; i++){
	// Generate elementary reflector H(i) = I - tau * v * v**T
	//  to annihilate A(i+2:n,i)
    norma2 = 0.0e+00 ;
    for ( k = i+2; k < n ; k++ ) {
       norma2 += A[k][i] * A[k][i];
    }
    norma = sqrt( A[i+1][i] * A[i+1][i] + norma2 ) ;
    A[i+1][i] = ( A[i+1][i] > 0 ) ? (A[i+1][i] + norma) : (A[i+1][i] - norma) ;
    taui = 2.0e+00 / ( 1.0e+00 + norma2 / ( A[i+1][i] * A[i+1][i] ) ) ;

    for (k = i+2; k < n ; k++) {
       A[k][i] /= A[i+1][i] ;
    }
    A[i+1][i] = ( A[i+1][i] > 0.0e+00 ) ? ( - norma ) : ( norma ) ;

	// Apply H(i) from both sides to A(i+1:n,i+1:n)
	// Compute  x := tau * A * v  storing x in TAU(i:n-1)
    for(k = i+1; k < n; k++){
       tau[k-1] = A[k][i+1];
       for(j = i+2; j <= k; j++){
          tau[k-1] += A[k][j] * A[j][i];
       }
       for(j = k+1; j < n; j++){
          tau[k-1] += A[j][k] * A[j][i];
       }
       tau[k-1] *= taui;
    }
	// Compute  w := x - 1/2 * tau * (x**T * v) * v storing w in TAU(i:n-1)
    alpha = tau[i];
    for(k = i+2; k < n; k++){
       alpha += tau[k-1] * A[k][i];
    }
    alpha = -0.5e+00 * taui * alpha;
    tau[i] += alpha ;
    for(k = i+2; k < n; k++){
       tau[k-1] += alpha * A[k][i];
    }
	// Apply the transformation as a rank-2 update:
	//    A := A - v * w**T - w * v**T
    A[i+1][i+1] -= 2.0e+00 * tau[i] ;
    for(j = i+2; j < n; j++){
       A[j][i+1] -= tau[j-1] ;
       A[j][i+1] -= tau[i] * A[j][i];
       for(k = i+2; k <= min(ksplit,j); k++){
          A[j][k] -= tau[j-1] * A[k][i];
          A[j][k] -= tau[k-1] * A[j][i];
       }
       for(k = max(i+2,ksplit); k <= j; k++){
          A[j][k] -= tau[j-1] * A[k][i];
          A[j][k] -= tau[k-1] * A[j][i];
       }
    }
    tau[i] = taui;
  }
}
#pragma endscop
}

