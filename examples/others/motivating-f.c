void kernel_motivating(int N,
   double A[N][N],
   double B[N][N],
   double C[N][N]
	       )
{
  int i, j, k;
  double s, f=0;
#pragma scop
for ( k=0; k<N ; k++){
  s = 0;
  for ( i =0; i < N ; i ++){
    for ( j =0; j < N ; j ++){
      C[i][j] = C[i][j] + f * A[i][k] * B [k][j];
      s = s + C[i][j];
    }
    f += s / ( N * N );
  }
}
#pragma endscop
}
