
// This was checked it is correct, by comparison with the original Polybench computation on the MINI_DATASET (6 Feb 2025)
void kernel_nussinov(int n, char seq[ n],
      int table[ n][n])
{
   int j, k, l;

#pragma scop
   // Old domain: 0 <= i < k < j < n
   // Change of basis: l = j-i-1
   //
   // => New domain:
   //    0 <= l < n-1
   //    l < j <= n-1
   //    j-l-1 < k < j
   for (l = 0; l<n-1; l++) {
      for (j = l+1; j<n; j++) {
         // Replace "i" by "j-l-1"

         if (j-1>=0)
            // S_0
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][j-1]) ? table[j-l-1][j] : table[j-l-1][j-1]);
         if (j-l<n)
            // S_1
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j]) ? table[j-l-1][j] : table[j-l][j]);

         if (j-1>=0 && j-l<n) {

            if (0<l)
               // S_2
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0)) ?
                  table[j-l-1][j] : table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0));
            else
               // S_3
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]) ? table[j-l-1][j] : table[j-l][j-1]);
         }

         for (k=j-l; k<j; k++) {
            // S_4
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][k] + table[k+1][j]) ?
                  table[j-l-1][j] : table[j-l-1][k] + table[k+1][j]);
         }
      }
   }
#pragma endscop
}

// Output IOLB:
//1/2*n^2
//1/6*S^(-1/2)*n^3
//-1+5/2*n+1/2*n^2+max(0,-14+1/6*S^(-1/2)*n^3-10*S^(-1/2)-2*S^(-1/2)*n^2+47/6*S^(-1/2)*n-4*S+7*n+65/4*S^(-1)-n^2+9/8*S^(-1)*n^2-67/8*S^(-1)*n)
