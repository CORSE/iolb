
// This was checked it is correct, by comparison with the original Polybench computation on the MINI_DATASET (6 Feb 2025)
void kernel_nussinov(int n, int m, char seq[ n],
      int table[ n][n])
{
   int j, k, l;

#pragma scop
   // Old domain: 0 <= i < k < j < n
   // Change of basis: l = j-i-1
   //
   // => New domain:
   //    0 <= l < n-1
   //    l < j <= n-1
   //    j-l-1 < k < j
  if (m>0) {
  if (n>m) {
   for (l = 0; l<min(m,n-1); l++) {
      for (j = l+1; j<n; j++) {
         // Replace "i" by "j-l-1"

         if (j-1>=0)
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][j-1]) ? table[j-l-1][j] : table[j-l-1][j-1]);
         if (j-l<n)
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j]) ? table[j-l-1][j] : table[j-l][j]);

         if (j-1>=0 && j-l<n) {

            if (0<l)
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0)) ?
                  table[j-l-1][j] : table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0));
            else
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]) ? table[j-l-1][j] : table[j-l][j-1]);
         }

         for (k=j-l; k<j; k++) {
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][k] + table[k+1][j]) ?
                  table[j-l-1][j] : table[j-l-1][k] + table[k+1][j]);
         }
      }
   }
   for (l = m; l<n-1; l++) {
      for (j = l+1; j<n; j++) {
         // Replace "i" by "j-l-1"

         if (j-1>=0)
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][j-1]) ? table[j-l-1][j] : table[j-l-1][j-1]);
         if (j-l<n)
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j]) ? table[j-l-1][j] : table[j-l][j]);

         if (j-1>=0 && j-l<n) {

            if (0<l)
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0)) ?
                  table[j-l-1][j] : table[j-l][j-1]+(((seq[j-l-1])+(seq[j])) == 3 ? 1 : 0));
            else
               table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l][j-1]) ? table[j-l-1][j] : table[j-l][j-1]);
         }

         for (k=j-l; k<j; k++) {
            table[j-l-1][j] = ((table[j-l-1][j] >= table[j-l-1][k] + table[k+1][j]) ?
                  table[j-l-1][j] : table[j-l-1][k] + table[k+1][j]);
         }
      }
   }
   }
   }
#pragma endscop
}
//1/2*m^2
//1/6*n^3*S^(-1/2)+m*n^2*S^(-1/2)+5/3*m^3*S^(-1/2)
//2+1/2*m^2+7/2*m+max(0,-22+45/8*m*S^(-1)+13/4*n^2*S^(-1)+1/6*n^3*S^(-1/2)+m^2-5/2*m^2*n*S^(-1/2)-5*m+11/6*m*S^(-1/2)-m*n-3*n^2*S^(-1/2)+m*n^2*S^(-1/2)+47/6*n*S^(-1/2)-1/2*m*n*S^(-1/2)-n^2+12*n+97/4*S^(-1)+5/2*m^2*S^(-1/2)-33/2*n*S^(-1)-13/4*m*n*S^(-1)+5/3*m^3*S^(-1/2)-9*S^(-1/2)+13/8*m^2*S^(-1)-11*S)


// No hourglass completion was found.
