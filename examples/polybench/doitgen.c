void kernel_doitgen(int nr, int nq, int np,
      double A[ nr][nq][np],
      double C4[ np][np],
      double sum[ np])
{
   int r, q, p, s;

#pragma scop
   for (r = 0; r < nr; r++)
      for (q = 0; q < nq; q++) {
         for (p = 0; p < np; p++) {
            sum[p] = 0.0;
            for (s = 0; s < np; s++)
               sum[p] += A[r][q][s] * C4[s][p];
         }
         for (p = 0; p < np; p++)
            A[r][q][p] = sum[p];
      }
#pragma endscop
}
