#include <math.h>

void kernel_gramschmidt(int m, int n,
      double A[ m][n],
      double R[ n][n],
      double Q[ m][n])
{
   int i, j, k;

   double nrm;

#pragma scop
   for (k = 0; k < n; k++)
   {
      nrm = 0.0;                                   // S_0
      for (i = 0; i < m; i++)
         nrm += A[i][k] * A[i][k];                 // S_1
      R[k][k] = sqrt(nrm);                         // S_2
      for (i = 0; i < m; i++)
         Q[i][k] = A[i][k] / R[k][k];              // S_3
      for (j = k + 1; j < n; j++)
      {
         R[k][j] = 0.0;                            // S_4
         for (i = 0; i < m; i++)
            R[k][j] += Q[i][k] * A[i][j];          // S_5
         for (i = 0; i < m; i++)
            A[i][j] = A[i][j] - Q[i][k] * R[k][j]; // S_6
      }
   }
#pragma endscop
}
