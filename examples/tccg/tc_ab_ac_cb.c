
// Class 7
// Tensor contraction for ab = ac * cb
void kernel_tc_ab_acd_dbc(int A, int B, int C,
   double Out[A][B],
   double I1[A][C],
   double I2[C][B])
{
  int a, b, c;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
    Out[a][b] += I1[a][c] * I2[c][b];
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2+2*B-C*A+C-2*B*A*S^(-1/2)-B*C+2*A-B*A-S+2*B*C*A*S^(-1/2)
-2-2*n^2*S^(-1/2)+5*n+2*n^3*S^(-1/2)-3*n^2-S




Best lower bound:
-2+2*B-C*A+C-2*B*A*S^(-1/2)-B*C+2*A-B*A-S+2*B*C*A*S^(-1/2)

-2-2*n^2*S^(-1/2)+5*n+2*n^3*S^(-1/2)-3*n^2-S

2*n^3*S^(-1/2)-3*n^2-S


Complete lower bound:
C*A+B*C+B*A + max(0, -2+2*B-C*A+C-2*B*A*S^(-1/2)-B*C+2*A-B*A-S+2*B*C*A*S^(-1/2))

%C*A+B*C+B*A

$3*n^2 + \max(0, 2*n^3*S^(-1/2)-3*n^2-S)$

When all params >> S:
2*B+C+2*A+2*B*C*A*S^(-1/2)
5*n+2*n^3*S^(-1/2)



C*A+B*C+B*A
2*B*C*A*S^(-1/2)
C*A+B*C+B*A+max(0,-2+2*B-C*A+C-2*B*A*S^(-1/2)-B*C+2*A-B*A-S+2*B*C*A*S^(-1/2))
*/
