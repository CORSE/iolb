
// Tensor contraction for ab = acd*dbc

void kernel_tc_ab_acd_dbc(int A, int B, int C, int D,
   double Out[A][B],
   double I1[A][C][D],
   double I2[D][B][C])
{
  int a, b, c, d;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
    Out[a][b] += I1[a][c][d] * I2[d][b][c];
  }
  }
  }
  }
#pragma endscop
}



