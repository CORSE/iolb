
// Class 6
// Tensor contraction for ab = cad * dcb
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D,
   double Out[A][B],
   double I1[C][A][D],
   double I2[D][C][B])
{
  int a, b, c, d;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
    Out[a][b] += I1[c][a][d] * I2[d][c][b];
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-3-S-C*D*A-2*A*B+3*A+2*C*S^(-1/2)*D*A*B+C*D-2*S^(-1/2)*A*B-C*D*B+3*B
-3-S+6*n+2*S^(-1/2)*n^4-2*S^(-1/2)*n^2-n^2-2*n^3




Best lower bound:
-3-S-C*D*A-2*A*B+3*A+2*C*S^(-1/2)*D*A*B+C*D-2*S^(-1/2)*A*B-C*D*B+3*B

-3-S+6*n+2*S^(-1/2)*n^4-2*S^(-1/2)*n^2-n^2-2*n^3

-S+2*S^(-1/2)*n^4-2*n^3


Complete lower bound:
C*D*A+A*B+C*D*B + max(0, -3-S-C*D*A-2*A*B+3*A+2*C*S^(-1/2)*D*A*B+C*D-2*S^(-1/2)*A*B-C*D*B+3*B)

%C*D*A+A*B+C*D*B

$2*n^3 + \max(0, -S+2*S^(-1/2)*n^4-2*n^3)$

When all params >> S:
2*C*S^(-1/2)*D*A*B+C*D
2*S^(-1/2)*n^4+n^2



C*D*A+A*B+C*D*B
2*C*S^(-1/2)*D*A*B
C*D*A+A*B+C*D*B+max(0,-3-S-C*D*A-2*A*B+3*A+2*C*S^(-1/2)*D*A*B+C*D-2*S^(-1/2)*A*B-C*D*B+3*B)
*/



