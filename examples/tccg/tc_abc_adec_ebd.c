
// Class 5
// Tensor contraction for abc = adec * ebd
void kernel_tc_abc_adec_ebd(int A, int B, int C, int D, int E,
   double Out[A][B][C],
   double I1[A][D][E][C],
   double I2[E][B][D])
{
  int a, b, c, d, e;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
  for (e = 0; e < E; e++) {
    Out[a][b][c] += I1[a][d][e][c] * I2[e][b][d];
  }
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-3+3*C*A-S-D*E*B-2*S^(-1/2)*C*A*B+2*S^(-1/2)*C*D*A*E*B+D*E-C*D*A*E-2*C*A*B+3*B
-3-S+2*S^(-1/2)*n^5-3*n^3+4*n^2+3*n-n^4-2*S^(-1/2)*n^3




Best lower bound:
-3+3*C*A-S-D*E*B-2*S^(-1/2)*C*A*B+2*S^(-1/2)*C*D*A*E*B+D*E-C*D*A*E-2*C*A*B+3*B

-3-S+2*S^(-1/2)*n^5-3*n^3+4*n^2+3*n-n^4-2*S^(-1/2)*n^3

-S+2*S^(-1/2)*n^5-n^4


Complete lower bound:
D*E*B+C*D*A*E+C*A*B + max(0, -3+3*C*A-S-D*E*B-2*S^(-1/2)*C*A*B+2*S^(-1/2)*C*D*A*E*B+D*E-C*D*A*E-2*C*A*B+3*B)

%D*E*B+C*D*A*E+C*A*B

$n^4 + \max(0, -S+2*S^(-1/2)*n^5-n^4)$

When all params >> S:
2*S^(-1/2)*C*D*A*E*B+D*E
2*S^(-1/2)*n^5+n^2



D*E*B+C*D*A*E+C*A*B
2*S^(-1/2)*C*D*A*E*B
D*E*B+C*D*A*E+C*A*B+max(0,-3+3*C*A-S-D*E*B-2*S^(-1/2)*C*A*B+2*S^(-1/2)*C*D*A*E*B+D*E-C*D*A*E-2*C*A*B+3*B)
*/
