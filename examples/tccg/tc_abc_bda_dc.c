
// Class 3
// Tensor contraction for abc = bda * dc
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D,
   double Out[A][B][C],
   double I1[B][D][A],
   double I2[D][C])
{
  int a, b, c, d;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
    Out[a][b][c] += I1[b][d][a] * I2[d][c];
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2-S+2*C-B*C*A+2*B*S^(-1/2)*C*D*A+D-B*D*A-C*D-2*B*S^(-1/2)*C*A+2*B*A
-2-2*S^(-1/2)*n^3+n^2-S-2*n^3+3*n+2*S^(-1/2)*n^4




Best lower bound:
-2-S+2*C-B*C*A+2*B*S^(-1/2)*C*D*A+D-B*D*A-C*D-2*B*S^(-1/2)*C*A+2*B*A

-2-2*S^(-1/2)*n^3+n^2-S-2*n^3+3*n+2*S^(-1/2)*n^4

-S-2*n^3+2*S^(-1/2)*n^4


Complete lower bound:
B*C*A+B*D*A+C*D + max(0, -2-S+2*C-B*C*A+2*B*S^(-1/2)*C*D*A+D-B*D*A-C*D-2*B*S^(-1/2)*C*A+2*B*A)

%B*C*A+B*D*A+C*D

$2*n^3 + \max(0, -S-2*n^3+2*S^(-1/2)*n^4)$

When all params >> S:
2*C+2*B*S^(-1/2)*C*D*A+D+2*B*A
2*n^2+3*n+2*S^(-1/2)*n^4



B*C*A+B*D*A+C*D
2*B*S^(-1/2)*C*D*A
B*C*A+B*D*A+C*D+max(0,-2-S+2*C-B*C*A+2*B*S^(-1/2)*C*D*A+D-B*D*A-C*D-2*B*S^(-1/2)*C*A+2*B*A)
*/