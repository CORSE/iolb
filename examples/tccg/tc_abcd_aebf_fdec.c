
// Class 8
// Tensor contraction for abcd = aebf * fdec
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D, int E, int F,
   double Out[A][B][C][D],
   double I1[A][E][B][F],
   double I2[F][D][E][C])
{
  int a, b, c, d, e, f;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
  for (e = 0; e < E; e++) {
  for (f = 0; f < F; f++) {
    Out[a][b][c][d] += I1[a][e][b][f] * I2[f][d][e][c];
  }
  }
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-3+3*C*D+2*C*D*A*E*S^(-1/2)*B*F-A*E*B*F-2*C*D*A*S^(-1/2)*B-2*C*D*A*B-C*D*E*F-S+3*A*B+E*F
-3-2*S^(-1/2)*n^4+2*S^(-1/2)*n^6+7*n^2-4*n^4-S




Best lower bound:
-3+3*C*D+2*C*D*A*E*S^(-1/2)*B*F-A*E*B*F-2*C*D*A*S^(-1/2)*B-2*C*D*A*B-C*D*E*F-S+3*A*B+E*F

-3-2*S^(-1/2)*n^4+2*S^(-1/2)*n^6+7*n^2-4*n^4-S

2*S^(-1/2)*n^6-4*n^4-S


Complete lower bound:
A*E*B*F+C*D*A*B+C*D*E*F + max(0, -3+3*C*D+2*C*D*A*E*S^(-1/2)*B*F-A*E*B*F-2*C*D*A*S^(-1/2)*B-2*C*D*A*B-C*D*E*F-S+3*A*B+E*F)

%A*E*B*F+C*D*A*B+C*D*E*F

$3*n^4 + \max(0, 2*S^(-1/2)*n^6-4*n^4-S)$

When all params >> S:
2*C*D*A*E*S^(-1/2)*B*F+E*F
2*S^(-1/2)*n^6+n^2



A*E*B*F+C*D*A*B+C*D*E*F
2*C*D*A*E*S^(-1/2)*B*F
A*E*B*F+C*D*A*B+C*D*E*F+max(0,-3+3*C*D+2*C*D*A*E*S^(-1/2)*B*F-A*E*B*F-2*C*D*A*S^(-1/2)*B-2*C*D*A*B-C*D*E*F-S+3*A*B+E*F)
*/

