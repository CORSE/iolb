
// Class 2
// Tensor contraction for abcd = dbea * ec
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D, int E,
   double Out[A][B][C][D],
   double I1[D][B][E][A],
   double I2[E][C])
{
  int a, b, c, d, e;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
  for (e = 0; e < E; e++) {
    Out[a][b][c][d] += I1[d][b][e][a] * I2[e][c];
  }
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2+2*A*E*B*S^(-1/2)*C*D+E-2*A*B*S^(-1/2)*C*D-S+2*C+2*A*B*D-A*E*B*D-A*B*C*D-E*C
-2+3*n-2*n^4+2*n^5*S^(-1/2)-2*n^4*S^(-1/2)-S-n^2+2*n^3




Best lower bound:
-2+2*A*E*B*S^(-1/2)*C*D+E-2*A*B*S^(-1/2)*C*D-S+2*C+2*A*B*D-A*E*B*D-A*B*C*D-E*C

-2+3*n-2*n^4+2*n^5*S^(-1/2)-2*n^4*S^(-1/2)-S-n^2+2*n^3

-2*n^4+2*n^5*S^(-1/2)-S


Complete lower bound:
A*E*B*D+A*B*C*D+E*C + max(0, -2+2*A*E*B*S^(-1/2)*C*D+E-2*A*B*S^(-1/2)*C*D-S+2*C+2*A*B*D-A*E*B*D-A*B*C*D-E*C)

%A*E*B*D+A*B*C*D+E*C

$2*n^4 + \max(0, -2*n^4+2*n^5*S^(-1/2)-S)$

When all params >> S:
2*A*E*B*S^(-1/2)*C*D+E+2*C+2*A*B*D
3*n+2*n^5*S^(-1/2)+2*n^3



A*E*B*D+A*B*C*D+E*C
2*A*E*B*S^(-1/2)*C*D
A*E*B*D+A*B*C*D+E*C+max(0,-2+2*A*E*B*S^(-1/2)*C*D+E-2*A*B*S^(-1/2)*C*D-S+2*C+2*A*B*D-A*E*B*D-A*B*C*D-E*C)
*/