
// Class 1
// Tensor contraction for abcde = efbad * cf
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D, int E, int F,
   double Out[A][B][C][D][E],
   double I1[E][F][B][A][D],
   double I2[C][F])
{
  int a, b, c, d, e, f;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
  for (e = 0; e < E; e++) {
//    Out[a][b][c][d][e] = 0.0;
  for (f = 0; f < F; f++) {
    Out[a][b][c][d][e] += I1[e][f][b][a][d] * I2[c][f];
  }
  }
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2-2*D*A*E*B*C*S^(-1/2)-D*A*E*B*F-D*A*E*B*C+2*D*A*E*B+F+2*D*A*E*B*F*C*S^(-1/2)+2*C-F*C-S
-2-n^2+3*n+2*n^6*S^(-1/2)+2*n^4-2*n^5*S^(-1/2)-2*n^5-S




Best lower bound:
-2-2*D*A*E*B*C*S^(-1/2)-D*A*E*B*F-D*A*E*B*C+2*D*A*E*B+F+2*D*A*E*B*F*C*S^(-1/2)+2*C-F*C-S

-2-n^2+3*n+2*n^6*S^(-1/2)+2*n^4-2*n^5*S^(-1/2)-2*n^5-S

2*n^6*S^(-1/2)-2*n^5-S


Complete lower bound:
D*A*E*B*F+D*A*E*B*C+F*C + max(0, -2-2*D*A*E*B*C*S^(-1/2)-D*A*E*B*F-D*A*E*B*C+2*D*A*E*B+F+2*D*A*E*B*F*C*S^(-1/2)+2*C-F*C-S)

%D*A*E*B*F+D*A*E*B*C+F*C

$2*n^5 + \max(0, 2*n^6*S^(-1/2)-2*n^5-S)$

When all params >> S:
2*D*A*E*B+F+2*D*A*E*B*F*C*S^(-1/2)+2*C
3*n+2*n^6*S^(-1/2)+2*n^4


D*A*E*B*F+D*A*E*B*C+F*C
2*D*A*E*B*F*C*S^(-1/2)
D*A*E*B*F+D*A*E*B*C+F*C+max(0,-2-2*D*A*E*B*C*S^(-1/2)-D*A*E*B*F-D*A*E*B*C+2*D*A*E*B+F+2*D*A*E*B*F*C*S^(-1/2)+2*C-F*C-S)
*/


