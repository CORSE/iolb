
// Class 4
// Tensor contraction for abcdef = dega * gfbc
void kernel_tc_ab_acd_dbc(int A, int B, int C, int D, int E, int F, int G,
   double Out[A][B][C][D][E][F],
   double I1[D][E][G][A],
   double I2[G][F][B][C])
{
  int a, b, c, d, e, f, g;
#pragma scop
  for (a = 0; a < A; a++) {
  for (b = 0; b < B; b++) {
  for (c = 0; c < C; c++) {
  for (d = 0; d < D; d++) {
  for (e = 0; e < E; e++) {
  for (f = 0; f < F; f++) {
  for (g = 0; g < G; g++) {
    Out[a][b][c][d][e][f] += I1[d][e][g][a] * I2[g][f][b][c];
  }
  }
  }
  }
  }
  }
  }
#pragma endscop
}

/* === Result - no small dims ===
Summed lower bounds:
-----------------------
-2+2*F*C*B-S+G+2*F*S^(-1/2)*C*G*D*A*E*B-2*F*S^(-1/2)*C*D*A*E*B-G*D*A*E-F*C*D*A*E*B+2*D*A*E-F*C*G*B
-2-S+n-2*n^4-n^6+2*S^(-1/2)*n^7-2*S^(-1/2)*n^6+4*n^3




Best lower bound:
-2+2*F*C*B-S+G+2*F*S^(-1/2)*C*G*D*A*E*B-2*F*S^(-1/2)*C*D*A*E*B-G*D*A*E-F*C*D*A*E*B+2*D*A*E-F*C*G*B

-2-S+n-2*n^4-n^6+2*S^(-1/2)*n^7-2*S^(-1/2)*n^6+4*n^3

-S-n^6+2*S^(-1/2)*n^7


Complete lower bound:
G*D*A*E+F*C*D*A*E*B+F*C*G*B + max(0, -2+2*F*C*B-S+G+2*F*S^(-1/2)*C*G*D*A*E*B-2*F*S^(-1/2)*C*D*A*E*B-G*D*A*E-F*C*D*A*E*B+2*D*A*E-F*C*G*B)

%G*D*A*E+F*C*D*A*E*B+F*C*G*B

$n^6 + \max(0, -S-n^6+2*S^(-1/2)*n^7)$

When all params >> S:
2*F*C*B+G+2*F*S^(-1/2)*C*G*D*A*E*B+2*D*A*E
n+2*S^(-1/2)*n^7+4*n^3



G*D*A*E+F*C*D*A*E*B+F*C*G*B
2*F*S^(-1/2)*C*G*D*A*E*B
G*D*A*E+F*C*D*A*E*B+F*C*G*B+max(0,-2+2*F*C*B-S+G+2*F*S^(-1/2)*C*G*D*A*E*B-2*F*S^(-1/2)*C*D*A*E*B-G*D*A*E-F*C*D*A*E*B+2*D*A*E-F*C*G*B)
*/



