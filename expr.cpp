/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include <iostream>
#include <sstream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <map>
#include <vector>
#include <string>
#include <cmath>
#include <cassert>
#include <ginac/ginac.h>
#include <ginac/container.h>

using namespace std;
using namespace GiNaC;

const double big_num = 1e12;

static lst param_instance;

static lst symbols;
static ex *param_S = NULL; //expression representing the pebble count 'S'. In case the input program has a parameter named 'S', to prevent confliciting with it, we use a special expression for pebble count 'S'

struct bounds_cpp {
	ex *lb, *ub;
};

extern "C" ex * expr_new(const char * const sym)
{
	return new ex(sym, symbols);
}

extern "C" ex * expr_new_frac(long numer, long denom)
{
	return new ex(numeric(numer, denom));
}


extern "C" void expr_free(ex *e)
{
	delete e;
}

//Returns the special expression for the pebble count 'S'
extern "C" ex* expr_S()
{
	if(param_S == NULL)
		param_S = new ex(constant("S"));
	return param_S;
}

extern "C" void expr_symbol(const char * const sym)
{
	string s(sym);
	//Check if the symbol is already present in the list. If not, add it to the list.
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b)
	{
		if(ex_to<possymbol>(*b).get_name() == s)
			return;
	}
	symbols.prepend(possymbol(s));
}

ex* expr_simplify(const ex &orig, bool bS = false)
{
#if (VERBOSE > 3)
	cout << "\nSimplifying expr: " << orig << "\n";
#endif
	try
	{
		ex orig_expr = orig.expand(); //Add 1 to make it an add expression
		if(is_a<numeric>(orig_expr))
		{
			return (new ex(orig));
		}
		if(!is_a<add>(orig_expr)) //to handle case where orig_expr already has a "-1" 
			return (new ex(orig));
		assert(is_a<add>(orig_expr));
		//ex *new_expr = new ex();
		ex new_expr;
		//if(orig_expr.nops() == 0 || is_a<power>(orig_expr)) //case where orig_expr contains just single term
		//{
		//	*new_expr = orig_expr/orig_expr.integer_content();
		//}
		//else
		{
			//Take all pairs of terms (b1,b2) from the original expression. If b1/b2 is
			//of the form 'const'/'symbol', then there is a higher ordered term 'b2' than
			//'b1'. So don't include 'b1' into final expression.
			ex temp_expr;
			for(const_iterator b1=orig_expr.begin(), e1=orig_expr.end(); b1!=e1; ++b1)
			{
				bool flag = true;
				for(const_iterator b2=orig_expr.begin(), e2=orig_expr.end(); b2!=e2; ++b2)
				{
					ex div = (*b1)/(*b2);
					ex numer = div.numer();
					ex denom = div.denom();
					//if((numer==numer.integer_content()) && (denom!=denom.integer_content()))
					ex S = *expr_S();
					//if(bS)
					//cout << numer.subs(S == (bS? 1 : S)) << "   " << is_a<numeric>(numer.subs(S == (bS? 1 : S)).evalf()) << "      " << denom << '\n';
					symbol x("x");

					if(is_a<numeric>(numer.subs(S == (bS? 1 : ex(x))).evalf()) && !is_a<numeric>(denom))
					{
//					cout << numer.subs(S == (bS? 1 : ex(x))) << "   " << numer.evalf() << "   " << is_a<numeric>(numer.subs(S == (bS? 1 : S)).evalf()) << "      " << denom << '\n';
						flag = false;
						break;
					}
				}
				if(flag==true)
				{
					//Remove the co-efficient and add to final expression
					//(*new_expr) += ((*b1)/(*b1).integer_content());
					temp_expr += (*b1);
				}
			}
			if(is_a<add>(temp_expr))
			{
				for(const_iterator b1=temp_expr.begin(), e1=temp_expr.end(); b1!=e1; ++b1)
				{
					new_expr += (*b1);
				}
			}
			else
				new_expr = temp_expr;
		}
#if (VERBOSE > 3)
		cout << "Simplified expr: " << new_expr << "\n";
#endif
		return (new ex(new_expr));
	}
	catch(...)
	{
		cerr << "Unable to symplify due to exception. Using the original expression as is.\n";
		return (new ex(orig));
	}

}

// Declared in "utility.h"
extern "C" {
	bool is_small_dim(const char* name_param);
}

extern "C" ex* expr_params_equal(const ex * const expr)
{
	expr_symbol("n");
	ex ex_n;
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b) {
		if(ex_to<possymbol>(*b).get_name() == "n")
			ex_n = *b;
	}

	ex sub_ex(*expr);
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b) {
		assert(is_a<symbol>(*b));

		std::ostringstream b_str;
		print_dflt b_dflt = print_dflt(b_str);
		b->print(b_dflt);

		const char* str_b = b_str.str().c_str();

		// Do not substitute if "*b" small parameter
		if (is_small_dim(str_b)) {
			continue;
		}

		sub_ex = sub_ex.subs(*b == ex_n, subs_options::algebraic);
	}
	return (new ex(sub_ex));
}

extern "C" ex* expr_normalize(const ex * const expr) {
	return (new ex(expr->normal()));
}


extern "C" ex* expr_simplify_str(const char * const orig)
{
	ex orig_expr(orig, symbols);
	return expr_simplify(orig_expr);
}

extern "C" ex* expr_simplify_expr(const ex * const orig)
{
	return expr_simplify(*orig);
}

extern "C" ex* expr_simplify_S(const ex * const orig)
{
	return expr_simplify(*orig, true);
}

extern "C" void expr_print_to_stream(const ex * const expr, FILE *out_stream)
{
	std::ostringstream expr_str;
#if defined(LATEX)
	print_latex c = print_latex(expr_str);
#else
	print_dflt c = print_dflt(expr_str);
#endif
	expr->print(c);
	fprintf(out_stream, "%s", expr_str.str().c_str());
}

extern "C" void expr_print_symbols(FILE *out_stream)
{
   auto b = symbols.begin(), e = symbols.end();
   if(param_S != NULL)
      expr_print_to_stream(param_S, out_stream);
   else
      fprintf(out_stream, "S");
   for(auto i = b; i != e; ++i) {
      fprintf(out_stream, ", ");
      expr_print_to_stream(&(*i), out_stream);
   }
}

extern "C" void expr_print(const ex * const expr)
{
	expr_print_to_stream(expr, stdout);
}

extern "C" bool expr_is_equal(const ex * const ex1, const ex * const ex2)
{
	return (*ex1 == *ex2);
}

extern "C" ex* expr_pow(const ex * const ex1, const ex * const exp)
{
	return (new ex(pow(*ex1, *exp)));
}

extern "C" ex* expr_add(const ex * const ex1, const ex * const ex2)
{
	return (new ex((*ex1)+(*ex2)));
}

extern "C" ex* expr_sub(const ex * const ex1, const ex * const ex2)
{
	return (new ex((*ex1)-(*ex2)));
}

extern "C" ex* expr_mul(const ex * const ex1, const ex * const ex2)
{
	return (new ex((*ex1)*(*ex2)));
}

extern "C" ex* expr_div(const ex * const ex1, const ex * const ex2)
{
	return (new  ex((*ex1)/(*ex2)));
}



extern "C" int expr_dim_aux(const ex ex1)
{
	ex sub_ex(ex1);
	//Substitute each symbol by a big number 'B'
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b)
	{
		sub_ex = sub_ex.subs(*b == big_num);
	}
	//Find log_B(sub_ex)
#ifndef NDEBUG
	//Make sure that the substituted expression is not symbolic.
	ex numer = sub_ex.numer();
	ex denom = sub_ex.denom();
	//assert((numer==numer.integer_content()) && (denom==denom.integer_content()));
	assert(is_a<numeric>(numer) && is_a<numeric>(denom));
#endif
	double value = ex_to<numeric>(sub_ex).to_double();
	double dim_d;
	int dim_i;
	if(value > 0)
	{
		dim_d = log(value)/log(big_num);
		assert(dim_d >= 0.0 || !(std::cout << "Failed due to value: " << value << "; dim_d: " << dim_d << "\n"));
		dim_i = floor(dim_d + 0.5);
	}
	else
	{
		dim_d = 0;
		dim_i = 0;
	}
#if (VERBOSE > 3)
	cout << "Dimension of expr " << ex1 << ": " << dim_d << " (=" << dim_i << ")\n";
#endif

	return dim_i;
}

extern "C" int expr_dim(const ex* const ex1) {
	// DEBUG
	//cout << "expr_dim ex1 = " << (*ex1) << "\n";

	// If ex1 is a sum of several expresion, iterate over them and take the max
	// Doc: https://www.ginac.de/tutorial/#Checking-expression-types
	if (is_a<add>(*ex1)) {
		int card_dim = 0;
		for (size_t i=0; i<ex1->nops(); i++) {
			ex sexpr = ex1->op(i);

			// DEBUG
			//cout << "expr_dim - subexpr sexpr = " << sexpr << "\n";

			// Checking its value
			int ncard_dim = expr_dim_aux(sexpr);

			// Keeping the max
			if (ncard_dim > card_dim) {
				card_dim = ncard_dim;
			}
		}
		return card_dim;
	} else {
		return expr_dim_aux(*ex1);
	}
}



//if there exists an x>0 s.t. ex1^x == ex2, returns x
static int expr_get_power(const ex * const ex1, const ex * const ex2)
{
	if(is_a<power>(*ex1) == false)
		return -1;
	if((*ex1).nops() != 2 || !is_a<numeric>((*ex1).op(1)) || ((*ex1).op(1)<=0)) //third test bug
		return -1;
	if(!is_a<power>(*ex2))
	{
		if((*ex1).op(0) == (*ex2))
			return (*ex1).op(1).integer_content().to_int();
	}
	else
	{
		if((*ex1).op(0) == (*ex2).op(0)) 
		{
			ex pow_diff = (*ex1).op(1) - (*ex2).op(1); 
			if(!is_a<numeric>(pow_diff))
				return -1;
			if(pow_diff > 0)
				return pow_diff.integer_content().to_int();
		}
	}
	return -1;
}

extern "C" void expr_to_params(const ex ** const exprs, size_t n_exprs, ex ***params, size_t *n_params, size_t ** const param_idx, int ** const param_pow, size_t * const param_len)
{
	*n_params = 0;
	*params = (ex**)malloc(n_exprs* sizeof(ex*));
	int *pow = (int*)malloc(n_exprs * sizeof(int));
	int *pos = (int*)malloc(n_exprs * sizeof(int));
	for(size_t i=0; i<n_exprs; ++i) 
	{
		param_idx[i] = (size_t*)malloc(sizeof(size_t));
		param_pow[i] = (int*)malloc(sizeof(int));
		param_pow[i][0] = 1;
		param_len[i] = 1;
		size_t j=0;
		for(; j<i; ++j)
		{
			if(*(exprs[i]) == *(exprs[j]))
			{
				param_idx[i][0] = param_idx[j][0];
				break;
			}
		}
		pow[i] = -1;
		pos[i] = -1;
		for(size_t k=0; k<n_exprs; ++k)
		{
			int curr_pow = expr_get_power(exprs[i], exprs[k]);
			if(curr_pow > pow[i])
			{
				pow[i] = curr_pow;
				pos[i] = k;
			}
		}
		if(i==j && pow[i]==-1)
		{
			param_idx[i][0] = (*n_params);
			(*params)[*n_params] = new ex(*(exprs[i]));
			++(*n_params);
		}
	}
	for(size_t i=0; i<n_exprs; ++i)
	{
		if(pow[i] != -1)
		{
			param_idx[i][0] = param_idx[pos[i]][0];
			param_pow[i][0] = pow[i];
		}
	}
	free(pow);
	free(pos);
	//*params = (ex**)realloc(*params, *n_params);
}

static size_t get_container_idx(GiNaC::container<std::vector>::const_iterator b, GiNaC::container<std::vector>::const_iterator e, ex &expr)
{
	size_t cnt=0;
	for(; b!=e; ++b, ++cnt)
	{
		if(*b == expr)
			return cnt;
	}
	assert(false); //Should not reach here
}

extern "C" void expr_reduce_params(const ex ** const exprs, size_t n_exprs, ex ***params, size_t *n_params, size_t ** const param_idx, int ** const param_pow, size_t * const param_len)
{
	container<std::vector> C;
	for(size_t i=0; i<n_exprs; ++i)
	{
		const ex* curr_expr = exprs[i];
		if(!is_a<mul>(*curr_expr))
		{
			if(is_a<power>(*curr_expr))
				C.append(curr_expr->op(0));
			else
				C.append(*curr_expr);
		}
		else
		{
			for(const_iterator b1=curr_expr->begin(), e1=curr_expr->end(); b1!=e1; ++b1)
			{
				if(is_a<power>(*b1))
					C.append((*b1).op(0));
				else
					C.append(*b1);
			}
		}
	}
	C = C.unique();
	cout << "Unique container: " << C << "\n";
	*n_params = C.nops();
	*params = (ex**)malloc(*n_params * sizeof(ex*));
	for(size_t i=0; i<*n_params; ++i)
		(*params)[i] = new ex(C.op(i));
	for(size_t i=0; i<n_exprs; ++i)
	{
		const ex* curr_expr = exprs[i];
		if(!is_a<mul>(*curr_expr))
		{
			param_len[i] = 1;
			param_idx[i] = (size_t*)malloc(sizeof(size_t));
			param_pow[i] = (int*)malloc(sizeof(int));
			if(is_a<power>(*curr_expr))
			{
				ex temp_ex = curr_expr->op(0);
				param_idx[i][0] = get_container_idx(C.begin(), C.end(), temp_ex);
				param_pow[i][0] = curr_expr->op(1).integer_content().to_int();
			}
			else
			{
				ex temp_ex = *curr_expr;
				param_idx[i][0] = get_container_idx(C.begin(), C.end(), temp_ex);
				param_pow[i][0] = 1;
			}
		}
		else
		{
			vector<size_t> idx;
			vector<int> pow;
			for(const_iterator b1=curr_expr->begin(), e1=curr_expr->end(); b1!=e1; ++b1)
			{
				if(is_a<power>(*b1))
				{
					ex temp_ex = b1->op(0);
					idx.push_back(get_container_idx(C.begin(), C.end(), temp_ex));
					pow.push_back(b1->op(1).integer_content().to_int());
				}
				else
				{
					ex temp_ex = *b1;
					idx.push_back(get_container_idx(C.begin(), C.end(), temp_ex));
					pow.push_back(b1->integer_content().to_int());
				}
			}
			size_t s = idx.size();
			param_len[i] = s;
			param_idx[i] = (size_t*)malloc(s*sizeof(size_t));
			param_pow[i] = (int*)malloc(s*sizeof(int));
			for(size_t j=0; j<s; ++j)
			{
				param_idx[i][j] = idx[j];
				param_pow[i][j] = pow[j];
			}
		}
	}
}

extern "C" bool expr_is_zero(const ex * const expr)
{
	return (*expr == 0);
}

extern "C" bool expr_is_numeric(const ex * const expr)
{
	return is_a<numeric>(*expr);
}

extern "C" ex* expr_one()
{
	return (new ex(1));
}

extern "C" int expr_to_int(const ex * const expr)
{
	assert(is_a<numeric>(*expr));
	numeric n = ex_to<numeric>(*expr);
	assert(n.is_pos_integer());
	return n.to_int();
}

extern "C" ex* expr_expand(const ex* const expr)
{
	return (new ex(expr->expand()));
}

extern "C" ex* expr_factor(const ex* const expr)
{
	return (new ex(collect_common_factors(*expr)));
}

bool is_neg(const ex& e)
{
	ostringstream s;
	s << e << ends;
	return s.str()[0] == '-';
}

// Note: only works on affine expressions (iterate over the top-most summation to determine
//		positivity, i.e., ub or lb to be used)
extern "C" ex* remove_new_params(const ex* const expr, const bounds_cpp* const bnds, size_t dim)
{
	//when getting the cardinal of the preimage, we want to get an 
	//expression free of any "my{i}" variables, which is an underestimate
	//So we remove any positive term with "my{i}" and overestimate them
	//by -n for negative terms

	exmap sub_lb, sub_ub;
	assert(dim<=9);		// Management of the "myX" name in the next loop breaks if dim>9
	for(size_t i=0; i<dim; i++) {
		string svar("myi");
		svar[2] = '0' + i;
		// Ex: svar = "my5" for "i=5"

		expr_symbol(svar.c_str());	// Register the new symbol to Ginac
		ex myi(svar, symbols);		// Create the new Ginac expression "my[i]"

		sub_lb[myi] = *bnds[i].lb;	// First bound
		sub_ub[myi] = *bnds[i].ub; // Second bound
	}

	/* DEBUG
	for (size_t i=0; i<dim; i++) {
		string svar("myi");
		svar[2] = '0' + i;
		ex temp_myi(svar, symbols);
		cout << "sub_lb[my" << i << "] = " << sub_lb[temp_myi] << "\n";
		cout << "sub_ub[my" << i << "] = " << sub_ub[temp_myi] << "\n";
	}
	//*/

	
	ex e = expr->expand();
	//cout << "Input expr: " << e << '\n';

	//if(dim == 2) 
	//	e = e.subs(ex("my0", symbols) == ex("n/2 - 1", symbols));		// WHYYYYYY ????

	if(!is_a<add>(e)) {
		if(is_neg(e))
			return (new ex(e.subs(sub_ub, subs_options::algebraic)));
		else
			return (new ex(e.subs(sub_lb, subs_options::algebraic)));
	}
	ex new_e(0);
	for(const ex& f : e) {
		//cout << "term " << f;
		if(is_neg(f)) {
			new_e += f.subs(sub_ub, subs_options::algebraic);
			//cout << "  neg->  " << f.subs(sub_ub, subs_options::algebraic) << '\n';
		}
		else {
			new_e += f.subs(sub_lb, subs_options::algebraic);
			//cout << "  pos->  " << f.subs(sub_lb, subs_options::algebraic) << '\n';
		}
	}
	new_e = new_e.expand();
	//cout << "After removal: " << new_e << '\n';
	return new ex(new_e);
}

extern "C" ex* get_min_expr_card(const ex* const expr, const bounds_cpp* const bnds,
		const char** arr_name_dim, int dim) {
	// Preparing the substitutions
	exmap sub_lb, sub_ub;
	for(int i=0; i<dim; i++) {
		string name_dim = arr_name_dim[i] ;

		expr_symbol(name_dim.c_str());	// Register the new symbol to Ginac
		ex ex_name_dim(name_dim, symbols);

		sub_lb[ex_name_dim] = *bnds[i].lb;	// Lower bound of dim
		sub_ub[ex_name_dim] = *bnds[i].ub;  // Upper bound of dim
	}
	ex e = expr->expand();

	// Single atom case
	if(!is_a<add>(e)) {
		if (is_neg(e)) {
			return (new ex(e.subs(sub_ub, subs_options::algebraic)));
		} else {
			return (new ex(e.subs(sub_lb, subs_options::algebraic)));
		}
	}

	// This is an "add" => iterate over the subterms
	ex new_e(0);
	for(const ex& f : e) {
		if(is_neg(f)) {
			new_e += f.subs(sub_ub, subs_options::algebraic);
		}
		else {
			new_e += f.subs(sub_lb, subs_options::algebraic);
		}
	}
	new_e = new_e.expand();
	return new ex(new_e);
}



extern "C" bool expr_is_greater(const ex* const e1, const ex* const e2)
{
	ex* diff = expr_simplify(*e1 - *e2);
	if(!is_a<add>(*diff))
		return !is_neg(*diff);
	for(const ex& f : *diff) 
		if(is_neg(f))
			return false;
	return true;
}

extern "C" double expr_evaluate(const ex* const e, double x)
{
	ex sub_ex(*e);
	//Substitute each symbol by a big number 'B'
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b) {
		sub_ex = sub_ex.subs(*b == x);
	}
	sub_ex = sub_ex.subs(*expr_S() == sqrt(x));
	sub_ex = evalf(sub_ex);
	//cout << sub_ex << endl;
	return ex_to<numeric>(sub_ex).to_double();
}

extern "C" ex* expr_partial_evaluate(const ex* const e, char* sym, double x) {
	ex sub_ex(*e);
	
	// Recover the symbol of "symbols" that matches "sym":
	ex ex_sym;
	for(GiNaC::lst::const_iterator b=symbols.begin(), e=symbols.end(); b!=e; ++b) {
		assert( is_a<symbol>(*b) );
		symbol sym_b = ex_to<symbol>(*b);
		if ( sym_b.get_name().compare(sym) == 0) {
			ex_sym = *b;
			//cout << "PING !!! sym_b = " << sym_b << " | x = " << x << endl;
		}
	}

	// OLD: symbol s_sym = possymbol(sym);
	sub_ex = sub_ex.subs(ex_sym == x);
	return (new ex(sub_ex));
}


extern "C" ex* expr_remove_negative(const ex* e)
{
	if(!is_a<add>(*e))
		return is_neg(*e) ? (new ex(0)) : (new ex(*e));
	ex ret(0);
	for(const ex& f : *e)
		if(!is_neg(f))
			ret += f;
	return (new ex(ret));
}

extern "C" void add_instance(const char* const param, int val)
{
   expr_symbol(param);
   ex ex_param(param, symbols);
   param_instance.prepend(ex_param == val);
}

extern "C" double expr_eval_instance(const ex* e, int S)
{
   ex ev = e->subs(param_instance);
   ev = ev.subs(*expr_S() == S, subs_options::algebraic);
   cout << param_instance << '\n';
   ev = evalf(ev);
   //cout << "DEBUG " << ev << endl;
   return ex_to<numeric>(ev).to_double();
}

