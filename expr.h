#ifndef EXPR_H
#define EXPR_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "utility.h"

typedef void expr; //expr* is used to point to GiNaC::ex object

struct bounds {
	expr *lb, *ub;
};
typedef struct bounds bounds;

expr* expr_new_frac(const long numer, const long denom);
expr* expr_new(const char * const str);
void expr_free(void *ex);
expr* expr_S();
void expr_symbol(const char * const sym);
expr* expr_normalize(const expr * const expr) ;
expr* expr_simplify_str(const char * const orig);
expr* expr_simplify_expr(const expr * const orig);
expr* expr_simplify_S(const expr * const orig);
bool expr_is_equal(const expr * const ex1, const expr * const ex2);
void expr_print(const expr * const expr);
void expr_print_to_stream(const expr * const expr, FILE *out_stream);
void expr_print_symbols(FILE *out_stream);
expr* expr_add(const expr * const ex1, const expr * const ex2);
expr* expr_sub(const expr * const ex1, const expr * const ex2);
expr* expr_mul(const expr * const ex1, const expr * const ex2);
expr* expr_div(const expr * const ex1, const expr * const ex2);
expr* expr_pow(const expr * const ex1, const expr * const exp);
int expr_dim(const expr* const ex);
void expr_reduce_params(const expr ** const exprs, size_t n_exprs, expr ***params, size_t *n_params, size_t ** const param_idx, int ** const param_pow, size_t * const param_len);
void expr_to_params(const expr ** const exprs, size_t n_exprs, expr ***params, size_t *n_params, size_t ** const param_idx, int ** const param_pow, size_t * const param_len);
bool expr_is_zero(const expr ** const);
expr* expr_one();
int expr_to_int(const expr* const);
expr* expr_expand(const expr* const);
expr* expr_factor(const expr* const);
bool expr_is_numeric(const expr * const expr);
expr* remove_new_params(const expr* const, const bounds* bnds, size_t dim);
expr* get_min_expr_card(const expr* const expr, const bounds* bnds,
        const char** arr_name_dim, int dim);
expr* expr_params_equal(const expr* const);
bool expr_is_greater(const expr* const, const expr* const);
double expr_evaluate(const expr* const, double x );
expr* expr_partial_evaluate(const expr* const e, char* sym, double x);
expr* expr_remove_negative(const expr* const);
void add_instance(const char* param, int val);
double expr_eval_instance(const expr* const, int S);
#endif
