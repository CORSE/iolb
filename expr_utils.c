/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "expr_utils.h"
#include <isl/map.h>
#include <isl/aff.h>
#include <isl/multi.h>
#include <isl/mat.h>
#include <isl/polynomial.h>
#include <barvinok/isl.h>
#include <string.h>
#include <assert.h>
#include <math.h>


// Replace the erronous "isl_set_dim_has_any_upper_bound(param_set, isl_dim_param, i)"
// Indeed, situations like "N <= M" was triggering this function for "N"
bool param_is_upper_bounded(__isl_keep isl_set *s, int i) {
	// [Too Complicated algorithm whose idea got shot down]
	// Algo (assiming ith parameter is N):
	// * Get the matrices of constraint for s
	// * Assume no equality linking only parameters/constants and the one we are looking at
	// * Check the inequalities involving N:
	//	- If ... <= N (positive coefficient) : ignore
	//  - If N <= ... (negative coeff): check the positive coefficient (rhs)
	//		=> If there are no parameters: return FALSE
	//		=> If there are several parameter: return OR( recursive_call( [these_params] ) )
	// Warning with self loop (ex: N <= M <= 2N )
	// => Maintain a list of parameters that interacts with N and are/are not bounded
	
	// Now, we just do it with projections. Lots of them.

	isl_set *param_dom = isl_set_params(isl_set_copy(s));
	int n_params = isl_set_dim(param_dom, isl_dim_param);
	assert(i<n_params);

	// Project the [i+1; nparam[ parameters
	param_dom = isl_set_project_out(param_dom, isl_dim_param, (i+1), n_params-i-1);

	// Project the [0; i[ parameters
	if (i>0) {
		param_dom = isl_set_project_out(param_dom, isl_dim_param, 0, i);
	}

	// Now, check if the single remaining param has an upper-bound remaining
	bool is_upp_bounded = isl_set_dim_has_any_upper_bound(param_dom, isl_dim_param, 0);

	// Clean-up
	isl_set_free(param_dom);

	return is_upp_bounded;
}


// ===================
// ===================
// ===================

typedef struct card_data card_data;
struct card_data
{
	expr** card;
	int r;
	isl_set **dom_params;
};

// Convert an ISL qp into a Ginac expr (through the isl_printer + parsing of Ginac)
static expr* qpolynomial_to_expr(__isl_take isl_qpolynomial *qp, int rounding)
{
	isl_printer *p_str = isl_printer_to_str(isl_qpolynomial_get_ctx(qp));
	isl_printer_set_output_format(p_str, ISL_FORMAT_C);
	isl_printer_print_qpolynomial(p_str, qp);
	char *qp_str = isl_printer_get_str(p_str);
	//char *start_idx = strchr(qp_str, '{') + 1;
	//char *end_idx = strchr(qp_str, '}');
	//assert(end_idx >= start_idx);
	//*end_idx = '\0';
	//char *floor_idx = start_idx;
	char* start_idx = qp_str;

	//printf("\nDEBUG  %s\n", qp_str);

	expr *qp_expr = expr_expand(expr_new(start_idx));
	//printf("\nDEBUG  %s\n", qp_str);
	//expr_print(qp_expr);
	//printf("\n");

	free(qp_str);
	isl_printer_free(p_str);
	isl_qpolynomial_free(qp);

	return qp_expr;
}

// Applied on each branch of the piecewise polynomial that [over/under]-approximate the cardinality
static int add_cardinality(__isl_take isl_set *s, __isl_take isl_qpolynomial *q, void *card_v)
{
	card_data* cd = (card_data*) card_v;
	expr **card = cd->card;
	int rounding = cd->r;

	//only keep the the unbounded piece
	if(*card != NULL) {
		isl_set* param_set = get_param_constraints(isl_union_set_from_set(isl_set_copy(s)));
		param_set = isl_set_remove_divs(param_set);
		int npar = isl_set_dim(param_set, isl_dim_param);
		for(int i=0; i<npar; i++)
			if(param_is_upper_bounded(param_set, i)) {
				isl_set_free(param_set);
				isl_set_free(s);
				return 0;
			}
	}

	isl_set **dom_params = cd->dom_params;
	if(dom_params != NULL) {
		*dom_params = s;
	}

    /*
    printf("DEBUG\n");
    PRINT_ISL(p_out, isl_printer_print_set, s);
    PRINT_ISL(p_out, isl_printer_print_qpolynomial, q);
    printf("\n\n");
    */
	expr *curr_card = qpolynomial_to_expr(q, rounding);
	assert(curr_card != NULL);
	*card = curr_card;

	return 0;
}

//rounding : 1 gives upper bound on card, 0 lower bound
static expr* get_cardinality(__isl_take isl_pw_qpolynomial *poly, int rounding, __isl_give isl_set** dom_params)
{
	expr *card = NULL;

	// Register all parameters to ginac symbol list
	isl_space *sp = isl_pw_qpolynomial_get_space(poly);
	size_t n_params = isl_space_dim(sp, isl_dim_param);
	for(size_t i=0; i<n_params; ++i) {
		// Register the param name
		assert(isl_space_has_dim_name(sp, isl_dim_param, i));
		expr_symbol(isl_space_get_dim_name(sp, isl_dim_param, i));
	}
	isl_space_free(sp);

	// Return card of first piece
	card_data cd;
	cd.card = &card;	// Ref ici
	cd.r = rounding;
	cd.dom_params = dom_params;

	// [From doc ISL 0.14]
	// "Approximate each quasipolynomial by a polynomial. If sign is positive, the poly-
	//nomial will be an overapproximation. If sign is negative, it will be an underapproxi-
	//mation. If sign is zero, the approximation will lie somewhere in between"
	// "pw" = piecewise
	poly = isl_pw_qpolynomial_to_polynomial(poly, rounding == 1 ? 1 : -1);
	if(isl_pw_qpolynomial_foreach_piece(poly, add_cardinality, &cd) < 0) {
		// This part is triggered if an error code (return value < 0) is triggered by add_cardinality
		printf("Unable to get cardinality of pw_qpolynonial: parts have different cardinalities\n");
		PRINT_ISL(p_out, isl_printer_print_pw_qpolynomial, poly);
		fflush(stdout);
		return NULL;
	}

	isl_pw_qpolynomial_free(poly);
	if(card == NULL)
		return NULL;

	//expr *simplified_card = expr_simplify_expr(card);
	//expr_free(card);
	return card;
}

expr* get_basic_set_cardinality(__isl_take isl_basic_set *s, int rounding, isl_set **dom_params)
{
	if(s==NULL)
		return NULL;
	return get_cardinality(isl_basic_set_card(s), rounding, dom_params);
}
expr* get_set_cardinality(__isl_take isl_set *s, int rounding, isl_set **dom_params)
{
	if (isl_set_is_empty(s)) {
		return expr_new("0");
	}

	if(s==NULL)
		return NULL;
	return get_cardinality(isl_set_card(s), rounding, dom_params);
}

static int add_union_cardinality(__isl_take isl_pw_qpolynomial *q, void *card_v)
{
	card_data *cd = card_v;

	expr *q_card = get_cardinality(q, 0, cd->dom_params);
	if(*cd->card == NULL)
		*cd->card = q_card;
	else
	{
		if(!expr_is_equal(q_card, *cd->card)) {
			expr_free(q_card);
			return -1;
		}
		expr_free(q_card);
	}
	return 0;
}

expr* get_union_set_cardinality(__isl_take isl_union_set *s, int rounding, isl_set **dom_params)
{
	if(s == NULL)
		return NULL;
	expr *card = NULL;
	card_data cd = {.card = &card, .r = rounding, .dom_params = dom_params};
	isl_union_pw_qpolynomial *poly = isl_union_set_card(s);
	poly = isl_union_pw_qpolynomial_coalesce(poly);
	if(isl_union_pw_qpolynomial_foreach_pw_qpolynomial(poly, add_union_cardinality, &cd) < 0) {
		printf("Unable to get cardinality of union set: parts have different cardinalities\n");
		return NULL;
	}
	isl_union_pw_qpolynomial_free(poly);
	if(card == NULL)
		return NULL;
	//expr *simplified_card = expr_simplify_expr(card);
	//expr_free(card);
	return card;
}

// ===================
// ===================
// ===================


int find_expr_dim(expr *ex)
{
	if(ex==NULL)
		return 0;
	return expr_dim(ex);
}

int find_basic_set_dim(__isl_take isl_basic_set *s)
{
	expr *card_expr = get_basic_set_cardinality(s, 0, NULL);
	int dim = find_expr_dim(card_expr);
	expr_free(card_expr);
	return dim;
}

int find_set_dim(__isl_take isl_set *s)
{
	/* DEBUG
	printf("DEBUG - find_set_dim | s = ");
	PRINT_ISL(p_out, isl_printer_print_set, s);
	printf("\n");
	fflush(stdout);
	//*/

	expr *card_expr = get_set_cardinality(s, 0, NULL);

	/* DEBUG
	printf("DEBUG - expr_utils :: find_set_dim | card_expr = ");
	expr_print(card_expr);
	printf("\n");
	fflush(stdout);
	//*/

	int dim = find_expr_dim(card_expr);
	expr_free(card_expr);
	return dim;
}

int find_union_set_dim(__isl_take isl_union_set *s)
{
	expr *card_expr = get_union_set_cardinality(s, 0, NULL);
	int dim = find_expr_dim(card_expr);
	expr_free(card_expr);
	return dim;
}

bool is_vector_equal(const double * const v1, const double * const v2, size_t n)
{
	if(v1 == v2)
		return true;
	double len1=0., len2=0., dot_prod=0.;
	for(size_t i=0; i<n; ++i)
	{
		len1 += (v1[i]*v1[i]);
		len2 += (v2[i]*v2[i]);
		dot_prod += (v1[i]*v2[i]);
	}
	dot_prod = pow(dot_prod, 2);
	if(fabs(len1*len2 - dot_prod) < 0.0001)
		return true;

	return false;
}

bool is_vector_in(const double ** const mat, const double * const v, size_t m, size_t n)
{
	for(size_t i=0; i<m; ++i)
	{
		if(is_vector_equal(mat[i], v, n))
			return true;
	}
	return false;
}


expr* constraint_to_expr(__isl_take isl_constraint *c)
{
	//printf("DEBUG:");
	//PRINT_ISL(p_out, isl_printer_print_constraint, c);
	//printf("\n");

	//Insert all parameters to ginac symbol list
	isl_space *sp = isl_constraint_get_space(c);
	size_t n_params = isl_space_dim(sp, isl_dim_param);
	for(size_t i=0; i<n_params; ++i)
	{
		assert(isl_space_has_dim_name(sp, isl_dim_param, i));
		expr_symbol(isl_space_get_dim_name(sp, isl_dim_param, i));
	}
	isl_space_free(sp);

	isl_printer *p_str = isl_printer_to_str(isl_constraint_get_ctx(c));
	isl_printer_print_constraint(p_str, c);
	char *c_str = isl_printer_get_str(p_str);
	char *start_idx = strchr(c_str, '=') + 1;
	char *end_idx = strchr(c_str, '}');
	assert(end_idx >= start_idx);
	*end_idx = '\0';

	isl_constraint_free(c);
	expr *c_expr = expr_new(start_idx);
	return c_expr;
}

int constraint_to_bound(__isl_take isl_constraint *c, void *v_b)
{
	//printf("DEBUG:");
	//PRINT_ISL(p_out, isl_printer_print_constraint, c);
	//printf("\n");
	if(!isl_constraint_involves_dims(c, isl_dim_set, 0, 1))
		return 0;
	bounds* b = v_b;
	if(isl_constraint_is_equality(c)) {
		b->ub = constraint_to_expr(isl_constraint_copy(c));
		b->lb = constraint_to_expr(c);
		return 0;
	}
	else if(isl_constraint_is_lower_bound(c, isl_dim_set, 0)) {
		b->lb = constraint_to_expr(c);
		return 0;
	}
	else if(isl_constraint_is_upper_bound(c, isl_dim_set, 0)) {
		b->ub = constraint_to_expr(c);
		return 0;
	}
	return -1;
}

// bnds is the output : array (of size dim) of bounds (cf expr.h : lb/ub both expr)
void get_dim_bounds(__isl_take isl_set *dom, int dim, bounds *bnds)
{
#if (VERBOSE > 4)
	printf("Computing dimension bounds for:");
	PRINT_ISL(p_out, isl_printer_print_set, dom);
	printf("\n");
#endif
	for(int i=0; i<dim; i++) {
		isl_set *proj_i = isl_set_copy(dom);
	    if(i > 0)
			proj_i = isl_set_project_out(proj_i, isl_dim_set, 0, i);
		if(i < dim-1)
			proj_i = isl_set_project_out(proj_i, isl_dim_set, 1, dim-i-1);
		isl_basic_set *proj_hull = isl_set_simple_hull(proj_i);
		//proj_hull = isl_basic_set_drop_constraints_not_involving_dims(proj_hull, isl_dim_set, 0, 1);

		isl_basic_set_foreach_constraint(proj_hull, constraint_to_bound, &bnds[i]);
		assert(bnds[i].ub != NULL && bnds[i].lb != NULL);
		
#if (VERBOSE > 4)
		printf("simple hull for dimension #%d:", i);
		PRINT_ISL(p_out, isl_printer_print_basic_set, proj_hull);
		printf("bounds : lb "); expr_print(bnds[i].lb);
		printf("  ub "); expr_print(bnds[i].ub);
		printf("\n\n");
#endif
	}
}

static int set_get_param_constraints(__isl_take isl_set *s, void* v_ps)
{
	isl_set** ps = v_ps;
	//PRINT_ISL(p_out, isl_printer_print_set, isl_set_params(s));
	*ps = isl_set_coalesce(isl_set_intersect(*ps, isl_set_params(s)));
	if(*ps == NULL)
		return -1;
	return 0;
}

__isl_give isl_set* get_param_constraints(__isl_keep isl_union_set *s)
{
	isl_set* ps = isl_set_universe(isl_union_set_get_space(s));
	isl_union_set_foreach_set(s, set_get_param_constraints, &ps);
	return ps;
}

// ===================
// ===================
// ===================

// Auxilliary function: tell us if setting all parameter to the same value is feasible
//		in respect to the constraints on the parameters
bool param_dom_equality_possible(isl_set* param_set) {
	isl_ctx* ctx = isl_set_get_ctx(param_set);
	isl_space* space_param = isl_set_get_space(param_set);
	int num_param = isl_set_dim(param_set, isl_dim_param);

	// DEBUG
	//PRINT_ISL(p_out, isl_printer_print_set, param_set);
	//printf("\n");
	//fflush(stdout);


	// We build the parameter diagonal basic set, which is basically "N=M=K=..."
	//		where N,M,K, ... are the parameters
	// str ="{[i] : i=N and i=M and i=K and ... }";
	// Note: 200 is arbitrary
	char str_bset_diag[200] = "[";
	for (int i=0; i<num_param; i++) {
		if (i>0) {
			strcat(str_bset_diag, ",");
		}
		const char* str_name_param = isl_space_get_dim_name(space_param, isl_dim_param, i);
		strcat(str_bset_diag, str_name_param);
	}
	strcat(str_bset_diag, "] -> { [i] : ");

	for (int i=0; i<num_param; i++) {
		if (i>0) {
			strcat(str_bset_diag, " and ");
		}

		strcat(str_bset_diag, "i = ");
		const char* str_name_param = isl_space_get_dim_name(space_param, isl_dim_param, i);
		strcat(str_bset_diag, str_name_param);
	}
	strcat(str_bset_diag, " }");

	/* DEBUG
	printf("str_bset_diag = %s\n", str_bset_diag);
	fflush(stdout);
	//*/

	isl_basic_set* bset_diag = isl_basic_set_read_from_str(ctx, str_bset_diag);

	// Intersect it with param_set and check the emptyness
	isl_set* set_diag = isl_set_from_basic_set(bset_diag);

	isl_set* param_set_with_i = isl_set_add_dims(isl_set_copy(param_set),
		isl_dim_set, 1);

	/* DEBUG
	printf("param_set_with_i = ");
	PRINT_ISL(p_out, isl_printer_print_set, param_set_with_i);
	fflush(stdout);
	//*/

	isl_set* inter_set = isl_set_intersect(set_diag, param_set_with_i);

	return isl_set_is_empty(inter_set);
}

