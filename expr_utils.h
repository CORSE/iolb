#ifndef EXPR_UTILS_H
#define EXPR_UTILS_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include <isl/set.h>
#include <isl/printer.h>
#include "utility.h"
#include "expr.h"

bool param_is_upper_bounded(__isl_keep isl_set *s, int i);

//rounding : 1 gives upper bound on card, 0 lower bound
expr* get_basic_set_cardinality(__isl_take isl_basic_set*, int rounding, __isl_give isl_set **dom_params);
expr* get_set_cardinality(__isl_take isl_set*, int rounding, __isl_give isl_set **dom_params);
expr* get_union_set_cardinality(__isl_take isl_union_set*, int rounding, __isl_give isl_set **dom_params);

int find_expr_dim(expr *ex);
int find_basic_set_dim(__isl_take isl_basic_set*);
int find_set_dim(__isl_take isl_set*);
int find_union_set_dim(__isl_take isl_union_set*);
bool is_vector_equal(const double * const v1, const double * const v2, size_t n);
bool is_vector_in(const double ** const mat, const double * const v, size_t m, size_t n);
void get_dim_bounds(__isl_take isl_set *dom, int dim, bounds *bnds);
__isl_give isl_set* get_param_constraints(__isl_keep isl_union_set *s);

bool param_dom_equality_possible(isl_set* param_set);

#endif
