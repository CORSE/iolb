#!/usr/bin/python

# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry, Guillaume Iooss
#    Date of creation: 2019 - 2021
#
#    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================


from sympy.parsing.sympy_parser import *
from sympy import *
import csv

S = Symbol('S')
def parse_str(s):
    return parse_expr(s, local_dict = {'S' : S},transformations=standard_transformations + (convert_xor,) )

def read_table(filename):
    tbl = []
    with open(filename, newline='') as tblfile:
        reader = csv.DictReader(tblfile, delimiter=';')
        for row in reader:
            d = {
                    'kernel' : row['kernel'],
                    'input' : row['input'],
                    'IOlb' : row['IOlb'],
                    'fIOlb' : row['fIOlb']
                }
            tbl.append(d)
    return tbl
            
def read_ops(filename, tbl):
    with open(filename, newline='') as tblfile:
        reader = csv.DictReader(tblfile, delimiter=';')
        i = 0
        for row in reader:
            tbl[i].update({ 'ops' : row['ops']})
            i += 1

def write_csv(filename, t):
    with open(filename, 'w', newline='') as f:
        fieldnames = ['kernel', 'input', 'ops', 'IOlb', 'fIOlb']
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()
        for row in t:
            writer.writerow(row)

def to_tex(f):
    g = f
    g = cancel(g)
    repl = [ ('tsteps', 'T'), ('tmax', 'T'), ('ni', 'N_i'), ('nj', 'N_j'), ('nk', 'N_k'), ('nl', 'N_l'), ('nm', 'N_m'),
            ('nx', 'N_x'), ('ny', 'N_y'), ('np', 'N_p'), ('nq', 'N_q'), ('nr', 'N_r'), ('ns', 'N_s'), 
            ('w', 'W'), ('h', 'H'), ('n', 'N'), ('m', 'M'), ('C', 'S') ]
    for a,b in repl:
        g = g.subs(symbols(a),symbols(b))
    s = str(latex(g, mode='inline'))
    if s == '$0$':
        return '$-$'
    return s

order = [
'2mm',
'3mm',
'cholesky',
'correlation',
'covariance',
'doitgen',
'fdtd-2d',
'floyd-warshall',
'gemm',
'gramschmidt',
'heat-3d',
'jacobi-1d',
'jacobi-2d',
'lu',
'ludcmp',
'nussinov',
'seidel-2d',
'symm',
'syr2k',
'syrk',
'trmm',
'atax',
'bicg',
'deriche',
'gemver',
'gesummv',
'mvt',
'trisolv',
'adi',
'durbin',
]

def gen_table(t, filename):
    t.sort(key = lambda r: order.index(r['kernel']))
    with open(filename, 'w') as f:
        f.write(r'''\documentclass{minimal}
\begin{document}
\begin{tabular}{|c|c|c|c|c|}
    kernel & \#input data & \#ops & ratio & OI$_{up}$\\
\hline
''')
        for ker in t:
            inp = parse_str(ker['input'])
            ops = parse_str(ker['ops'])
            ratio = ops/inp
            if(not denom(ratio).is_constant()):
                ratio = sympify('0')
            OI = ops/parse_str(ker['IOlb'])
            f.write(r'\textsf{' + ker['kernel'] + '} &'
            + to_tex(inp)  + ' & '
            + to_tex(ops)  + ' & '
            + to_tex(ratio)  + ' & '
            + to_tex(OI)  + r'\\' + '\n')
        f.write(r'\hline\end{tabular}' + '\n'+ r'\end{document}' + '\n')

def gen_IO_latex(t, filename):
    t.sort(key = lambda r: order.index(r['kernel']))
    with open(filename, 'w') as f:
        for ker in t:
            IO = parse_str(ker['IOlb'])
            f.write(to_tex(IO)  + '&\n')
        f.write(r'\hline\end{tabular}' + '\n'+ r'\end{document}' + '\n')


t = read_table('tmp.csv')
read_ops('ops.csv', t)

write_csv('results/all.csv', t)

gen_table(t, 'results/table1.tex')
gen_IO_latex(t,'IO.tex')
