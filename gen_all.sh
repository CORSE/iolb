#/bin/bash

echo 'Running IOLB on all polybench kernels...'
echo 'kernel;input;IOlb;fIOlb' > tmp.csv
for x in examples/polybench/*.c; do
	echo `basename $x .c`
	echo -n "$(basename $x .c);" >> tmp.csv
	out=$(./iolb-affine $x | tail -3)
	for y in $out; do
		echo -n "$y;" >> tmp.csv
	done
	echo  >> tmp.csv
done

echo 'Formatting results...'
if [ ! -d "results" ]; then
	mkdir results
fi
python format_results.py
#rm tmp.csv
#cd results
#pdflatex table1
	
#cp all.csv /vagrant/
#cp table1.pdf /vagrant/
