
#include "hourglass.h"



// Aux function to "transform_input_dim_as_param"
int map_fun_transf_input_as_param(isl_basic_map *bmap, void* res_void) {
	list_item** result = (list_item**) res_void;

	/* DEBUG
	printf("DEBUG - map_fun_transf_input_as_param : ping!\n");
	printf("DEBUG - map_fun_transf_input_as_param : bmap = ");
	PRINT_ISL(p_out, isl_printer_print_basic_map, bmap);
	p_out = isl_printer_flush(p_out);
	printf("\n");
	fflush(stdout);
	//*/

	// Get the matrices
	isl_mat *mat_eq = isl_basic_map_equalities_matrix(bmap,
		isl_dim_param, isl_dim_in, isl_dim_out, isl_dim_div, isl_dim_cst);
	isl_mat *mat_ineq = isl_basic_map_inequalities_matrix(bmap,
		isl_dim_param, isl_dim_in, isl_dim_out, isl_dim_div, isl_dim_cst);

	// Create the new space, by shifting all "isl_dim_in" into "isl_dim_param"
	isl_space *space_bmap = isl_basic_map_get_space(bmap);


	/* DEBUG
	printf("DEBUG - map_fun_transf_input_as_param : space_bmap = ");
	PRINT_ISL(p_out, isl_printer_print_space, space_bmap);
	p_out = isl_printer_flush(p_out);
	printf("\n");
	fflush(stdout);
	//*/

	unsigned int n_input_dim = isl_space_dim(space_bmap, isl_dim_in);
	unsigned int n_param_dim = isl_space_dim(space_bmap, isl_dim_param);
	isl_space *nspace = isl_space_move_dims(space_bmap,
		isl_dim_param, n_param_dim,
		isl_dim_in, 0,
		n_input_dim); // Quantity
	// Go back to a space of a set
	nspace = isl_space_range(nspace);

	/* DEBUG
	printf("DEBUG - map_fun_transf_input_as_param : nspace = ");
	PRINT_ISL(p_out, isl_printer_print_space, nspace);
	p_out = isl_printer_flush(p_out);
	fflush(stdout);
	//*/

	// Build back a set from it
	isl_basic_set *bset = isl_basic_set_from_constraint_matrices(
		nspace,
		mat_eq, mat_ineq,
		isl_dim_param, isl_dim_set, isl_dim_div, isl_dim_cst);

	/* DEBUG
	printf("DEBUG - map_fun_transf_input_as_param : bset = ");
	PRINT_ISL(p_out, isl_printer_print_basic_set, bset);
	p_out = isl_printer_flush(p_out);
	printf("\n");
	fflush(stdout);
	//*/

	// Store bset inside result
	list_item* n_list_item = malloc(sizeof(list_item));
	n_list_item->data = bset;
	n_list_item->next = (*result);

	*result = n_list_item;

	return 0; // isl_stat_ok
}


// Translate the input dims of a relation in the param space
isl_set* transform_input_dim_as_param(isl_map* rel) {
	
	// Iterate over all the basic map of rel
	list_item* res_lbset = NULL;
	list_item** p_res_lbset = &res_lbset;
	int stat = isl_map_foreach_basic_map(rel, map_fun_transf_input_as_param, p_res_lbset);
	assert(stat==0);

	/* DEBUG
	printf("DEBUG - transform_input_dim_as_param : exit isl_map_foreach_basic_map\n\n");
	if (*p_res_lbset==NULL) {
		printf("DEBUG - transform_input_dim_as_param : ISSUE: result is NULL\n");
		fflush(stdout);
	}
	fflush(stdout);
	//*/

	// Transform the chained list of basic set into a full set
	isl_set* res_set = NULL;
	list_item* head = (list_item*) (*p_res_lbset);

	while (head != NULL) {
		isl_set *nset = isl_set_from_basic_set(head->data);

		/* DEBUG
		printf("DEBUG - transform_input_dim_as_param : nset = ");
		PRINT_ISL(p_out, isl_printer_print_set, nset);
		printf("\n");
		fflush(stdout);
		//*/

		if (res_set==NULL) {
			res_set = nset;
		} else {
			// __isl_give isl_set *isl_set_union(
			//	__isl_take isl_set *set1,
			//	__isl_take isl_set *set2);
			res_set = isl_set_union(res_set, nset);
		}
		
		// Iterates
		head = head->next;
	}
	assert(res_set!=NULL);

	// Clean-up: free p_res_lbset
	head = (list_item*) (*p_res_lbset);
	while (head!=NULL) {
		list_item* old_head = head;
		head = head->next;
		free(old_head);
	}


	return res_set;
}


// Check if an expression can be as large as possible, by playing with
//		its parameter values
bool is_expression_positive_param(expr* expr_card, __isl_keep isl_set* set_inters) {
	// Replace the small dimensions with their values
	expr* e_eval = expr_card;
	for (list_item* ptr = _l_small_dims; ptr!=NULL; ptr=ptr->next) {
		small_dim_info* sdi = (small_dim_info*) ptr->data;
		e_eval = expr_partial_evaluate(e_eval, sdi->name_param, sdi->value);
	}

	// Remove the negative terms
	e_eval = expr_expand(e_eval);
	e_eval = expr_remove_negative(e_eval);

	// Is there any parameters remaining here?
	// => set it to a "big_num" (ex: 1e9)
	double ev = expr_evaluate(e_eval, 1e9);

	if (ev>1e6) {
		return true;
	}
	return false;
}


// ====================================================

// Printer for debug (for the compl_hourglass_p data struct)
void print_compl_hourglass_p(compl_hourglass_p* hrgl_p) {
	printf("[[[\n");
	printf("  hrgl_rel = ");
	PRINT_ISL(p_out, isl_printer_print_map, hrgl_p->hrgl_rel);
	printf("  Path = (%lu -> %lu -R-> %lu -> %lu -B-> %lu)\n",
			hrgl_p->node_S2, hrgl_p->node_SR, hrgl_p->node_SR,
			hrgl_p->node_S1, hrgl_p->node_S2);
	printf("  Dims = [");
	for (int i=0; i<hrgl_p->ndim_S2; i++) {
		if (hrgl_p->is_temporal_dim[i])
			printf("T, ");
		else if (hrgl_p->is_neutral_dim[i])
			printf("N, ");
		else {
			assert(hrgl_p->is_red_bcst_dim[i]);
			printf("R, ");
		}
	}
	printf("]\n");
	printf("  res_set_inters = ");
	PRINT_ISL(p_out, isl_printer_print_set, hrgl_p->res_set_inters);
	printf("  expr_card = ");
	expr_print(hrgl_p->expr_card);
	printf("\n]]]\n");
	return;
}




// Tries to complete a path to recognize an hourglass path
// Returns "NULL" if it did not work, else a list_item of compl_hourglass_p
list_item* complete_hourglass_path(const dfg * const g, const path * const p_incompl) {
	// Check should have been done previously
	assert(p_incompl->type==BCAST);

	// Get which node can reach which other node through injective edges
	bool** matrix_reach = compute_reachable_nodes_bool(g);

	/* DEBUG
	for (int i=0; i<g->n_nodes; i++) {
		for (int j=0; j<g->n_nodes; j++) {
			printf("  matrix_reach[%d][%d] = %d\n", i, j, matrix_reach[i][j] );
		}
	}
	//*/

	// Structure of the path to be recognized:
	// 	S2 --inj--> SR --red--> SR --inj--> S1 --p_incompl--> S2
	size_t node_S1 = p_incompl->source_idx;
	size_t node_S2 = p_incompl->sink_idx;

	// DEBUG
	//printf("p_incompl->rel =");
	//PRINT_ISL(p_out, isl_printer_print_map, p_incompl->rel);
	//printf("node_S1 = %lu  | node_S2 = %lu\n", node_S1, node_S2);
	//fflush(stdout);


	// Candidates for SR? (which nodes has a reduction loop?)
	// arr_red_nodes: boolean array (true = the node has a reduction self loop)
	bool* arr_red_node = malloc(g->n_nodes * sizeof(bool));
	for (int i=0; i<g->n_nodes; i++) {
		arr_red_node[i] = (g->nodes[i].red_chain_bcst_rel != NULL);
	}

	/* OLD CODE, only for reduction
	for (int i=0; i<g->n_nodes; i++) {
		arr_red_node[i] = false;

		// Check if we have a broadcast edge here
		list_item* ptr_bcast_edge = g->bcast_list[i];
		while (ptr_bcast_edge!=NULL) {
			edge* e_bcast = (edge*) ptr_bcast_edge->data;
			if (e_bcast->is_reduction) {
				assert(e_bcast->source_idx == e_bcast->sink_idx);
				assert(e_bcast->source_idx == i);
				arr_red_node[i] = true;
				break;
			}
			ptr_bcast_edge = ptr_bcast_edge->next;
		}
	}
	*/

	/* DEBUG
	printf("  [hourglass::complete_hourglass_path] Reduction candidate ready\n");
	for (int i=0; i<g->n_nodes; i++) {
		printf("	arr_red_node[%d] = %d\n", i, arr_red_node[i] );
	}
	fflush(stdout);
	//*/


	list_item* res_list = NULL;	// List result

	// For each of these candidate, check if we have an injective path to S1 and from S2
	for (int i=0; i<g->n_nodes; i++) {
		if (arr_red_node[i]) {
			// "node_SR = i"
			int pathSR_S1= matrix_reach[i][node_S1];
			int pathS2_SR = matrix_reach[node_S2][i];

			// DEBUG
			//printf("pathSR_S1 (SR = %d, S1 = %lu) = %d\n", i, node_S1, pathSR_S1);
			//printf("pathS2_SR (S2 = %lu, SR = %d) = %d\n", node_S2, i, pathS2_SR);
			//fflush(stdout);


			if ((pathSR_S1>0) && (pathS2_SR>0)) {
				// We have an interesting loop that satisfy the second criterion here
				size_t node_SR = (size_t) i;

				// DEBUG
				//printf("  [hourglass::complete_hourglass_path] Getting relations\n");
				//printf("    => S1 = %lu, S2 = %lu, SR = %lu\n", node_S1, node_S2, node_SR);
				//fflush(stdout);

				// Obtain the relations (must be freshly built by these functions)
				isl_basic_map* rel_S2_SR = get_injective_relation(g, node_S2, node_SR);
				isl_basic_map* rel_SR_S1 = get_injective_relation(g, node_SR, node_S1);


				assert(rel_S2_SR!=NULL);
				assert(rel_SR_S1!=NULL);

				/* DEBUG
				printf("[DEBUG] rel_SR_S1 :");
				fflush(stdout);
				PRINT_ISL(p_out, isl_printer_print_basic_map, rel_SR_S1);
				printf("\n");
				fflush(stdout);
				printf("[DEBUG] rel_S2_SR :");
				fflush(stdout);
				PRINT_ISL(p_out, isl_printer_print_basic_map, rel_S2_SR);
				printf("\n");
				fflush(stdout);
				//*/

				isl_basic_map* rel_SR = isl_basic_map_copy(get_reduction_relation(g, node_SR));

				assert(rel_SR!=NULL);

				// DEBUG
				//printf("  [hourglass::complete_hourglass_path] Composing relations\n");
				//fflush(stdout);

				// Compose: rel_S2_SR o rel_SR o rel_SR_S1 o rel_S1_S2;
				// Note: "isl_basic_map_apply_range" args are "isl_take"
				isl_basic_map* rel_result_basic = isl_basic_map_apply_range(rel_S2_SR, rel_SR);
				rel_result_basic = isl_basic_map_apply_range(rel_result_basic, rel_SR_S1);

				// Convert to map here ("p_incompl->rel" is a isl_map)
				isl_map* rel_result = isl_map_from_basic_map(rel_result_basic);
				rel_result = isl_map_apply_range(rel_result, isl_map_copy(p_incompl->rel));

				/* DEBUG
				printf("[DEBUG] rel_result :");
				fflush(stdout);
				PRINT_ISL(p_out, isl_printer_print_map, rel_result);
				printf("\n");
				fflush(stdout);
				//*/


				// Filter if the relation is empty
				if (isl_map_is_empty(rel_result)) {
					// We ignore that candidate
					isl_map_free(rel_result);
					continue;
				}

				/* DEBUG
				printf("  [Hourglass detection] Hourglass completion found!\n");
				printf("     (%lu -> %lu -R-> %lu -> %lu -B-> %lu)\n",
						node_S2, node_SR, node_SR, node_S1, node_S2);
				fflush(stdout);
				//*/

				compl_hourglass_p* hrgl_p = (compl_hourglass_p*) malloc(sizeof(compl_hourglass_p));
				hrgl_p->hrgl_rel = rel_result;
				hrgl_p->node_S1 = node_S1;
				hrgl_p->node_S2 = node_S2;
				hrgl_p->node_SR = node_SR;

				int ndim_S2 = g->nodes[node_S2].dim;
				hrgl_p->ndim_S2 = ndim_S2;

				// To be filled later
				hrgl_p->is_temporal_dim = (bool*) malloc(ndim_S2 * sizeof(bool));
				hrgl_p->is_neutral_dim = (bool*) malloc(ndim_S2 * sizeof(bool));
				hrgl_p->is_red_bcst_dim = (bool*) malloc(ndim_S2 * sizeof(bool));
				hrgl_p->expr_card = NULL;
				hrgl_p->res_set_inters = NULL;

				// We definitively have a candidate now: add it to the result
				list_item* n_res_list = (list_item*) malloc(sizeof(list_item));
				n_res_list->data = hrgl_p;
				n_res_list->next = res_list;

				res_list = n_res_list;
			}
		}
	}


	// Clean-up
	for (int i=0; i<g->n_nodes; i++) {
		free(matrix_reach[i]);
	}
	free(matrix_reach);
	free(arr_red_node);

	return res_list;
}


// (Part of the) Return structure for the "get_dimensional_information_bmap" function
typedef struct {
	bool is_match;
	int ndim;
	bool* is_temporal_dim;	// "k"
	bool* is_neutral_dim;	// "j"
	bool* is_red_bcst_dim;	// "i"
} res_get_dim_info;

// Printer for debug
void print_res_get_dim_info(res_get_dim_info* info) {
	if (!info->is_match) {
		printf("No match");
		return;
	} else {
		printf("Match - dim = [");
		for (int i=0; i<info->ndim; i++) {
			if (info->is_temporal_dim[i])
				printf("T, ");
			else if (info->is_neutral_dim[i])
				printf("N, ");
			else {
				assert(info->is_red_bcst_dim[i]);
				printf("R, ");
			}
		}
		printf("]");
		return;
	}
}


// Auxilliary function for "get_dimensional_information" (basic_map level)
int get_dimensional_information_bmap(isl_basic_map* bmap, void* res_void) {
	list_item** result = (list_item**) res_void;

	// Get the equality matrix
	isl_mat* eq_mat = isl_basic_map_equalities_matrix(bmap,
	 	isl_dim_in, isl_dim_out, isl_dim_div, isl_dim_param, isl_dim_cst);

	/* DEBUG
	printf("[hourglass :: get_dimensional_information_bmap] eq_mat =\n");
	for (int i=0; i<isl_mat_rows(eq_mat); i++) {
		printf("[");
		for (int j=0; j<isl_mat_cols(eq_mat); j++) {
			printf("%f ", isl_val_get_d(isl_mat_get_element_val(eq_mat, i, j)));
		}
		printf("]\n");
	}
	printf("\n");
	//*/


	int ndim_in = isl_basic_map_n_in(bmap);
	int ndim_out = isl_basic_map_n_out(bmap);
	int ndim_param = isl_basic_map_n_param(bmap);
	assert(isl_basic_map_n_div(bmap)==0);
	assert(ndim_in == ndim_out);

	// Preparing the data structure
	res_get_dim_info* res_info = (res_get_dim_info*) malloc(sizeof(res_get_dim_info));
	res_info->is_match = true;
	res_info->ndim = ndim_out;
	res_info->is_temporal_dim = (bool*) malloc(ndim_out * sizeof(bool));
	res_info->is_neutral_dim = (bool*) malloc(ndim_out * sizeof(bool));
	res_info->is_red_bcst_dim = (bool*) malloc(ndim_out * sizeof(bool));
	for (int i=0; i<ndim_out; i++) {
		res_info->is_temporal_dim[i] = false;
		res_info->is_neutral_dim[i] = false;
		res_info->is_red_bcst_dim[i] = false;
	}

	// Let's analyse that!
	for (int idim=0; idim<ndim_out; idim++) {

		// DEBUG
		//printf("[DEBUG hourglass :: get_dimensional_information_bmap] - entering idim = %d\n", idim);

		// 3 cases for the classification:
		//	idim-th output dimension is equal to the same input dimension => neutral
		//	idim-th output dimension is equal to the same input dimension + a shift => temporal
		//	idim-th output dimension is independent from the inputs dims => red_bcst
		// Else: template recognition FAILED!

		// Check the rows (eq. constraints) that involves the idim-th output dimension
		int num_constr = 0;
		int last_row_constr = -1;
		for (int i=0; i<isl_mat_rows(eq_mat); i++) {
			isl_val* elem_i_idim = isl_mat_get_element_val(eq_mat, i, ndim_in + idim);
			if (! isl_val_is_zero(elem_i_idim) ) {
				num_constr++;
				last_row_constr = i; // Last constraint appearing
			}
			// Clean-up
			isl_val_free(elem_i_idim);
		}

		// DEBUG
		//printf("\tnum_constr = %d\n", num_constr);

		// Check the number of constraints
		if (num_constr==0) {
			// Free dim => red_bcst
			res_info->is_red_bcst_dim[idim] = true;
		} else if (num_constr>2) {
			// Recognition fail: early exit
			res_info->is_match = false;
			break;
		} else {
			assert(num_constr==1);
			// 2 cases remaining here: neutral or temporal
			// Or recognition fail (early exit)

			// Check the input/output parts (if not correct, trigger a reco failure)
			// Are all the elements in the Input/output part 0s (except the ones for idim)
			for (int j=0; j<ndim_in; j++) {
				if (j!=idim) {
					// Input part
					isl_val* elem_j_in = isl_mat_get_element_val(eq_mat, last_row_constr, j);
					bool non_zero = ! isl_val_is_zero(elem_j_in);
					isl_val_free(elem_j_in);
					if (non_zero) {
						// Recognition fail
						res_info->is_match = false;
						break;
					}

					// Output part
					isl_val* elem_j_out = isl_mat_get_element_val(eq_mat, last_row_constr, ndim_in + j);
					non_zero = ! isl_val_is_zero(elem_j_out);
					isl_val_free(elem_j_out);
					if (non_zero) {
						// Recognition fail
						res_info->is_match = false;
						break;
					}

				}
			}

			// There was a recognition fail: continue the early exit
			if (! res_info->is_match) {
				break;
			}

			// DEBUG
			//printf("\t(num_constr = 1) - 0s on in/output parts are ok\n");

			// Check the input and output parts (-X / X or the opposite, sum must be 0 / rest is 0)
			isl_val* elem_idim_in = isl_mat_get_element_val(eq_mat, last_row_constr, idim);
			isl_val* elem_idim_out = isl_mat_get_element_val(eq_mat, last_row_constr, ndim_in + idim);
			if (isl_val_is_zero(elem_idim_in)) {
				// Recognition fail: all input/output are 0s
				res_info->is_match = false;
				isl_val_free(elem_idim_in);
				isl_val_free(elem_idim_out);
				break;
			}

			isl_val* sum_val = isl_val_add(elem_idim_in, elem_idim_out);
			bool non_zero = ! isl_val_is_zero(sum_val);
			isl_val_free(sum_val);
			if (non_zero) {
				// Recognition fail
				res_info->is_match = false;
				break;
			}

			// DEBUG
			//printf("\t(num_constr = 1) - In/output parts are ok\n");


			// At that point, recognition succeed
			// If there are only 0s on params/cst: neutral. Else, temporal
			bool is_neutral = true;
			for (int j=0; j<ndim_param; j++) {
				isl_val* elem_param = isl_mat_get_element_val(eq_mat, last_row_constr, ndim_in+ndim_out+j);
				if (! isl_val_is_zero(elem_param)) {
					is_neutral = false;
					break;
				}
				isl_val_free(elem_param);
			}
			isl_val* elem_cst = isl_mat_get_element_val(
				eq_mat, last_row_constr, ndim_in+ndim_out+ndim_param);
			if (! isl_val_is_zero(elem_cst)) {
				is_neutral = false;
			}
			isl_val_free(elem_cst);

			// Now we know if it was a neutral or a temporal dimension
			if (is_neutral) {
				res_info->is_neutral_dim[idim] = true;
			} else {
				res_info->is_temporal_dim[idim] = true;
			}
		}
	} // End "idim"


	// Put all of that in a chained list
	list_item* n_list_item = malloc(sizeof(list_item));
	n_list_item->data = res_info;
	n_list_item->next = (*result);

	*result = n_list_item;


	 // Clean-up
	 isl_mat_free(eq_mat);

	return 0;  // isl_stat_ok
}


// Once an hourglass pattern is detected, analyze its relation to find the role of the dimensions
// Output is true if it works, false if we were not able to pattern-match all the dimensions
//	"hrgl_pat" missing field (about dimensions) are updated
bool get_dimensional_information(compl_hourglass_p* hrgl_pat) {

	// Check all the basic map composing "hrgl_rel"
	list_item* res_bmap = NULL;
	list_item** p_res_bmap = &res_bmap;
	isl_map_foreach_basic_map(hrgl_pat->hrgl_rel, get_dimensional_information_bmap, p_res_bmap);

	list_item* head = (list_item*) (*p_res_bmap);
	assert(head!=NULL);	// List is not supposed to be empty

	int ndim_out = isl_map_n_out(hrgl_pat->hrgl_rel);


	// Coherence pass between the different basic map
	// First one: reco ok, and at least one of the array at true for each dim?
	res_get_dim_info* first_res_get_dim_info = (res_get_dim_info*) head->data;

	/* DEBUG
	printf("  [DEBUG - Hourglass detection] first_res_get_dim_info = ");
	if (first_res_get_dim_info==NULL) {
		printf("NULL");
	} else {
		print_res_get_dim_info(first_res_get_dim_info);
	}
	printf("\n");
	//*/



	bool res_is_match = first_res_get_dim_info->is_match;
	if (res_is_match) {
		// Sanity check: all dims were recognized
		for (int i=0; i<ndim_out; i++) {
			assert( first_res_get_dim_info->is_neutral_dim[i]
				|| first_res_get_dim_info->is_temporal_dim[i]
				|| first_res_get_dim_info->is_red_bcst_dim[i] );
		}
	}

	// Comparison with the rest: identical recognition?
	// If res_is_match==false: skip that and go to clean-up
	if (res_is_match) {
		head = head->next;
		while (head!=NULL) {
			res_get_dim_info* rgdinfo_mid = (res_get_dim_info*) head->data;

			res_is_match = (res_is_match && rgdinfo_mid->is_match);
			if (!res_is_match) {
				break;
			}

			// Check coherence between arrays
			for (int i=0; i<ndim_out; i++) {
				if (first_res_get_dim_info->is_neutral_dim[i] != rgdinfo_mid->is_neutral_dim[i]) {
					res_is_match = false;
					break;
				}
				if (first_res_get_dim_info->is_temporal_dim[i] != rgdinfo_mid->is_temporal_dim[i]) {
					res_is_match = false;
					break;
				}
				if (first_res_get_dim_info->is_red_bcst_dim[i] != rgdinfo_mid->is_red_bcst_dim[i]) {
					res_is_match = false;
					break;
				}
			}
			if (!res_is_match) {
				break;
			}

			// Let's continue
			head = head->next;
		}
	}

	// Build the final results: fill hrgl_pat (might be useless if not res_is_match)
	for (int i=0; i<ndim_out; i++) {
		hrgl_pat->is_temporal_dim[i] = first_res_get_dim_info->is_temporal_dim[i];
		hrgl_pat->is_neutral_dim[i] = first_res_get_dim_info->is_neutral_dim[i];
		hrgl_pat->is_red_bcst_dim[i] = first_res_get_dim_info->is_red_bcst_dim[i];		
	}

	// Clean-up
	head = (list_item*) (*p_res_bmap);
	while (head!=NULL) {
		// Free the inner res_get_dim_info
		res_get_dim_info* data = (res_get_dim_info*) head->data;
		free(data->is_temporal_dim);
		free(data->is_neutral_dim);
		free(data->is_red_bcst_dim);
		free(data);
		
		// Free the list_item cell
		list_item* old_head = head;
		head = head->next;
		free(old_head);
	}

	return res_is_match;
}


// Complete res_set_inters + expr_card): check that the size is parametric & unbounded
// 	If we apply twice the path (k -> k+1) and (k+1 -> k+2)
//		If we fix a point in k/(k+2), is the volume of point inbetween at (k+1) parametric?
// Return false if this size is finite (bounded by constants, including "empty")
bool check_expr_card(compl_hourglass_p* hrgl_pat, isl_set* set_stmt) {

	// === Algo:
	// Given rel_path : [\vec{N}] -> { [\vec{i_k}] -> [\vec{i^{next}_k}] : ... }
	// - Compute rel_path_inv : [\vec{N}] -> { [\vec{i_k}] -> [\vec{i^{prev}_k}] : ... }
	// - Pass the \vec{i_k} in the parameters (need to go through matrices)
	//		[\vec{N}, \vec{i_k}] -> { [\vec{i^{next}_k}] : ... }
	//		[\vec{N}, \vec{i_k}] -> { [\vec{i^{prev}_k}] : ... }
	// - Intersect both of them:
	//		[\vec{N}, \vec{i_k}] -> { [\vec{i^{mid}_k}] : ... }
	// - Get the card & check if bounded by a constant or not
	//		(can do that by checking the bounding box, or by evaluating it with big values)

	isl_map* rel_path = hrgl_pat->hrgl_rel;
	isl_map* rel_path_inv = isl_map_reverse(isl_map_copy(rel_path));

	// Aux function: translate the input in the param space
	isl_set* set_rel_path = transform_input_dim_as_param(rel_path);
	isl_set* set_rel_path_inv = transform_input_dim_as_param(rel_path_inv);

	/* DEBUG
	printf("DEBUG - hourglass_path_detection :\n   set_rel_path = ");
	PRINT_ISL(p_out, isl_printer_print_set, set_rel_path);
	printf("   set_rel_path_inv = ");
	PRINT_ISL(p_out, isl_printer_print_set, set_rel_path_inv);
	printf("\n");
	fflush(stdout);
	//*/

	// We project out the temporal dimension from it
	for (int i=hrgl_pat->ndim_S2 - 1; i>=0; i--) {
		if (hrgl_pat->is_temporal_dim[i]) {
			set_rel_path = isl_set_project_out(set_rel_path, isl_dim_set, i, 1);
			set_rel_path_inv = isl_set_project_out(set_rel_path_inv, isl_dim_set, i, 1);
		}
	}


	/* DEBUG
	printf("DEBUG - hourglass_path_detection :\n   set_rel_path = ");
	PRINT_ISL(p_out, isl_printer_print_set, set_rel_path);
	printf("   set_rel_path_inv = ");
	PRINT_ISL(p_out, isl_printer_print_set, set_rel_path_inv);
	printf("\n");
	fflush(stdout);
	//*/

	// Intersect both
	isl_set* set_inters = isl_set_intersect(set_rel_path, set_rel_path_inv);

	/* DEBUG
	printf("\nDEBUG DEBUG [Hourglass] set_inters = \n");
	PRINT_ISL(p_out, isl_printer_print_set, set_inters);
	printf("\n");
	fflush(stdout);
	//*/

	// Clean-up
	isl_map_free(rel_path_inv);

	// If nothing in the intersection: early exit
	if (isl_set_is_empty(set_inters)) {
		// Clean-up
		isl_set_free(set_inters);
		return false;
	}


	// Function from "expr_utils.c/h"
	isl_set *dom_params = isl_set_params(isl_set_copy(set_inters));
	int rounding = 0; // Lower bound on cardinality
	expr *expr_card = get_set_cardinality(isl_set_copy(set_inters), rounding, &dom_params);

	expr_card = expr_normalize(expr_card);


	/* DEBUG
	printf("  [Hourglass detection] expr_card (before index removal) = ");
	expr_print(expr_card);
	printf("\n");
	fflush(stdout);
	//*/


	// Remove all trace of indices here (to have only parameters)
	// ex: "-2-k+M" => "-2-N+M" in qr_householder_a2v
	
	int ndim_set_inters = isl_set_dim(set_stmt, isl_dim_set);
	bounds* bnds = malloc(ndim_set_inters * sizeof(bounds));
	get_dim_bounds(set_stmt, ndim_set_inters, bnds);

	/* DEBUG
	for (int i=0; i<ndim_set_inters; i++) {
		printf("bounds[%d] : lb ", i);
		expr_print(bnds[i].lb);
		printf("  ub ");
		expr_print(bnds[i].ub);
		printf("\n");
	}
	//*/

	// Use bnds to get the min of the expression
	const char** arr_name_dim = malloc(ndim_set_inters * sizeof(char*));
	isl_space* space_stmt = isl_set_get_space(set_stmt);
	for (int i=0; i<ndim_set_inters; i++) {
		arr_name_dim[i] = isl_space_get_dim_name(space_stmt, isl_dim_set, i);
	}

	expr_card = get_min_expr_card(expr_card, bnds, arr_name_dim, ndim_set_inters);

	/* DEBUG
	printf("  [Hourglass detection] expr_card = ");
	expr_print(expr_card);
	printf("\n");
	fflush(stdout);
	//*/


	// Update "hrgl_pat"
	hrgl_pat->res_set_inters = set_inters;
	hrgl_pat->expr_card = expr_card;


	// We now check if expr_card has a positive parameter component, which is not a small dim
	//	=> Do that by setting the parameters one by one to big_val, and check if the
	//	expression is also big
	bool is_expr_posit_param = is_expression_positive_param(expr_card, set_inters);

	if (!is_expr_posit_param) {
		// Clean-up
		isl_set_free(set_inters);
		expr_free(expr_card);
	}

	// Clean-up
	isl_set_free(dom_params);

	return is_expr_posit_param;
}



// ====================================================
static bool _debug_hourglass = true;

// Hourglass pattern detection (applied to a path "p")
// Return a list_item (potentially "NULL" if nothing detected), of compl_hourglass_p
list_item* hourglass_path_detection(const dfg * const g, const path * const p_incompl) {

	//* DEBUG
	if (_debug_hourglass) {
		printf("\n[Hourglass_path_detection] path (");
		if (p_incompl->type == BCAST) {
			printf("BROADCAST):\n  ");
		} else {
			assert(p_incompl->type == INJECTION);
			printf("INJECTION):\n  ");
		}
		PRINT_ISL(p_out, isl_printer_print_map, p_incompl->rel);
		fflush(stdout);
	}
	//*/

	// Criterion 1: we must complete a broadcast path
	if (p_incompl->type!=BCAST) {
		return NULL;
	}


	// Criterion 2: we must be able to complete this broadcast path S1 --> S2
	//	into a (looping) pattern of S2 --inj--> SR --red--> SR --inj--> S1 --> S2
	list_item *l_compl_paths = complete_hourglass_path(g, p_incompl);
	if (l_compl_paths==NULL) {
		// DEBUG
		if (_debug_hourglass)
			printf("  [Hourglass detection] No hourglass completion was found.\n");

		// Completion did not work
		return NULL;
	}

	// DEBUG
	if (_debug_hourglass) {
		printf("  [Hourglass detection] Graph completion worked.\n");
		fflush(stdout);
	}


	// Criterion 3: We managed to get the infos on dimension from it
	//	=> Filter the l_compl_paths
	list_item *l_compl_dim_paths = NULL;
	list_item *p_l_compl_paths = l_compl_paths;
	while (p_l_compl_paths!=NULL) {
		compl_hourglass_p* cmpl_hrgl_p = (compl_hourglass_p*) p_l_compl_paths->data;

		bool b_dim_detect_passed = get_dimensional_information(cmpl_hrgl_p);

		if (b_dim_detect_passed) {
			// Add it to l_compl_dim_paths
			list_item *nl_compl_dim_paths = malloc(sizeof(list_item));
			nl_compl_dim_paths->data = cmpl_hrgl_p;
			nl_compl_dim_paths->next = l_compl_dim_paths;
			l_compl_dim_paths = nl_compl_dim_paths;
		} else {
			// Free the compl_hrgl_p for which it did not pass
			isl_map_free(cmpl_hrgl_p->hrgl_rel);
			free(cmpl_hrgl_p->is_temporal_dim);
			free(cmpl_hrgl_p->is_neutral_dim);
			free(cmpl_hrgl_p->is_red_bcst_dim);
			free(cmpl_hrgl_p);
		}
		p_l_compl_paths = p_l_compl_paths->next;
	}

	// Clean-up the previous chained list (l_compl_paths)
	// (the data is either in the new list l_compl_dim_paths, or already freed)
	p_l_compl_paths = l_compl_paths;
	while (p_l_compl_paths!=NULL) {
		list_item* p_temp = p_l_compl_paths;
		p_l_compl_paths = p_l_compl_paths->next;
		free(p_temp);
	}

	if (l_compl_dim_paths==NULL) {
		// DEBUG
		if (_debug_hourglass)
			printf("  [Hourglass detection] No hourglass survived dimension recognition.\n"); // How sad.

		// Completion did not work
		return NULL;
	}

	// DEBUG
	if (_debug_hourglass) {
		printf("  [Hourglass detection] Dimension recognition passed.\n");
		fflush(stdout);
	}


	// Last criterion (res_set_inters + expr_card): check that the size is parametric & unbounded
	// If we apply twice the path (k -> k+1) and (k+1 -> k+2)
	//	If we fix a point in k/(k+2), is the volume of point inbetween at (k+1) parametric?
	list_item *l_compl_final = NULL;
	list_item *p_l_compl_dim_path = l_compl_dim_paths;
	while (p_l_compl_dim_path!=NULL) {
		compl_hourglass_p* cmpl_hrgl_p = (compl_hourglass_p*) p_l_compl_dim_path->data;

		// Check the expr_card
		isl_set* set_stmt = g->nodes[cmpl_hrgl_p->node_S2].dom;
		bool expr_card_passed = check_expr_card(cmpl_hrgl_p, set_stmt);

		if (expr_card_passed) {
			// Add it to l_compl_final
			list_item *nl_compl_final = (list_item*) malloc(sizeof(list_item));
			nl_compl_final->data = cmpl_hrgl_p;
			nl_compl_final->next = l_compl_final;
			l_compl_final = nl_compl_final;
		} else {
			// Free the compl_hrgl_p for which it did not pass
			isl_map_free(cmpl_hrgl_p->hrgl_rel);
			free(cmpl_hrgl_p->is_temporal_dim);
			free(cmpl_hrgl_p->is_neutral_dim);
			free(cmpl_hrgl_p->is_red_bcst_dim);
			// card_expr and res_set_inters already freed or never allocated
			free(cmpl_hrgl_p);
		}

		p_l_compl_dim_path = p_l_compl_dim_path->next;
	}

	// Clean-up the previous chained list (l_compl_dim_paths)
	// (the data is either in the new list l_compl_final, or already freed)
	p_l_compl_dim_path = l_compl_dim_paths;
	while (p_l_compl_dim_path!=NULL) {
		list_item* p_temp = p_l_compl_dim_path;
		p_l_compl_dim_path = p_l_compl_dim_path->next;
		free(p_temp);
	}

	// End of the criteria. If we are still here, the remaining elements are hourglass patterns

	// DEBUG
	if (_debug_hourglass) {
		if (l_compl_final==NULL) {
			printf("  [Hourglass detection] No hourglass survived the size bottleneck criterion!\n");
		} else {
			printf("  [Hourglass detection] Hourglass detected!\n");

			printf("First hourglass detected = ");
			list_item *p_l_compl_final = l_compl_final;
			while (p_l_compl_final!=NULL) {
				print_compl_hourglass_p(l_compl_final->data);
				p_l_compl_final = p_l_compl_final->next;
			}
		}
	}

	return l_compl_final;
}


// Hourglass pattern detection on a list of path (cf "complexity.c::solve")
// Return "NULL" if none are an hourglass pattern
list_item* hourglass_path_detection_list_path(const dfg * const g, list_item *K) {
	// Iterate over the collection of paths
	list_item *ptr = K;
	list_item* l_hg_info = NULL; // Empty list for starting
	while (ptr!=NULL) {
		path* p = (path*) ptr->data;

		// Append the new path to the previous one
		list_item* n_l_hg_info = hourglass_path_detection(g, p);
		l_hg_info = list_item_append(l_hg_info, n_l_hg_info);

		ptr = ptr->next;
	}

	// Return the first hourglass path found, or NULL if none where found.
	return l_hg_info;
}
