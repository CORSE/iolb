#ifndef HOURGLASS_H
#define HOURGLASS_H

#include <assert.h>
#include <stdbool.h>
#include <string.h>

//#include <isl/union_map.h>
#include <isl/map.h>
#include <isl/space.h>
#include <isl/vec.h>
#include <isl/mat.h>
#include <isl/aff.h>
#include <isl/val.h>
#include <isl/ctx.h>

#include "utility.h"
#include "dfg.h"
#include "kpart_type.h"
#include "expr.h"

// Note: compl_hourglass_p data struct is in "kpart_type.h"
void print_compl_hourglass_p(compl_hourglass_p* hrgl_p);

// Tell is a path fits the hourglass pattern (cf Polybench Gramschmidt (MGS), Householder, ...)
list_item* hourglass_path_detection(const dfg * const g, const path * const p_incompl);

// Same for a list of paths
list_item* hourglass_path_detection_list_path(const dfg * const g, list_item *K);



#endif