/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "iolb.h"

int _instance = 0;
int CACHE_SIZE = 1024;



// K-partitioning algorithm
expr* compute_paths(dfg * const g, const int count, bits (*bitmap)[count], expr* input_size)
{
	// Step 1 - compute the lower bounds of each nodes of the CFG
	size_t n_nodes = g->n_nodes;
	isl_set* param_dom = isl_set_params(isl_set_copy(g->nodes[n_nodes-1].dom));

	// Place to store the lower bounds bounds on each nodes of the CFG
	list_item *lbs = NULL;

	// For each node of the CFG...
	for(size_t i=0; i<n_nodes; ++i)
	{
		printf("\n=============================\nAnalyzing stmt %zu; Domain:",i);
		isl_printer_print_set(p_out, g->nodes[i].dom);
		printf("\n");

		bits mask = (1 << (+i%BITS_SIZE));

		// Place to collect the paths
		list_item *paths = NULL;

		int dim = g->nodes[i].dim;

		// Find the injective paths
		if(bitmap[i][i/BITS_SIZE] & mask) {
			for(const list_item *curr = g->inj_list[i]; curr != NULL; curr = curr->next) {
				edge* e = curr->data;
				isl_map *rel = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
				isl_union_map *dom = isl_union_map_from_map(isl_map_copy(rel));
				path p = {.source_idx=i, .sink_idx=i, .rel = rel, .dom = dom,
							.type = INJECTION, .contains_reduction=e->is_reduction};
				find_paths(g, i, e->sink_idx, i, &p, dim, &paths, e->is_reduction);

				/*
				printf("find_paths %zu %zu\n", i, e->sink_idx);
				for(int i=0; i<g->n_nodes; i++)
			   for(const list_item *curr = g->inj_list[i]; curr != NULL; curr = curr->next){
					edge* e = curr->data;
					if(e->visited)
						printf("visited: %zu->%zu\n", e->source_idx, e->sink_idx);
				}
				*/

				isl_map_free(rel);
				isl_union_map_free(dom);
			}
		}

		// Find the bcast paths
		for(size_t j = 0; j < n_nodes; j++) {
			for(const list_item *curr = g->bcast_list[j]; curr != NULL; curr = curr->next) {
				edge* e = curr->data;
				int sink_idx = e->sink_idx;
				if((sink_idx==i || bitmap[sink_idx][i/BITS_SIZE] & mask) && (g->nodes[sink_idx].dim >= dim)) {
					isl_map *rel = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
					if(sink_idx == e->source_idx) //loop case
					{
						isl_map* rel_copy = rel;
						//PRINT_ISL(p_out, isl_printer_print_map, rel);
						rel = isl_map_subtract_range(isl_map_copy(rel), isl_map_domain(isl_map_copy(rel)));
						isl_map_free(rel_copy);
					}
					isl_union_map *dom = isl_union_map_from_map(isl_map_copy(rel));
					path p = {.source_idx=j, .sink_idx=i, .rel = rel, .dom = dom,
									.type = BCAST, .contains_reduction = e->is_reduction};
					find_paths(g, i, e->sink_idx, e->source_idx, &p, dim, &paths, e->is_reduction);
					isl_map_free(rel);
					isl_union_map_free(dom);
				}
			}
		}

		size_t npaths = 0;
		for(list_item *curr = paths; curr != NULL; curr = curr->next) {
#if (VERBOSE > 0)
			path* p = curr->data;
			printf("\n\nStmt #%zu, Path #%zu ",i, npaths);
			if (p->type==BCAST) {
				printf("(Type BCAST):\n");
			}
			else {
				assert(p->type==INJECTION);
				printf("(Type INJECTION):\n");
			}
			printf("--------\n");
			printf("Relation:\n");
			PRINT_ISL(p_out, isl_printer_print_map, p->rel);

			printf("source_idx = %lu  | sink_idx = %lu\n", p->source_idx, p->sink_idx);

			printf("Domain:\n");
			PRINT_ISL(p_out, isl_printer_print_union_map, p->dom);
			printf("Kernel:\n");
			for(size_t i=0; i<p->k.n; ++i){
				printf("(");
				for(size_t j=0; j<dim; ++j){
					printf("%lf, ", p->k.M[i][j]);
				}
				printf(")\n");
			}
#endif
			npaths++;
		}
#if (VERBOSE > 0)
		printf("Total number of paths for vertex %zu: %zu\n\n",i, npaths);
		fflush(stdout);
#endif
		if(npaths == 0)
			continue;

		// Paths from a node were computed and stored inside "paths"
		// Combine these paths to get the bound on this statement
		// Store the result inside "&lbs"
		isl_union_map *dom_empty = isl_union_map_empty(isl_set_get_space(g->nodes[i].dom));
		space_info empty = {.D = g->nodes[i].dom, .dim = dim, .n_bases = 0, .base = NULL,
						.dom_inter = dom_empty, .K = NULL, .mul = g->nodes[i].n_iterations,
						.small_dims = g->nodes[i].small_dims,
						.small_dims_volume = g->nodes[i].small_dims_volume};
		bruteforce_combine_paths(g, empty, paths, &lbs, time(NULL));
	}

	printf("\n================================================================\n");
	fflush(stdout);		// To be sure to flush all the previous debug message
	size_t nlbs = 0;
	for(list_item *curr = lbs; curr != NULL; curr = curr->next) {
		lb_data* p = curr->data;
		printf("\n\nBound #%zu\n--------\nDomain:\n", nlbs);
		PRINT_ISL(p_out, isl_printer_print_union_set, p->dom);
		printf("\n");

		isl_set* ps = get_param_constraints(p->dom);
		bool b_asympt_param_cannot_be_equal = param_dom_equality_possible(ps);

		PRINT_ISL(p_out, isl_printer_print_set, ps);
		isl_set_free(ps);
		printf("\n");
		printf("Expression:\n");
		expr_print(p->bnd);
		printf("\n");

		// Sometimes, a bound forbid that all parameters have the same value (ex: 1/(M-N))
		if (b_asympt_param_cannot_be_equal) {
			expr_print(expr_simplify_expr(p->bnd));
		} else {
			expr_print(expr_simplify_expr(expr_params_equal(p->bnd)));
		}

		nlbs++;
	}
	printf("\n\n------------------------------\n");
	printf("\nTotal number of simple bounds: %zu\n",nlbs);
	printf("\n--------------------------------\n\n");
	fflush(stdout);


	// Step 2 - Combine the lower bounds
	expr* best_lb = NULL;
	list_item* lb_sums = compute_interferences(lbs, nlbs, g->all_edges);
	double max_val = 0;
	for(list_item* l = lb_sums; l != NULL; l = l->next) {

		// DEBUG
		//printf("LB EVALUATED => ");
		//expr_print(l->data);
		//fflush(stdout);

		double ev = evaluate_bound_on_big_value(l->data, param_dom);

		/* DEBUG
		printf("LB EVALUATED => ");
		expr_print(l->data);
		printf("\nPING EVAL => %lf\n", ev);
		fflush(stdout);
		//*/

		if(ev > max_val) {
			max_val = ev;
			best_lb = l->data;
		}
	}

	/* DEBUG
	if (best_lb==NULL) {
		printf("BESTLB STILL NULL !!!\n");
	}
	//*/

	if(input_size == NULL)
		input_size = expr_new("0");

	if(best_lb != NULL) {
		//printf("\n\n\nBest lower bound:\n");
		//expr_print(best_lb);
		//printf("\n\n");
		//expr_print(expr_params_equal(best_lb));
		//printf("\n\n");
	   // expr_print(expr_simplify_expr(expr_params_equal(best_lb)));
		//printf("\n\n");
      if(_instance) {
         double val_lb = expr_eval_instance(best_lb, CACHE_SIZE);
         if(val_lb < 0) val_lb = 0;
         double val_input = expr_eval_instance(input_size, CACHE_SIZE);
         printf("INSTANCE %lf + %lf = %d\n\n", val_lb, val_input, (int)(val_input + val_lb));
         //printf("INSTANCE simplif %lf\n\n", expr_eval_instance(expr_remove_negative(expr_simplify_expr(best_lb)), CACHE_SIZE));
      }
	}

    //complete lower bound
   // printf("\nComplete lower bound:\n");
	//expr_print(input_size);
	//if(best_lb != NULL) {
	//	printf(" + max(0, ");
	//	expr_print(best_lb);
	//    printf(")\n");
	//}

	//printf("\n%%");expr_print(expr_simplify_expr(expr_remove_negative(expr_add(input_size,best_lb))));printf("\n\n");
	//printf("\n%%");expr_print(expr_simplify_expr(input_size));printf("\n\n");

	//lower bound with all parameters equal, removing little o terms
	//printf("$");
	//expr_print(expr_simplify_expr(expr_params_equal(input_size)));
	//if(best_lb != NULL) {
	//	printf(" + \\max(0, ");
	//	expr_print(expr_simplify_expr(expr_params_equal(best_lb)));
	//    printf(")");
//	}
//	printf("$\n");

	if(best_lb == NULL)
		best_lb = expr_new("0");
	//operational intensity
	return best_lb;
}


// Gather the bounds from all methods (K-partitioning, wavefront ...)
void combined_LB(dfg* g, dfg* gred, expr* input_size)
{
	printf("=== START combined_LB ===\n");
	printf("\nDFG with %d dims\n\n\n", g->nodes[0].sched_size);
	//print_dfg(g);
	print_dfg(gred);

	printf("=== (end print_dfg) ===\n");

	// Compute the trans closure of the adjacency matrix (reachable nodes)
	size_t count = (g->n_nodes+BITS_SIZE-1)/BITS_SIZE; //encoded size of bitvect
	bits (*bitmap)[count] = calloc(g->n_nodes*count, sizeof(bits));
	compute_reachable_nodes(g, count, bitmap);


	// K-partitioning method (with reduction)
	printf("\n=== K-PARTITIONING METHOD ===\n");
	expr* lb1 = compute_paths(gred, count, bitmap, input_size);


	// Wavefront method (without reduction)
	printf("\n=== WAVEFRONT METHOD ===\n");
	expr* lb2 = cut_LB(g, count, bitmap);

	free(bitmap);


	/* DEBUG
	printf("\n\t\t\tFirst bound lb1:\n");
	expr_print(lb1);
	printf("\n\t\t\tSecond bound lb2:\n");
	expr_print(lb2);
	printf("\n==========================================\n");
	//*/


	// Checking which bound is larger & output a summary
	size_t n_nodes = g->n_nodes;
	isl_set* param_dom = isl_set_params(isl_set_copy(g->nodes[n_nodes-1].dom));
	bool b_asympt_param_cannot_be_equal = param_dom_equality_possible(param_dom);
	expr *lb;
	if(evaluate_bound_on_big_value(lb1, param_dom) > evaluate_bound_on_big_value(lb2, param_dom))
		lb = lb1;
	else
		lb = lb2;


	printf("\n=== CONCLUSIONS ===\n");

	printf("\n\nInput size:\n");
	expr_print(expr_simplify_expr(input_size));

	expr* lb_asymptotic = expr_simplify_S(expr_remove_negative(expr_simplify_expr(expr_add(lb, input_size))));
	printf("\n\n\nComplete lower bound:\n");
	expr_print(input_size);
	printf("+max(0,");
	expr_print(lb);
	printf(")\n");

	printf("\nWhen all params >> S");
	if (_small_dim) {
		printf(" and all small parameters << S");
	}
	printf(":\n");
	expr_print(lb_asymptotic);
	printf("\n\n");
	printf("When all non-small parameters are equal:\n");
	if (b_asympt_param_cannot_be_equal) {
		printf(" [Constraints on param forbid equality]");
	} else {
		expr_print(expr_params_equal(lb_asymptotic));
	}
	printf("\n");

	printf("\n\n\n");
	expr_print(expr_simplify_expr(input_size));
	printf("\n");
	expr_print(lb_asymptotic);
	printf("\n");
	expr_print(input_size);
	printf("+max(0,");
	expr_print(lb);
	printf(")\n");

	//if(g->nodes[0].sched_size > 0) {
	//	dfg* g1 = remove_outer_dim(g);
	//	combined_LB(g1, input_size);
	//}

	for(size_t i=0; i<g->n_nodes; ++i)
	{
		isl_set_free(g->nodes[i].dom);
		//TODO: sched has to be freed only for the whole dfg.
		//free(g->nodes[i].sched[i]);
		//Delete injective/bcast/small-bcast lists
		
		free(g->nodes[i].small_dims);		// Note: these 2 objects are shared with space_infos
		expr_free(g->nodes[i].small_dims_volume);

		list_item *next = g->inj_list[i];
		while(next != NULL)
		{
			list_item *curr = next;
			next = curr->next;
			isl_basic_map_free(((edge*)(curr->data))->rel);
			free(curr->data);
			free(curr);
		}
		next = g->bcast_list[i];
		while(next != NULL)
		{
			list_item *curr = next;
			next = curr->next;
			isl_basic_map_free(((edge*)(curr->data))->rel);
			free(curr->data);
			free(curr);
		}
	}
	if(g->all_edges != NULL)
		isl_union_map_free(g->all_edges);
	free(g->nodes);
	free(g->inj_list);
	free(g->bcast_list);
}

// MAIN function
int main(int argc, char **argv)
{
	char *file_name, *fn_name;

	FILE* dot_file = NULL;

	// By default, detect reduction, hourglass, but do not consider small dims
	_reduction_detection = true;
	_hourglass_detection = true;
	_small_dim = false;

	int c;
	while (1) {
		static struct option long_options[] =
		{
			{"verbose", optional_argument, 0, 'v'},
			{"instance", required_argument, 0, 'i'},
			{"output-dot", required_argument, 0, 'g'},

			// Specify which dims are small
			{"small", required_argument, 0, 's'},

			// Disable some part of the proof
			{"no-reduction", no_argument, 0, 'r'},  // Disable reduction detection
			{"no-hourglass", no_argument, 0, 'h'},  // Disable hourglass detection
			{0, 0, 0, 0}
		};
		int option_index = 0;

		// Doc on the ":" => https://www.gnu.org/software/libc/manual/html_node/Using-Getopt.html
		c = getopt_long(argc, argv, "v::i:g:s:rh", long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 'v':
			//_verbose = 1;
			break;
		case 's':
		{
			// Get small dims
			FILE* file_small_dim = fopen(optarg, "r");
			if (file_small_dim == NULL) {
				fprintf(stderr, "Error opening small dimensions file\n");
				return -1;
			}
			// Parse the file
			_l_small_dims = NULL;
			char str[50];		// Program parameter are not bigger than 50 chars
			int val;
			while (fscanf(file_small_dim, "%s%d", str, &val) >0) {
				add_small_dim_name(str, val);
			}
			fclose(file_small_dim);

			_small_dim = true;

			break;
		}
		case 'i':
		{
			FILE* inst_file = fopen(optarg, "r");
			if(inst_file == NULL) {
			   fprintf(stderr, "Error opening instance file\n");
			   return -1;
			}
			char str[20];
			int val;
			while(fscanf(inst_file, "%s%d", str, &val) > 0) {
			   add_instance(str, val);
			}
			fclose(inst_file);
			_instance = 1; 
			break;
	    }
		case 'g':
			dot_file = fopen(optarg, "w");
			if(dot_file == NULL) {
				fprintf(stderr, "Error opening dot file");
				return -1;
			}
			break;

		case 'r':
			_reduction_detection = false;  // Disable the reduction recognition
			break;

		case 'h':
			_hourglass_detection = false;  // Disable the hourglass recognition
			break;

		case '?':
			break;

		default:
		    abort ();
		}
	}

   if(optind >= argc)
   {
      fprintf(stderr,
      	"Usage: %s [-v [level] -i <instance_file> -g <dot_file> -m <mul> -s <small_dims> -r -h] <input_file> \n",
      	argv[0]);
      return -1;
   }

	//Initialize the isl context
	struct pet_options *opts = pet_options_new_with_defaults();
	isl_ctx *ctx = isl_ctx_alloc_with_options(&pet_options_args, opts);
	isl_options_set_on_error(ctx, ISL_ON_ERROR_CONTINUE);
	if(ctx == NULL)
	{
		fprintf(stderr, "Error allocating isl_ctx\n");
		return -1;
	}

        /*
	if(argc == 3)
	{
		fn_name = argv[2];
		pet_options_set_autodetect(ctx, 1);
	}
	else
        */
	{
		fn_name = NULL;
		pet_options_set_autodetect(ctx, 0);
	}
	file_name = argv[optind];

	//Initialize the isl printers
	p_out = isl_printer_to_file(ctx, stdout);
	p_err = isl_printer_to_file(ctx, stderr);

	//Extract the scop from input file
	struct pet_scop *scop = pet_scop_extract_from_C_source(ctx, file_name, fn_name);
	if(scop == NULL)
	{
		fprintf(stderr, "Error extracting scop\n");
		isl_ctx_free(ctx);
		return -1;
	}

	/* DEBUG - check _l_small_dims content
	printf("_l_small_dims =\n");
	for (list_item* ptr=_l_small_dims; ptr!=NULL; ptr=ptr->next) {
		small_dim_info* sdi = ptr->data;
		printf("\t- %s = %d\n", sdi->name_param, sdi->value);
	}
	//*/


	// Get info about if a statement of the scop is a += or a *=
	//	(before it is mysteriously corrupted by "create_dfg")
	bool* stm_has_red_op = get_info_reduction_op(scop);
	int** stm_dim_stride = get_info_strides_dim(scop);

	//Create DFG
	dfg g;
	int verbose_lvl = 1;
	expr* input_size = create_dfg(scop, &g, verbose_lvl);

	pip_init();

	// Detecting reductions
	dfg gred;
	//expr* input_size1 = create_dfg(scop, &gred);
	create_dfg(scop, &gred, 0);
	if (_reduction_detection) {
		reduction_detection(scop, stm_has_red_op, stm_dim_stride, &gred, verbose_lvl);
	}

	// Note: we will need both g and gred in combined_LB
	//	- g: DFG without reduction (needed for wavefront method)
	// - gred: DFG with reduction (needed for the K-partitioning method)

	free(stm_has_red_op);
	for (int i=0; i<scop->n_stmt; i++) {
		free(stm_dim_stride[i]);
	}
	free(stm_dim_stride);
	pet_scop_free(scop);

	// DEBUG - TEMP STOP (for reduction debugging)
	//fflush(stdout);
	//assert(false);


	if(dot_file != NULL) {
	  dfg_to_dot(&g, dot_file);
	  fclose(dot_file);
	}

	// Main function
	combined_LB(&g, &gred, input_size);


	//print_dfg(&g);

	pip_close();

	//Free the isl printers and context 
	isl_printer_free(p_out);
	isl_printer_free(p_err);
	isl_ctx_free(ctx);

	//printf("\nDone.\n");
	return 0;
}
