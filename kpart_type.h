#ifndef KPART_TYPE_H
#define KPART_TYPE_H

#include <isl/map.h>
#include <isl/space.h>

#include "utility.h"
#include "dfg.h"


typedef struct {
	double **M; //Array of vectors
	size_t n; //No. of vectors
} subspace;


typedef struct {
	isl_set *D; //Domain of interest
	int dim; //dimension of D
	double **base; //matrix containing the bases
	size_t n_bases; //No. of bases
	list_item *K; //Set of paths; each element is a subspace spanned by a single path
	isl_union_map *dom_inter; //interference domain
	expr* mul; //multiplicator  (cf iolb.c : number of iteration of the node)

	bool* small_dims; // Is a dimension a small dims (array of size "dim")
	expr* small_dims_volume;
} space_info;


typedef struct {
	size_t source_idx;
	size_t sink_idx;
	
	isl_map *rel; //relation of the path
	isl_union_map *dom; //domain of the path
	edge_type type; //type
	subspace k;
  bool contains_reduction;  // [OLD - not used anymore] Does the path contain an
  													//				edge which is a reduction?
                            // (only for INJECTION path, else, default is false)
} path;



// Data structure regrouping all the information of the completed hourglass pattern
typedef struct {
	// Relation of the hourglass path
	isl_map* hrgl_rel;

	// Nodes identifiers (in the graph g)
	// Path is: S2 --inj--> SR --red--> SR --inj--> S1 --bcast--> S2
	size_t node_S1;
	size_t node_S2;
	size_t node_SR;

	// Dimensions (in respect to the dimensions of S2)
	int ndim_S2;
	bool* is_temporal_dim;	// "k"
	bool* is_neutral_dim;	// "j"
	bool* is_red_bcst_dim;	// "i"

	// Size of the tall part of the hourglass
	isl_set* res_set_inters;	// Domain of the points in the middle of S[k] and S[k+2]
	expr *expr_card;
} compl_hourglass_p;



typedef struct {
	expr *bnd;
	isl_union_set *dom;
} lb_data;

#endif
