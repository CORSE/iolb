

#include "path.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <gmp.h>
#include <isl/map.h>
#include <isl/aff.h>
#include <isl/multi.h>
#include <isl/local_space.h>
#include <isl/constraint.h>
#include <barvinok/isl.h>
#include "utility.h"


// Linear algebra auxilliary functions

int isl_pw_multi_aff_n_piece(isl_pw_multi_aff*);

// Check if relation matrix is Id. If yes, give back its dependence vector
static int ray(__isl_take isl_set *s, __isl_take isl_multi_aff *multi_aff, void *v_v)
{
	isl_set_free(s);
	double **v = ((double**)(v_v));
	//Get no. of rows
	int n = isl_multi_aff_dim(multi_aff, isl_dim_out);
	*v = (double*)malloc(n * sizeof(double));
#if (VERBOSE>2)
	printf("\nAnalyzing the expression:\n");
	PRINT_ISL(p_out, isl_printer_print_multi_aff, multi_aff);
#endif
	//For each row check the coefficients.
	for(int i=0; i<n; ++i)
	{
		isl_aff *aff = isl_multi_aff_get_aff(multi_aff, i);
		if(aff == NULL)
		{
			fprintf(stderr, "\nError getting the affine expression %d from:\n", i);
			PRINT_ISL(p_err, isl_printer_print_multi_aff, multi_aff);
			continue;
		}
		int m = isl_aff_dim(aff, isl_dim_in);
		for(int j=0; j<m; ++j)
		{
			isl_val *val = isl_aff_get_coefficient_val(aff, isl_dim_in, j);
			//Check if the matrix is identity
			if((val==NULL) || (i==j && isl_val_is_one(val)!=1) || (i!=j && isl_val_is_zero(val)!=1))
			{
				isl_aff_free(aff);
				free(*v);
				*v = NULL;
				if(val==NULL)
				{
					fprintf(stderr, "\nError getting the co-efficient of affine expression %d from:\n", i);
					PRINT_ISL(p_err, isl_printer_print_aff, aff);
					isl_multi_aff_free(multi_aff);
					return -1;
				}
				else
				{
#if (VERBOSE>1)
					printf("Matrix is not identity. i: %d; j: %d; val: %lf\n", i, j, isl_val_get_d(val));
#endif
				}
				isl_val_free(val);
				isl_multi_aff_free(multi_aff);
				return 0;
			}
			isl_val_free(val);
		}
		//Get the constant and add it to the vector
		isl_val *val = isl_aff_get_constant_val(aff);
		isl_aff_free(aff);
		if(val==NULL)
		{
			fprintf(stderr, "\nError getting the constant of affine expression %d from:\n", i);
			PRINT_ISL(p_err, isl_printer_print_aff, aff);
			isl_multi_aff_free(multi_aff);
			return -1;
		}
		(*v)[i] = isl_val_get_d(val);
		isl_val_free(val);
	}
	isl_multi_aff_free(multi_aff);
	return 0;
}

// Get the kernel of a relation matrix
static int kernel(__isl_take isl_set *s, __isl_take isl_multi_aff *multi_aff, void *v_k)
{
	subspace *k = ((subspace*)(v_k));
	//Get no. of rows and columns
	int m = isl_multi_aff_dim(multi_aff, isl_dim_out);
	int n = isl_multi_aff_dim(multi_aff, isl_dim_in);
	//assert(m <= n || !(printf("Failed due to m: %d, n: %d\n", m, n)));
#if (VERBOSE>2)
	printf("\nAnalyzing the expression:\n");
	PRINT_ISL(p_out, isl_printer_print_multi_aff, multi_aff);
#endif
	//Create matrix M.
	isl_mat *M = isl_mat_alloc(isl_set_get_ctx(s), m, n);
	isl_set_free(s);
	//Populate M
	for(int i=0; i<m; ++i)
	{
		isl_aff *aff = isl_multi_aff_get_aff(multi_aff, i);
		if(aff == NULL)
		{
			fprintf(stderr, "\nError getting the affine expression %d from:\n", i);
			PRINT_ISL(p_err, isl_printer_print_multi_aff, multi_aff);
			continue;
		}
		for(int j=0; j<n; ++j)
		{
			isl_val *val = isl_aff_get_coefficient_val(aff, isl_dim_in, j);
			M = isl_mat_set_element_val(M, i, j, val);
		}
	}
#ifndef NDEBUG
	int nnz = 0;
	for(int i=0; i<m; ++i)
	{
		for(int j=0; j<n; ++j)
		{
			isl_val *val = isl_mat_get_element_val(M, i, j);
			double val_d = isl_val_get_d(val);
			isl_val_free(val);
			if(val_d != 0)
			{
				++nnz;
				break;
			}
		}
	}
#endif
	//Get the kernel
	isl_mat *ker = isl_mat_right_kernel(M);
	if(ker == NULL)
	{
		fprintf(stderr, "\nError getting the kernel from:\n");
		PRINT_ISL(p_err, isl_printer_print_multi_aff, multi_aff);
		k->n = 0;
		isl_multi_aff_free(multi_aff);
		return 0;
	}
	int n_k = isl_mat_cols(ker);
	//assert((n_k + nnz == n) || !(printf("Failed due to n_k: %d, nnz: %d, m: %d\n", n_k, nnz, n)));
	//assert(isl_mat_rows(ker) == n);
	//assert(n_k <= n);
	for(int i=0; i<n_k; ++i)
	{
		k->M[i] = (double*)malloc(n*sizeof(double));
		for(int j=0; j<n; ++j)
		{
			isl_val *val = isl_mat_get_element_val(ker, j, i);
			k->M[i][j] = isl_val_get_d(val);
			isl_val_free(val);
		}
	}
	k->n = n_k;

	isl_mat_free(ker);
	isl_multi_aff_free(multi_aff);
	return 0;
}


// ====================================================


//Return the set difference for set of vectors
static const double** complement_space(const double** const full_space, const int s1, const double ** const sub_space, const int s2, const int n)
{
	//assert(s1 >= s2);
	const double **comp_space = (const double**)malloc(s1 * sizeof(double*));
	int cnt = 0;
	for(int i=0; i<s1; ++i)
	{
		bool ret = is_vector_in(sub_space, full_space[i], s2, n);
		if(ret == false)
		{
			comp_space[cnt] = full_space[i];
			++cnt;
		}
	}
	//assert((cnt >= (s1-s2)) || !(printf("Failed for values cnt: %d, s1: %d, s2: %d\n", cnt, s1, s2)));
	return comp_space;
}

//Return true if the subspace k is redundant in K
//i.e. if \exists k' \in K :  (b\k) \subset (b\k'), return true
static bool is_subspace_redundant(const double** const b, const int n_b, const subspace * const k, const list_item * const K, const int dim)
{
	const double **k_comp = complement_space(b, n_b, ((const double ** const)(k->M)), k->n, dim);
	int k_comp_n = n_b - k->n;
	bool ret = false;
	//Check for each subspace in K
	for(const list_item *curr = K; curr!=NULL; curr=curr->next)
	{
		const subspace * const kp = &((const path * const)(curr->data))->k;
		const double **kp_comp = complement_space(b, n_b, ((const double ** const)(kp->M)), kp->n, dim);
		int kp_comp_n = n_b - kp->n;
		if(kp_comp_n < k_comp_n)
		{
			free(kp_comp);
			continue;
		}
		int cnt = 0;
		//check if all vectors in k_comp are in kp_comp
		for(int i=0; i<k_comp_n; ++i)
		{
			for(int j=0; j<kp_comp_n; ++j)
			{
				if(kp_comp[j] == k_comp[i])
				{
					++cnt;
					break;
				}
			}
			if(cnt==k_comp_n)
			{
				ret = true;
				break;
			}
		}
		free(kp_comp);
		if(ret == true)
			break;
	}

	free(k_comp);
	return ret;
}

/* === Unused
static int set_to_basic_set(isl_basic_set *s, void *b_v)
{
	isl_basic_set **b = ((isl_basic_set**)(b_v));
	assert(*b == NULL); //The set should contain only one basic set
	*b = s;
	return 0;
}
*/

// Extract the projection associated to a path ("p->k" is the true output)
// Return true if it worked, else return false
bool path_proj_subspace(path* p, int dim)
{
	subspace k;
	k.M = (double**)malloc(dim * sizeof(double*));
	isl_map* rel = p->rel;

	// Path is an injection, vector is the shift
	if(p->type == INJECTION)
	{
		//Get the multi-affine expr
		isl_pw_multi_aff *pw_multi_aff = isl_pw_multi_aff_from_map(isl_map_copy(rel));
		if(pw_multi_aff == NULL) {
			fprintf(stderr, "\nError extracting the multi-affine expression for the relation:\n");
			PRINT_ISL(p_err, isl_printer_print_map, rel);
			free(k.M);
			return false;
		}
		assert(isl_pw_multi_aff_n_piece(pw_multi_aff) == 1);
		//Call ray(); ray() checks if the relation matrix is identity, if so, gives the dependence vector.
		isl_pw_multi_aff_foreach_piece(pw_multi_aff, ray, k.M);
		isl_pw_multi_aff_free(pw_multi_aff);
		if(k.M[0] == NULL) {
			free(k.M);
			return false;
		}
		k.n = 1;
	}
	else
	{
		assert(p->type == BCAST);
		// Path is a Broadcast: vector are the kernel of the inverse of this relation

		isl_pw_multi_aff *pw_multi_aff = isl_pw_multi_aff_from_map(isl_map_reverse(isl_map_copy(p->rel)));
		if(pw_multi_aff == NULL)
		{
			fprintf(stderr, "\nError extracting the multi-affine expression for the relation:\n");
			PRINT_ISL(p_err, isl_printer_print_map, p->rel);
			free(k.M);
			return false;
		}
		assert(isl_pw_multi_aff_n_piece(pw_multi_aff) == 1);
		//kernel() tries to get the kernel of the relation matrix
		isl_pw_multi_aff_foreach_piece(pw_multi_aff, kernel, &k);
		isl_pw_multi_aff_free(pw_multi_aff);
		if(k.n == 0){
			free(k.M);
			return false;
		}
	}
	p->k = k;
	return true;
}


// On of the main function for the K-partitioning method
// The paths for a node were already computed (in "paths"), we compute the lower bounds
//	and store them inside "lbs"
void bruteforce_combine_paths(const dfg * const g, space_info curr_tuple, const list_item* const paths,
		list_item **lbs, time_t start_time)
{
#ifdef TIMEOUT
	if(difftime(time(NULL), start_time) > TIMEOUT)
		return;
#endif
	// Recover the infos from "curr_tuple"
	int dim = curr_tuple.dim;
	int n_bases = curr_tuple.n_bases;
	double **base = curr_tuple.base;
	isl_set *D = curr_tuple.D;
	list_item* K = curr_tuple.K;

#ifdef DEBUG
	   printf("DEBUG\n");
	     printf("Domain D:\n");
	     PRINT_ISL(p_out, isl_printer_print_set, D);
	     printf("Interference domain:\n");
	     PRINT_ISL(p_out, isl_printer_print_union_map, curr_tuple.dom_inter);
	     printf("Basis:\n");
	     for(size_t i=0; i<n_bases; ++i){
		     printf("(");
		     for(size_t j=0; j<dim; ++j){
			     printf("%lf, ", base[i][j]);
		     }
		     printf(")\n");
	     }
#endif

	/* DEBUG
	printf("Ping - bruteforce_combine_paths | n_bases = %i | dim = %i\n", n_bases, dim);
	printf("base =\n");
	for (int i=0; i<n_bases; i++) {
		printf("\t[");
		for (int j=0; j<dim; j++)
			printf(" %f", base[i][j]);
		printf("]\n");
	}
	//*/

	
	// Termination condition #1
	// If the tuple spans the whole graph, add the complexity to lbs and return
	if(n_bases == dim) {
		// DEBUG
		//printf("Ping - n_bases = dim !\n");

#if (VERBOSE > 2)
		printf("\nFound path combination\n");
		printf("Domain D:\n");
		PRINT_ISL(p_out, isl_printer_print_set, D);
		printf("Interference domain:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, curr_tuple.dom_inter);
		printf("Basis:\n");
		for(size_t i=0; i<n_bases; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", base[i][j]);
			}
			printf(")\n");
		}
#endif

		// We have a covering of the space: get the LB!
		lb_data* lb = solve(g, &curr_tuple);

		if(lb != NULL) {
			bool keep = 1;
			for(list_item* l = *lbs; l != NULL; l = l->next) {
				lb_data* curr_lb = l->data;
				if(expr_is_greater(curr_lb->bnd, lb->bnd) && 
						find_union_set_dim(isl_union_set_intersect(
							isl_union_set_copy(curr_lb->dom),
							isl_union_set_copy(lb->dom))) > 0) {
					keep = 0;
					break;
				}
			}

			if(!keep)
				return;
			
			list_item **pre = lbs;
			for(list_item* l = *lbs; l != NULL;) {
				lb_data* curr_lb = l->data;
				if(expr_is_greater(curr_lb->bnd, lb->bnd) && 
						find_union_set_dim(isl_union_set_intersect(
							isl_union_set_copy(curr_lb->dom),
							isl_union_set_copy(lb->dom))) > 0) {
						*pre = l->next;
						isl_union_set_free(((lb_data*)l->data)->dom);
						free(l);
						l = *pre;
				}
				else {
					pre = &l;
					l = l->next;
				}
			}
			list_item* llb = malloc(sizeof(list_item));
			llb->data = lb;
			llb->next = *lbs;
			*lbs = llb;
		}
		return;
	}

	// Termination condition #2: no more paths
	if(paths == NULL)
		return;


	// First branch: recursive call (not selecting this path)
	bruteforce_combine_paths(g, curr_tuple, paths->next, lbs, start_time);


	// Second branch: selection this path in the combination
	// Now we deal with the current path (of the chained list "paths")
	path* p = paths->data;
	isl_map *rel = p->rel;
	subspace k = p->k;
	
	// new_D = output_space_rel \inters space_info.D  (is the combination pertinent?)
	isl_set *new_D = isl_map_range(isl_map_copy(rel));
	new_D = isl_set_align_params(new_D, isl_set_get_space(D));
	new_D = isl_set_intersect(new_D, isl_set_copy(D));
	
/*
		printf("\n\nDomain D:\n");
		PRINT_ISL(p_out, isl_printer_print_set, D);
		printf("Interference domain:\n");
		PRINT_ISL(p_out, isl_printer_print_union_set, curr_tuple.dom_inter);
		printf("Basis:\n");
		for(size_t i=0; i<n_bases; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", base[i][j]);
			}
			printf(")\n");
		}
		printf("Trying to add path:\n");
		PRINT_ISL(p_out, isl_printer_print_map, rel);
		printf("Kernel:\n");
		for(size_t i=0; i<k.n; ++i){
			printf("(");
			for(size_t j=0; j<dim; ++j){
				printf("%lf, ", k.M[i][j]);
			}
			printf(")\n");
		}
		printf("new_D:\n");
		PRINT_ISL(p_out, isl_printer_print_set, new_D);
*/

	// If new_D is reduced in dimentionality (ie, path only cover a smaller dim space)
	//		then cast out this combination
	if(new_D == NULL)
		return;
	int	new_dim = find_set_dim(isl_set_copy(new_D));
	if(new_dim < dim)
	{
		isl_set_free(new_D);
		return;
	}

	// The combination of path is potentially interesting!
	// Now, let's look at the independence of their associated vectors.
	size_t new_base_cnt = 0;
	double **new_base = (double**)malloc(k.n * sizeof(double*));
	for (size_t i=0; i<k.n; ++i) {
		/* DEBUG
		printf("\t\t-> can_expand_base - Input base =\n");
		for (int k=0; k<n_bases; k++) {
			printf("\t\t\t[");
			for (int j=0; j<dim; j++)
				printf(" %f", base[k][j]);
			printf("]\n");
		}
		printf("\t\tk.M[i] = [");
		for (int j=0; j<dim; j++)
			printf(" %f", k.M[i][j]);
		printf("]\n");
		//*/

		// Check base completion
		// - ret_code == -1 => not independent from base
		// - ret_code == 0  => in base
		// - ret_code == 1  => New independent vector !!!
		int ret_code = can_expand_base(isl_map_get_ctx(rel), ((const double**)(base)), n_bases, k.M[i], dim);

		/* OLD: if(ret_code == -1) break; */
		// Note: k.M[i] are already independent vector => no need to restart the loop every times
		if(ret_code == 1) {
			new_base[new_base_cnt] = k.M[i];
			++new_base_cnt;
		}
	}

	// OLD code: if (ret_code == -1 || new_base_cnt == 0
	//	|| is_subspace_redundant(((const double ** const)(new_base)), new_base_cnt, &k, K, dim))	
	// If cannot expand the base, continue to next tuple
	if (new_base_cnt == 0 ||
			is_subspace_redundant(((const double ** const)(new_base)), new_base_cnt, &k, K, dim)) {
		free(new_base);
		isl_set_free(new_D);
		return;
	}

	double** combined_base = (double**) malloc((n_bases+new_base_cnt)*sizeof(double*));
	memcpy(combined_base, base, sizeof(double*)*n_bases);
	for(size_t i=0; i<new_base_cnt; ++i) {
		combined_base[n_bases+i] = new_base[i];
	}
	free(new_base);

	/* DEBUG
	for (int k=0; k<n_bases+new_base_cnt; k++) {
		printf("\t\t\t[");
		for (int j=0; j<dim; j++)
			printf(" %f", combined_base[k][j]);
		printf("]\n");
	}
	//*/


	space_info new_tuple;
	new_tuple.D = new_D;
	new_tuple.dim = new_dim;
	new_tuple.mul = curr_tuple.mul;
	new_tuple.small_dims = curr_tuple.small_dims;
	new_tuple.small_dims_volume = curr_tuple.small_dims_volume;
	
	new_tuple.base = combined_base;
	new_tuple.n_bases = n_bases+new_base_cnt;
	
	new_tuple.K = (list_item*)malloc(sizeof(list_item));
	new_tuple.K->data = p;
	new_tuple.K->next = K;
	/*subspace *new_k = malloc(sizeof(subspace));
	new_k->M = (double**)malloc(n_k * sizeof(double*));
	new_k->n = n_k;
	for(size_t i=0; i<n_k; ++i) {
		new_k->M[i] = k.M[i];
	}*/
	new_tuple.dom_inter = isl_union_map_coalesce(isl_union_map_union(isl_union_map_copy(curr_tuple.dom_inter), isl_union_map_copy(p->dom)));

	// New recursion, with the current "path" included (in new_tuple)
	bruteforce_combine_paths(g, new_tuple, paths->next, lbs, start_time);
}

// Note (TODO clean-up code): part of the "find_paths" procedure is in "iolb.c"
//		(the init part, which build the first path to start with)
// Note 2: no freeing of the intermediate paths in the next algo... :/


// Arguments:
//	- g = graph
//	- target_id = Node to reach (goal)
//	- curr_id = Current node we are on
//	- start_id = Node we started on
//	- p = current path
//	- dim = dimensionality of the target_id node
//	- paths = list of paths founds  [True output]
//	- has_reduction = does the path contain a reduction edge?  [OLD - not used anymore]
void find_paths(const dfg * const g,
			const size_t target_id, const size_t curr_id, const size_t start_id,
			const path * const p, const int dim, list_item **paths, bool has_reduction)
{
	if(dim <= 1)
		return;

	if(curr_id == target_id) {
#if (VERBOSE > 2)
		printf("\n\nFound path of type %d:\n Relation:\n", p->type);
		PRINT_ISL(p_out, isl_printer_print_map, p->rel);
		printf("\nDomain:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, p->dom);
		printf("\n");
#endif
		// Build the new (looping) path
		path *new_p = malloc(sizeof(path));
		new_p->source_idx = start_id;
		new_p->sink_idx = target_id;
		new_p->rel = isl_map_copy(p->rel);
	    //new_p->dom = isl_union_set_copy(p->dom); 
		new_p->dom = isl_union_map_union(isl_union_map_copy(p->dom),
				isl_union_map_from_map(isl_map_copy(p->rel)));
		new_p->type = p->type;
		if(!path_proj_subspace(new_p, dim)) {
			// Extracting the corresponding vector did not work: abandon the path
			free(new_p);
			return;
		}
		new_p->contains_reduction = has_reduction;

		// check is path is useful
		//for(list_item *curr = *paths; curr != NULL; curr=curr->next) {
		//	path* p_i = curr->data;
		//}
		
		// Add it to the list of paths found
		list_item *it = malloc(sizeof(list_item));
		it->next = *paths;
		it->data = new_p;
		*paths = it;

		return;
	}

#if (VERBOSE > 2)
	printf("\n target %zu  curr %zu:\nrel:  ", target_id, curr_id);
	PRINT_ISL(p_out, isl_printer_print_map, p->rel);
	printf("\ndom:  ");
	PRINT_ISL(p_out, isl_printer_print_union_map, p->dom);
	printf("\n");
#endif

	// For every injective edges, recursive call on that direction
	for(list_item *curr = g->inj_list[curr_id]; curr!=NULL; curr=curr->next) {

		edge *e = ((edge*)(curr->data));
		if(e->visited) //TODO : this prevents finding some injective paths
			continue;

		isl_map* rel = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
		//printf("DEBUG rel:\n");
		//PRINT_ISL(p_out, isl_printer_print_map, rel);
		isl_union_map* rel_umap = isl_union_map_from_map(isl_map_copy(rel));
		rel_umap = isl_union_map_subtract_range(rel_umap, isl_union_map_range(isl_union_map_copy(p->dom)));

		/* DEBUG
		printf("DEBUG rel_umap:\n");
		PRINT_ISL(p_out, isl_printer_print_union_map, rel_umap);
		fflush(stdout);
		//*/
		
		if(isl_union_map_is_empty(rel_umap)) {
			isl_union_map_free(rel_umap);
			isl_map_free(rel);
			continue;
		}
		rel = isl_map_from_union_map(rel_umap);
		rel = isl_map_apply_range(isl_map_copy(p->rel), rel);

		int new_dim = find_set_dim(isl_map_range(isl_map_copy(rel)));

		if (new_dim < dim){
			// DEBUG
			//printf("\t... issue with dim? new_dim = %d | dim = %d\n", new_dim, dim);
			isl_map_free(rel);
			continue;
		}
		isl_union_map *new_dom = isl_union_map_union(isl_union_map_copy(p->dom), isl_union_map_from_map(isl_map_copy(rel)));
		path new_p = {.source_idx=curr_id, .sink_idx=target_id,
			.rel = rel, .dom = new_dom, .type = p->type};

		bool n_has_reduction = has_reduction || (e->is_reduction);

		// Recursive call
		e->visited = true;
		find_paths(g, target_id, e->sink_idx, start_id, &new_p, dim, paths, n_has_reduction);
		e->visited = false;

		// Clean-up
		isl_union_map_free(new_dom);
		isl_map_free(rel);
	}
}

list_item* compute_interferences(const list_item * const lbs, size_t n_lbs, isl_union_map *all_edges)
{
	int** indep = malloc(n_lbs*sizeof(int*));
	expr** bnds = malloc(n_lbs*sizeof(expr*));

	size_t i = 0, j = 0;
	for(const list_item* curr = lbs; curr != NULL; curr = curr->next)
		bnds[i++] = ((lb_data*)curr->data)->bnd;

	/* DEBUG
	printf("compute_interferences : bnds =\n");
	for (i=0; i<n_lbs; i++) {
		printf("\t* ");
		expr_print(bnds[i]);
		printf("\n");
	}
	//*/


	for(i=0; i<n_lbs; i++)
		indep[i] = calloc(n_lbs, sizeof(int));

	i = 0;
	for(const list_item* i1 = lbs; i1 != NULL; i1 = i1->next) {
		lb_data* l1 = i1->data;
		j = i + 1;
		for(const list_item* i2 = i1->next; i2 != NULL; i2 = i2->next) {
			lb_data* l2 = i2->data;
			//two lbs do not interfere if their domain are disjoint,
			//or if the image of the intersection is contained in one of them
			isl_union_set *d1 = isl_union_set_copy(l1->dom);
			isl_union_set *d2 = isl_union_set_copy(l2->dom);
			isl_union_set *inter = isl_union_set_intersect(d1, d2);
			//printf("Inter %zu %zu\n", i, j);
			//PRINT_ISL(p_out, isl_printer_print_union_set, inter);
			if(isl_union_set_is_empty(inter)) {
				indep[i][j] = 1;
				indep[j][i] = 1;
				isl_union_set_free(inter);
			}
			else {
				isl_union_set *img = isl_union_set_apply(inter, isl_union_map_copy(all_edges));
				isl_union_set *img_inter_d1 = isl_union_set_intersect(isl_union_set_copy(img), isl_union_set_copy(l1->dom));
				isl_union_set *img_inter_d2 = isl_union_set_intersect(img, isl_union_set_copy(l2->dom));
				if(isl_union_set_is_empty(img_inter_d1) || isl_union_set_is_empty(img_inter_d2)) {
					indep[i][j] = 1;
					indep[j][i] = 1;
				}
				isl_union_set_free(img_inter_d1);
				isl_union_set_free(img_inter_d2);
			}
			j++;
		}
		i++;
	}

#if (VERBOSE > 0)
	printf("\n\nInterference matrix:\n");
	for(i = 0; i<n_lbs; i++) {
		for(j = 0; j < n_lbs; j++) {
			printf("%d ", indep[i][j]);
		}
		printf("\n");
	}
#endif

	//naive cover
	list_item* lb_sums = NULL;
	int* summed = calloc(n_lbs, sizeof(int));
	for(i = 0; i < n_lbs; i++)
		if(!summed[i]) {
			expr* sum = bnds[i];
			list_item *clique = malloc(sizeof(list_item));
			clique->next = NULL;
			clique->data = (void*)i;
			for(j = i + 1; j < n_lbs; j++)
				if(!summed[j]) {
					bool summable = 1;
					for(list_item* l = clique; l != NULL; l = l->next)
						if(!indep[j][(size_t)l->data]) {
							summable = 0;
							break;
						}
					if(summable) {
						sum = expr_add(sum, bnds[j]);
						summed[j] = 1;
					}
				}

			summed[i] = 1;
			bool keep = 1;
			for(list_item* l = lb_sums; l != NULL; l = l->next) {
				if(expr_is_greater(l->data, sum)) {
					keep = 0;
					break;
				}
			}

			if(!keep)
				continue;
			
			list_item **pre = &lb_sums;
			for(list_item* l = lb_sums; l != NULL;) {
				if(expr_is_greater(sum, l->data)) {
					*pre = l->next;
					free(l);
					l = *pre;
				}
				else {
					pre = &l;
					l = l->next;
				}
			}
			list_item *new_l = malloc(sizeof(list_item));
			new_l->data = sum;
			new_l->next = lb_sums;
			lb_sums = new_l;
		}
	printf("\n\n\nSummed lower bounds:\n-----------------------\n");
	for(list_item *l = lb_sums; l != NULL; l = l->next) {
		lb_data* lb = l->data;

		expr_print(lb);
		printf("\n");

		isl_set* ps = get_param_constraints(lb->dom);
		bool b_asympt_param_cannot_be_equal = param_dom_equality_possible(ps);
		if (!b_asympt_param_cannot_be_equal) {
			expr_print(expr_params_equal(lb));
			printf("\n\n");
		}
	}
	
	free(summed);
	for(i=0; i<n_lbs; i++)
		free(indep[i]);
	free(indep);

	return lb_sums;
}
