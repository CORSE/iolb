#ifndef PATH_H
#define PATH_H

#include <math.h>
#include <time.h>
#include <isl/val.h>
#include <isl/mat.h>
#include <isl/set.h>
#include <piplib/piplibMP.h>
#include <isl/polynomial.h>
#include "dfg.h"
#include "expr_utils.h"
#include "complexity.h"

// Find all the paths from target_id (recursive calls)
void find_paths(const dfg * const g,
			const size_t target_id, const size_t curr_id, const size_t start_id,
			const path * const p, const int dim, list_item **paths, bool has_reduction);

// K-partitioning method on a statement, based on the paths (recursive calls)
void bruteforce_combine_paths(const dfg * const g, space_info curr_tuple, const list_item * const paths, list_item **lbs, time_t start_time);

// Combine lower bounds from different statements
list_item* compute_interferences(const list_item * const lbs, size_t n_lbs, __isl_keep isl_union_map *all_edges);

#endif