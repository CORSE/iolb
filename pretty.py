#!/usr/bin/env python3

# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry, Guillaume Iooss
#    Date of creation: 2019 - 2021
#
#    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================


from sympy.parsing.sympy_parser import *
from sympy import *
import sys
import string
import re
import argparse

def reindent(s, numSpaces):
    s = s.splitlines()
    s = [(numSpaces * ' ') + line for line in s]
    s = '\n'.join(s)
    return s

S = Symbol('S')
E = Symbol('E')
N = Symbol('N')
def parse_str(s):
    return parse_expr(s, local_dict = {'S' : S, 'E' : E, 'N' : N, 'I': I, 'O': O},transformations=standard_transformations + (convert_xor,) )

inp=re.compile('^Input size')
complete=re.compile('^Complete lower bound')
asymptotic=re.compile('^When all params')
equals=re.compile('.*are equal:$')
complexity=dict()
for log in sys.stdin:
    if complete.match(log): complexity['complete']=sys.stdin.readline()
    if asymptotic.match(log): complexity['asymptotic']=sys.stdin.readline()
    if equals.match(log): complexity['equals']=sys.stdin.readline()
    if inp.match(log): complexity['inputs']=sys.stdin.readline()

def bold(string):
    return '\033[1m'+string+'\033[0m'

init_printing()
def prettyIO(description, expr):
    print(bold(description)+':')
    print(reindent(pretty(parse_str(expr)),5))
    #print(pretty(parse_str(log[linenb])))
    print('\n')


parser = argparse.ArgumentParser("Pretty filter for ioaffine output")
parser.add_argument('--inputs', action='store_true', help='I/O due to called miss of inputs inly')
parser.add_argument('--complete', action='store_true', help='Complete precise expression for the I/O')
parser.add_argument('--asymptotic', action='store_true', help='Asymptotic expression assumming small parameters (if any) <<S and others >> S')
parser.add_argument('--simplified', action='store_true', help='Asymptotic assuming all non-small parameters are equal')
parser.add_argument('--all', action='store_true', help='all bounds')
args=parser.parse_args()

none=True
if args.inputs or args.all:
    prettyIO("Inputs", complexity['inputs'])
    none=False
if args.complete or args.all:
    prettyIO("Full expression", complexity['complete'])
    none=False
if args.asymptotic or args.all:
    prettyIO("Asymptotic expression",complexity['asymptotic'])
    none=False
if args.simplified or args.all:
    prettyIO("Simplified expression", complexity['equals'])
    none=False

#Default if none chosen
if none:
    prettyIO("Asymptotic expression",complexity['asymptotic'])

