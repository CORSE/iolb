/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "reduction.h"

// Reduction detection in a DFG

// Structure to store the detected reduction data
//		Reduction must be along canonical dimensions, and on rectangular domains
typedef struct {
	int dim_num;			// Dimension id on which the reduction is (= size of ind_coeff)

	isl_val** min;			// Min of the dim (assume rect domain / [ind_coeff | params_coeff | const_coeff] )
	isl_val** max;			// Max of the dim (assume rect domain / [ind_coeff | params_coeff | const_coeff] )
	int nind_param_cst;		// Size of min/max 1D arrays (= nind + nparam + 1 (cst term))
	int nind;
	int nparam;

	bool direction;         // True = toward positive / False = toward negative

	edge* inj_edge_red;		// Corresponding injective edge composing the reduction
} reduction_data;

void free_reduction_data(reduction_data* rd) {
	for (int i=0; i<rd->nind_param_cst; i++) {
		isl_val_free(rd->max[i]);
		isl_val_free(rd->min[i]);
	}
	free(rd->min);
	free(rd->max);

	isl_basic_map_free(rd->inj_edge_red->rel);

	free(rd);

	return;
}

void print_reduction_data(reduction_data* rd) {
	printf("[ Reduced_dim = %i  | (nind + nparam + cst = %d + %d + 1 = %d)\n",
			rd->dim_num, rd->nind, rd->nparam, rd->nind_param_cst);

	printf("\tmin_bound = [");
	for (int i=0; i<rd->nind_param_cst; i++) {
		printf("%f, ", isl_val_get_d(rd->min[i]));
	}
	printf("]\n\tmax_bound = [");
	for (int i=0; i<rd->nind_param_cst; i++) {
		printf("%f, ", isl_val_get_d(rd->max[i]));
		//PRINT_ISL(p_out, isl_printer_print_val, rd->max[i]);
	}
	fflush(stdout);
	printf("] (dir = ");
	if (rd->direction) {
		printf("increasing)");
	} else {
		printf("decreasing)");
	}
	printf("\n\tinj_edge_red = ");
	edge* e = rd->inj_edge_red;
	if (e->is_reduction) {
		printf("( %zu -> %zu ) [Reduction] ", e->source_idx, e->sink_idx);
	} else {
		printf("( %zu -> %zu ) ", e->source_idx, e->sink_idx);
	}
	isl_map* m = isl_map_from_basic_map(isl_basic_map_copy(e->rel));
	PRINT_ISL(p_out, isl_printer_print_map, m);
	isl_map_free(m);
	printf("]\n");
	return;
}

void print_lreduction_data(list_item* lreduction_data) {
	if (lreduction_data==NULL)
		return;
	print_reduction_data( (reduction_data*) lreduction_data->data );
	print_lreduction_data(lreduction_data->next);
}

// Auxilliary function that checks if an element is inside a list_item of reduction_data
bool is_in_red_data(edge* e, list_item* l) {
	if (l==NULL)
		return false;
	if (e == (edge*) ((reduction_data*) l->data)->inj_edge_red )
		return true;
	return is_in_red_data(e, l->next);
}


/* ============================== */

// Print function for debugging
void print_vectval(isl_val** vec_val, int n_vsize) {
	printf("[ ");
	p_out = isl_printer_start_line(p_out);
	for (int i=0; i<n_vsize; i++) {
		p_out = isl_printer_print_val(p_out, vec_val[i]);
		printf(" , ");
	}
	printf("]");
	p_out = isl_printer_end_line(p_out);
	return;
}

// Auxilliary function for debugging
void print_item_list_constraints(list_item* l_constr, int n_vsize) {
	if (l_constr==NULL)
		return;
	isl_val** vec_val = (isl_val**) l_constr->data;
	print_vectval(vec_val, n_vsize);
	print_item_list_constraints(l_constr->next, n_vsize);
}



/* ============================== */

// Auxilliary function that examine self loops and return the list of self-loop edges
//	that forms the multidimentional reduction.
//		- reduction_start is a list of "reduction_data" (from outer to inner loops)
list_item* get_ordered_self_loop_red_detection(list_item* reduction_start, int n_dims,
	list_item* l_selfloop_remaining, int* l_stm_dim_stride) {

	// Get the list of dimensions already in the reduction
	//	+ their associated min/max bounds
	int* mask = (int*)calloc(n_dims, sizeof(int));		// Note: already init it with 0s (= False)
	isl_val*** max_dim = (isl_val***) malloc(n_dims * sizeof(isl_val**));
	isl_val*** min_dim = (isl_val***) malloc(n_dims * sizeof(isl_val**));
	for (int i=0; i<n_dims; i++) {
		max_dim[i] = NULL;
		min_dim[i] = NULL;
		mask[i] = 0;
	}

	// Gather the reduction informations already found previously to be part of the reduction
	for (list_item* l = reduction_start; l!=NULL; l=l->next) {
		reduction_data* rd = (reduction_data*) l->data;
		assert(mask[rd->dim_num] == 0);		// No repetition of a reduced dimension

		if (rd->direction) {
			mask[rd->dim_num] = 1;
		} else {
			mask[rd->dim_num] = -1;
		}

		// Copy for debugging
		//max_dim[rd->dim_num] = rd->max;
		//min_dim[rd->dim_num] = rd->min;
		max_dim[rd->dim_num] = malloc(rd->nind_param_cst * sizeof(isl_val*));
		min_dim[rd->dim_num] = malloc(rd->nind_param_cst * sizeof(isl_val*));
		for (int i=0; i<rd->nind_param_cst; i++) {
			max_dim[rd->dim_num][i] = rd->max[i];
			min_dim[rd->dim_num][i] = rd->min[i];
		}
	}

	// Iterate over l_selfloop_remaining to find the right edge to continue
	for (list_item* l = l_selfloop_remaining; l!=NULL; l=l->next) {
		edge* e_cand = (edge*) l->data;

		assert(e_cand->source_idx == e_cand->sink_idx);	// Should already be a self-loop
		assert(e_cand->type == INJECTION);				// Should be an injection

		// === Criterions on e_cand->rel:
		// - On dims not in "mask":
		//		* output dims (isl_dim_out) are Id
		//		* constants should be 0, except for a SINGLE ONE dimension (new dim of red)
		//	=> "k' = k"  + (for a single one) "k' = k + 1"
		// - On dims in "mask":
		//		* output dims (isl_dim_out) are 0 (isl_param_dim might not)
		//		 and constant dims should be equal to the "min" of the corresponding dim
		//		* input dims should be constraint to the (param) "max" of the corresponding dim
		//	=> "k = max" / "k' = min"
		bool form_match = true;
		int new_dim_red = -1;			// New dimensions along which the reduction is
		bool direction = true;			// Direction of the reduction (true = increasing / false = decreasing)

		isl_basic_map* rel_simpl = isl_basic_map_remove_redundancies(
			isl_basic_map_copy(e_cand->rel));

		isl_mat* mat_eq = isl_basic_map_equalities_matrix(rel_simpl,
			isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_div, isl_dim_cst);
		int n_eq = isl_mat_rows(mat_eq);

		isl_mat* mat_ineq = isl_basic_map_inequalities_matrix(rel_simpl,
			isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_div, isl_dim_cst);
		int n_ineq = isl_mat_rows(mat_ineq);

		int n_dim_in = isl_basic_map_dim(e_cand->rel, isl_dim_in);
		int n_dim_out = isl_basic_map_dim(e_cand->rel, isl_dim_out);
		assert(n_dim_in == n_dim_out);
		int n_dim_param = isl_basic_map_dim(e_cand->rel, isl_dim_param);
		int n_dim_all = isl_basic_map_total_dim(e_cand->rel);		// Note: does not count constant

		/* DEBUG
		printf("\nrel_simpl = ");
		PRINT_ISL(p_out, isl_printer_print_basic_map, rel_simpl);
		printf("n_dim_in = %i\n", n_dim_in);
		printf("n_dim_out = %i\n", n_dim_out);
		printf("n_dim_param = %i\n", n_dim_param);
		printf("n_dim_div = %i\n", isl_basic_map_dim(rel_simpl, isl_dim_div));
		printf("n_dim_all = %i\n", n_dim_all);
		
		printf("mat_eq =\n");
		isl_mat_print_internal(mat_eq, stdout, 0);
		printf("mat_ineq =\n");
		isl_mat_print_internal(mat_ineq, stdout, 0);
		printf("mask = [");
		for (int i=0; i<n_dims; i++) {
			printf("%i ", mask[i]);
		}
		printf("]\n");
		fflush(stdout);
		//*/


		assert(n_dim_all == n_dim_in + n_dim_out + n_dim_param); // No existential params (div)

		// Checks on the equality matrix
		// Due to order of the loop, new reduction dimension should be
		//		the last dimension which was not already matched previously
		//	=> When checking chains of dependence, priority to the last dimension
		for (int d=n_dims-1; d>=0; d--) {
			/* DEBUG
			printf("d = %i", d);
			if (form_match)
				printf(" | form_match (at start) = true\n");
			else
				printf(" | form_match (at start) = false\n");
			//*/

			if (mask[d]==0) {
				int stride_d = l_stm_dim_stride[d];

				// Not a "for" loop but a "if" statement in that dim => skip
				if (stride_d == 0) {
					continue;
				}
				assert( (stride_d == 1) || (stride_d == (-1)) );

				// Dimension not recognized by a reduction (yet?)
				for (int ieq=0; ieq<n_eq; ieq++) {
					// Check if this is the equality about the dth out dimension
					isl_val* val_out_d = isl_mat_get_element_val(mat_eq, ieq, d+n_dim_in);
					assert(val_out_d!=NULL);

					// Coeff is 0 => We can skip this constraint
					if (isl_val_is_zero(val_out_d)) {
						isl_val_free(val_out_d);
						continue;
					}

					// Coeff is 1 => Ok
					if (isl_val_is_one(val_out_d)) {
						// DEBUG
						//printf("PING (mask_d==0) + (val_out_d==1)\n");

						// Linear part of the function is Id (ie, equality between dth in dim and dth out dim)
						for (int j=0; j<n_dim_in; j++) {
							isl_val* val_in_j = isl_mat_get_element_val(mat_eq, ieq, j);
							assert(val_in_j!=NULL);
							if (j==d) {
								if (!isl_val_is_negone(val_in_j)) {
									form_match = false;
								}
							} else {
								if (!isl_val_is_zero(val_in_j)) {
									form_match = false;
								}
							}
							isl_val_free(val_in_j);
						}

						// Check that none of the other output dim has a non-0 coeff
						for (int j=0; j<n_dim_out; j++) {
							if (j!=d) {
								isl_val* val_out_j = isl_mat_get_element_val(mat_eq, ieq, j+n_dim_in);
								assert(val_out_j!=NULL);
								if (!isl_val_is_zero(val_out_j)) {
									form_match = false;
								}
								isl_val_free(val_out_j);
							}
						}

						// Constant value: exactly one at "1", rest at "0"
						isl_val* val_const_d = isl_mat_get_element_val(mat_eq, ieq, n_dim_all);

						// DEBUG
						//printf("** stride_d = %d\n", stride_d);
						//printf("** val_const_d = %f\n", isl_val_get_d(val_const_d));

						bool cond_cst = ( (stride_d == 1) && (isl_val_is_negone(val_const_d)) )
								|| ( (stride_d == (-1)) && (isl_val_is_one(val_const_d)) );
						if (cond_cst) {
							if (new_dim_red!=-1) {
								// Warning: dim_red was already found:
								// 2 rows (of unmasked output dim) have their constant at "1"
								//		 => wrong  (reduction dependence not along canonical dim)
								form_match = false;
							}
							new_dim_red = d;
						} else {
							if (!isl_val_is_zero(val_const_d)) // Value neither 0 or 1
								form_match = false;
						}
						isl_val_free(val_const_d);

						/* DEBUG
						printf("d = %i", d);
						if (form_match)
							printf(" | form_match (at end mask_0) = true\n");
						else
							printf(" | form_match (at end mask_0) = false\n");
						printf("END constat checking - dim %d ", d);
						//*/
					} else {
						// Coeff is non-0 different than 1: FAIL
						form_match = false;

						// Note: might still match if "-1" (need to add it that happens)
					}
					isl_val_free(val_out_d);
				}

				// If the first dimension (starting from the end) with a !mask[d]
				//	did not match, this is a fail.
				//		===> Arrange that in order to recognize chain of dependences
				//if (new_dim_red==-1) {
				//	form_match = false;
				//}

			} else {
				// Dimensions that were recognized by a reduction
				// => We need to ensure that the start/end of the reduction do match
				assert( (mask[d]==1) || (mask[d] == (-1)) );

				// a) Check the constraints on the dth output dim
				for (int ieq=0; ieq<n_eq; ieq++) {
					// Is this constraint pertinent for the d-th output dim?
					isl_val* val_out_d = isl_mat_get_element_val(mat_eq, ieq, d+n_dim_in);
					assert(val_out_d!=NULL);

					// Coeff is 0 => We can skip this constraint
					if (isl_val_is_zero(val_out_d)) {
						isl_val_free(val_out_d);
						continue;
					}

					// On the constraint where the output dim is 1
					//	=> if mask[d]==1 : "k' = min"
					//  => if mask[d]==-1 : "k' = max"
					if (isl_val_is_one(val_out_d)) {
						// Check the rest of the output dims (must be 0s)
						for (int j=0; j<n_dim_out; j++) {
							if (j!=d) {
								isl_val* val_out_j = isl_mat_get_element_val(mat_eq, ieq, j+n_dim_in);
								assert(val_out_j!=NULL);
								if (!isl_val_is_zero(val_out_j)) {
									form_match = false;
								}
								isl_val_free(val_out_j);
							}
						}


						// "Input Constant dimension + param dim" = min of corresponding dim
						isl_val** val_constr_dim;
						if (mask[d]==1) {
							val_constr_dim = min_dim[d];
						} else {
							val_constr_dim = max_dim[d];
						}
						// [Old, for mask[d]==1] : isl_val** val_min_dim = min_dim[d];

						// We need to check that the values of "- min_dim[d]" corresponds
						// 	to the coefficients of the input dims + param dims + cst coeff
						for (int j=0; j<n_dim_in; j++) {
							isl_val* val_in_j = isl_mat_get_element_val(mat_eq, ieq, j);
							isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[j]));
							assert(val_in_j!=NULL);
							assert(minus_val_constr_dim!=NULL);
							if (!isl_val_eq(val_in_j, minus_val_constr_dim)) {
								form_match = false;
							}
							isl_val_free(val_in_j);
							isl_val_free(minus_val_constr_dim);
						}
						for (int j=0; j<n_dim_param; j++) {
							isl_val* val_param_j = isl_mat_get_element_val(mat_eq, ieq, n_dim_in+n_dim_out+j);
							isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[n_dim_in+j]));
							assert(val_param_j!=NULL);
							assert(minus_val_constr_dim!=NULL);
							if (!isl_val_eq(val_param_j, minus_val_constr_dim)) {
								form_match = false;
							}
							isl_val_free(val_param_j);
							isl_val_free(minus_val_constr_dim);
						}
						isl_val* val_const = isl_mat_get_element_val(mat_eq, ieq, n_dim_all);
						isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[n_dim_in+n_dim_param]));
						assert(val_const!=NULL);
						assert(minus_val_constr_dim!=NULL);
						if (!isl_val_eq(val_const, minus_val_constr_dim)) {
							form_match = false;
						}
						isl_val_free(val_const);
						isl_val_free(minus_val_constr_dim);
						// Check done
					} else {
						// Coeff is non-0 different than 1: FAIL
						form_match = false;

						// Note: might still match if "-1" (need to add it that happens)
					}
					isl_val_free(val_out_d);
				}

				/* DEBUG
				printf("d = %i", d);
				if (form_match)
					printf(" | form_match (mid - mask!=0) = true\n");
				else
					printf(" | form_match (mid - mask!=0) = false\n");
				//*/

				// b) Check the constraints on the dth input dim
				for (int ieq=0; ieq<n_eq; ieq++) {
					// Is this constraint pertinent for the d-th input dim?
					isl_val* val_in_d = isl_mat_get_element_val(mat_eq, ieq, d);
					assert(val_in_d!=NULL);

					// Coeff is 0 => We can skip this constraint
					if (isl_val_is_zero(val_in_d)) {
						isl_val_free(val_in_d);
						continue;
					}

					// On the constraint where the input dim is 1
					//	=> if mask[d]==1 : "k = max"
					//  => if mask[d]==-1 : "k = min"
					if (isl_val_is_one(val_in_d)) {
						// Other output dims empty
						for (int j=0; j<n_dim_out; j++) {
							isl_val* val_out_j = isl_mat_get_element_val(mat_eq, ieq, j+n_dim_in);
							assert(val_out_j!=NULL);
							if (!isl_val_is_zero(val_out_j)) {
								form_match = false;
							}
							isl_val_free(val_out_j);
						}

						// If we look at the coefficients of "-max_dim[d]",
						//	they should be on the other dims of input/param/cst
						isl_val** val_constr_dim;
						if (mask[d]==1) {
							val_constr_dim = max_dim[d];
						} else {
							val_constr_dim = min_dim[d];
						}
						// [Old, for mask[d]==1] : isl_val** val_max_dim = max_dim[d];
						
						for (int j=0; j<n_dim_in; j++) {
							if (j!=d) {
								isl_val* val_in_j = isl_mat_get_element_val(mat_eq, ieq, j);
								isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[j]));

								assert(val_in_j!=NULL);
								assert(minus_val_constr_dim!=NULL);
								if (!isl_val_eq(val_in_j, minus_val_constr_dim)) {
									form_match = false;
								}
								isl_val_free(val_in_j);
								isl_val_free(minus_val_constr_dim);
							}
						}
						for (int j=0; j<n_dim_param; j++) {
							isl_val* val_param_j = isl_mat_get_element_val(mat_eq, ieq, j+n_dim_in+n_dim_out);
							isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[n_dim_in+j]));
							assert(val_param_j!=NULL);
							assert(minus_val_constr_dim!=NULL);
							if (!isl_val_eq(val_param_j, minus_val_constr_dim)) {
								form_match = false;
							}
							isl_val_free(val_param_j);
							isl_val_free(minus_val_constr_dim);
						}

						isl_val* val_const = isl_mat_get_element_val(mat_eq, ieq, n_dim_all);
						isl_val* minus_val_constr_dim = isl_val_neg(isl_val_copy(val_constr_dim[n_dim_in+n_dim_param]));
						assert(val_const!=NULL);
						assert(minus_val_constr_dim!=NULL);
						if (!isl_val_eq(val_const, minus_val_constr_dim)) {
							form_match = false;
						}
						isl_val_free(val_const);
						isl_val_free(minus_val_constr_dim);
					} else {
						// Coeff is non-0 different than 1: FAIL
						form_match = false;
						// Note: might still match if "-1" (need to add it that happens)
					}
					isl_val_free(val_in_d);
				}
			}

			/* DEBUG
			printf("d = %i", d);
			if (form_match)
				printf(" | form_match (at end) = true\n");
			else
				printf(" | form_match (at end) = false\n");
			//*/
			
			if (!form_match)
				break;
		} // End checks on the equality matrix

		if (new_dim_red==(-1)) {
			// DEBUG
			//printf("\tNo new dimension reduction found (in equality matrix) => form_match = false\n");

			form_match = false;
		}

		/* DEBUG
		printf("END OF THE EQUALITY MATRIX CHECK ");
		if (form_match)
			printf("form_match = true (new_dim_red = %d)\n", new_dim_red);
		else
			printf("form_match = false\n");
		fflush(stdout);
		//*/


		// Match found! (modulo the check that some values matches)
		if (form_match) {
			assert(new_dim_red!=(-1));  // At least a dimension was found

			// DEBUG
			printf("Form matched for reduction dimension %i\n", new_dim_red);
			fflush(stdout);

			// We enrich the reduction_start and perform a recursive call
			reduction_data* rd = (reduction_data*) malloc(sizeof(reduction_data));
			rd->dim_num = new_dim_red;
			rd->nind = n_dim_in;
			rd->nparam = n_dim_param;
			rd->nind_param_cst = n_dim_in + n_dim_param + 1;
			rd->inj_edge_red = e_cand;
			if (l_stm_dim_stride[new_dim_red]==(-1)) {
				direction = false;
			}
			rd->direction = direction;

			bool min_constr_found = false;
			bool max_constr_found = false;

			// rd->min and rd->max are still missing: we extract them from the inequality matrix
			for (int iineq=0; iineq<n_ineq; iineq++) {
				// DEBUG
				//printf("[Building min/max] Checking constraint %d\n", iineq);
				//fflush(stdout);
				
				isl_val* val_in_d = isl_mat_get_element_val(mat_ineq, iineq, new_dim_red);

				// Constraint not useful for the new reduction dimension
				if (isl_val_is_zero(val_in_d)) {
					isl_val_free(val_in_d);
					continue;
				}

				// Value is neither 0, 1 and -1 => FAIL
				if (!isl_val_is_one(val_in_d) && !isl_val_is_negone(val_in_d)) {
					isl_val_free(val_in_d);
					form_match = false;
					break;
				}

				// a) Min constraint detected?
				if (isl_val_is_one(val_in_d)) {
					if (min_constr_found) {
						// We have 2 minimum constraint => abandon ship!
						isl_val_free(val_in_d);
						form_match = false;
						break;
					}
					min_constr_found = true;

					// Check no other values in the out dims
					for (int j=0; j<n_dim_out; j++) {
						isl_val* val_out_j = isl_mat_get_element_val(mat_ineq, iineq, j+n_dim_in);
						assert(val_out_j!=NULL);
						if (!isl_val_is_zero(val_out_j)) {
							form_match = false;
						}
						isl_val_free(val_out_j);
					}

					if (!form_match) {
						isl_val_free(val_in_d);
						break;
					}

					// min_constraint is negation of rest of input + param + constants vals here
					isl_val** min_constr = (isl_val**) malloc( (n_dim_in+n_dim_param+1) * sizeof(isl_val*));
					for (int j=0; j<n_dim_in; j++) {
						if (j!=new_dim_red) {
							isl_val* val_in_j = isl_mat_get_element_val(mat_ineq, iineq, j);
							min_constr[j] = isl_val_neg(val_in_j);
							// val_in_j taken: no need to free
						} else {
							// On the dimension of the reduction, just put a 0 here
							// (should not be used)
							min_constr[j] = isl_val_zero(isl_val_get_ctx(val_in_d));
						}
					}

					for (int j=0; j<n_dim_param; j++) {
						isl_val* val_param_j = isl_mat_get_element_val(mat_ineq, iineq, n_dim_in+n_dim_out+j);
						min_constr[n_dim_in+j] = isl_val_neg(val_param_j);
						// val_param_j taken: no need to free
					}
					// WARNING: Constant is substracted by 1 if decreasing
					isl_val* val_const = isl_mat_get_element_val(mat_ineq, iineq, n_dim_all);
					if (direction) {
						min_constr[n_dim_in+n_dim_param] = isl_val_neg(val_const);
					} else {
						min_constr[n_dim_in+n_dim_param] = isl_val_sub(isl_val_neg(val_const),
								isl_val_one(isl_val_get_ctx(val_in_d))
							);
					}
					
					// val_const taken: no need to free

					rd->min = min_constr;

					/* DEBUG
					printf("\t [Building min/max] min_constr = [");
					fflush(stdout);
					for (int j=0; j<n_dim_in+n_dim_param+1; j++)
						printf("%f, ", isl_val_get_d(min_constr[j]));
					printf("]\n");
					fflush(stdout);
					//*/

				}

				// b) Max constraint detected?
				if (isl_val_is_negone(val_in_d)) {
					if (max_constr_found) {
						// We have 2 minimum constraint => abandon ship!
						isl_val_free(val_in_d);
						form_match = false;
						break;
					}
					max_constr_found = true;

					// Check no other values in the out dims
					for (int j=0; j<n_dim_out; j++) {
						isl_val* val_out_j = isl_mat_get_element_val(mat_ineq, iineq, j+n_dim_in);
						assert(val_out_j!=NULL);
						if (!isl_val_is_zero(val_out_j)) {
							form_match = false;
						}
						isl_val_free(val_out_j);
					}

					if (!form_match) {
						isl_val_free(val_in_d);
						break;
					}

					// max_constraint is rest of input + param + constants vals here
					// WARNING: Constant is added by 1 (only if increasing)
					isl_val** max_constr = (isl_val**) malloc( (n_dim_in+n_dim_param+1) * sizeof(isl_val*));
					for (int j=0; j<n_dim_in; j++) {
						if (j!=new_dim_red) {
							max_constr[j] = isl_mat_get_element_val(mat_ineq, iineq, j);
						} else {
							// On the dimension of the reduction, just put a 0 here
							// (should not be used)
							max_constr[j] = isl_val_zero(isl_val_get_ctx(val_in_d));
						}
					}
					for (int j=0; j<n_dim_param; j++) {
						max_constr[n_dim_in+j] = isl_mat_get_element_val(mat_ineq, iineq, n_dim_in+n_dim_out+j);
					}
					isl_val* val_const = isl_mat_get_element_val(mat_ineq, iineq, n_dim_all);
					if (direction) {
						max_constr[n_dim_in+n_dim_param] = isl_val_add(val_const,
								isl_val_one(isl_val_get_ctx(val_in_d))
							);
					} else {
						max_constr[n_dim_in+n_dim_param] = val_const;
					}

					/* DEBUG
					printf("\t [Building min/max] max_constr = [");
					fflush(stdout);
					for (int j=0; j<n_dim_in+n_dim_param+1; j++)
						printf("%f, ", isl_val_get_d(max_constr[j]));
					printf("]\n");
					fflush(stdout);
					//*/

					rd->max = max_constr;
				}

				isl_val_free(val_in_d);
			}

			// If the domain was not rectangular along reduction dims
			// 		free stuffs and continue on the next edge candidate
			if (!form_match) {
				// DEBUG
				printf("\tNon rectangular domain of reduction (ineq mat check) => Cancel last detection!\n");

				isl_mat_free(mat_eq);
				isl_mat_free(mat_ineq);
				free(rd);
				continue;
			}

			// Recursive call to continue discover new dimensions
			list_item* n_reduction_start = calloc(1, sizeof(list_item*));
			n_reduction_start->data = rd;
			n_reduction_start->next = reduction_start;


			// Free stuffs
			isl_mat_free(mat_eq);
			isl_mat_free(mat_ineq);
			free(mask);
			free(max_dim);		// Note: inside of max_dim and min_dim are elements of reduction_start
			free(min_dim);		//			=> Should not be freed here

			// DEBUG
			//printf("[get_ordered_self_loop_red_detection] BEFORE RECURSION:\n");
			//print_lreduction_data(reduction_start);
			//printf("Done.\n");
			//fflush(stdout);


			// The edge was found => no other edge can be found here
			return get_ordered_self_loop_red_detection(n_reduction_start, n_dims,
					l_selfloop_remaining, l_stm_dim_stride);
		} // END match found


		// Free the constraint matrices
		isl_mat_free(mat_eq);
		isl_mat_free(mat_ineq);
	}


	// Free temp data structure
	free(mask);
	free(max_dim);		// Note: inside of max_dim and min_dim are elements of reduction_start
	free(min_dim);		//			=> Should not be freed here

	return reduction_start;
}

// Auxilliary function that, given the reduction_data that forms a multidimentional
//	reduction, build the corresponding broadcast edge.
// First edge is coming from the first element of the reduction, to any element of the reduction.
//		Example, for gemm: { S[i,j,0] -> S[i,j,k'] : 0 <= k'< K }
edge* build_broadcast_reduction_edge(list_item* lreduction_data, int n_dims, int node_idx) {
	assert(lreduction_data!=NULL);
	// Note: lreduction_data is outer to inner
	
	// Alloc
	edge* brdc_edge = (edge*) malloc(sizeof(edge));

	// Filling infos
	brdc_edge->source_idx = node_idx;
	brdc_edge->sink_idx = node_idx;		// Must be a self-loop
	brdc_edge->type = BCAST;
	brdc_edge->visited = false;
	brdc_edge->is_reduction = true;

	// Relation: build self loop "[prog_param] -> { S[ ... ind_in ... ] -> S[ ... ind_out ... ] : ... }"
	//	with equality constraints:
	//		- ind_in = min_dim for the reduced dims
	//		- ind_out = fresh vars for the reduced dims
	//		- these fresh vars (of the reduced dims) are between min_dim and max_dim
	
	// ctx/dim_space => Just take the first space available from the injective edges
	isl_basic_map *arbitrary_rel = ((reduction_data*) lreduction_data->data)->inj_edge_red->rel;
	isl_ctx *ctx = isl_basic_map_get_ctx(arbitrary_rel);
	isl_space* dim_space = isl_basic_map_get_space(arbitrary_rel);

	// Get info about reduction from l_reduction_data
	unsigned n_dim_red = 0;
	bool* bmask_red = (bool*) malloc(n_dims * sizeof(bool));
	isl_val*** min_dim_red = (isl_val***) malloc(n_dims * sizeof(isl_val**));
	isl_val*** max_dim_red = (isl_val***) malloc(n_dims * sizeof(isl_val**));

	for (int i=0; i<n_dims; i++) {
		bmask_red[i] = false;
		min_dim_red[i] = NULL;
		max_dim_red[i] = NULL;
	}

	for (list_item* l = lreduction_data; l!=NULL; l=l->next) {
		n_dim_red++;

		reduction_data* rd = (reduction_data*) l->data;
		bmask_red[rd->dim_num] = true;
		min_dim_red[rd->dim_num] = rd->min;
		max_dim_red[rd->dim_num] = rd->max;
	}

	unsigned nparam = isl_basic_map_dim(arbitrary_rel, isl_dim_param);
	unsigned ndimIn = isl_basic_map_dim(arbitrary_rel, isl_dim_in);
	unsigned ndimOut = isl_basic_map_dim(arbitrary_rel, isl_dim_out);
	assert(ndimIn == ndimOut);
	unsigned n_col_mat = ndimIn + ndimOut + nparam + 1;

	/* DEBUG
	printf("nparam = %i\n", nparam);
	printf("ndimIn = %i\n", ndimIn);
	printf("ndimOut = %i\n", ndimOut);
	printf("n_dim_red = %i\n", n_dim_red);
	printf("bmask_red = [");
	for (int i=0; i<n_dims; i++) {
		printf("%i ", bmask_red[i]);
	}
	printf("]\n");
	//*/


	// 1) Equality matrix should contain:
	//	- equality for the input dimensions for the reduced dims
	//	- Id equality linking in&outputs dimensions for the non-reduced dims
	isl_mat* mat_eq = isl_mat_alloc(ctx, n_dims, n_col_mat);
	for (int i=0; i<n_dims; i++)		// Else there are stuffs in the isl mat_eq
		for (int j=0; j<n_col_mat; j++)
			mat_eq = isl_mat_set_element_si(mat_eq, i, j, 0);

	// mat_eq:
	// [  In  |  Out  |  Param  | Cst ]
	// [  -Id |  Id   |    0    |   0 ] for dimensions which are not reduction dim
	// OR
	// [ Id/-min_dim_red |   0   |  -min_dim_red ] for dimensions which are reduction dim
	for (int i=0; i<n_dims; i++) {
		if (bmask_red[i]) {
			// ith input = -min[i] (on param/const dims)
			mat_eq = isl_mat_set_element_si(mat_eq, i, i, 1);

			// Other Input & Param & constant dimensions
			for (int j=0; j<ndimIn; j++) {
				if (j!=i) {
					isl_val* valneg = isl_val_neg(isl_val_copy(min_dim_red[i][j]));
					mat_eq = isl_mat_set_element_val(mat_eq, i, j, valneg);
				}
			}
			for (int j=0; j<nparam; j++) {
				isl_val* valneg = isl_val_neg(isl_val_copy(min_dim_red[i][ndimIn+j]));
				mat_eq = isl_mat_set_element_val(mat_eq, i, ndimIn + ndimOut + j, valneg);
				// valneg is taken by isl_mat_set_element_val => no need to free it
			}
			isl_val* valneg_cst = isl_val_neg(isl_val_copy(min_dim_red[i][ndimIn+nparam]));
			mat_eq = isl_mat_set_element_val(mat_eq, i, ndimIn + ndimOut + nparam, valneg_cst);

		} else {
			// dim_in_i = dim_out_i
			// Putting a -1 coeff at ith input / 1 coeff at ith output
			mat_eq = isl_mat_set_element_si(mat_eq, i, i, (-1));
			mat_eq = isl_mat_set_element_si(mat_eq, i, ndimIn+i, 1);
		}
	}

	/* DEBUG
	printf("mat_eq =\n");
	isl_mat_print_internal(mat_eq, stdout, 0);
	//*/


	// Gather the constraints which are not involving reduction dimensions
	//		=> Stored in the chained list l_constr_no_red (of size n_constr_nored)
	int n_constr_nored = 0;		// Count of the constraints of the domain matrix (from arbitrary_rel)
	list_item* l_constr_no_red = NULL;
	isl_mat* mat_ineq_arb_rel = isl_basic_map_inequalities_matrix(arbitrary_rel,
			isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_div, isl_dim_cst);
	for (int i=0; i<isl_mat_rows(mat_ineq_arb_rel); i++) {
		// Does this row involves red_dim constraints ? (check dim in/out for )
		bool is_not_red_constr = true;

		for (int j=0; j<ndimIn; j++) {
			if (bmask_red[j]) {
				isl_val* valIn = isl_mat_get_element_val(mat_ineq_arb_rel, i, j);
				if (!isl_val_is_zero(valIn)) {
					is_not_red_constr = false;
				}
				free(valIn);
			}
		}
		for (int j=0; j<ndimOut; j++) {
			if (bmask_red[j]) {
				isl_val* valOut = isl_mat_get_element_val(mat_ineq_arb_rel, i, ndimIn + j);
				if (!isl_val_is_zero(valOut)) {
					is_not_red_constr = false;
				}
				free(valOut);
			}
		}

		if (is_not_red_constr) {
			// We add the constraint to l_constr_no_red
			isl_val** constr_no_red = malloc(n_col_mat * sizeof(isl_val*));
			for (int j=0; j<n_col_mat; j++)
				constr_no_red[j] = isl_mat_get_element_val(mat_ineq_arb_rel, i, j);

			list_item* nl_constr_no_red = malloc(sizeof(list_item));
			nl_constr_no_red->data = constr_no_red;
			nl_constr_no_red->next = l_constr_no_red;
			l_constr_no_red = nl_constr_no_red;

			n_constr_nored++;
		}
	}

	/* DEBUG
	printf("n_constr_nored = %i\n", n_constr_nored);
	print_item_list_constraints(l_constr_no_red, n_col_mat);
	//*/


	// 2) Inequality matrix should containt:
	//	- the constraints which does not involve reduction dimensions (use l_constr_no_red)
	//	- the constraints "min <= dim_red <= max" for every reduction dimension
	int n_row_ineq = n_constr_nored + 2*n_dim_red;
	isl_mat* mat_ineq = isl_mat_alloc(ctx, n_row_ineq, n_col_mat);
	for (int i=0; i<n_row_ineq; i++)		// Else there are stuffs in the isl mat_eq
		for (int j=0; j<n_col_mat; j++)
			mat_ineq = isl_mat_set_element_si(mat_ineq, i, j, 0);

	list_item* ptr_l_constr_no_red = l_constr_no_red;
	for (int i=0; i<n_constr_nored; i++) {	// First dimensions are just a copy of l_constr_no_red
		isl_val** constr_no_red = (isl_val**) ptr_l_constr_no_red->data;

		for (int j=0; j<n_col_mat; j++)
			mat_ineq = isl_mat_set_element_val(mat_ineq, i, j, isl_val_copy(constr_no_red[j]));

		ptr_l_constr_no_red = ptr_l_constr_no_red->next;
	}
	assert(ptr_l_constr_no_red==NULL);

	/* DEBUG
	printf("mat_ineq =\n");			// Top half filled
	isl_mat_print_internal(mat_ineq, stdout, 0);
	//*/

	// Bottom half of the mat_ineq matrix: "min <= dim_red <= max"
	int k_mat_ineq = 0;		// Reduced dimension index
	for (int i=0; i<n_dims; i++) {
		if (bmask_red[i]) {

			// Min constraint on the dimension  ( out_dim_red - min_dim_red => 0)
			mat_ineq = isl_mat_set_element_si(mat_ineq, 2*k_mat_ineq+n_constr_nored, ndimIn+i, 1);

			// Input/Param/const dimensions
			for (int j=0; j<ndimIn; j++) {
				if (j!=i) {
					isl_val* isl_val_neg_input = isl_val_neg(isl_val_copy(min_dim_red[i][j]));
					mat_ineq = isl_mat_set_element_val(mat_ineq,
							n_constr_nored + 2*k_mat_ineq, j,
							isl_val_neg_input);
				}
			}

			for (int j=0; j<nparam; j++) {
				isl_val* isl_val_neg_param = isl_val_neg(isl_val_copy(min_dim_red[i][ndimIn+j]));
				mat_ineq = isl_mat_set_element_val(mat_ineq,
						n_constr_nored + 2*k_mat_ineq, ndimIn + ndimOut + j,
						isl_val_neg_param);
			}
			isl_val* isl_val_neg_cst = isl_val_neg(isl_val_copy(min_dim_red[i][ndimIn+nparam]));
			mat_ineq = isl_mat_set_element_val(mat_ineq,
				n_constr_nored + 2*k_mat_ineq, n_col_mat-1, isl_val_neg_cst);


			// Max constraint on the dimension ( - out_dim_red + max_dim_red => 0)
			mat_ineq = isl_mat_set_element_si(mat_ineq, 2*k_mat_ineq+1+n_constr_nored, ndimIn+i, -1);

			// Input/Param/const dims
			for (int j=0; j<ndimIn; j++) {
				if (j!=i) {
					mat_ineq = isl_mat_set_element_val(mat_ineq, n_constr_nored + 2*k_mat_ineq + 1, j,
							isl_val_copy(max_dim_red[i][j]) );
				}
			}
			for (int j=0; j<nparam; j++) {
				mat_ineq = isl_mat_set_element_val(mat_ineq, 2*k_mat_ineq+1+n_constr_nored, ndimIn+ndimOut+j,
					isl_val_copy(max_dim_red[i][ndimIn+j]) );
			}
			mat_ineq = isl_mat_set_element_val(mat_ineq, 2*k_mat_ineq+1+n_constr_nored, n_col_mat-1,
				isl_val_copy(max_dim_red[i][ndimIn+nparam]) );

			k_mat_ineq++;
		}
	}
	assert(k_mat_ineq==n_dim_red);


	/* DEBUG
	//printf("mat_ineq =\n");	// Matrix filled!
	//isl_mat_print_internal(mat_ineq, stdout, 0);
	//*/


	// Note: no need to free dim_space / mat_eq and mat_ineq (taken by following function)
	isl_basic_map* bmap_brdc_edge = isl_basic_map_from_constraint_matrices(
		dim_space, mat_eq, isl_mat_copy(mat_ineq),
		isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_div, isl_dim_cst);

	brdc_edge->rel = bmap_brdc_edge;



	// 3) Other relation (specific to reduction)
	// When broadcast relation is (on reduction dimensions): (0 -> i')
	//		this relation is (i -> N-1)

	// mat_eq_red_rel:
	// [  In  |  Out  |  Param  | Cst ]
	// [  -Id |  Id   |    0    |   0 ] for dimensions which are not reduction dim
	// OR
	// [  0  |   Id   |  -max_dim_red ] for dimensions which are reduction dim

	isl_mat* mat_eq_red_rel = isl_mat_alloc(ctx, n_dims, n_col_mat);
	for (int i=0; i<n_dims; i++)		// Else there are stuffs in the isl mat_eq
		for (int j=0; j<n_col_mat; j++)
			mat_eq_red_rel = isl_mat_set_element_si(mat_eq_red_rel, i, j, 0);

	for (int i=0; i<n_dims; i++) {
		if (bmask_red[i]) {
			// ith input = -min[i] (on param/const dims)
			mat_eq_red_rel = isl_mat_set_element_si(mat_eq_red_rel, i, ndimIn + i, 1);

			// Input & Param & constant dimensions
			for (int j=0; j<ndimIn; j++) {
				if (j!=i) {
					isl_val* valneg = isl_val_neg(isl_val_copy(max_dim_red[i][j]));
					mat_eq_red_rel = isl_mat_set_element_val(mat_eq_red_rel, i, j, valneg);
					// valneg is taken by isl_mat_set_element_val => no need to free it
				}
			}
			for (int j=0; j<nparam; j++) {
				isl_val* valneg = isl_val_neg(isl_val_copy(max_dim_red[i][ndimIn+j]));
				mat_eq_red_rel = isl_mat_set_element_val(mat_eq_red_rel, i, ndimIn + ndimOut + j, valneg);
				// valneg is taken by isl_mat_set_element_val => no need to free it
			}
			isl_val* valneg_cst = isl_val_neg(isl_val_copy(max_dim_red[i][ndimIn+nparam]));
			mat_eq_red_rel = isl_mat_set_element_val(mat_eq_red_rel, i, ndimIn + ndimOut + nparam, valneg_cst);

		} else {
			// dim_in_i = dim_out_i
			// Putting a -1 coeff at ith input / 1 coeff at ith output
			mat_eq_red_rel = isl_mat_set_element_si(mat_eq_red_rel, i, i, (-1));
			mat_eq_red_rel = isl_mat_set_element_si(mat_eq_red_rel, i, ndimIn+i, 1);
		}
	}



	/* DEBUG
	printf("mat_eq_red_rel =\n");			// Top half filled
	isl_mat_print_internal(mat_eq_red_rel, stdout, 0);
	//*/

	isl_basic_map* bmap_red_rel_edge = isl_basic_map_from_constraint_matrices(
		dim_space, mat_eq_red_rel, mat_ineq,
		isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_div, isl_dim_cst);
	brdc_edge->rel_red = bmap_red_rel_edge;




	// Free all temporary datastructures
	free(bmask_red);
	free(min_dim_red);		// Inside elements are still in reduction_data => should not be freed here
	free(max_dim_red);
	free(mat_ineq_arb_rel);

	while(l_constr_no_red!=NULL) {
		isl_val** constr = l_constr_no_red->data;
		for (int i=0; i<n_col_mat; i++)
			isl_val_free(constr[i]);
		free(constr);

		list_item* nl_ptr = l_constr_no_red->next;
		free(l_constr_no_red);
		l_constr_no_red = nl_ptr;
	}

	return brdc_edge;
}


/* ============================== */

// OLD VERSION (DEPRECATED)! -  Check if the statement stm_num in the scop is a "+=" or a "*="
bool has_reduction_operator(struct pet_scop * const scop, int stm_num) {
	// By construction of the dfg compared to the scop,
	//		the order of the statements are the same, but we have the arrays before
	int ind_scop = stm_num - scop->n_array;

	// Using pet's getters to get the info we need...
	pet_tree* stm_tree = scop->stmts[ind_scop]->body;
	enum pet_tree_type ty = pet_tree_get_type(stm_tree);

	//printf("ping - has_reduction_operator 1\n");

	// - PET < 0.5
	/*if ( (ty==pet_tree_error)			// All variations
		|| (ty==pet_tree_expr)
		|| (ty==pet_tree_block)
		|| (ty==pet_tree_break)
		|| (ty==pet_tree_continue)
		|| (ty==pet_tree_decl)
		|| (ty==pet_tree_decl_init)
		|| (ty==pet_tree_if)
		|| (ty==pet_tree_if_else)
		|| (ty==pet_tree_for)
		|| (ty==pet_tree_infinite_loop)
		|| (ty==pet_tree_while) ) {
			printf("ECHO !!! \n");
	}

	printf("ty = %i\n", (int) ty);		// => 32 ? oO
	printf("pet_tree_expr = %i\n", (int) pet_tree_expr); // 0
	printf("pet_tree_block = %i\n", (int) pet_tree_block); // 1
	printf("pet_tree_break = %i\n", (int) pet_tree_break); // 2
	printf("pet_tree_error = %i\n", (int) pet_tree_error); // -1
	printf("pet_tree_while = %i\n", (int) pet_tree_while); // 10

	printf("PLUS D'ECHO !!!\n"); */


	if ((ty==pet_tree_expr)) {
		pet_expr* stm_expr = pet_tree_expr_get_expr(stm_tree);
		enum pet_op_type optype = pet_expr_op_get_type(stm_expr);

		if ( (optype == pet_op_add_assign) || (optype == pet_op_mul_assign) )
		/*	|| (optype == pet_op_and_assign) || (optype == pet_op_or_assign)
			|| (optype == pet_op_xor_assign) ) */  // Note: these 3 optype not in Pet 0.5 (version used by compiler)
			return true;
		else
			return false;
	}

	return false;
}


// Check if all statement stm_num in the scop is a "+=" or a "*="
bool* get_info_reduction_op(struct pet_scop * const scop) {
	bool* stm_has_red_op = (bool*) calloc(scop->n_stmt, sizeof(bool));

	for (int i=0; i<scop->n_stmt; i++) {
		pet_tree* stm_tree = scop->stmts[i]->body;
		enum pet_tree_type ty = pet_tree_get_type(stm_tree);

		if ((ty==pet_tree_expr)) {
			pet_expr* stm_expr = pet_tree_expr_get_expr(stm_tree);
			enum pet_op_type optype = pet_expr_op_get_type(stm_expr);
			if ( (optype == pet_op_add_assign) || (optype == pet_op_mul_assign) ) {
			/*	|| (optype == pet_op_and_assign) || (optype == pet_op_or_assign)
				|| (optype == pet_op_xor_assign) ) */
			// Note: these 3 last optype not in Pet <0.5 (version used by compiler)
				stm_has_red_op[i] = true;
			} else {
				stm_has_red_op[i] = false;
			}
		} else {
			stm_has_red_op[i] = false;		
		}
	}

	/* DEBUG
	printf("stm_has_red_op =\n");
	for (int i=0; i<scop->n_stmt; i++) {
		printf("\t ... [%i] = ", i);
		if (stm_has_red_op[i])
			printf("true\n");
		else
			printf("false\n");
	} */

	return stm_has_red_op;
}

// Note: "/usr/include/pet.h" for the used version of pet in the Docker repository


// Recover the stride of all the loops of the statements
int** get_info_strides_dim(struct pet_scop * const scop) {
	int** ll_stm_dim_stride = (int**) calloc(scop->n_stmt, sizeof(int*));

	// For inspection
	//pet_scop_dump(scop);
	//fflush(stdout);


	for (int i=0; i<scop->n_stmt; i++) {
		isl_map* stm_sched = isl_map_copy(scop->stmts[i]->schedule);

		// DEBUG
		//printf("\nPING - inspecting statement %d\n", i);
		// For inspection
		//pet_tree_dump(scop->stmts[i]->body);
		// => No informations about surrounding loops schedule :/
		//printf("\nStatement %d\n\t", i);
		//PRINT_ISL(p_out, isl_printer_print_map, stm_sched);
		//fflush(stdout);

		// It should be a 2D+1 schedule
		int n_dim_in = isl_map_dim(stm_sched, isl_dim_in);
		int n_dim_out = isl_map_dim(stm_sched, isl_dim_out);
		
		//assert( (n_dim_out == n_dim_in * 2) || (n_dim_out == n_dim_in*2 + 1) || (n_dim_out == n_dim_in*2 + 2) );

		// __isl_give isl_basic_map *isl_map_simple_hull(__isl_take isl_map *map);
		//	Note: supposed to be a single map anyway...
		isl_basic_map* bmap_sched = isl_map_simple_hull(stm_sched);

		// __isl_give isl_mat *isl_basic_map_equalities_matrix(
		//			__isl_keep isl_basic_map *bmap, enum isl_dim_type c1,
		//			enum isl_dim_type c2, enum isl_dim_type c3,
		//			enum isl_dim_type c4, enum isl_dim_type c5);
		isl_mat* mat_eq_sched = isl_basic_map_equalities_matrix(bmap_sched,
			isl_dim_in, isl_dim_out, isl_dim_div, isl_dim_param, isl_dim_cst);

		/* DEBUG
		isl_mat_print_internal(mat_eq_sched, stdout, 0);
		fflush(stdout);
		//*/


		// Output for this statement
		int* l_dim_stride = (int*) calloc(n_dim_in, sizeof(int));

		// Check all equalities constraint one by one
		for (int ieq=0; ieq<isl_mat_rows(mat_eq_sched); ieq++) {
			// Check if the constraint involves the output dimension

			// Scanning the output dims to know if one of them is valid
			bool is_active_output_dim = false;
			//int dim_output_dim = -1;
			for (int d=0; d<n_dim_out; d++) {
				// Check if this is the equality about the dth out dimension
				isl_val* val_out_d = isl_mat_get_element_val(mat_eq_sched, ieq, d+n_dim_in);
				assert(val_out_d!=NULL);

				// Coeff is 0 => We check the next dim
				if (isl_val_is_zero(val_out_d)) {
					isl_val_free(val_out_d);
					continue;
				}

				if (is_active_output_dim) {
					// Issue: several output dim activated at once
					assert(false);
				}

				// Only remaining val for output dim should be "1"
				assert(isl_val_is_one(val_out_d));
				is_active_output_dim = true;
				//dim_output_dim = d;
			}

			if (!is_active_output_dim) {
				// Constraint on parameter
				continue;
			}

			// DEBUG
			//printf("Scanning constraints %d - dim_output_dim = %d\n", ieq, dim_output_dim);
			//fflush(stdout);

			// We have a (2d+1) (or 2d) schedule: the even dimensions are constants
			//		=> filter them out
			// Example: [ni, nk, nj] -> { S_1[i, k, j] -> [0, i, 1, -k, 0, j] }
			// Other example: [M, N] -> { S_0[k] -> [0, 0, k, 0] }  (with a if statement at the start)

			// => Cannot just filter out the even dimensions due to "if" statements adding dim at the middle
			// => Instead, we inspect the input dim to see if there is one activated (or not).
			// Still assume order of input dim are the same than they appear in output dim.

			double num_stride = 0.0;
			int num_dim_in_activated = 0;
			int loop_dim = 0;
			for (int d=0; d<n_dim_in; d++) {
				// Check if this is the equality about the dth in dimension
				isl_val* val_in_d = isl_mat_get_element_val(mat_eq_sched, ieq, d);
				assert(val_in_d!=NULL);

				if (!isl_val_is_zero(val_in_d)) {
					// Non-zero input found - saving the value
					num_stride = isl_val_get_d(val_in_d);
					num_dim_in_activated++;
					loop_dim = d;
				}

				// Cleaning-up
				isl_val_free(val_in_d);
			}
			assert(num_dim_in_activated<=1); // Else, we have an issue with the schedule formatting
			if (num_dim_in_activated==1) {
				// Interesting case
				// Commit the stride (reverse it because of equality constraint "out - str.in = 0")
				l_dim_stride[loop_dim] = - ((int) round(num_stride) );
			}
			// num_dim_in_activated = 0 => do nothing

		}

		// Free structures
		isl_mat_free(mat_eq_sched);


		// Commit the info about this statement
		ll_stm_dim_stride[i] = l_dim_stride;
	}

	return ll_stm_dim_stride;
}


/* ============================== */


// Auxilliary function that accumulate basic_map in a union_map
isl_union_map* add_basic_map(isl_union_map* acc, isl_basic_map* bmap) {
	if (acc==NULL) {
		return isl_union_map_from_basic_map(bmap);
	} else {
		return isl_union_map_union(acc, isl_union_map_from_basic_map(bmap));
	}
}


// Recompute g->all_edges from the edge->rel
isl_union_map* recompute_all_edges(list_item** inj_list, list_item** bcast_list, int n_nodes) {
	isl_union_map* all_edges = NULL;

	for (int i=0; i<n_nodes; i++) {
		for (list_item* l = inj_list[i]; l!=NULL; l=l->next ) {
			edge* e_inj = (edge*) l->data;
			all_edges = add_basic_map(all_edges, isl_basic_map_copy(e_inj->rel));
		}

		for (list_item* l = bcast_list[i]; l!=NULL; l=l->next) {
			edge* e_bcast = (edge*) l->data;
			all_edges = add_basic_map(all_edges, isl_basic_map_copy(e_bcast->rel));
		}
	}

	return all_edges;
}


/* ============================== */


// === Heuristic to detect reductions in the dfg
//  - Detect successive Injection self-loops and transform them into a broadcast
//	- Note: we only focus on reductions along canonical dimensions
//			and following to the increasing lexicographic order.
// ALSO
//	- detect "reduction-like" chains of dependences
//		=> Fill the field "red_chain_bcst_rel" in a dfg when something is detected (else NULL)
void reduction_detection(struct pet_scop * const scop, bool* stm_has_red_op, int** ll_stm_dim_stride,
		dfg * const g, int verbose) {

	// DEBUG
	if (verbose) {
		printf("\nReduction detection - START\n");
	}
	/*
	printf("g->n_arrays = %li\n", g->n_arrays);
	printf("g->n_nodes = %li\n", g->n_nodes);
	*/

	/* DEBUG
	printf("stm_has_red_op =\n");
	for (int i=0; i<scop->n_stmt; i++) {
		printf("\t ... [%i] = ", i);
		if (stm_has_red_op[i])
			printf("true\n");
		else
			printf("false\n");
	}
	//*/


	// 1) We list the nodes on which there is an injection edge which is a self loop.
	for (int i=g->n_arrays; i<g->n_nodes; i++) {	// Only consider statement nodes
		
		// DEBUG
		if (verbose) {
			printf("Reduction detection (node %i)\n", i);
		}

		// Check if the statement is a "+=" or a "*="
		// WARNING: do not try to check for patterns like "... = ... + ...' "
		//if (! (has_reduction_operator(scop, i)) )
		bool is_reduction_computation = stm_has_red_op[i - g->n_arrays];
		int* l_stm_dim_stride = ll_stm_dim_stride[i - g->n_arrays];

		// DEBUG
		if (verbose && is_reduction_computation) {
			printf("\tReduction computation (+= or *=) detected!\n");
		}

		/* DEBUG
		printf("\tl_stm_dim_stride = [");
		for (int k=0; k<g->nodes[i].dim; k++) {
			printf("%d, ", l_stm_dim_stride[k]);
		}
		printf("];\n");
		//*/

		// Look for the self loops in the graph
		//bool b_outside_dep_to_stm = false;
		list_item* l_selfloop = NULL;
		for (list_item *l = g->inj_list[i]; l!=NULL; l = l->next) {
			edge* e = (edge*) l->data;

			assert(e->type==INJECTION);

			// Is it a self-loop ?
			if (e->source_idx == e->sink_idx) {
				list_item *t = (list_item*) calloc(1, sizeof(list_item));
				t->data = e;
				t->next = l_selfloop;
				l_selfloop = t;
			} else {
				// Check if the sink_idx is a statement
				//if (e->sink_idx > g->n_arrays)
				//	b_outside_dep_to_stm = true;
			}
		}

		/* DEBUG
		if (b_outside_dep_to_stm)
			printf("PING - b_outside_dep_to_stm = true\n");
		else
			printf("PING - b_outside_dep_to_stm = false\n");
		//*/

		// Note: I DON'T THINK THIS IS PERTINENT IN TERM OF IOLB. FOR REDUCTION DETECTION (WITH
		//	A PROGRAM AS OUTPUT), YES. BUT NOT FOR PATH DETECTION
		//if (b_outside_dep_to_stm)	// If dependence from node to outside statement, reject
		//	continue;
		// Note: should be improved (big overapprox), to check which value of reduction is used
		//	(only max, or inside the body?)
		if (l_selfloop == NULL) {
			// No potential for reduction/reduction-like dep chain
			continue;
		}

		// DEBUG
		if (verbose) {
			printf("\tNode %i, Injection self-loop detected\n", i);
		}

		// Check the patterns of the self-dep of l_selfloop
		list_item* lreduction_data = get_ordered_self_loop_red_detection(NULL,
			g->nodes[i].dim, l_selfloop, l_stm_dim_stride);


		// Note: lreduction_data is the succession of injective dep
		//		that corresponds to the reduction.
		if (lreduction_data!=NULL) { // If reduction pattern detected...

		// DEBUG
		if (verbose) {
			printf("\t=> Reduction detected at node %i\n", i);
			printf("\tREDUCTION DATA = [[[\n");
			print_lreduction_data(lreduction_data);
			printf("\t]]]\n");
			fflush(stdout);
		}

		// Replace them with a broadcast
		edge* brd_red_edge = build_broadcast_reduction_edge(lreduction_data, g->nodes[i].dim, i);

		// DEBUG
		//printf("brd_red_edge =\n");
		//PRINT_ISL(p_out, isl_printer_print_basic_map, brd_red_edge->rel);

		// If we are in a real reduction
		if (is_reduction_computation) {

			// Add brd_red_edge to the graph
			list_item *t = (list_item*)calloc(1, sizeof(list_item));
			t->data = brd_red_edge;
			t->next = g->bcast_list[i];
			g->bcast_list[i] = t;

			// Remove the edges of lreduction_data from the graph
			list_item *new_inj_list = NULL;
			for (list_item *l = g->inj_list[i]; l!=NULL; l = l->next) {
				edge* e = (edge*) l->data;
				if (! is_in_red_data(e, lreduction_data) ) {
					// Add it to new_inj_list
					list_item* t = (list_item*)calloc(1, sizeof(list_item));
					t->data = e;
					t->next = new_inj_list;
					new_inj_list = t;
				}
			}
			// Free current list g->inj_list[i] (not its elements) 
			list_item *l = g->inj_list[i];
			while (l!=NULL) {
				list_item* ln = l->next;
				free(l);
				l = ln;
			}
			// Replace it with the new list of injective edges
			g->inj_list[i] = new_inj_list;

			isl_union_map_free(g->all_edges);	// Throw away all_edges to recompute it
			g->all_edges = recompute_all_edges(g->inj_list, g->bcast_list, g->n_nodes);
			
			// DEBUG
			//printf("New DFG =\n");
			//print_dfg(g);
		}

		// Reduction-like chain of dep (happens even if we have a real reduction here)
		g->nodes[i].red_chain_bcst_rel = brd_red_edge->rel_red;


		// Free all the elements of lreduction_data
		while(lreduction_data != NULL){
			list_item *curr = lreduction_data;
			lreduction_data = curr->next;
			free_reduction_data((reduction_data*) curr->data);
			free(curr);
		}

		} // END of reduction pattern detected

		// "free(l_selfloop);"
		while (l_selfloop!=NULL) {
			list_item* ln = l_selfloop->next;
			free(l_selfloop);
			l_selfloop = ln;
		}
	}


	// DEBUG
	if (verbose) {
		printf("Reduction detection - END\n");
	}

	return;
}
