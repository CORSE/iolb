#ifndef REDUCTION_H
#define REDUCTION_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include <assert.h>
#include <stdbool.h>
#include <math.h>

//#include <isl/union_map.h>
#include <isl/map.h>
#include <isl/space.h>
#include <isl/vec.h>
#include <isl/mat.h>
#include <isl/aff.h>
#include <isl/val.h>

#include "utility.h"
#include "dfg.h"


bool* get_info_reduction_op(struct pet_scop * const scop);
int** get_info_strides_dim(struct pet_scop * const scop);

void reduction_detection(struct pet_scop * const scop, bool* stm_has_red_op, int** stm_dim_stride,
        dfg * const g, int verbose);

#endif