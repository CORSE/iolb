/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include "utility.h"


list_item* list_item_append(list_item* l1, list_item* l2) {
	// Trivial cases
	if (l1==NULL)
		return l2;
	if (l2==NULL)
		return l1;

	list_item* p_l1 = l1;
	while (p_l1->next!=NULL)
		p_l1 = p_l1->next;
	// p_l1 is at the last cell of l1
	p_l1->next = l2;

	return l1;
}

list_item* list_item_reverse(list_item* l) {
	if (l==NULL)
		return NULL;

	list_item* inv_l = NULL;
	for (list_item* p_l=l; p_l!=NULL; p_l=p_l->next) {
		// Create a new cell and link it
		list_item* ncell = malloc(sizeof(list_item));
		ncell->data = p_l->data;
		ncell->next = inv_l;

		inv_l = ncell;
	}
	return inv_l;
}


isl_printer *p_out;
isl_printer *p_err;

bool _reduction_detection;
bool _hourglass_detection;

bool _small_dim;
list_item* _l_small_dims;

void print_small_dim_info(small_dim_info* sdi) {
	printf("%s (= %d)", sdi->name_param, sdi->value);
	return;
}


void add_small_dim_name(char* param, int val) {
	char* copy_param = malloc(strlen(param) * sizeof(char));
	strcpy(copy_param, param);

	small_dim_info* sdinfo = malloc(sizeof(small_dim_info));
	sdinfo->name_param = copy_param;
	sdinfo->value = val;

	list_item* temp = (list_item*) malloc(sizeof(list_item));
	temp->data = sdinfo;
	temp->next = _l_small_dims;

	_l_small_dims = temp;
}

bool is_small_dim(const char* name_param) {
	bool b_is_small_dim = false;
	for (list_item* ptr=_l_small_dims; ptr!=NULL; ptr=ptr->next) {
		small_dim_info* sdinfo = (small_dim_info*) ptr->data;
		
		if (strcmp(name_param, sdinfo->name_param)==0) {	// String equals
			b_is_small_dim = true;
		}
	}

	return b_is_small_dim;
}

void print_l_small_dims() {
	printf("_l_small_dims =\n");
	for (list_item* ptr=_l_small_dims; ptr!=NULL; ptr=ptr->next) {
		small_dim_info* sdinfo = (small_dim_info*) ptr->data;
		printf("\t - ");
		print_small_dim_info(sdinfo);
		printf("\n");
	}
	return;
}


