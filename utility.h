#ifndef UTILITY_H
#define UTILITY_H

/*  ========================================================================
    Copyright (C) INRIA
    Contributors: Auguste Olivry, Guillaume Iooss
    Date of creation: 2019 - 2021

    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr

    This software is a computer program whose purpose is to derive
    automatically a lower-bound to the IO-complexity of a polyhedral
    program.

    This software is governed by the CeCILL  license under French law and
    abiding by the rules of distribution of free software.  You can  use, 
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info". 

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability. 

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or 
    data to be ensured and,  more generally, to use and operate it in the 
    same conditions as regards security. 

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.
    ======================================================================== */

#include <stdbool.h>
#include <string.h>
#include <isl/set.h>
#include <isl/printer.h>

//static int _verbose;  // Macro used instead

#define BITS_PER_BYTE 8
typedef char bits;
#define BITS_SIZE (sizeof(bits)*BITS_PER_BYTE)

typedef struct list_item list_item;
struct list_item
{
	void *data;
	list_item *next;
};

// Append 2 list_item, by modifying their connexion (l1 -> l2)
//      => Warning: don't use l1 as a normal list_item afterward!
list_item* list_item_append(list_item* l1, list_item* l2);

// Create a new list_item whose element order is reversed
list_item* list_item_reverse(list_item* l);


extern isl_printer *p_out;
extern isl_printer *p_err;

#define PRINT_ISL(p, print_fn, data) \
(p) = isl_printer_start_line((p)); \
(p) = (print_fn((p), (data))); \
(p) = isl_printer_end_line((p));

// Options to the proof
extern bool _reduction_detection;
extern bool _small_dim;
extern bool _hourglass_detection;

// Small dimension registration
typedef struct {
	char* name_param;
	int value;
} small_dim_info;
void print_small_dim_info(small_dim_info* sdi);


extern list_item* _l_small_dims;
void add_small_dim_name(char* param, int val);
bool is_small_dim(const char* name_param);
void print_l_small_dims();


#endif
