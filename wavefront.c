
#include "wavefront.h"



struct depth_info_s
{
	int depth;
	isl_union_map *rels;
	int *common_depth;
	int offs;
};




// Used to estimate which bound in stronger asymptotically than the others
double evaluate_bound_on_big_value(expr* e, isl_set* param_dom) {
	// Evaluate the small dimensions first
	for (list_item* ptr = _l_small_dims; ptr!=NULL; ptr=ptr->next) {
		small_dim_info* sdi = (small_dim_info*) ptr->data;
		e = expr_partial_evaluate(e, sdi->name_param, sdi->value);
	}

	// Algo:
	// - Propagate small dim values to the param_dom
	// - Check param_dom for which parameters are bounded or not
	// - Set the unbounded parameters to "asympt_value"
	// - Find the max value for all remaining bounded parameters
	//	  __isl_give isl_val *isl_set_min_val(__isl_keep isl_set *set, __isl_keep isl_aff *obj);
	// - Propagate these values in "e"

	// TODO: Note: might be buggy on the combination of small dim and bounds on params
	// 	=> Good luck for the poor soul that encounter that bug. }:-D

	// Big value, on which we will evaluate the bound
	double asympt_value = 1e9;

	// Convert the isl_set into a isl_basic_set
	isl_basic_set* param_bdom = isl_set_polyhedral_hull(isl_set_copy(param_dom));

	/* DEBUG
	printf("[evaluate_bound_on_big_value] param_bdom = ");
	PRINT_ISL(p_out, isl_printer_print_basic_set, param_bdom);
	printf("\n");
	fflush(stdout);
	//*/

	// We check which parameter are bounded by others, and which ones are not
	isl_mat* mat_ineq = isl_basic_set_inequalities_matrix(param_bdom,
			isl_dim_set, isl_dim_param, isl_dim_div, isl_dim_cst);
	int n_ineq = isl_mat_rows(mat_ineq);
	int nparam = isl_basic_set_dim(param_bdom, isl_dim_param);
	assert(isl_basic_set_dim(param_bdom, isl_dim_set)==0);
	assert(isl_basic_set_dim(param_bdom, isl_dim_div)==0);


	bool* b_is_param_bounded = malloc(nparam * sizeof(bool));
	for (int i=0; i<nparam; i++) {
		b_is_param_bounded[i] = false;
	}
	for (int i_ineq=0; i_ineq<n_ineq; i_ineq++) {
		for (int j=0; j<nparam; j++) {
			isl_val* vij = isl_mat_get_element_val(mat_ineq, i_ineq, j);

			if (isl_val_is_zero(vij)) {
				isl_val_free(vij);
				continue;
			}

			if (isl_val_is_neg(vij)) {
				b_is_param_bounded[j] = true;
			}
			isl_val_free(vij);
		}
	}
	isl_mat_free(mat_ineq);

	/* DEBUG
	printf("[evaluate_bound_on_big_value] b_is_param_bounded = [");
	for (int i=0; i<nparam; i++) {
		if (b_is_param_bounded[i])
			printf("T, ");
		else
			printf("F, ");
	}
	printf("]\n");
	fflush(stdout);
	//*/


	// Set the unbounded parameters to "asympt_value"
	isl_space* space_param_dom = isl_set_get_space(param_dom);
	double asympt_value_low = asympt_value - 1e3;
	for (int i=0; i<nparam; i++) {
		if (!b_is_param_bounded[i]) {
			char* param_name_i = (char*) isl_space_get_dim_name(space_param_dom, isl_dim_param, i);
			// DEBUG
			//printf("ping (unbounded) - %s\n", param_name_i);
			
			e = expr_partial_evaluate(e, param_name_i, asympt_value);
		} else {
			// Too lazy to do something very sophisticated (which might not be needed)
			// Trick here that can be improved
			char* param_name_i = (char*) isl_space_get_dim_name(space_param_dom, isl_dim_param, i);
			// DEBUG
			//printf("ping (bounded) - %s\n", param_name_i);
			
			e = expr_partial_evaluate(e, param_name_i, asympt_value_low);
		}
	}

	/* DEBUG
	printf("[evaluate_bound_on_big_value] e after unbounded subst = ");
	expr_print(e);
	printf("\n asympt_value_low = %f\n", asympt_value_low);
	fflush(stdout);
	//*/


	// If want to fully do a clean solution:
	// - Find the max value for all remaining bounded parameters
	//	  __isl_give isl_val *isl_set_min_val(__isl_keep isl_set *set, __isl_keep isl_aff *obj);
	// - Propagate these values in "e"

	// Rest of parameters are evaluated on a big value
	// (and cache size on sqrt(...) of this big value)
	double ev = expr_evaluate(e, asympt_value);
	return ev;
}



struct intersection_data
{
	isl_union_map *r1;
	isl_union_map *r2;
	int depth;
};


// =====================================

static isl_union_set* create_point(isl_ctx *ctx, const char *sp_name, const int depth, const int dim, const int dist)
{
	char *param1 = (char*)malloc(6*dim * sizeof(char));
	char *param2 = (char*)malloc(6*dim * sizeof(char));
	strcpy(param1, "my0");
	strcpy(param2, "my0");
	int len1 = 3, len2 = 3;
	if(dist!=0 && depth==0)
	{
		len2 += sprintf(param2 + 3, "+%d", dist);
	}
	for(int i=1; i<dim; ++i)
	{
		assert(len1 < 6*dim);
		assert(len2 < 6*dim);
		len1 += sprintf(param1 + len1, ",my%d", i);
		len2 += sprintf(param2 + len2, ",my%d", i);
		if(dist!=0 && depth==i)
		{
			len2 += sprintf(param2 + len2, "+%d", dist);
		}
	}
	size_t str_len = len1 + len2 + strlen(sp_name) + 12;
	char *pt_str = (char*)malloc(str_len * sizeof(char));
	strcpy(pt_str, "[");
	strcat(pt_str, param1);
	strcat(pt_str, "]->{");
	strcat(pt_str, sp_name);
	strcat(pt_str, "[");
	strcat(pt_str, param2);
	strcat(pt_str, "]};");
	isl_union_set *s = isl_union_set_read_from_str(ctx, pt_str);

	free(param1);
	free(param2);
	free(pt_str);
	return s;
}

int create_relation(isl_map *m, void *data_v)
{
	struct intersection_data *data = ((struct intersection_data*)(data_v));
	int depth = data->depth;
	//printf("\nDEBUG\n");
	//PRINT_ISL(p_out, isl_printer_print_map, m);


	//Add equality constraint for all outer loop dimensions
	isl_map *new_m = isl_map_copy(m);
	isl_local_space *lsp = isl_local_space_from_space(isl_map_get_space(m));
	for(int i=0; i<depth; ++i)
	{
		if(i == depth-1)
		{
			data->r2 = isl_union_map_union(data->r2, isl_union_map_from_map(isl_map_copy(new_m)));
		}
		isl_constraint *cst = isl_equality_alloc(isl_local_space_copy(lsp)); //Note: allocation also initializes the coeffs with zero.
		assert(isl_map_n_in(new_m) > i || !(printf("Failed due to in_dim: %d, i: %d\n", isl_map_n_in(new_m), i)));
		assert(isl_map_n_out(new_m) > i || !(printf("Failed due to in_dim: %d, i: %d\n", isl_map_n_out(new_m), i)));
		cst = isl_constraint_set_coefficient_si(cst, isl_dim_in, i, -1);
		cst = isl_constraint_set_coefficient_si(cst, isl_dim_out, i, 1);
		new_m = isl_map_add_constraint(new_m, cst);
		assert(new_m != NULL);
	}
	data->r1 = isl_union_map_union(data->r1, isl_union_map_from_map(new_m));

	isl_local_space_free(lsp);
	isl_map_free(m);
	return 0;
}


expr* card_preimage(__isl_take isl_union_map *union_rel, node *v, const int depth)
{
	assert(union_rel!=NULL);
	assert(depth > 0);
	isl_set *dom = v->dom;
	int dim = v->dim;
	if(isl_union_map_is_empty(union_rel) == 1)
	{
		isl_union_map_free(union_rel);
		return NULL;
	}
#if (VERBOSE > 3)
	printf("Union relation:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, union_rel);
#endif
  isl_union_map *r1, *r2;
  //r1 is the union of relations with no dependences carried at dims
  //0...(d-1)
  r1 = isl_union_map_empty(isl_union_map_get_space(union_rel));
  //r2 is the union of relations with no dependences carried at dims
  //0...(d-2)
  r2 = isl_union_map_empty(isl_union_map_get_space(union_rel));
  struct intersection_data data = {.r1 = r1, .r2 = r2, .depth = depth};

  //Check for dependencies and add to r1 or r2
  isl_union_map_foreach_map(union_rel, create_relation, &data);
  r2 = isl_union_map_subtract(r2, isl_union_map_copy(r1));
#if (VERBOSE > 3)
	printf("Relation 1:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, r1);
	printf("Relation 2:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, r2);
#endif

	int exact;
	isl_union_map *T = isl_union_map_transitive_closure(r1, &exact);
	//Stop if closure is not exact
	if(exact != 1)
	{
#if (VERBOSE > 2)
		printf("Reached inexact transitive closure at depth %d\n", depth);
#endif
		isl_union_map_free(r2);
		isl_union_map_free(T);
		return NULL;
	}
	T = isl_union_map_coalesce(isl_union_map_union(T, isl_union_set_identity(isl_union_map_domain(union_rel))));
	//R3 = R1* o R2 o R1*
	isl_union_map *r3;
	r3 = isl_union_map_apply_range(isl_union_map_copy(T), isl_union_map_copy(r2));
#if (VERBOSE > 3)
	printf("Relation R1*:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, T);
	printf("Relation R1* o R2:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, r3);
#endif
	assert(r3 != NULL);
	r3 = isl_union_map_coalesce(isl_union_map_apply_range(r3, isl_union_map_copy(T)));
	assert(r3 != NULL);
	isl_union_map *r3_inv = isl_union_map_reverse(isl_union_map_copy(r3));
	assert(r3_inv != NULL);
#if (VERBOSE > 4)
	printf("Relation R3:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, r3);
	printf("Relation R3^-1:\n");
	PRINT_ISL(p_out, isl_printer_print_union_map, r3_inv);
#endif
	isl_union_map_free(r3);

  //Create parametric points
  assert(isl_set_has_tuple_name(dom));
  const char *sp_name = isl_set_get_tuple_name(dom);
  isl_union_set *p1;
  isl_ctx *ctx = isl_set_get_ctx(dom);
  p1 = create_point(ctx, sp_name, depth-1, dim, 0);
#if (VERBOSE > 4)
	printf("Point 1:\n");
	PRINT_ISL(p_out, isl_printer_print_union_set, p1);
#endif

  isl_union_set *s1 = isl_union_set_apply(p1, r3_inv);
  s1 = isl_union_set_intersect(s1, isl_union_set_from_set(isl_set_copy(v->dom)));
  s1 = isl_union_set_coalesce(s1);
#if (VERBOSE > 4)
	printf("Preimage:\n");
	PRINT_ISL(p_out, isl_printer_print_union_set, s1);
	//PRINT_ISL(p_out, isl_printer_print_union_pw_qpolynomial, isl_union_set_card(isl_union_set_copy(s1)));
#endif
	expr *card = get_union_set_cardinality(s1, 0, NULL);
#if (VERBOSE > 3)
	printf("\nCard:\n");
	if(card == NULL)
		printf("NULL");
	else
		expr_print(card);
	printf("\n");
#endif

	return card;
}


// =====================================

static int add_edges_in_same_loopnest(isl_map *m, void *data_v)
{
	struct depth_info_s *data = ((struct depth_info_s *)(data_v));
	int in_id = -1, out_id = -1;
	sscanf(isl_map_get_tuple_name(m, isl_dim_in), "S_%d", &in_id);
	sscanf(isl_map_get_tuple_name(m, isl_dim_out), "S_%d", &out_id);
	if(in_id == -1 || out_id == -1)
		return 0;
	if(data->common_depth[in_id+data->offs] >= (data->depth-1) && data->common_depth[out_id+data->offs] >= (data->depth-1))
	{
       //printf("\nin %d  out %d   cd_in %d   cd_out %d   depth %d\n", in_id, out_id, data->common_depth[in_id+data->offs], data->common_depth[out_id+data->offs], data->depth);
		data->rels = isl_union_map_union(data->rels, isl_union_map_from_map(m));
	}
	return 0;
}


// Wavefront algorithm
expr* cut_LB(dfg* const g, const int count, bits (*bitmap)[count])
{
	expr* best_lb = expr_new("0");
	size_t n_nodes = g->n_nodes;
	int *common_depth = (int*)malloc(n_nodes * sizeof(int));

	// Arrays might not have all the parameter constraints, if we have a "if" in the program
	isl_set* param_dom = isl_set_params(isl_set_copy(g->nodes[n_nodes-1].dom));

	for(size_t i=g->n_arrays; i<n_nodes; ++i)
	{
		int dim = g->nodes[i].dim;
		if(dim > 3)
			break;
		//If node  'i' is reachable from itself, try to find the
		//cardinality of ancestor-descendant intersection.
		//Determine the loop-depth upto which a statement 'j' is common
		//with 'i'
		bits mask = (1 << (i%BITS_SIZE));
		if(bitmap[i][i/BITS_SIZE] & mask)
		{
			for(size_t j=g->n_arrays; j<n_nodes; ++j)
			{
                int depth=0;
                common_depth[j] = -1;
				for(int k=0; k<g->nodes[i].sched_size; ++k)
				{
					if(g->nodes[i].sched[k] == g->nodes[j].sched[k])
					{
						if(g->nodes[i].sched[k] == -1)
						{
							common_depth[j] = depth;
							++depth;
						}
					}
					else
						break;
				}
			}

			bounds* dim_bounds = malloc(dim*sizeof(bounds));
		  get_dim_bounds(isl_set_copy(g->nodes[i].dom), dim, dim_bounds);

			for(int j=g->nodes[i].loop_depth-1; j>0; --j)
			{
				isl_union_map *rels = isl_union_map_empty(isl_union_map_get_space(g->all_edges));
				struct depth_info_s depth_info = {.depth = j, .rels = rels, .common_depth = common_depth, .offs=g->n_arrays};
				isl_union_map_foreach_map(g->all_edges, add_edges_in_same_loopnest, &depth_info);
				expr *card = card_preimage(rels, &(g->nodes[i]), j);

				if(card == NULL)
					continue;

				/* DEBUG
				printf("card - pre remove_new_params =");
				expr_print(card);
				printf("\n");
				//*/

				card = remove_new_params(card, dim_bounds, dim);

				/* DEBUG
				printf("card - post remove_new_params =");
				expr_print(card);
				printf("\n\n");
				//*/



				int expr_dim = find_expr_dim(card);
				//printf("card - expr_dim = %d\n\n", expr_dim);

#if (VERBOSE > 2)
				printf("\nLoop depth: %d; Preimage dim: %d; Cardinality: ", j, expr_dim);
				expr_print(card);
				printf("\n");
#endif
				if(expr_dim > 0)
				{
					printf("Found cut lower bound:\nQ >= ");
					expr* n = expr_sub(dim_bounds[j-1].ub, dim_bounds[j-1].lb);
					expr* lb = expr_expand(expr_mul(expr_sub(n, expr_one()), expr_sub(card, expr_sub(expr_S(), expr_one()))));
					if(evaluate_bound_on_big_value(lb, param_dom)
								> evaluate_bound_on_big_value(best_lb, param_dom)) {
							best_lb = lb;
					}
					expr_print(lb);
					printf("\nQ = Omega(");
					expr_print(expr_simplify_expr(lb));
					printf(")\n\n");
				}
			}
		}
	}
	free(common_depth);
	return best_lb;
}