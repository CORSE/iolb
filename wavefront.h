#ifndef WAVEFRONT_H
#define WAVEFRONT_H

#include <string.h>
#include "complexity.h"

// Aux function to quickly check which bound is asymptotically stronger
double evaluate_bound_on_big_value(expr* e, isl_set* param_dom);


// Aux function used by wavefront
expr* card_preimage(__isl_take isl_union_map *union_rel, node *v, const int depth);

// Wavefront algorithm main function
expr* cut_LB(dfg* const g, const int count, bits (*bitmap)[count]);


#endif